/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsuserstorage.hpp"

#include <QxtLogger>


std::unique_ptr<DsUserStorage> DsUserStorage::m_singleton;


DsUserStorage::DsUserStorage() :
    QObject(nullptr),
    m_mutex(QMutex::Recursive)
{
}


DsUserStorage::~DsUserStorage()
{
    qDebug() << "Deleting user storage...";
}


DsUserStorage * DsUserStorage::getInstance()
{
    return m_singleton.get();
}


void DsUserStorage::freeInstance()
{
    m_singleton.reset();
}


void DsUserStorage::allocateInstance()
{
    m_singleton.reset(new DsUserStorage());
}


void DsUserStorage::add(const Darkmires::UserPointer & user)
{
    QMutexLocker mutex(&m_mutex);

    m_users[user->getId()] = user;

    emit usersEvent(DarkmiresEvent::EVENT_USER_JOINED);
}


void DsUserStorage::remove(int user)
{
    QMutexLocker mutex(&m_mutex);

    if (m_users.remove(user))
    {
        emit usersEvent(DarkmiresEvent::EVENT_USER_LEFT);
    }
}


bool DsUserStorage::contains(int id)
{
    QMutexLocker mutex(&m_mutex);

    return m_users.contains(id);
}


DsUser * DsUserStorage::get(int id)
{
    if (m_users.contains(id))
    {
        return m_users[id].get();
    }

    return nullptr;
}


QList<Darkmires::UserMap::key_type> DsUserStorage::getUsers()
{
    return m_users.keys();
}


void DsUserStorage::lock()
{
    m_mutex.lock();
}


bool DsUserStorage::tryLock(int timeout)
{
    return m_mutex.tryLock(timeout);
}


void DsUserStorage::unlock()
{
    m_mutex.unlock();
}



bool DsUserStorage::hasPrivileges(int id, Darkmires::UserPrivileges privileges)
{
    QMutexLocker mutex(&m_mutex);

    return m_users.value(id)->hasPrivileges(privileges);
}


bool DsUserStorage::inSession(const QString & session, int id)
{
    QMutexLocker mutex(&m_mutex);

    return m_users.value(id)->inSession(session);
}


void DsUserStorage::wipe()
{
    for (std::shared_ptr<DsUser> & user : m_users)
    {
        qxtLog->debug() << QString("[USER STORAGE] Deleting user %1 (%2 references)").arg(user->getId()).arg(QString::number(user.use_count()));

        user.reset();
    }

    m_users.clear();
}


bool DsUserStorage::inAnySession(int id)
{
    QMutexLocker mutex(&m_mutex);

    return m_users.value(id)->sessionCount() > 0;
}


int DsUserStorage::sessionCount(int id)
{
    QMutexLocker mutex(&m_mutex);

    return m_users.value(id)->sessionCount();
}


bool DsUserStorage::isUnique(const QString & name)
{
    QMutexLocker mutex(&m_mutex);

    for (std::shared_ptr<DsUser> & user : m_users)
    {
        if (user->getName() == name)
        {
            return false;
        }
    }

    return true;
}
