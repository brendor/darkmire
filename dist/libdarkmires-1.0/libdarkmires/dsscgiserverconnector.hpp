#ifndef DARKMIRESCGISERVERCONNECTOR_HPP
#define DARKMIRESCGISERVERCONNECTOR_HPP

#include <QxtScgiServerConnector>

///
/// \brief DsScgiServerConnector je trieda špecifikujúca pripájač k
/// serveru SCGI.
///
class DsScgiServerConnector : public QxtScgiServerConnector
{
        Q_OBJECT

    public:
        DsScgiServerConnector(QObject * parent);
        virtual ~DsScgiServerConnector();

    protected:
        ///
        /// \brief parseRequest spracuje požiadavku vo forme buffra bajtov.
        /// \param buffer buffer bajtov
        /// \return HTTP požiadavka
        ///
        virtual QHttpRequestHeader parseRequest(QByteArray & buffer);
};

#endif // DARKMIRESCGISERVERCONNECTOR_HPP
