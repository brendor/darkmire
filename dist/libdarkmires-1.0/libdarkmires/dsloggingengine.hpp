#ifndef DARKMIRESLOGGINGENGINE_HPP
#define DARKMIRESLOGGINGENGINE_HPP

#include <QxtBasicSTDLoggerEngine>

#include "darkmires.hpp"

///
/// \brief DsLoggingEngine je trieda logovania v aplikácii Darkmire.
/// Prostredníctvom nej je možné logovať správy bez závislosti od výstupného média.
///
class Q_DECL_HIDDEN DsLoggingEngine : public QxtBasicSTDLoggerEngine
{
    public:
        ///
        /// \brief DsLoggingEngine je konštruktor tejto triedy.
        ///
        explicit DsLoggingEngine();

        ///
        /// \brief setLoggingFunction nastaví funkciu zavolanú pri každej prichádzajúcej správe
        /// \param function funkcia
        ///
        inline void setLoggingFunction(Darkmires::LoggingFunction function)
        {
            m_function = function;
        }

    protected:
        virtual void writeToStdErr(const QString & str_level, const QList<QVariant> & msgs) override;
        virtual void writeToStdOut(const QString & str_level, const QList<QVariant> & msgs) override;

    private:
        Darkmires::LoggingFunction m_function;

        void buildAndDispatchMessage(const QString & str_level, const QList<QVariant> & msgs);
};

#endif // DARKMIRESLOGGINGENGINE_HPP
