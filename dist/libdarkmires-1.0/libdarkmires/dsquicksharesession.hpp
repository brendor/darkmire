#ifndef DARKMIRESMANAGEDREPOSITORY_HPP
#define DARKMIRESMANAGEDREPOSITORY_HPP

#include "dssession.hpp"
#include "dssessionuserdata.hpp"

///
/// \brief Trieda DsQuickShareSession dedí od triedy DsSession a prináša
/// funkcionalitu spojenú s rýchlym zdieľaním príspevkov.
///
class Q_DECL_HIDDEN DsQuickShareSession : public DsSession
{
        Q_OBJECT

    public:
        explicit DsQuickShareSession(DsSessionInterface * const interface);
        virtual ~DsQuickShareSession();

        ///
        /// \brief RequestType enumuerácia definuje typy požiadaviek
        /// špecifické pre reláciu rýchleho zdieľania.
        ///
        enum RequestType : unsigned int
        {
            QuickShareRequestNormal = 0,
            QuickShareRequestEntry = 1,
            QuickShareRequestList = 2
        };

    public slots:
        virtual void processRequest(Darkmires::PacketPointer packet) override;
        virtual void addUser(int user) override;
        virtual bool init() override;

        // DsSession interface
    protected:
        virtual void populate(std::shared_ptr<DsSessionUserData> data);
};

///
/// \brief Trieda DsQuickShareSessionUser dedí od triedy DsSessionUserData a pridáva
/// do nej zoznam príspevkov, ktoré budú priradené pripojenému používateľovi.
///
class DARKMIRESSHARED_EXPORT DsQuickShareSessionUser : public DsSessionUserData
{
        Q_OBJECT

    public:
        explicit DsQuickShareSessionUser(int user);
        virtual ~DsQuickShareSessionUser();

        /// \brief Štruktúra definujúca obsah príspevku.
        typedef struct
        {
            QDateTime date;
            QString code;
            QString mime;
        } Entry;

        ///
        /// \brief addEntry pridá príspevok do zoznamu špecifického pre vlastníka týchto údajov.
        /// \param entry príspevok
        ///
        void addEntry(const Entry & entry);

        typedef QMap<int, Entry> EntryList;

        ///
        /// \brief getEntries získa referenciu na zoznam príspevkov tohto používateľa.
        /// \return referencia na zoznam príspevkov tohto používateľa
        ///
        inline const EntryList getEntries() const
        {
            return m_entries;
        }

    private:
        EntryList m_entries;
};

#endif // DARKMIRESMANAGEDREPOSITORY_HPP
