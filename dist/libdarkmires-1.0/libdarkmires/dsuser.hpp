#ifndef DARKMIRESUSER_HPP
#define DARKMIRESUSER_HPP

#include "darkmires_global.hpp"

#include "darkmires.hpp"

#include <QObject>
#include <QTimer>
#include <QUuid>
#include <QQueue>

///
/// \brief Trieda DsUser predstavuje údaje prihláseného používateľa.
///
class DARKMIRESSHARED_EXPORT DsUser : public QObject
{
        Q_OBJECT

    public:
        explicit DsUser(int id, QObject * parent = 0);
        DsUser(const DsUser &) = delete;

        virtual ~DsUser();

        inline int getId() const
        {
            return m_id;
        }

        inline void setSessionID(const QUuid & uuid)
        {
            m_sessionID = uuid;
        }
        inline const QUuid & getSessionID() const
        {
            return m_sessionID;
        }

        inline void setUuid(const QUuid & uuid)
        {
            m_uuid = uuid;
        }
        inline const QUuid & getUuid() const
        {
            return m_uuid;
        }

        inline void setName(const QString & name)
        {
            m_name = name;
        }
        inline const QString & getName() const
        {
            return m_name;
        }

        inline void setPrivileges(Darkmires::UserPrivileges privileges)
        {
            m_privileges = privileges;
        }
        inline Darkmires::UserPrivileges getPrivileges() const
        {
            return m_privileges;
        }
        inline void addPrivileges(Darkmires::UserPrivileges privileges)
        {
            m_privileges |= privileges;
        }
        inline void removePrivileges(Darkmires::UserPrivileges privileges)
        {
            m_privileges &= ~privileges;
        }
        inline bool hasPrivileges(Darkmires::UserPrivileges privileges) const
        {
            return m_privileges & privileges;
        }

        ///
        /// \brief resetTimer reštartuje časovač požiadaviek, ktorý vyvoláva
        /// signál timeout.
        ///
        void resetTimer();

        bool inSession(const QString & session) const;
        void addSession(const QString & session);
        void removeSession(const QString & session);
        int sessionCount() const;

        ///
        /// \brief getSessions získa zoznam relácií, v ktorých je používateľ
        /// pripojený.
        /// \return referencia na zoznam relácií
        ///
        inline const QList<QString> & getSessions() const
        {
            return m_sessions;
        }

        typedef QQueue<Darkmires::UserMessage> MessageQueue;

        void enqueueMessage(Darkmires::UserMessage && message);

        void processMessages(Darkmires::PacketPointer packet);

    signals:
        ///
        /// \brief timeout signalizuje, že daný používateľ neposlal po istý čas
        /// žiadnu požiadavky a mal by preto byť pravdepodobne odstránený.
        /// \param id ID používateľa
        ///
        void timeout(int id);

    private slots:
        void timerEvent();

    public slots:
        ///
        /// \brief wantsChat zistí, či daný používateľ chce živú diskusiu
        /// \return true ak áno, false ak nie
        ///
        inline bool wantsChat()
        {
            return m_chat;
        }

        inline void setWantsChat(bool wc)
        {
            m_chat = wc;
        }

    private:
        QTimer m_timer;

        QString m_name;

        int m_id;

        QUuid m_sessionID;
        QUuid m_uuid;

        Darkmires::UserPrivileges m_privileges = Darkmires::UserRegular;

        QList<QString> m_sessions;

        MessageQueue m_messages;

        bool m_chat = true;
};

#endif // DARKMIRESUSER_HPP
