/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "darkmiresevent.hpp"


DarkmiresEvent::DarkmiresEvent() :
    m_type(DarkmiresEvent::EVENT_NONE)
{
}


DarkmiresEvent::DarkmiresEvent(const DarkmiresEvent::EventType & t, const QVariant & d) :
    m_type(t),
    m_data(d)
{ }


DarkmiresEvent::DarkmiresEvent(const DarkmiresEvent & event)
{
    m_type = event.m_type;
    m_data = event.m_data;
}


DarkmiresEvent::DarkmiresEvent(DarkmiresEvent && event)
{
    m_type = event.m_type;
    m_data = event.m_data;
}


DarkmiresEvent::~DarkmiresEvent()
{
}
