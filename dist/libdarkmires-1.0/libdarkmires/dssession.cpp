/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dssession.hpp"

#include <QxtLogger>
#include <QxtWebRequestEvent>

#include "dssessionuserdata.hpp"

#include "dsuserstorage.hpp"

#include "dswebevent.hpp"


DsSession::DsSession(DsSessionInterface * const interface) :
    QObject(nullptr),
    m_interface(interface),
    m_thread_id(0)
{
}


DsSession::~DsSession()
{
    QMutexLocker mutex(&m_interface->m_mutex);

    qxtLog->debug() << QString("Deleting session (%1) in thread: %2").arg(m_interface->m_name).arg(QString::number(QThread::currentThreadId()));
}


const std::shared_ptr<DsSessionUserData> DsSession::getSessionUser(int id)
{
    return m_users.value(id);
}


void DsSession::start()
{
    QMutexLocker mutex(&m_interface->m_mutex);

    qxtLog->debug() << QString("Starting session (%1) in thread: %2").arg(m_interface->m_name).arg(QString::number(QThread::currentThreadId()));

    if (typeid(Qt::HANDLE) == typeid(unsigned long))
    {
        m_thread_id = (unsigned long)QThread::currentThreadId();
    }
}


void DsSession::stop()
{
    QMutexLocker mutex(&m_interface->m_mutex);

    qxtLog->debug() << QString("Stopping session (%1) in thread: %2").arg(m_interface->m_name).arg(QString::number(QThread::currentThreadId()));

    m_thread_id = 0;
}


/**
 * @brief DarkmiresSession::processChat parses chat messages
 * @param id
 * @param in
 * @param out
 * @note mutex should be locked before calling this function
 */
void DsSession::processChat(Darkmires::PacketPointer packet, const QVariantMap & in)
{
    if (in.contains("chat"))
    {
        LockUserStorage
        bool wants = DsUserStorage::getInstance()->get(packet->event->sessionID)->wantsChat();
        UnlockUserStorage

        if (! wants)
        {
            return;
        }

        Darkmires::ChatMessage msg;
        msg.user = packet->event->sessionID;
        msg.message = in.value("chat").toMap().value("message").toString();
        msg.time = QDateTime::currentDateTimeUtc();

        msg.id = (m_chat.size() ? ((m_chat.end() - 1).value().id + 1) : 1);

        if (! msg.message.isEmpty())
        {
            m_chat[msg.time.toMSecsSinceEpoch()] = std::move(msg);

            emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_NEW_CHAT_MESSAGE, m_interface->getUuid().toString()));
        }

        qint64 previous = in.value("chat").toMap().value("previous").toLongLong();
        int previousId = in.value("chat").toMap().value("previous-id").toInt();

        QVariantList messages;

        if (previous != 0)
        {
            for (Darkmires::ChatStorage::const_iterator it = m_chat.upperBound(previous); it != m_chat.end(); ++it)
            {

                if (it.value().user == packet->event->sessionID)
                {
                    continue;
                }
                if (it.value().id <= previousId)
                {
                    continue;
                }

                QVariantMap message;

                /* Need to pass it as a string because JSON in Qxt has problem with long integers
                 * (it does not append any value in the final string resulting in invalid JSON)
                 */
                message["timestamp"] = QString::number(it.key());

                message["text"] = it.value().message;

                if (it.value().user == 0)
                {
                    message["author"] = "[SERVER]";
                }
                else
                {
                    LockUserStorage
                    message["author"] = DsUserStorage::getInstance()->get(it.value().user)->getName();
                    UnlockUserStorage
                }
                message["id"] = it.value().id;

                messages.append(message);
            }
        }


        packet->data["chat"] = messages;
    }
}


void DsSession::sessionError(const Darkmires::PacketPointer packet, const QString & message)
{
    emit replySignal(DsWebEvent::construct(packet, Darkmires::WebEventSessionError, message));
}


void DsSession::removeUser(int user)
{
    if (m_users.remove(user))
    {
        emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_USER_LEFT_SESSION, m_interface->m_uuid.toString()));
    }
}


void DsSession::processCommand(QString command)
{
    Q_UNUSED(command)
}
