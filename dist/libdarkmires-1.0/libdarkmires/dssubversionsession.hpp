#ifndef DSSUBVERSIONREPOSITORY_HPP
#define DSSUBVERSIONREPOSITORY_HPP

#include "dssession.hpp"
#include "dssessionuserdata.hpp"

#include <svnqt/repository.h>
#include <svnqt/repositorylistener.h>

#include <QDir>

class DsSubversionSessionUserData;

///
/// \brief Trieda DsSubversionSession predstavuje reláciu pracujúcu
/// so systémom kontroly verzií Subversion.
///
class DsSubversionSession : public DsSession
{
    Q_OBJECT

    public:
        DsSubversionSession(DsSessionInterface * const interface);
        virtual ~DsSubversionSession();

        // DsSession interface
    public:
        virtual void load(QDataStream & in);
        virtual void save(QDataStream & out);

        virtual bool init();

    public slots:
        virtual void processRequest(Darkmires::PacketPointer packet);

        virtual void addUser(int user);
        virtual void processCommand(QString command);

        // DsSession interface
    protected:
        virtual void populate(std::shared_ptr<DsSessionUserData> data);

        QSqlDatabase m_database;

        ///
        /// \brief getRepositoryPath získa cestu schránky daného používateľa
        /// \param userName meno daného používateľa, s ktorým bola schránka vytvorená
        /// \return cesta schránky
        ///
        QString getRepositoryPath(const QString & userName);

    private:
        QSqlQuery m_userQuerySelect;
        QSqlQuery m_userQueryUpdate;

        void appendInfo(Darkmires::PacketPointer packet, DsSubversionSessionUserData * userData);
};


///
/// \brief Trieda DsSubversionSessionUserData obaľuje dáta registrovaného používateľa
/// v systéme správy verzií Subversion.
///
class DARKMIRESSHARED_EXPORT DsSubversionSessionUserData : public DsSessionUserData
{
        Q_OBJECT

    public:
        explicit DsSubversionSessionUserData(int user);
        virtual ~DsSubversionSessionUserData();

        ///
        /// \brief getRepository získa pointer na schránku v systéme SvnKit.
        /// \return pointer na schránku v systéme SvnKit
        ///
        svn::repository::Repository * getRepository() const;

        ///
        /// \brief open otvorí repozitár v systéme SvnKit.
        /// \param path cesta k repozitáru
        /// \return true pri úspechu, inak false
        ///
        bool open(const QString & path);

        ///
        /// \brief createOpen vytvorí a otvorí repozitár v systéme SvnKit.
        /// \param cesta k repozitáru
        /// \return true pri úspechu, inak false
        ///
        bool createOpen(const QString & path);

        ///
        /// \brief isOpen zistí, či bola schránka vytvorená.
        /// \return true ak áno, inak nie
        ///
        bool isOpen() const;

        ///
        /// \brief path získa cestu k schránke tohto používateľa
        /// \return cesta k schránke tohto používateľa
        ///
        QString path() const;

        typedef struct SubversionUserData
        {
            QString name;
            QString password;
        } SubversionUserData;

        ///
        /// \brief data získa dáta tohto používateľa obsahujúce meno a heslo k
        /// schránke v systéme SvnKit.
        /// \return referencia na dáta tohto používateľa
        ///
        const SubversionUserData & data() const;

        ///
        /// \brief repositoryId
        /// \return
        ///
        const QUuid & repositoryId() const;

        ///
        /// \brief update obnoví dáta daného používateľa v databáze
        /// \param uuid jedinečný identifikátor používateľa
        /// \param update SQL požiadavka update
        /// \param select SQL požiadavka select
        ///
        void update(const QUuid & uuid, QSqlQuery & update, QSqlQuery & select);

    private:

        class DsSubversionRepositoryListener : public svn::repository::RepositoryListener
        {
            public:
                DsSubversionRepositoryListener(DsSubversionSessionUserData * context);

                // RepositoryListener interface
            public:
                virtual void sendWarning(const QString & message);
                virtual void sendError(const QString & message);
                virtual bool isCanceld();

            private:
                DsSubversionSessionUserData * m_context = nullptr;
        };

        svn::repository::Repository * m_repository = nullptr;
        DsSubversionRepositoryListener m_listener;

        bool m_running = false;

        bool m_isOpen = false;

        QDir m_path;

        SubversionUserData m_data;

        QUuid m_repositoryId;
};

#endif // DSSUBVERSIONREPOSITORY_HPP
