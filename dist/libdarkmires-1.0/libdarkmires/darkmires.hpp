#ifndef DARKMIRES_HPP
#define DARKMIRES_HPP

#include "darkmires_global.hpp"

#include <memory>
#include <functional>
#include <string>

#include <QHostAddress>
#include <QObject>
#include <QSettings>
#include <QHash>
#include <QDateTime>
#include <QUuid>
#include <QFileInfo>
#include <QElapsedTimer>

#define DarkSettings Darkmires::getSettings()

class DarkmiresImpl;

class QxtWebRequestEvent;

class DsSessionInterface;
class DsUser;

class DarkmiresEvent;
class DsWebEvent;

class DsAuthenticationLayer;
class DsDataLayer;

///
/// \brief
/// Hlavná trieda serverovej časti aplikácie Darkmire a
/// predstavuje rozhranie pre front-end servera. Drží si
/// inštancie tried reprezentujúcich jednotlivé vrstvy a
/// poskytuje všetky potrebné dátové štruktúry.
///
class DARKMIRESSHARED_EXPORT Darkmires : public QObject
{
        Q_OBJECT

    public:
        typedef std::function<void(const QString, const QString)> LoggingFunction;

        ///
        /// \brief Definuje podporované protokoly.
        ///
        enum Protocol
        {
            SCGI,
            HTTP
        };


        ///
        /// \brief Definuje možné typy požiadaviek.
        ///
        enum RequestType : int
        {
            RequestUnknown = -1,
            RequestError = 0,
            RequestNormal = 1,
            RequestAuthenticate = 2,
            RequestModerate = 3,
            RequestListSessions = 4,
            RequestDisconnect = 5,
            RequestServerInfo = 6,
            RequestUserModify = 7
        };

        /// \brief Obsahuje všetky možné typy požiadaviek na zmenu používateľských nastavení,
        /// ktoré môže server od klienta obdržať.
        enum ModifyEventType
        {
            ModifyUserPassword = 1
        };


        typedef unsigned int Flags;

        ///
        /// \brief Definuje jednotlivé bity bitovej masky
        /// ovládajúcej správanie servera.
        ///
        enum DarkmiresFlag : unsigned int
        {
            NoFlags           = 0x00,
            FlagForceHTTPS    = 0x01,
            FlagUseThreads    = 0x02,
            FlagAllowLDAP     = 0x04,
            FlagDenyExternal  = 0x08,
            FlagCompression   = 0x10,
            FlagDebug         = 0x20,
            FlagStatistics    = 0x40
        };


        /// \brief Hlavná dátová štruktúra paketu.
        typedef struct PacketData
        {
            QVariant json;
            QVariantMap data;
            const QxtWebRequestEvent * event;
            RequestType type = RequestUnknown;
            QString session;
            QUuid uuid; /// Registered UUID (non-null if registered)
            QUuid sessionID; /// HTTP cookie session
            QElapsedTimer time;
            quint64 database = 0;
        } PacketData;

        typedef std::shared_ptr<Darkmires::PacketData> PacketPointer;


        typedef std::shared_ptr<DsUser> UserPointer;
        typedef QHash<int, UserPointer> UserMap;
        typedef QPair<const int, std::unique_ptr<DsUser>> UserPair;


        typedef QHash<QString, std::shared_ptr<DsSessionInterface>> SessionMap;
        typedef QPair<const QString, std::unique_ptr<DsSessionInterface>> SessionPair;

        /// \brief Štruktúra obaľujúca informácie o skripte.
        typedef struct
        {
            QFileInfo script;
            QHash<QString, QFileInfo> translations;
        } ScriptInfo;

        typedef QHash<QString, ScriptInfo> ScriptInfoStorage;

        typedef DarkmiresEvent Event;

        /// \brief Štruktúra obaľujúca informácie o správe z
        /// chatovacieho okna.
        typedef struct ChatMessage
        {
            ChatMessage() = default;
            ChatMessage(const ChatMessage &) = default;

            ChatMessage(ChatMessage && other) : id(other.id),
                message(std::move(other.message)),
                user(other.user),
                time(std::move(other.time))
            { }

            ChatMessage & operator= (ChatMessage && other)
            {
                id = other.id;
                message = std::move(other.message);
                user = other.user;
                time = std::move(other.time);
                return *this;
            }
            ChatMessage & operator= (const ChatMessage &) = default;

            int id;
            QString message;
            int user;
            QDateTime time;
        } ChatMessage;

        typedef QMap<qint64, ChatMessage> ChatStorage;

        /// \brief Špecifikuje typ autentifikácie relácie.
        enum AuthenticationType
        {
            AuthenticationError = 0,
            AuthenticationNone = 1,
            AuthenticationLDAP = 2,
            AuthenticationLocalDatabase = 4
        };

        /// \brief Špecifikuje typ relácie.
        enum SessionMode
        {
            SessionNone = 0,
            SessionQuickShare = 1,
            SessionScripted = 2,
            SessionSubversion = 3
        };

        /// \brief Obsahuje všetky možné typy odpovedí,
        /// ktoré môže klient od servera obdržať.
        enum WebEventType
        {
            // Custom codes
            WebEventNormal = 0,
            WebEventError = 1,
            WebEventAuthenticationError = 2,
            WebEventAuthenticationSuccess = 3,
            WebEventAuthenticationDisconnect = 7,
            WebEventAuthenticationAlreadyAuthenticated = 9,
            WebEventSessionError = 4,
            WebEventSessionSuccess = 8,
            WebEventDataError = 5,
            WebEventDataSuccess = 6,

            // These follow standard HTTP error codes
            WebNotFound = 404,
            WebInternalError = 500,
            WebServiceNotAllowed = 405,
            WebBadRequest = 400
        };


        typedef unsigned int UserPrivileges;

        /// \brief Špecifikuje, aké privilégia má používateľ v
        /// prostredí klienta aj servera.
        enum UserPrivilege : unsigned int
        {
            UserNone      = 0x00,
            UserRegular   = 0x01,
            UserAdmin     = 0x02,
            UserModerator = 0x04
        };

        /// \brief Štruktúra obaľujúca informácie o správe od
        /// administrátora, servera alebo iného zdroja.
        typedef struct UserMessage
        {
            UserMessage() = default;
            UserMessage(const UserMessage &) = default;
            UserMessage(UserMessage && other) : text(std::move(other.text)) { }
            UserMessage & operator= (UserMessage && other)
            {
                text = other.text;
                return *this;
            }
            UserMessage & operator= (const UserMessage &) = default;

            QString text;
        } UserMessage;

        ///
        /// \brief setLoggingFunction nastaví callback funkciu, ktorá
        /// sa bude volať pri rôznych typoch správ od servera.
        /// \param function callback funkcia
        ///
        void setLoggingFunction(Darkmires::LoggingFunction function);

        ///
        /// \brief getSessions získa referenciu na zoznam súčasne aktívnych relácií.
        /// \return zoznam súčasne aktívnych relácií
        ///
        const SessionMap & getSessions();

        ///
        /// \brief getScripts získa referenciu na zoznam aktívnych skriptov.
        /// \return zoznam aktívnych skriptov
        ///
        const ScriptInfoStorage & getScripts();

        ///
        /// \brief init inicializuje server bez toho, aby ho spustil
        /// Túto funkciu je nutné zavolať pred akoukoľvek inou
        /// komunikáciou.
        ///
        void init();

        ///
        /// \brief getSettings získa pointer na triedu spravujúcu nastavenia servera.
        /// \return pointer na triedu spravujúcu nastavenia servera
        ///
        static QSettings * getSettings();

        ///
        /// \brief releaseSettings zmaže internú inštanciu triedy QSettings, ak bola predtým
        /// akýmkoľvek spôsobom použitá.
        /// \sa getSettings
        ///
        static void releaseSettings();

        ///
        /// \brief saveSettings uloží nastavenia servera zmenené po volaní getSettings.
        /// \sa getSettings
        ///
        static void saveSettings();

        ///
        /// \brief updateDefaults zmení všetky nastavenia v štruktúre QSettings na východzie.
        /// \sa getSettings
        ///
        static void updateDefaults();

        ///
        /// \brief authentication získa pointer na triedu DsAuthenticationLayer reprezentujúcu.
        /// autentifikačnú vrstvu
        /// \return pointer na triedu DsAuthenticationLayer
        ///
        DsAuthenticationLayer * authentication();

        ///
        /// \brief data získa pointer na triedu DsDataLayer reprezentujúcu.
        /// dátovú vrstvu
        /// \return pointer na triedu DsDataLayer
        ///
        DsDataLayer * data();


        /// \brief Definuje štruktúru páru obsahujúcu názov a typ požiadavky.
        typedef struct
        {
            const char * id;
            Darkmires::RequestType type;
        } RequestMap;
        static RequestMap RequestAliases[];

        ///
        /// \brief recognizeRequest rozpozná požiadavku na základe jej názvu a vráti jej kód.
        /// \param str názov požiadavky
        /// \return kód požiadavky
        ///
        static Darkmires::RequestType recognizeRequest(const QString & str);

        ///
        /// \brief chatMessage pošle chatovú správu do relácie.
        /// \param session identifikátor relácie
        /// \param message správa
        ///
        void chatMessage(const QString & session, ChatMessage && message);

        ///
        /// \brief userMessage pošle správu používateľovi
        /// \param user identifikátor pripojeného používateľa
        /// \param message správa
        ///
        void userMessage(int user, UserMessage && message);

        ///
        /// \brief getInstance získa statickú inštanciu tejto triedy podľa vzoru
        /// singleton. Ak inštancia neexistuje, vytvorí novú.
        /// Napríklad:
        /// \code
        /// Darkmires::getInstance()->init();
        /// \endcode
        /// \return inštanciu tejto triedy
        /// \sa freeInstance
        ///
        static Darkmires * getInstance();

        ///
        /// \brief freeInstance zmaže predtým vytvorenú inštanciu tejto triedy
        /// \sa getInstance
        ///
        static void freeInstance();

        ///
        /// \brief hasFlag zistí, či bol server zapnutý s daným atribútom
        /// \param flag atribúť
        /// \return boolovska hodnota reprezentujúca výsledok
        /// \sa start
        ///
        bool hasFlag(Darkmires::DarkmiresFlag flag);

        ///
        /// \brief flags vráti bitovú mapu atribútov, s ktorými bol server zapnutý
        /// \return bitovú mapu atribútov, s ktorými bol server zapnutý
        /// \sa start
        ///
        Flags flags();

    public slots:
        ///
        /// \brief start zapne chod servera. Od úspešného volania tejto funkcie je server schopný príjímať
        /// a odpovedať na požiadavky a je možné volať ostatné funkcie referujúce na jeho internú štruktúru
        /// a vrstvy
        /// \param host definuje hostovské rozhranie, väčšinou IADR_ANY
        /// \param port port, na ktorom bude server bežať a prijímať požiadavky
        /// \param protocol protokol, odporúča sa SCGI
        /// \param flags atribúty špecifikujúce vlastnosti behu servera
        /// \return pri úspechu true, pri chybe false a chyba je odoslaná cez logovaciu funkciu
        ///
        bool start(const QHostAddress & host,
                   quint16 port,
                   Darkmires::Protocol protocol = Darkmires::SCGI,
                   Flags flags = Darkmires::NoFlags);

        ///
        /// \brief stop zastaví chod servera. Na opätovné naštartovanie stačí zavolať funkciu start,
        /// nie je nutné server znova inicializovať
        ///
        void stop();

        ///
        /// \brief kickUser vyhodí používateľa zo všetkých relácií a deautentifikuje ho
        /// \param id identifikátor prihláseného používateľa
        ///
        void kickUser(int id);

        ///
        /// \brief addSession pridá reláciu počas behu servera
        /// \param name meno relácie
        /// \param authentication typ autentifikácie
        /// \param mode typ relácie
        /// \param id požadovaný identifikátor. Ak je prázdny, vygeneruje sa nový
        /// \return rozhranie danej relácie
        ///
        DsSessionInterface * addSession(const QString & name, Darkmires::AuthenticationType authentication, Darkmires::SessionMode mode, const QString & id);

        ///
        /// \brief removeSession zmaže reláciu počas behu servera
        /// \param id identifikátor relácie
        /// \param removeFile definuje, či sa má zmazať aj súbor relácie a teda všetky jej interné dáta
        ///
        void removeSession(const QString & id, bool removeFile = false);

        ///
        /// \brief command prijíme príkaz a spracuje ho
        /// \param name názov príkazu
        /// \param args argumenty
        /// \return návratové argumenty
        ///
        QVariantMap command(const QString & name, const QVariantMap & args);

    signals:
        ///
        /// \brief darkmiresEvent pošle signál o zmene interného stavu servera
        ///
        void darkmiresEvent(Darkmires::Event);

    private:
        explicit Darkmires();
        Darkmires(const Darkmires &) = delete;

        virtual ~Darkmires();

        class DarkmiresImplDeleter
        {
            public:
                DarkmiresImplDeleter() { }

                void operator()(DarkmiresImpl * p);
        };

        class DarkmiresDeleter
        {
            public:
                DarkmiresDeleter() { }

                void operator()(Darkmires * p);
        };

        std::unique_ptr<DarkmiresImpl, DarkmiresImplDeleter> m_darkmires;
        DarkmiresImplDeleter m_darkmiresDeleter;

        static std::unique_ptr<QSettings> m_settings;

        static std::unique_ptr<Darkmires, DarkmiresDeleter> m_singleton;

    public slots:
        ///
        /// \brief emitEvent je slot, ktorý spôsobí vyslanie signálu
        /// \param event signál, ktorý sa má vyslať
        ///
        void emitEvent(Darkmires::Event event);
};

#include "dssessioninterface.hpp"
#include "dsuser.hpp"

#include "darkmiresevent.hpp"

#endif // DARKMIRES_HPP
