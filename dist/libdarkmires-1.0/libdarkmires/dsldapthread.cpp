/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsldapthread.hpp"

#include <QxtLogger>

#include <ldap.h>

#include "dsldap.hpp"
#include "dsconstants.hpp"

DsLDAPThread::DsLDAPThread(QObject * parent) :
    QThread(parent)
{
}


void DsLDAPThread::run()
{
    //    LDAP * ldg[1];
    int rc;

    DsAuthenticationLayer::SearchResult result;


    qxtLog->debug() << "[LDAP] Connecting to LDAP host...";

    LDAP * ld = nullptr;

    if ((rc = ldap_initialize(&ld, DarkSettings->value("ldap/url").toString().toAscii().data())) != LDAP_SUCCESS)
    {
        result.error = QString(ldap_err2string(rc));

        qxtLog->error() << "[LDAP] Error initializing: " + result.error;

        emit searchFinished(m_id, result);

        return;
    }

    if (ld == nullptr)
    {
        qxtLog->error() << "[LDAP] Error initializing!";

        result.error = "Internal server error";

        emit searchFinished(m_id, result);

        return;
    }

    /* Use the LDAP_OPT_PROTOCOL_VERSION session preference to specify
       that the client is an LDAPv3 client.
    */

    int version = LDAP_VERSION3;

    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

    /* STEP 2: Bind to the server. */

    berval creds;

    QString ldap_pw = DarkSettings->value("ldap/bind-pw").toString();

    creds.bv_val = const_cast<char *>(ldap_pw.toUtf8().data());
    creds.bv_len = ldap_pw.size();

    QString mechanism = DarkSettings->value("ldap/mechanism").toString();

    rc = ldap_sasl_bind_s(ld,
                          DarkSettings->value("ldap/bind-dn").toString().toUtf8().data(), // DN
                          mechanism.isEmpty() ? nullptr : mechanism.toUtf8().data(), // Mechanism
                          &creds, // Cred
                          nullptr, // Server Controls
                          nullptr, // Client Controls
                          nullptr); // Servercredp

    if (rc != LDAP_SUCCESS)
    {

        result.error = QString(ldap_err2string(rc));

        qxtLog->error() << "[LDAP] Error binding: " + result.error;

        emit searchFinished(m_id, result);

        return;
    }


    /* STEP 3: Perform the LDAP operations.

    //    In this example, a simple search operation is performed.
    //    The client iterates through each of the entries returned and
    //    prints out the DN of each entry. */

    QString filter = QString(DarkSettings->value("ldap/filter").toString()).arg(m_name);

    timeval timeout;
    timeout.tv_sec = DarkSettings->value("ldap/search-timeout").toUInt();

    LDAPMessage * res = nullptr;
    LDAPMessage * entry = nullptr;

    rc = ldap_search_ext_s(ld,
                           DarkSettings->value("ldap/basedn").toString().toUtf8().data(),
                           LDAP_SCOPE_SUBTREE,
                           filter.toUtf8().data(),
                           nullptr,
                           0,
                           nullptr,
                           nullptr,
                           &timeout,
                           0,
                           &res);

    if (rc != LDAP_SUCCESS)
    {

        result.error = QString(ldap_err2string(rc));

        qxtLog->error() << "[LDAP] Error searching: " + result.error;

        goto free_and_unbind;
    }

    //    ldap_parse_sasl_bind_result(ld,
    //                                res,
    //                                nullptr,
    //                                0);

    if ((entry = ldap_first_entry(ld, res)))
    {

        char * dn;

        for (; entry != nullptr; entry = ldap_next_entry(ld, entry))
        {
            if ((dn = ldap_get_dn(ld, entry)) != NULL)
            {

                qxtLog->debug() << dn;

                ldap_memfree(dn);
            }
        }

        BerElement * aBe = nullptr;

        BerValue ** values;

        for (char * an = ldap_first_attribute(ld, res, &aBe) ; an; an = ldap_next_attribute(ld, res, aBe))
        {
            values = ldap_get_values_len(ld, res, an);

            qxtLog->write() << QString("[LDAP] Attribute: ") + an;

            for (int i = 0; i < ldap_count_values_len(values); i++)
            {
                qxtLog->write() << QString::fromUtf8(values[i]->bv_val, values[i]->bv_len);
            }

            ldap_memfree(an);
            ldap_value_free_len(values);
        }

        ber_free(aBe, 0);

        result.success = true;
    }

free_and_unbind:

    ldap_msgfree(res);

    /* STEP 4: Disconnect from the server. */

    ldap_unbind_ext_s(ld, nullptr, nullptr);

    if (! result.success)
    {
        result.error = "Invalid LDAP username/password";
    }

    emit searchFinished(m_id, result);
}
