/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "darkmires.hpp"

#include "darkmiresimpl.hpp"

#include "dsloggingengine.hpp"

#include "dswebserver.hpp"
#include "dsrootwebservice.hpp"

#include "dsconstants.hpp"

#include "dsdatalayer.hpp"
#include "dsauthenticationlayer.hpp"

#include "dswebevent.hpp"
#include "dsuserstorage.hpp"

#include <QxtHttpSessionManager>
#include <QxtLogger>

#include <QxtJSON>


std::unique_ptr<QSettings> Darkmires::m_settings;

std::unique_ptr<Darkmires, Darkmires::DarkmiresDeleter> Darkmires::m_singleton;


Darkmires::RequestMap Darkmires::RequestAliases[] =
{
    {"normal",           Darkmires::RequestNormal},
    {"authenticate",     Darkmires::RequestAuthenticate},
    {"admin",            Darkmires::RequestModerate},
    {"sessions",         Darkmires::RequestListSessions},
    {"disconnect",       Darkmires::RequestDisconnect},
    {nullptr,            Darkmires::RequestError}
};


void Darkmires::DarkmiresImplDeleter::operator()(DarkmiresImpl * p)
{
    delete p;
}


void Darkmires::DarkmiresDeleter::operator()(Darkmires * p)
{
    delete p;
}


Darkmires::Darkmires() :
    QObject(nullptr),
    m_darkmires(new DarkmiresImpl(this), m_darkmiresDeleter)
{
    qRegisterMetaType<Darkmires::PacketPointer>("Darkmires::PacketPointer");
    qRegisterMetaType<Darkmires::Event>("Darkmires::Event");

    connect(DsUserStorage::getInstance(),
            SIGNAL(usersEvent(Darkmires::Event)),
            this,
            SLOT(emitEvent(Darkmires::Event)));
}


Darkmires::~Darkmires()
{
    setLoggingFunction(Darkmires::LoggingFunction());

    m_darkmires->deinit();

    Darkmires::saveSettings();
    Darkmires::releaseSettings();
}


Darkmires * Darkmires::getInstance()
{
    if (! m_singleton.get())
    {
        m_singleton.reset(new Darkmires);
    }

    return m_singleton.get();
}


void Darkmires::freeInstance()
{
    m_singleton.reset();
}


void Darkmires::init()
{
    Darkmires::getSettings();
}


void Darkmires::emitEvent(Darkmires::Event event)
{
//    qDebug() << "Emitting event: " << event.type();

    emit darkmiresEvent(std::move(event));
}


bool Darkmires::start(const QHostAddress & host,
                      quint16 port,
                      Darkmires::Protocol protocol,
                      Flags flags)
{
    QxtHttpSessionManager::Connector connector = QxtHttpSessionManager::Scgi;

    switch (protocol)
    {
        case Darkmires::SCGI:
            connector = QxtHttpSessionManager::Scgi;
            break;

        case Darkmires::HTTP:
            qxtLog->error() << "HTTP not supported!";
            return false;
    }

    m_darkmires->init(host, port, connector, flags);

    connect(m_darkmires->getServer(),
            SIGNAL(serverEvent(Darkmires::Event)),
            this,
            SLOT(emitEvent(Darkmires::Event)));

    return m_darkmires->start();
}


void Darkmires::stop()
{
    m_darkmires->stop();
}


void Darkmires::setLoggingFunction(Darkmires::LoggingFunction function)
{
    m_darkmires->getLogger()->setLoggingFunction(function);
}


const Darkmires::SessionMap & Darkmires::getSessions()
{
    return m_darkmires->getServer()->getRoot()->getDataLayer()->getSessions();
}


const Darkmires::ScriptInfoStorage & Darkmires::getScripts()
{
    return m_darkmires->getServer()->getRoot()->getDataLayer()->getScripts();
}


Darkmires::RequestType Darkmires::recognizeRequest(const QString & str)
{
    for (int i = 0; Darkmires::RequestAliases[i].id != nullptr; i++)
    {
        if (str == Darkmires::RequestAliases[i].id)
        {
            return Darkmires::RequestAliases[i].type;
        }
    }

    return Darkmires::RequestError;
}


/**
 * @brief DarkmiresImpl::getSettings uses singleton concept to create and retrieve global settings object
 * @return Pointer to QSettings object
 */
QSettings * Darkmires::getSettings()
{
    if (m_settings.get() == nullptr)
    {
        m_settings.reset(new QSettings(QSettings::IniFormat, QSettings::UserScope,
                                       "brendor", "darkmire-server"));

        Darkmires::updateDefaults();

        qxtLog->info() << "Settings file opened: " + m_settings->fileName();
    }

    return m_settings.get();
}


void Darkmires::releaseSettings()
{
    m_settings.reset();
}


void Darkmires::saveSettings()
{
    if (Darkmires::getSettings())
    {
        Darkmires::getSettings()->sync();
    }
}


void Darkmires::updateDefaults()
{
    QSettings * settings = Darkmires::getSettings();

    QSettings defaults(":/data/defaults.ini", QSettings::IniFormat);

    for (const QString & key : defaults.allKeys())
    {
        if (! settings->contains(key))
        {
            settings->setValue(key, defaults.value(key));
        }
    }

    for (const QString & key : settings->allKeys())
    {
        if (! defaults.contains(key))
        {
            settings->remove(key);
        }
    }
}


DsAuthenticationLayer * Darkmires::authentication()
{
    return m_darkmires->getServer()->getRoot()->getAuthenticationLayer();
}

DsDataLayer * Darkmires::data()
{
    return m_darkmires->getServer()->getRoot()->getDataLayer();
}


DsSessionInterface * Darkmires::addSession(const QString & name, Darkmires::AuthenticationType authentication, Darkmires::SessionMode mode, const QString & id)
{
    DsSessionInterface * session = DsSessionInterface::create(name, authentication, mode, id);

    if (session)
    {
        m_darkmires->getServer()->getRoot()->getDataLayer()->addSession(session);
    }

    return session;
}


void Darkmires::removeSession(const QString & id, bool removeFile)
{
    m_darkmires->getServer()->getRoot()->getDataLayer()->removeSession(id, removeFile);
}


QVariantMap Darkmires::command(const QString & name, const QVariantMap & args)
{
    QVariantMap ret;
    bool ok = true;

    if      (name == "start")
    {
        if (args.contains("address") &&
            args.contains("port"))
        {
            QHostAddress host(args.value("address").toString());
            quint16 port = args.value("port").toUInt(&ok);

            if (! host.isNull() && ok)
            {
                start(host, port, Darkmires::SCGI, args.value("flags", Darkmires::NoFlags).toUInt());
            }
        }
        else
        {
            ret["error"] = "Invalid command arguments";
            ok = false;
        }

        return ret;

    } else if (name == "stop")
    {
        stop();
    }

    ret["result"] = ok;

    return ret;
}


void Darkmires::chatMessage(const QString & session, ChatMessage && message)
{
    m_darkmires->getServer()->getRoot()->getDataLayer()->chatMessage(session, std::move(message));
}


void Darkmires::userMessage(int user, Darkmires::UserMessage && message)
{
    LockUserStorage
    DsUserStorage::getInstance()->get(user)->enqueueMessage(std::move(message));
    UnlockUserStorage
}


void Darkmires::kickUser(int id)
{
    m_darkmires->getServer()->getRoot()->getAuthenticationLayer()->kickUser(id);
}


bool Darkmires::hasFlag(Darkmires::DarkmiresFlag flag)
{
    return m_darkmires->getFlags() & flag;
}


Darkmires::Flags Darkmires::flags()
{
    return m_darkmires->getFlags();
}
