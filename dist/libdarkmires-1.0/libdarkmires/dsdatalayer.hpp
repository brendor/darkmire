#ifndef DARKMIRESDATALAYER_HPP
#define DARKMIRESDATALAYER_HPP

#include <QObject>

#include "darkmires.hpp"

///
/// \brief DsDataLayer je trieda reprezentujúca dátovú vrstvu aplikácie servera.
///
class Q_DECL_HIDDEN DsDataLayer : public QObject
{
        Q_OBJECT

    public:
        ///
        /// \brief DsDataLayer je konštruktor tejto triedy.
        /// \param parent rodičovský objekt
        ///
        explicit DsDataLayer(QObject * parent = 0);

        ///
        /// \brief ~DsDataLayer je deštruktor tejto triedy.
        ///
        virtual ~DsDataLayer();

        ///
        /// \brief getSessions získa referenciu na interný zoznam relácií
        /// \return referencia na interný zoznam relácií
        ///
        inline const Darkmires::SessionMap & getSessions()
        {
            return m_sessions;
        }

        ///
        /// \brief addSession pridá reláciu do zoznamu
        /// \param session pointer na rozhranie relácie
        /// \return
        ///
        DsSessionInterface * addSession(DsSessionInterface * session);

        ///
        /// \brief removeSession odstráni danú reláciu zo zoznamu a odpojí z nej všetkých pripojených
        /// používateľov.
        /// \param id identifikátor relácie
        /// \param removeFile definuje, či sa majú odstrániť aj dátové súbory relácie
        /// \return
        ///
        Darkmires::SessionMap::const_iterator removeSession(const QString & id, bool removeFile = false);

        ///
        /// \brief removeUser odstráni používateľa zo všetkých relácií alebo z konkrétnej relácie, ku ktorej je pripojený.
        /// \param id identifkátor pripojeného používateľa
        /// \param session konkrétna relácia, ak je prázdne, odstráni ho zo všetkých
        ///
        void removeUser(int id, const QString & session = QString());

        ///
        /// \brief addUser pridá používateľa do relácie špecifikovanej v internej štruktúre požiadavky.
        /// \param packet definuje požiadavku
        /// \return
        ///
        bool addUser(const Darkmires::PacketPointer & packet);

        ///
        /// \brief chatMessage pošle chatovú správu do relácie.
        /// \param session identifikátor relácie
        /// \param message správa
        ///
        void chatMessage(const QString & session, Darkmires::ChatMessage && message);

        //        void userMessage(int user, Darkmires::UserMessage && message);

        ///
        /// \brief getScripts získa referenciu na vnútorný zoznam skriptov.
        /// \return referencia na vnútorný zoznam skriptov
        ///
        inline const Darkmires::ScriptInfoStorage & getScripts()
        {
            return m_scriptInfoStorage;
        }

    signals:

        ///
        /// \brief replySignal je signál vyslaný pri odosielaní odpovede na požiadavku.
        /// \param event pointer na paket reprezentujúci odpoveď
        ///
        void replySignal(DsWebEvent * event);

        ///
        /// \brief dataEvent je signál vyvolaný pri vzniku udalosti vo vnútri dátovej vrstvy.
        /// \param event udalosť
        ///
        void dataEvent(Darkmires::Event event);

    public slots:

        ///
        /// \brief replySlot slot pre prichádzajúce odpovede z ďalších vrstiev.
        /// \param event pointer na odpoveď
        ///
        void replySlot(DsWebEvent * event);

        ///
        /// \brief requestSlot zásuvka pre prichádzajúce požiadavku z predchádzajúcich vrstiev.
        /// \param packet pointer na požiadavku
        ///
        void requestSlot(Darkmires::PacketPointer packet);

    private slots:
        void timeout();

        void eventSlot(Darkmires::Event event);

    private:
        DsDataLayer(const DsDataLayer &) = delete;

        Darkmires::SessionMap m_sessions;

        QTimer m_timer;

        Darkmires::ScriptInfoStorage m_scriptInfoStorage;

        void dataError(const Darkmires::PacketPointer packet, const QString & message);
};

#endif // DARKMIRESDATALAYER_HPP
