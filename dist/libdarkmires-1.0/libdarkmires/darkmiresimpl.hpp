#ifndef DARKMIRESIMPL_HPP
#define DARKMIRESIMPL_HPP

#include <QObject>

#include <QxtScgiServerConnector>
#include <QxtHttpSessionManager>

class DsWebServer;
class DsRootWebService;
class DsLoggingEngine;

#include <memory>

#include "darkmires.hpp"

///
/// \brief DarkmiresImpl definuje rozhranie pre triedu Darkmire k webovému serveru podľa vzoru
/// pointer to implementation. Jej významom je zachovanie binárnej kompatibility aj pri
/// zmene predpisu triedy Darkmire.
/// \sa Darkmires
///
class Q_DECL_HIDDEN DarkmiresImpl : public QObject
{
        Q_OBJECT

    public:
        ///
        /// \brief DarkmiresImpl je konštruktor tejto triedy.
        /// \param parent rodičovský objekt
        ///
        explicit DarkmiresImpl(QObject * parent = 0);

        virtual ~DarkmiresImpl();

        ///
        /// \brief init inicializuje webový server.
        /// \param host špecifikuje interface, na ktorom bude server bežať
        /// \param port definuje port servera
        /// \param connector interná inštancia prepojovača
        /// \param flags atribúty
        /// \return
        ///
        bool init(const QHostAddress & host,
                  quint16 port,
                  QxtHttpSessionManager::Connector connector,
                  Darkmires::Flags flags);

        ///
        /// \brief deinit deinicializuje predtým inicializovaný server.
        ///
        void deinit();

        ///
        /// \brief start zapne webový server.
        /// \return pri úspešnom zapnutí true, inak false
        ///
        bool start();

        ///
        /// \brief stop vypne webový server.
        ///
        void stop();

        ///
        /// \brief getLogger získa logovací interface
        /// \return pointer na logovací interface
        ///
        inline DsLoggingEngine * getLogger() const
        {
            return m_logger;
        }

        ///
        /// \brief getServer získa pointer na webový server, ku ktorému je táto trieda rozhraním
        /// \return pointer na webový server
        ///
        inline DsWebServer * getServer()
        {
            return m_server.get();
        }

        ///
        /// \brief getFlags získa atribúty, s ktorými bola táto trieda inicializovaná
        /// \return
        ///
        inline Darkmires::Flags getFlags()
        {
            return m_flags;
        }

    private:
        DarkmiresImpl(const DarkmiresImpl &) = delete;

        std::unique_ptr<DsWebServer> m_server;
        DsLoggingEngine * const m_logger;

        Darkmires::Flags m_flags;
};

#endif // DARKMIRESIMPL_HPP
