/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dswebevent.hpp"

#include <QxtJSON>
#include <QxtLogger>

#include "dsconstants.hpp"
#include "dsutils.hpp"
#include "dsuserstorage.hpp"

DsWebEvent::DsWebEvent(int sessionID, int requestID, int status, QByteArray statusMessage) :
    QxtWebPageEvent(sessionID, requestID, statusMessage)
{
    QxtWebPageEvent::status = status;
}


DsWebEvent * DsWebEvent::construct(const Darkmires::PacketPointer packet,
                                   Darkmires::WebEventType type,
                                   const QVariant & data,
                                   Darkmires::UserPrivileges privileges,
                                   bool suppressCompression)
{
    QVariantMap root;

    QVariantMap darkmire;

    darkmire[DsConstants::JSON_STATUS_ELEMENT] = (int)type;
    darkmire[DsConstants::JSON_DATA_ELEMENT] = data;
    darkmire[DsConstants::JSON_PRIVILEGES_ELEMENT] = (int)privileges;

    LockUserStorage
    DsUser * user = DsUserStorage::getInstance()->get(packet->event->sessionID);
    if (user)
    {
        darkmire[DsConstants::JSON_USERNAME_ELEMENT] = user->getName();
    }
    UnlockUserStorage

    if (! suppressCompression
            && Darkmires::getInstance()->hasFlag(Darkmires::FlagCompression))
    {
        QByteArray compressed = DsUtils::compress(QxtJSON::stringify(darkmire).toUtf8()).toBase64();

        root[DsConstants::JSON_DARKMIRE_ELEMENT] = QString::fromAscii(compressed.constData(), compressed.size());
        root[DsConstants::JSON_CHECKSUM_ELEMENT] = DsUtils::Crc16CCITT(compressed.constData(), compressed.size());
    }
    else
    {
        root[DsConstants::JSON_DARKMIRE_ELEMENT] = darkmire;
    }

    qxtLog->info() << "Request user session: " << packet->sessionID.toString();
    qxtLog->debug() << "Request processing took: " << packet->time.nsecsElapsed() << " nanoseconds.";

    if (Darkmires::getInstance()->hasFlag(Darkmires::FlagStatistics))
    {
        QVariantMap d;
        d["type"] = DarkmiresEvent::STATISTICS_PACKET_PROCESSING;
        d["id"] = packet->event->requestID;
        d["context"] = type;
        d["time"] = packet->time.nsecsElapsed();

        Darkmires::Event event(DarkmiresEvent::EVENT_STATISTICS, d);

        Darkmires::getInstance()->emitEvent(event);
    }

    return new DsWebEvent(packet->event->sessionID, packet->event->requestID, 200, QxtJSON::stringify(root).toUtf8());
}
