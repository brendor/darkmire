#ifndef DARKMIRES_CONSTANTS_HPP
#define DARKMIRES_CONSTANTS_HPP

#ifdef __GNUC__
#define MAYBE_UNUSED __attribute__((used))
#elif defined _MSC_VER
#pragma warning(disable: Cxxxxx)
#define MAYBE_UNUSED
#else
#define MAYBE_UNUSED
#endif

///
/// \brief Špecifikuje konštanty pre túto aplikáciu
///
namespace DsConstants
{
MAYBE_UNUSED static const char * SESSION_ID = "sessionID";

MAYBE_UNUSED static const char * SESSIONS_FILENAME = "darkmires_sessions";

MAYBE_UNUSED static const char * JSON_SESSION_ELEMENT = "session";
MAYBE_UNUSED static const char * JSON_USERNAME_ELEMENT = "username";
MAYBE_UNUSED static const char * JSON_TYPE_ELEMENT = "type";
MAYBE_UNUSED static const char * JSON_DATA_ELEMENT = "data";
MAYBE_UNUSED static const char * JSON_STATUS_ELEMENT = "status";
MAYBE_UNUSED static const char * JSON_PRIVILEGES_ELEMENT = "privileges";
MAYBE_UNUSED static const char * JSON_NAME_ELEMENT = "name";
MAYBE_UNUSED static const char * JSON_PASSWORD_ELEMENT = "password";

MAYBE_UNUSED static const char * JSON_DARKMIRE_ELEMENT = "darkmire";
MAYBE_UNUSED static const char * JSON_CHECKSUM_ELEMENT = "checksum";
MAYBE_UNUSED static const char * JSON_ORIGINAL_SIZE_ELEMENT = "size";

//    MAYBE_UNUSED static int USER_TIMEOUT_SECONDS = 20;
//    MAYBE_UNUSED static int DEFAULT_LDAP_SEARCH_TIMEOUT_SECONDS = 5;

MAYBE_UNUSED static const char * SQL_DATABASE_CONNECTION_NAME = "DARKMIRE";
MAYBE_UNUSED static const char * SQL_DATABASE_SESSION_PREFIX = "DARKMIRE_SESSION_";
MAYBE_UNUSED static const char * SQL_DATABASE_SESSION_THREAD_PREFIX = "DARKMIRE_SESSION_THREAD_";

MAYBE_UNUSED static const char * LOGGER_ENGINE_NAME = "DARKMIRE";

MAYBE_UNUSED static const char * APPLICATION_NAME = "Darkmire Server";
MAYBE_UNUSED static const char * APPLICATION_VERSION = "0.0.1";

MAYBE_UNUSED static const char * DEFAULT_SESSION_ID = "sessionID";

MAYBE_UNUSED static const char * SESSION_TIMEOUT = "session-timeout";

MAYBE_UNUSED static int WAIT_FOR_SESSION = 50;
MAYBE_UNUSED static int WAIT_FOR_THREAD = 300;
}

#endif // DARKMIRES_CONSTANTS_HPP
