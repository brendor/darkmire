#ifndef DARKMIRESEVENT_HPP
#define DARKMIRESEVENT_HPP

#include <QVariant>

///
/// \brief Trieda DarkmiresEvent predstavuje udalosť servera.
/// Na túto udalosť môže front-end alebo iná aplikácia používajúca túto
/// knižnicu reagovať napríklad obnovením zoznamu používateľov alebo relácií.
///
class DarkmiresEvent
{
    public:
        ///
        /// \brief EventType enum definuje typ udalosti.
        ///
        enum EventType
        {
            EVENT_NONE = 0,
            EVENT_USER_JOINED,
            EVENT_USER_LEFT,
            EVENT_USER_EDITED,
            EVENT_USER_JOINED_SESSION,
            EVENT_USER_LEFT_SESSION,
            EVENT_SESSION_ADDED,
            EVENT_SESSION_REMOVED,
            EVENT_SESSION_NEW_DATA,
            EVENT_NEW_CHAT_MESSAGE,
            EVENT_COMMAND_RESULT,
            EVENT_STATISTICS
        };

        ///
        /// \brief StatisticsType enum definuje typ meranej vlastnosti.
        ///
        enum StatisticsType
        {
            STATISTICS_PACKET_PROCESSING = 0
        };

        ///
        /// \brief DarkmiresEvent skonštruuje prázdnu udalosť.
        ///
        DarkmiresEvent();

        ///
        /// \brief DarkmiresEvent skonštruuje udalosť s daným typom a dátami.
        /// \param t typ udalosti
        /// \param d dáta udalosti
        ///
        DarkmiresEvent(const EventType & t, const QVariant & d = QVariant());

        ///
        /// \brief DarkmiresEvent skonštruuje udalosť skopírovaním inej udalosti.
        /// \param event udalosť na skopírovanie
        ///
        DarkmiresEvent(const DarkmiresEvent & event);
        DarkmiresEvent(DarkmiresEvent && event);

        ///
        /// \brief ~DarkmiresEvent je deštruktorom tejto triedy.
        ///
        virtual ~DarkmiresEvent();

        ///
        /// \brief type získa typ udalosti.
        /// \return typ udalosti
        ///
        EventType type()
        {
            return m_type;
        }

        ///
        /// \brief data získa data udalosti.
        /// \return dáta udalosti
        ///
        const QVariant & data()
        {
            return m_data;
        }

    private:
        EventType m_type;
        QVariant m_data;
};

#endif // DARKMIRESEVENT_HPP
