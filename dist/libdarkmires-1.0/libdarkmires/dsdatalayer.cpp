/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsdatalayer.hpp"

#include <QxtLogger>

#include "dswebevent.hpp"

#include "dsconstants.hpp"
#include "dsuserstorage.hpp"

#include "dsauthenticationlayer.hpp"

#include "dssessioninterface.hpp"

#include "dsutils.hpp"

#include <QFile>
#include <QDir>


DsDataLayer::DsDataLayer(QObject * parent) :
    QObject(parent)
{
    connect(&m_timer,
            SIGNAL(timeout()),
            this,
            SLOT(timeout()));

    m_timer.start(1000 * 60);


    QDir scriptDirectory(DarkSettings->value("scripts-file-path").toString());

    if (scriptDirectory.exists())
    {
        for (const QFileInfo & file : scriptDirectory.entryInfoList(QStringList() << "*.js", QDir::Files | QDir::Readable | QDir::NoDotAndDotDot))
        {
            qxtLog->debug() << tr("Script detected info for: ") + file.fileName();

            Darkmires::ScriptInfo si;

            si.script = file;

            m_scriptInfoStorage.insert(file.baseName(), si);
        }

        for (const QFileInfo & file : scriptDirectory.entryInfoList(QStringList() << "*.qm", QDir::Files | QDir::Readable | QDir::NoDotAndDotDot))
        {
            QString fileName = file.baseName().split('_', QString::SkipEmptyParts).at(0);

            if (m_scriptInfoStorage.contains(fileName))
            {

                QLocale loc(file.baseName().remove(0, fileName.length() + 1));

                qxtLog->debug() << tr("Adding translation info for: ") + loc.name();

                m_scriptInfoStorage[fileName].translations[loc.name()] = file;
            }
        }
    }


    QDir directory(DarkSettings->value("sessions-file-path").toString());

    if (directory.exists())
    {
        for (const QString & sessionFile : directory.entryList(QStringList() << "*.ses", QDir::Files |
                                                               QDir::Readable | QDir::NoDotAndDotDot))
        {
            QFile file(QDir::cleanPath(directory.absolutePath() + QDir::separator() + sessionFile));

            qxtLog->debug() << tr("Loading session from file: ") + file.fileName();

            if (file.open(QFile::ReadOnly))
            {
                QDataStream serializer(&file);

                QString id = QString("{%1}").arg(sessionFile);

                DsSessionInterface * session = addSession(DsSessionInterface::create(serializer, id));

                if (! session)
                {
                    qxtLog->error() << "Unable to create session!";
                }
                else if (! session->start())
                {
                    qxtLog->error() << "Unable to start session file!";

                    removeSession(session->getUuid().toString());
                }

                file.close();
            }
            else
            {
                qxtLog->debug() << "Unable to open session file!";
            }
        }
    }
}


void DsDataLayer::dataError(const Darkmires::PacketPointer packet, const QString & message)
{
    emit replySignal(DsWebEvent::construct(packet,
                                           Darkmires::WebEventDataError,
                                           message));
}


DsDataLayer::~DsDataLayer()
{
    for (const std::shared_ptr<DsSessionInterface> & session : m_sessions)
    {
        qxtLog->debug() << tr("[DATA] Session (%1) reference count before deletion: %2").arg(session->getName()).arg(QString::number(session.use_count()));
    }

    QDir().mkpath(DarkSettings->value("sessions-file-path").toString());

    for (Darkmires::SessionMap::const_iterator it = m_sessions.begin(); it != m_sessions.end(); ++it)
    {
        m_sessions.value(it.key())->disconnect();

        if (it.value()->getExpiration().isNull())
        {

            QString name = it.value()->getUuid().toString();

            DsUtils::removeFirstAndLast(name);

            QFile file(DarkSettings->value("sessions-file-path").toString() + name + ".ses");
            if (file.open(QFile::WriteOnly | QFile::Truncate))
            {
                QDataStream serializer(&file);

                it.value()->save(serializer);

                file.close();
            }
            else
            {
                qxtLog->debug() << tr("[SESSION] Could not save session: ") + file.errorString();
            }
        }
    }
}


DsSessionInterface * DsDataLayer::addSession(DsSessionInterface * session)
{
    if (! session)
    {
        return nullptr;
    }

    if (m_sessions.count(session->getUuid().toString()))
    {
        qxtLog->error() << tr("[SESSION] Already exists!");
        return nullptr;
    }

    connect(session,
            SIGNAL(replySignal(DsWebEvent *)),
            this,
            SLOT(replySlot(DsWebEvent *)));

    connect(session,
            SIGNAL(sessionEvent(Darkmires::Event)),
            this,
            SLOT(eventSlot(Darkmires::Event)));

    m_sessions[session->getUuid().toString()] = std::unique_ptr<DsSessionInterface>(session);

    emit dataEvent(DarkmiresEvent::EVENT_SESSION_ADDED);

    return session;
}


Darkmires::SessionMap::const_iterator DsDataLayer::removeSession(const QString & id, bool removeFile)
{
    Darkmires::SessionMap::iterator it = m_sessions.end();

    if (m_sessions.contains(id))
    {

        it = m_sessions.find(id);

        if (removeFile)
        {
            QString name = it.value()->getUuid().toString();
            name.remove(0, 1);
            name.chop(1);

            QString path = DarkSettings->value("sessions-file-path").toString() + name;

            /// Remove subdirectories
            if (! DsUtils::removeDir(path))
            {
                qxtLog->warning() << tr("Error deleting session directory: %1 (%2)").arg(id).arg(path);
            }

            /// Remove session file
            QDir().remove(DarkSettings->value("sessions-file-path").toString() + name + ".ses");

            /// Remove database
            QDir().remove(DarkSettings->value("sessions-file-path").toString() + name + ".db");
        }

        it = m_sessions.erase(it);

        emit dataEvent(DarkmiresEvent::EVENT_SESSION_REMOVED);
    }

    return it;
}


void DsDataLayer::requestSlot(Darkmires::PacketPointer packet)
{
    qxtLog->debug() << tr("[DATA] Packet incoming!");

    switch (packet->type)
    {
        case Darkmires::RequestNormal:
        {
            LockUserStorage
            DsUserStorage::getInstance()->get(packet->event->sessionID)->processMessages(packet);
            UnlockUserStorage

            m_sessions.value(packet->session)->request(packet);
            break;
        }

        case Darkmires::RequestListSessions:
        {
            QVariantList sessions;

            for (Darkmires::SessionMap::const_iterator it = m_sessions.begin(); it != m_sessions.end(); ++it)
            {

                QVariantMap session;

                session["id"] = it.key();
                session["authentication"] = DsAuthenticationLayer::authenticationTypeToString(it.value()->getAuthentication());
                session["mode"] = it.value()->getMode();
                session["expiration"] = it.value()->getExpiration().toString(Qt::ISODate);
                session["name"] = it.value()->getName();
                session["owner"] = it.value()->isOwner(packet->uuid);

                sessions.append(session);
            }

            emit replySignal(DsWebEvent::construct(packet,
                                                   Darkmires::WebEventDataSuccess,
                                                   sessions));

            break;
        }

        default:
            dataError(packet, "Request processing error");
            break;
    }
}


void DsDataLayer::removeUser(int id, const QString & session)
{
    if (session.isEmpty())
    {
        foreach(auto & session, m_sessions)
        {
            session->removeUser(id);
        }
    }
    else
    {
        m_sessions.value(session)->removeUser(id);
    }
}


bool DsDataLayer::addUser(const Darkmires::PacketPointer & packet)
{
    return m_sessions.value(packet->session)->addUser(packet);
}


void DsDataLayer::chatMessage(const QString & session, Darkmires::ChatMessage && message)
{
    m_sessions.value(session)->chatMessage(std::move(message));
}


//void DarkmiresDataLayer::userMessage(int user, Darkmires::UserMessage && message)
//{
//    m_sessions.value(session)->sessionMessage(user, std::move(message));
//}


void DsDataLayer::timeout()
{
    QDateTime now = QDateTime::currentDateTimeUtc();


    for (Darkmires::SessionMap::const_iterator it = m_sessions.begin(); it != m_sessions.end();)
    {
        if (! it.value()->getExpiration().isNull() &&
            it.value()->getExpiration() <= now)
        {
            it = removeSession(it.key());
        }
        else
        {
            ++it;
        }
    }
}


void DsDataLayer::eventSlot(Darkmires::Event event)
{
    emit dataEvent(std::move(event));
}


void DsDataLayer::replySlot(DsWebEvent * event)
{
    emit replySignal(event);
}

