#include "dsdatamodel.hpp"

#include <QxtLogger>

#include "darkmires_constants.hpp"

DsDataModel DsDataModel::create(const QVariant & variant)
{
    qxtLog->info() << "Creating data model...";

    DsDataModel root;

    root.make(variant);

    return std::move(root);
}


DsDataModel::~DsDataModel()
{
}


DsDataModel & DsDataModel::operator= (DsDataModel && other)
{
    m_requestType = other.m_requestType;
    m_session = std::move(other.m_session);

    return *this;
}


DsDataModel::DsDataModel(DsDataModel && other)
{
    m_requestType = other.m_requestType;
    m_session = std::move(other.m_session);
}


DsDataModel::DsDataModel()
{
}


bool DsDataModel::make(const QVariant & variant)
{
    if (! variant.canConvert(QVariant::Map)) {
        m_error = "Invalid JSON format";
        return false;
    }

    QVariantMap root = variant.toMap().value(DsConstants::JSON_ROOT_ELEMENT).toMap();

    if (! root.contains(DsConstants::JSON_TYPE_ELEMENT)) {
        m_error = "Malformed JSON";
        return false;
    }

    m_session = root.value(DsConstants::JSON_SESSION_ELEMENT).toString();

    m_requestType = Darkmires::recognizeRequest(root.value(DsConstants::JSON_TYPE_ELEMENT).toString());

    m_data = root.value(DsConstants::JSON_DATA_ELEMENT).toMap();

    return true;
}
