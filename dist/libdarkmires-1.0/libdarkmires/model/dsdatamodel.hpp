#ifndef DSMODEL_HPP
#define DSMODEL_HPP

#include "dsdatamodelelement.hpp"

#include "darkmires.hpp"

class DsDataModel : public DsModelElement
{
    public:
        static DsDataModel create(const QVariant & variant);

        virtual ~DsDataModel();

        DsDataModel & operator= (DsDataModel && other);
        DsDataModel(DsDataModel && other);

        inline const QString & session() { return m_session; }
        inline Darkmires::RequestType type() { return m_requestType; }
        inline const QVariantMap & data() { return m_data; }

    private:
        explicit DsDataModel();
        DsDataModel(const DsDataModel &) = delete;

        Darkmires::RequestType m_requestType;
        QString m_session;
        QVariantMap m_data;

    protected:
        virtual bool make(const QVariant & variant) final;
};

#endif // DSMODEL_HPP
