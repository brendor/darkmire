#ifndef DSSCRIPTEDREPOSITORY_HPP
#define DSSCRIPTEDREPOSITORY_HPP

#include "dssubversionsession.hpp"

#include <QtScript>


///
/// \brief Trieda DsScriptedSession predstavuje skriptovaný typ relácie.
///
class DsScriptedSession : public DsSession
{
        Q_OBJECT

        /// \brief chat získa v skriptovacom engine referenciu na zoznam správ.
        Q_PROPERTY(bool chat READ getAutoChat WRITE setAutoChat)

    public:
        explicit DsScriptedSession(DsSessionInterface * const interface);
        virtual ~DsScriptedSession();

        virtual void load(QDataStream & in) override;
        virtual void save(QDataStream & in) override;

        virtual bool init() override;

        ///
        /// \brief loadServerScriptFile spracuje skript zo súboru.
        /// \param file súbor obsahujúci skript
        /// \return true pri úspešnom spracovaní, inak false
        ///
        bool loadServerScriptFile(QFile & file);

        ///
        /// \brief packetToScriptValue zmení požiadavku na QScriptValue, ktorá môže byť následne spracovaná v skripte.
        /// \param engine skriptovací engine
        /// \param packet požiadavka
        /// \return skriptovo spracovateľný obal požiadavky
        ///
        static QScriptValue packetToScriptValue(QScriptEngine * engine, const Darkmires::PacketData & packet);

        ///
        /// \brief packetFromScriptValue zmení skriptový objekt na požiadavku.
        /// \param value skritpový objekt
        /// \param packet požiadavka
        ///
        static void packetFromScriptValue(const QScriptValue & value, Darkmires::PacketData & packet);

        bool getAutoChat()
        {
            return m_autoChat;
        }
        void setAutoChat(bool pc)
        {
            m_autoChat = pc;
        }

        ///
        /// \brief printProperties vypíše do logu hodnoty obsiahnuté v objekte hodnoty skriptu.
        /// \param value hodnota skriptu
        /// \param level hĺbka, do ktorej sa má výpis ponoriť
        ///
        static void printProperties(const QScriptValue & value, int level = 0);

        ///
        /// \brief serializeScriptObject zaserializuje hodnotu skriptovacieho enginu do textovej formy.
        /// \param value hodnota zo skriptu
        /// \param engine skritovací engine
        /// \return reťazec
        ///
        static QString serializeScriptObject(const QScriptValue & value, QScriptEngine & engine);

        ///
        /// \brief deserializeScriptObject odserializuje reťazec do hodnoty skriptovacieho enginu.
        /// \param json reťazec
        /// \param engine skriptovací engine
        /// \return hodnota pre skriptovací engine
        ///
        static QScriptValue deserializeScriptObject(const QString & json, QScriptEngine & engine);


        ///
        /// \brief getSessionUserScript získa informácie o používateľovi
        /// \param ctx kontext volania skriptu
        /// \param eng skriptovací engine
        /// \return hodnota poslaná naspäť do skriptu
        ///
        static QScriptValue getSessionUserScript(QScriptContext * ctx, QScriptEngine * eng);

        ///
        /// \brief logMessageScript zaznamená správu obsiahnutú ako argument volania v kontexte volania skriptu
        /// \param ctx kontext volania skriptu
        /// \param eng skriptovací engine
        /// \return hodnota poslaná naspäť do skriptu
        ///
        static QScriptValue logMessageScript(QScriptContext * ctx, QScriptEngine * eng);

        ///
        /// \brief postScriptEvent pošle udalosť zo skriptu do vnútorného radu udalosti
        /// \param ctx kontext volania skriptu
        /// \param eng skriptovací engine
        /// \return hodnota poslaná naspäť do skriptu
        ///
        static QScriptValue postScriptEvent(QScriptContext * ctx, QScriptEngine * eng);

    public slots:
        virtual void processRequest(Darkmires::PacketPointer packet) override;
        virtual void addUser(int user) override;
        virtual void processCommand(QString command) override;

    protected:
        QScriptValue wrap(QObject * pointer);

    private:
        QScriptEngine m_scriptEngine;

        QScriptProgram m_program;

        QString m_fileName;

        QScriptValue m_this;

        QScriptValue m_requestFunction;
        QScriptValue m_userFunction;
        QScriptValue m_eventFunction;

        QTranslator m_translator;

        void reportScriptError(const QScriptValue & value);
        void reportScriptException();

        bool m_autoChat = false;

        bool m_valid = false;

        // DsSession interface
    protected:
        virtual void populate(std::shared_ptr<DsSessionUserData> data);
};


///
/// \brief Trieda DsScriptedSessionUserData predstavuje dáta používateľa v skriptovanej relácii.
///
class DARKMIRESSHARED_EXPORT DsScriptedSessionUserData : public DsSessionUserData
{
        Q_OBJECT

        /// \brief userdata získa vnútorné dáta zo skriptu
        Q_PROPERTY(QVariant userdata READ getData WRITE setData)

    public:
        explicit DsScriptedSessionUserData(int user);
        virtual ~DsScriptedSessionUserData();

        ///
        /// \brief getData získa dáta používateľa.
        /// \return dáta používateľa
        ///
        inline const QVariant & getData()
        {
            return m_data;
        }

        ///
        /// \brief setData nastaví dáta používateľa.
        /// \param data nové dáta používateľa
        ///
        inline void setData(const QVariant & data)
        {
            m_data = data;
        }

        ///
        /// \brief getEvents získa rad udalostí, ktoré sa nakumulovali volaní
        /// funkcie post v skripte.
        /// \return rad udalostí
        ///
        inline QQueue<QScriptValue> & getEvents()
        {
            return m_events;
        }

    private:
        QVariant m_data;

        QQueue<QScriptValue> m_events;
};


Q_DECLARE_METATYPE(Darkmires::PacketData)


#endif // DSSCRIPTEDREPOSITORY_HPP
