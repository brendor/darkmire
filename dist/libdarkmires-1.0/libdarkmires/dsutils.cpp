/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/
#include "dsutils.hpp"

#include <QtEndian>
#include <QDataStream>
#include <QTextStream>
#include <QDir>


/**
 * Source: http://introcs.cs.princeton.edu/java/51data/CRC16CCITT.java.html
 */
int DsUtils::Crc16CCITT(const char * data, int len)
{
    int crc = 0xffff;
    const int polynomial = 0x1021; // 0001 0000 0010 0001  (0, 5, 12)

    for (int j = 0; j < len; j++)
    {
        for (int i = 0; i < 8; i++)
        {

            bool bit = ((data[j] >> (7 - i) & 1) == 1);
            bool c15 = ((crc >> 15 & 1) == 1);

            crc <<= 1;

            if (c15 ^ bit)
            {
                crc ^= polynomial;
            }
        }
    }

    crc &= 0xffff;

    return crc;
}


QByteArray DsUtils::compress(const QByteArray & bytes, int level)
{
    return qCompress(bytes, level);
}


QByteArray DsUtils::decompress(QByteArray & bytes, quint32 originalSize)
{
    QByteArray operate(4, '\0');

    QDataStream stream(&operate, QIODevice::WriteOnly);
    stream.setByteOrder(QDataStream::BigEndian);

    stream << originalSize;

    bytes.prepend(operate);

    return qUncompress(bytes);
}


void DsUtils::removeFirstAndLast(QString & string)
{
    if (string.length() <= 2) return;

    string.remove(0, 1);
    string.chop(1);
}

/*!
   Delete a directory along with all of its contents.

   \param dirName Path of directory to remove.
   \return true on success; false on error.
*/
bool DsUtils::removeDir(const QString &dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName))
    {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst))
        {
            if (info.isDir())
            {
                result = DsUtils::removeDir(info.absoluteFilePath());
            }
            else
            {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result)
            {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }

    return result;
}


QString DsUtils::escapeJson(const QString & string)
{
    if (string.length() == 0)
    {
        return "\"\"";
    }

    QChar c = 0;
    int i;
    int len = string.length();
    QString sb;
    QString t;

    for (i = 0; i < len; i += 1)
    {
        c = string.at(i);

        switch (c.unicode())
        {
            case '\\':
            case '"':
                sb.append('\\');
                sb.append(c);
                break;
            case '/':
//                if (b == '<') {
                sb.append('\\');
//                }
                sb.append(c);
                break;
            case '\b':
                sb.append("\\b");
                break;
            case '\t':
                sb.append("\\t");
                break;
            case '\n':
                sb.append("\\n");
                break;
            case '\f':
                sb.append("\\f");
                break;
            case '\r':
                sb.append("\\r");
                break;
            default:
                if (c < ' ')
                {
                    QString result;
                    QTextStream ts(&result);
                    ts.setIntegerBase(16);
                    ts << c;

                    t = "000" + result;
                    sb.append("\\u" + t.remove(0, t.length() - 4));
                }
                else
                {
                    sb.append(c);
                }
                break;
        }
    }

    return sb;
}

