#ifndef DARKMIRESAUTHENTICATIONLAYER_HPP
#define DARKMIRESAUTHENTICATIONLAYER_HPP

#include <QObject>

#include <QSqlDatabase>
#include <QSqlQuery>

#include <QMutex>

#include <QxtWebRequestEvent>

#include <memory>

#include "darkmires.hpp"

class DsDataLayer;
class DsUser;
class DsLDAP;

///
/// \brief DsAuthenticationLayer je trieda predstavujúca autentifikačnú vrstvu servera
///
class Q_DECL_HIDDEN DsAuthenticationLayer : public QObject
{
        Q_OBJECT

    public:
        ///
        /// \brief DsAuthenticationLayer je konštruktor tejto triedy.
        /// \param data referencia na predtým vytvorenú dátovú vrstvu
        /// \param parent rodičovský objekt
        ///
        explicit DsAuthenticationLayer(const std::weak_ptr<DsDataLayer> & data, QObject * parent = 0);

        ///
        /// \brief ~DsAuthenticationLayer je deštruktor tejto triedy.
        ///
        virtual ~DsAuthenticationLayer();

        /// \brief štruktúra priraďujúca meno typu autentifikácie k jeho kódu.
        typedef struct
        {
            const char * id;
            Darkmires::AuthenticationType type;
        } AuthenticationMap;
        static AuthenticationMap AuthenticationAliases[];

        ///
        /// \brief recognizeAuthenticationType rozpozná typ autentifikácie na základe jej mena.
        /// \param str meno typu autentifikácie
        /// \return kód typu autentifikácie
        ///
        static Darkmires::AuthenticationType recognizeAuthenticationType(const QString & str);


        ///
        /// \brief authenticationTypeToString premení kód typu autentifikácie na reťazec
        /// \param type kód typu autentifikácie
        /// \return reťazec reprezentujúci daný typ relácie
        ///
        static QString authenticationTypeToString(Darkmires::AuthenticationType type);

        ///
        /// \brief setAllowLDAP nastaví, či je povolená autentifikácia prostredníctvom
        /// protokolu LDAP.
        /// \param allow boolovska hodnota
        ///
        inline void setAllowLDAP(bool allow)
        {
            m_ldap_allowed = allow;
        }

        /// \brief Štruktúra definujúca výsledok hľadania v databázi používateľov.
        typedef struct SearchResult
        {
            bool success = false;
            QString error;
        } SearchResult;

        ///
        /// \brief escapeName odstráni z mena nepovolené znaky.
        /// \param name meno
        /// \return meno bez nepovolených znakov
        ///
        static QString escapeName(const QString & name);

        ///
        /// \brief searchUserByUuid vyhľadá používateľa podľa jeho jedinečného identifikátora
        /// v databáze registrovaných používateľov.
        /// \param uuid jedinečný identifikátor
        /// \return meno používateľa
        ///
        QString searchUserByUuid(const QUuid & uuid);

    signals:
        ///
        /// \brief replySignal signál vyvolaný pri odpovedi z nasledujúcich vrstiev.
        /// \param event odpoveď
        ///
        void replySignal(DsWebEvent * event);

        ///
        /// \brief requestSignal signál vyvolaný pri príchode požiadavky.
        /// \param packet paket požiadavky
        ///
        void requestSignal(Darkmires::PacketPointer packet);

        ///
        /// \brief authenticationEvent signál vyvolaný pri vnútornej udalosti tejto vrstvy.
        /// \param event udalosť
        ///
        void authenticationEvent(Darkmires::Event event);

    public slots:
        ///
        /// \brief requestSlot slot, na ktorý je potrebné napojiť prichadzajúce požiadavky od webového servera.
        /// \param packet paket reprezentujúci požiadavku
        ///
        void requestSlot(Darkmires::PacketPointer packet);

        ///
        /// \brief replySlot slot, na ktorý je potrebné napojiť nasledujúce vrstvy, aby odpovede prešli touto vrstvou ďalej.
        /// \param event odpoveď
        ///
        void replySlot(DsWebEvent * event);

        ///
        /// \brief kickUser odpojí daného pripojeného používateľa
        /// \param id identifikátor používateľa
        ///
        void kickUser(int id);

    private slots:
        void removeUser(int id);

        void searchFinished(const Darkmires::PacketPointer packet, DsAuthenticationLayer::SearchResult result);

    private:
        DsAuthenticationLayer(const DsAuthenticationLayer &) = delete;

        QSqlDatabase m_database;
        QSqlQuery m_userQuerySelect;
//        QSqlQuery m_userQueryUpdate;

        void authenticationError(const Darkmires::PacketPointer packet,
                                 const QString & message,
                                 Darkmires::WebEventType type = Darkmires::WebEventAuthenticationError);

        void authenticationSuccess(const Darkmires::PacketPointer & packet);

        std::weak_ptr<DsDataLayer> m_data;

        std::unique_ptr<DsLDAP> m_ldap;

        void authenticateUser(const Darkmires::PacketPointer & packet, Darkmires::UserPrivileges privileges);
        bool authenticateSession(const Darkmires::PacketPointer & packet);

        bool searchSQL(const Darkmires::PacketPointer & packet, Darkmires::UserPrivileges privs, QString & salt);

        bool m_ldap_allowed = false;
};

#endif // DARKMIRESAUTHENTICATIONLAYER_HPP
