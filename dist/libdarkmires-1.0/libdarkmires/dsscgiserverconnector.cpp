/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsscgiserverconnector.hpp"

#include <QHttpRequestHeader>

DsScgiServerConnector::DsScgiServerConnector(QObject * parent) :
    QxtScgiServerConnector(parent)
{
}

DsScgiServerConnector::~DsScgiServerConnector()
{
}

QHttpRequestHeader DsScgiServerConnector::parseRequest(QByteArray & buffer)
{
    char precious = *(buffer.end() - 1);

    QHttpRequestHeader header = QxtScgiServerConnector::parseRequest(buffer);

    buffer.append(precious);

    /**
     * There is a bug in the original implementation.
     *
     * (http://dev.libqxt.org/libqxt/src/1128111df9f25f15ba7b58eeead1fedf888f8ae1/src/web/qxtscgiserverconnector.cpp)
     * Line 197:
     *  buffer.chop(1);
     *
     * Chopping from the end works unless there is
     * userdata sent to be parsed. In that case,
     * the last byte of userdata is removed instead
     * of the ',' - behavior the authors expected.
     */

    /**
     * Note: Thank gods for implicitly shared byte arrays.
     */
    buffer = buffer.remove(0, 1);

    return header;
}
