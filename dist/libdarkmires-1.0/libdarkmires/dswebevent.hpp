#ifndef DARKMIRESWEBEVENT_HPP
#define DARKMIRESWEBEVENT_HPP


#include <QxtWebPageEvent>

#include "darkmires.hpp"

///
/// \brief Trieda DsWebEvent napriek svojmu názvu, ktorý evokuje udalosť, predstavuje
/// skôr odpoveď, ktorá vznikne v serveri ako reakcia na požiadavku.
///
class Q_DECL_HIDDEN DsWebEvent : public QxtWebPageEvent
{
    public:
        ///
        /// \brief construct skonštruuje odpoveď na požiadavku.
        /// \param packet požiadavka
        /// \param type typ odpovede podľa enumerácie WebEventType
        /// \param data dáta v ľubovoľnej forme, ktoré budú zabalené do požiadavky
        /// \param privileges privilégiá, ktoré boli požadované na vybavenie tejto požiadavky
        /// \param supressCompression značí, že sa má potlačiť kompresný krok
        /// \return pointer na objekt DsWebEvent, ktorý musí volajúci uvoľniť
        ///
        static DsWebEvent * construct(const Darkmires::PacketPointer packet,
                                      Darkmires::WebEventType type,
                                      const QVariant & data, Darkmires::UserPrivileges privileges = Darkmires::UserNone,
                                      bool supressCompression = false);

    private:
        explicit DsWebEvent(int sessionID, int requestID, int status, QByteArray statusMessage);
};

#endif // DARKMIRESWEBEVENT_HPP
