#ifndef DARKMIRESLDAPJOB_HPP
#define DARKMIRESLDAPJOB_HPP

#include <QThread>

#include "dsauthenticationlayer.hpp"

///
/// \brief DsLDAPThread je interná trieda reprezentujúca vlákno hľadania v databáze LDAP.
///
class DsLDAPThread : public QThread
{
        Q_OBJECT

    public:
        ///
        /// \brief DsLDAPThread je konštruktor tejto triedy.
        /// \param parent rodičovský objekt
        ///
        explicit DsLDAPThread(QObject * parent);

        ///
        /// \brief run je zdedená metóda triedy QThread, ktorá je
        /// zavolaná, keď je vlákno vytvorené a pripravené na beh
        ///
        virtual void run();

        int m_id;
        QString m_name;

    signals:
        ///
        /// \brief searchFinished je signál vyvolaný pri ukončení hľadania v databáze
        /// \param id identifikátor procesu hľadania
        /// \param result výsledok hľadania
        ///
        void searchFinished(int id, DsAuthenticationLayer::SearchResult result);
};

#endif // DARKMIRESLDAPJOB_HPP
