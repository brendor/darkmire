Format: 3.0 (quilt)
Source: libdarkmires
Binary: libdarkmires1
Architecture: any
Version: 1.0-1
Maintainer: Tomáš Kováč <brendorrmt@gmail.com>
Standards-Version: 3.9.4
Build-Depends: gcc (>= 4.7.1~), debhelper (>= 9~)
Package-List: 
 libdarkmires1 deb web optional
Checksums-Sha1: 
 f880cbe54e86c75833d032d6a65195e78488feb3 126004 libdarkmires_1.0.orig.tar.xz
 0dd8b26e87e1eecbc1ac54594f224e3d1cc3bbdf 954 libdarkmires_1.0-1.debian.tar.gz
Checksums-Sha256: 
 e2e815b90dd0f56918195265c4bafbdb1a0023881edf73850f22ad7a87ab7400 126004 libdarkmires_1.0.orig.tar.xz
 694f78b16be76423677817bdbb9c96d8c2b4ae5c862b9011bbc0ec33bcc89ca6 954 libdarkmires_1.0-1.debian.tar.gz
Files: 
 89070993f20eb3e1928f297751ed414d 126004 libdarkmires_1.0.orig.tar.xz
 db2f77d7049121e4b4be77550ac3ab43 954 libdarkmires_1.0-1.debian.tar.gz
