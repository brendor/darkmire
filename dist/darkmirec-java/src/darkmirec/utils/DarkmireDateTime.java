package darkmirec.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Predstavuje triedu určenú na prácu a formátovanie reťazcov s dátumom a časom.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmireDateTime
{

    static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static Date getUTC()
    {
        return new Date();
    }


    /**
     * Sformátuje dátum reprezentovaný inštanciou triedy Date na reťazec.
     * @param date inštancia triedy date
     * @return reťazec s dátumom
     */
    public static String getUTCString(Date date)
    {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(date);
    }


    /**
     * Skonvertuje reťazec s dátumom na inštanciu triedy Date.
     * @param date reťazec s dátumom
     * @return inštancia triedy Date
     */
    public static Date toUTC(String date)
    {
        Date dateReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try {
            dateReturn = (Date) dateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }

        return dateReturn;
    }
}
