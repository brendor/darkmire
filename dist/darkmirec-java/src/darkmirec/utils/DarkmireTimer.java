
package darkmirec.utils;

/**
 * Táto trieda reprezentuje časovač, ktorý je možné zapnúť, vypnúť a získať
 * z neho ubehnutý čas.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireTimer
{
    private long m_ticks = 0;
    private boolean m_running = false;

    public DarkmireTimer()
    {
    }
    
    /**
     * Začne meranie času.
     */
    public void start()
    {
        m_ticks = System.currentTimeMillis();
        m_running = true;
    }
    
    /**
     * Ukončí meranie času.
     */
    public void stop()
    {
        m_ticks = System.currentTimeMillis() - m_ticks;
        m_running = false;
    }
    
    /**
     * Získa počet ubehnutých milisekúnd.
     * @return počet ubehnutých milisekúnd
     */
    public long getMillis()
    {
        if (m_running)
        {
            return System.currentTimeMillis() - m_ticks;
        }
        else
        {
            return m_ticks;
        }
    }
}
