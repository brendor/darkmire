package darkmirec.utils;

import darkmirec.events.DarkmireEvent;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public interface DarkmireEventHandler {
    public abstract void event(DarkmireEvent event);
}
