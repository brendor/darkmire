package darkmirec.sessions;

import net.sf.json.JSONObject;

/**
 * Predstavuje továreň pre jednotlivé relácie.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireSessionFactory {
    
    /**
     * Definuje typ relácie.
     */
    public enum SessionType
    {
        SESSION_NONE (0),
        SESSION_QUICKSHARE (1),
        SESSION_SCRIPTED (2),
        SESSION_SUBVERSION (3);

        private final int code;

        private SessionType(int code)
        {
            this.code = code;
        }
        
        public Integer code()
        {
            return this.code;
        }
        
        public static SessionType fromInt(int code)
        {
            for (SessionType c : SessionType.values())
            {
                if (c.code == code) return c;
            }
            
            return null;
        }
    }
    
    
    /**
     * Na základe objektu v JSON notácii, ktorý napríklad prišiel od servera vytvorí reláciu.
     * @param session objekt v JSON notácii
     * @return relácia
     */
    public static DarkmireSession make(JSONObject session)
    {    
        DarkmireSession s = null;
        
        // {"data":[{"authentication":1,"expiration":null,"id":"{98c38f70-c9b8-44a5-bcad-71c758407aaf}","mode":1,"name":"cscsd"},{"authentication":1,"expiration":null,"id":"{00000000-0000-0000-0000-000000000002}","mode":1,"name":"Dada"}],"status":6}
        
        SessionType type = SessionType.fromInt(session.getInt("mode"));
        
        switch (type)
        {
            case SESSION_QUICKSHARE:
                s = new DarkmireQuickShareSession(
                        session.getString("id"),
                        session.getString("name"),
                        DarkmireSession.AuthenticationType.fromString(session.getString("authentication")),
                        session.getBoolean("owner"));
                
                break;
                
            case SESSION_SUBVERSION:
                s = new DarkmireSubversionSession(
                        session.getString("id"),
                        session.getString("name"),
                        DarkmireSession.AuthenticationType.fromString(session.getString("authentication")),
                        session.getBoolean("owner"));
                break;
                
            case SESSION_SCRIPTED:
                s = new DarkmireScriptedSession(
                        session.getString("id"),
                        session.getString("name"),
                        DarkmireSession.AuthenticationType.fromString(session.getString("authentication")),
                        session.getBoolean("owner"));
                
                DarkmireScriptedSession scripted = (DarkmireScriptedSession)s;
                
                scripted.setCustomName(session.optString("custom-name"));
                
                break;
        }
        
        return s;
    }
}
