package darkmirec.sessions;

/**
 * Táto trieda predstavuje reláciu QuickShare (relácia rýchleho zdieľania).
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireQuickShareSession extends DarkmireSession
{

    /**
     * Špecifikuje typ požiadavky špecifický pre tento typ relácie.
     */
    public enum RequestType
    {
        REQUEST_NORMAL (0),
        REQUEST_ENTRY (1),
        REQUEST_ENTRY_LIST (2);

        private final int code;

        private RequestType(int code)
        {
            this.code = code;
        }
        
        public Integer code()
        {
            return this.code;
        }
    }


    public DarkmireQuickShareSession(String id, String name, AuthenticationType auth, boolean owner)
    {
        super(id, name, auth, owner);
    }
}
