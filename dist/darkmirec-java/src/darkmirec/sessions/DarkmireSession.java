package darkmirec.sessions;

/**
 * Táto trieda predstavuje všeobecnú reláciu.
 * Je to abstraktná trieda, od ktorej jednotlivé relácie budú dediť.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public abstract class DarkmireSession {

    /**
     * Definuje spôsob autentifikácie relácie.
     */
    public enum AuthenticationType {
        
        AUTHENTICATION_ERROR ("error"),
        AUTHENTICATION_NONE ("none"),
        AUTHENTICATION_LDAP ("ldap"),
        AUTHENTICATION_MODERATE ("admin"),
        AUTHENTICATION_LOCAL_DATABASE ("local");
        
        private final String alias;

        private AuthenticationType(String alias)
        {
            this.alias = alias;
        }
        
        public String alias()
        {
            return this.alias;
        }
        
        private static final DarkmireSession.AuthenticationType[] m_values = DarkmireSession.AuthenticationType.values();
        
        public static DarkmireSession.AuthenticationType fromString(String alias)
        {
            for (AuthenticationType type : m_values) {
                if (type.alias.equals(alias)) return type;
            }
            
            return AUTHENTICATION_ERROR;
        }
    }
    
    private final String m_id;
    
    private final String m_name;
    
    private final AuthenticationType m_auth;
    
    private final boolean m_owner;
    
    public DarkmireSession(String id, String name, AuthenticationType auth, boolean owner)
    {
        m_id = id;
        m_name = name;
        m_auth = auth;
        m_owner = owner;
    }
    
    public String getId()
    {
        return m_id;
    }
    
    public String getName()
    {
        return m_name;
    }
    
    
    public AuthenticationType getAuthentication()
    {
        return m_auth;
    }
    
    /**
     * Zistí, či je používateľ, ktorý je do relácie prihlásený vlastníkom tejto relácie.
     * @return true ak je vlastníkom, inak false
     */
    public boolean isOwner()
    {
        return m_owner;
    }
}
