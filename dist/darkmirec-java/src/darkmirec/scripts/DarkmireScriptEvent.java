/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package darkmirec.scripts;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONObject;
import org.mozilla.javascript.NativeJSON;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.annotations.JSConstructor;
import org.mozilla.javascript.annotations.JSGetter;
import org.mozilla.javascript.annotations.JSSetter;
import org.mozilla.javascript.json.JsonParser;

/**
 * Táto trieda predstavuje udalosť v skripte. Tieto udalosti môžu putovať od
 * serveru ku klientovi alebo opačne, pričom ich odosielanie je synchronizované
 * s odosielaním bežných požiadaviek DarkmireDataEvent. Ich počet pritom nemusí
 * odpovedať počtu odoslaných požiadaviek a časový posun sa môže meniť.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireScriptEvent extends ScriptableObject
{
    private String m_json = "{}";
    private JSONObject m_object = null;
    
    
    private float m_priority = 0.0f;
    
    // The zero-argument constructor used by Rhino runtime to create instances
    public DarkmireScriptEvent()
    {
        
    }

    // Method jsConstructor defines the JavaScript constructor
    @JSConstructor
    public void jsConstructor()
    {
        
    }
    
    /**
     * Získa meno triedy v kontexte skriptu.
     * @return meno triedy v kontexte skriptu
     */
    @Override
    public String getClassName()
    {
        return "Event";
    }   
    
    /**
     * Získa dáta v kontexte skriptu.
     * @return dáta v kontexte skriptu
     */
    @JSGetter
    public Object getData()
    {
        Context cx = Context.enter();
        
        JsonParser p = new JsonParser(cx, this);
        
        Object o = null;
        
        try
        {
            o = p.parseValue(m_json);
        }
        catch (JsonParser.ParseException ex)
        {
            Logger.getLogger(DarkmireScriptEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            Context.exit();
        }
        
        return o;
    }
    
    /**
     * Nastaví dáta v kontexte skriptu.
     * @param o dáta v kontexte skriptu
     */
    @JSSetter
    public void setData(Object o)
    {
        Context cx = Context.enter();
        
        m_json = Context.toString(NativeJSON.stringify(cx, this, o, null, null));
        m_object = JSONObject.fromObject(m_json);
        
        Context.exit();
    }
    
    /**
     * Nastaví surový JSON reťazec mimo kontext skriptu.
     * @param json reťazec v notácii JSON
     */
    public void setJson(String json)
    {
        m_json = json;
        m_object = JSONObject.fromObject(m_json);
    }
    
    /**
     * Nastaví prioritu udalosti v kontexte skriptu.
     * @param p priorita udalosti
     */
    @JSSetter
    public void setPriority(float p)
    {
        m_priority = p;
    }
    
    /**
     * Získa prioritu udalosti.
     * @return priorita udalosti
     */
    public float getPriority()
    {
        return m_priority;
    }
    
    /**
     * Získa surový JSON reťazec.
     * @return reťazec v notácii JSON
     */
    public String getJson()
    {
        return m_json;
    }


    @Override
    public String toString()
    {
        return m_json;
    }

    /**
     * Získa východziu hodnotu.
     * @param typeHint
     * @return východzia hodnota
     */
    @Override
    public Object getDefaultValue(Class<?> typeHint)
    {
        return String.format("DarkmireScriptEvent (%s)", toString());
    }


    /**
     * Získa atribút udalosti podľa mena.
     * @param name meno atribútu
     * @param start
     * @return objekt pre skript v notácii JSON
     */
    @Override
    public Object get(String name, Scriptable start)
    {
        if ("data".equals(name))
        {
            return getData();
        }
        
        return m_object.opt(name);
    }

    /**
     * Získa atribút udalosti podľa indexu.
     * @param index index
     * @param start
     * @return objekt pre skript v notácii JSON
     */
    @Override
    public Object get(int index, Scriptable start)
    {
        return m_object.get(index);
    }
}
