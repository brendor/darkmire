package darkmirec.events;

import darkmirec.sessions.DarkmireSession;
import darkmirec.sessions.DarkmireSessionFactory;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Tento typ udalosti získa zoznam aktívnych relácií.
 * Je možné ho odoslať bez akejkoľvek autorizácie.
 * 
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmireSessionListEvent extends DarkmireEvent
{

    Map<String, DarkmireSession> m_sessions = null;


    @Override
    public JSONObject request()
    {
        JSONObject result = new JSONObject();

        result.put("type", RequestType.REQUEST_LIST_SESSIONS.alias());

        return result;
    }


    @Override
    public void response(JSONObject object)
    {
        m_sessions = new HashMap<>();

        JSONArray data = object.getJSONArray("data");

        Iterator<JSONObject> iterator = data.iterator();
        while (iterator.hasNext()) {
            JSONObject session = iterator.next();

            System.out.println(session.toString());

            DarkmireSession s = DarkmireSessionFactory.make(session);

            if (s != null) {
                m_sessions.put(s.getId(), s);
            }
        }
    }

    /**
     * Zoznam aktívnych relácií servera, ktoré server vrátil.
     * @return aktívne relácie servera
     */
    public HashMap<String, DarkmireSession> getSessions()
    {
        return (HashMap<String, DarkmireSession>) m_sessions;
    }
}
