
package darkmirec.events;

import darkmirec.core.DarkmireException;
import darkmirec.sessions.DarkmireSession;
import net.sf.json.JSONObject;

/**
 * Táto trieda predstavuje dátovú udalosť. Tieto sú posielané pravidelne 
 * a nesú dáta špecifické pre reláciu.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmireDataEvent extends DarkmireSessionEvent
{
    private JSONObject m_data = null;
    
    /**
     * Skonštruuje dátovú udalosť v kontexte danej relácie.
     * @param session relácie
     * @param data dáta udalosti
     */
    public DarkmireDataEvent(DarkmireSession session, JSONObject data)
    {
        super(session);
        
        m_data = data;
    }
    
    
    @Override
    public JSONObject request() throws DarkmireException
    {
        JSONObject result = new JSONObject();

        result.put("type", RequestType.REQUEST_NORMAL.alias());
        
        if (m_session != null) {
            result.put("session", m_session.getId());
        } else {
            throw new DarkmireException("No session bound to the data event");
        }

        result.put("data", m_data);

        return result;
    }


    @Override
    public void response(JSONObject object)
    {
        super.response(object);
        
        if (! m_status.isError())
        {
            m_data = object.optJSONObject("data");
        }
    }
    
    /**
     * Získa dáta vrátené serverom.
     * @return dáta
     */
    public JSONObject getData()
    {
        return m_data;
    }

}
