package darkmirec.events;

import darkmirec.core.DarkmireException;
import net.sf.json.JSONObject;

/**
 * Chybová udalosť, ktorú server posiela pri rôznych chybách.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmireErrorEvent extends DarkmireEvent {

    private DarkmireEvent m_origin = null;

    
    public DarkmireErrorEvent()
    {
    }

    
    /**
     * Vytvorí chybovú udalosť s hláškou, ktorú server uviedol ako príčinu chyby.
     * @param message chybová hláška
     */
    public DarkmireErrorEvent(String message)
    {
        m_message = message;
    }
    
    
    /**
     * Vytvorí chybovú udalosť s hláškou, ktorú server uviedol ako príčinu chyby a 
     * v na základe udalosti, v kontexte ktorej chyba vznikla.
     * @param message chybová hláška
     * @param event udalosť
     */
    public DarkmireErrorEvent(String message, DarkmireEvent event)
    {
        m_message = message;
        m_origin = event;
    }

    
    @Override
    public JSONObject request() throws DarkmireException
    {
        throw new DarkmireException("Errors should not be requested");
    }


    @Override
    public void response(JSONObject object)
    {
        
    }
    
    /**
     * Získa pôvod správy, ktorý vráti server.
     * @return pôvod správy
     */
    public DarkmireEvent getOrigin()
    {
        return m_origin;
    }
}
