package darkmirec.events;

import darkmirec.core.DarkmireException;
import darkmirec.sessions.DarkmireSession;
import net.sf.json.JSONObject;

/**
 * Udalosť odpojenia zo servera. Je vytvorená buď pri nevyžiadanom odpojení,
 * napríklad pri zastavení servera, alebo ako požiadavka na správne odpojenie
 * z relácie alebo zrušenie pripojenia.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireDisconnectEvent extends DarkmireSessionEvent
{
    private String m_reason = null;
    
    /**
     * Vytvorí udalosť odpojenia zo špecifikovaného dôvodu.
     * @param reason dôvod odpojenia
     * @param session relácia, v kontexte ktorej odpojenie prebieha
     */
    public DarkmireDisconnectEvent(String reason, DarkmireSession session)
    {
        super(session);
        m_reason = reason;
    }
    
    
    /**
     * Vytvorí udalosť odpojenia bez udania špecifikovaného dôvodu.
     * @param session relácia, v kontexte ktorej odpojenie prebieha
     */
    public DarkmireDisconnectEvent(DarkmireSession session)
    {
        super(session);
    }


    @Override
    public JSONObject request() throws DarkmireException
    {
        JSONObject result = new JSONObject();
        
        result.put("type", RequestType.REQUEST_DISCONNECT.alias());
        
        if (m_session != null)
        {
            result.put("session", m_session.getId());
        }
        
        return result;
    }


    @Override
    public void response(JSONObject object)
    {
        super.response(object);
    }


    /**
     * Získa dôvod odpojenia, ak server nejaký špecifikoval.
     * @return reťazec s dôvodom odpojenia
     */
    public String getReason()
    {
        return m_reason;
    }
}
