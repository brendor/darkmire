package darkmirec.events;

import darkmirec.core.DarkmireException;
import darkmirec.sessions.DarkmireSession;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Hex;

/**
 * Predstavuje udalosť autentifikácie, ktorá môže byť použitá na
 * prihlásenie sa na server.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmireAuthenticationEvent extends DarkmireSessionEvent
{
    private int m_id = 0;

    private String m_name = null;

    private char[] m_password = null;

    private String m_username = null;
    
    private DarkmireEvent.UserPrivilege m_userPrivilege = DarkmireEvent.UserPrivilege.USER_NONE;
    
    
    public DarkmireAuthenticationEvent()
    {
        super(null);
    }


    /**
     * Vytvorí udalosť autentifikácie.
     * @param session relácia, do ktorej sa chce používateľ prihlásiť
     * @param id identifikátor
     * @param name meno používateľa
     * @param password heslo používateľa
     * @param username meno lokálneho používateľa
     * @param privilege privilégá, s ktorými sa je možné prihlásiť
     */
    public DarkmireAuthenticationEvent(DarkmireSession session,
            int id, String name, char[] password,
            String username, UserPrivilege privilege)
    {
        super(session);
        
        m_id = id;
        m_name = name;
        m_password = password;
        m_username = username;
        m_userPrivilege = privilege;
    }


    @Override
    public JSONObject request() throws DarkmireException
    {
        JSONObject result = new JSONObject();

        result.put("type", RequestType.REQUEST_AUTHENTICATE.alias());

        JSONObject data = new JSONObject();
        
        if (m_session != null) {
            result.put("session", m_session.getId());
            data.put("type", m_session.getAuthentication().alias());
        } else {
            data.put("type", DarkmireSession.AuthenticationType.AUTHENTICATION_MODERATE.alias());
            data.put("privileges", m_userPrivilege.code);
        }

        if (m_name != null && m_password != null) {
            
            MessageDigest cript;
            try {
                cript = MessageDigest.getInstance("SHA-1");
                cript.reset();
                
                cript.update(new String(m_password).getBytes("utf8"));
                
                data.put("name", m_name);
                data.put("password", new String(Hex.encodeHex(cript.digest())));
                
            }
            catch (NoSuchAlgorithmException | UnsupportedEncodingException ex)
            {
                throw new DarkmireException("Unable to hash password: " + ex.getMessage());
            }

            for (int i = 0; i < m_password.length; ++i) {
                m_password[i] = '\0';
            }
        }

        if (m_username != null)
        {
            data.put("username", m_username);
        }

        result.put("data", data);


        return result;
    }


    @Override
    public void response(JSONObject object)
    {
        super.response(object);
    }

    /**
     * Získa používateľské ID priradené serverom.
     * @return používateľské ID
     */
    public int getId()
    {
        return m_id;
    }
}
