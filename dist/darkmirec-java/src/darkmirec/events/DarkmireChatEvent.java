package darkmirec.events;

import darkmirec.core.DarkmireException;
import darkmirec.sessions.DarkmireSession;
import darkmirec.utils.DarkmireDateTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Trieda DarkmireChatEvent predstavuje udalsť komunikácie v živej diskusii.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmireChatEvent extends DarkmireSessionEvent
{

    private DarkmireChatMessage m_chatMessage = null;

    private long m_previous = 0;
    
    private int m_previousId = 0;

    /**
     * Táto trieda predstavuje správu v systéme správ Darkmire.
     */
    public class DarkmireChatMessage
    {

        private String m_author = null;

        private String m_message = null;

        private long m_timestamp = 0;
        
        static final String DATEFORMAT = "H:m:s.S";

        /**
         * Skonštruuje inštanciu správy.
         * @param author autor
         * @param message správa
         * @param timestamp časová značka
         */
        public DarkmireChatMessage(String author, String message, long timestamp)
        {
            m_author = author;
            m_message = message;
            m_timestamp = timestamp;
        }


        @Override
        public String toString()
        {
            final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            
            return String.format("<%s> %s: %s", sdf.format(new Date(m_timestamp)), m_author, m_message);
        }
        
        /**
         * Získa časovú značku odoslania tejto správy.
         * @return časová značka
         */
        public long getTimestamp()
        {
            return m_timestamp;
        }
        
        /**
         * Získa správu obalenú touto triedou.
         * @return správa
         */
        public String getMessage()
        {
            return m_message;
        }
    }

    private SortedMap<Integer, DarkmireChatMessage> m_serverMessages = null;


    /**
     * Zkonštruuje inštanciu triedy DarkmireChatEvent.
     * @param session relácia, v kontexte ktorej je správa odoslaná
     * @param message správa
     * @param previous čas spracovania predchádzajúcej správy
     * @param previousId ID predchádzajúcej správy
     */
    public DarkmireChatEvent(DarkmireSession session, String message, long previous, int previousId)
    {
        super(session);

        if (message != null) {
            m_chatMessage = new DarkmireChatMessage("[You]", message, DarkmireDateTime.getUTC().getTime());
        }
        
        m_previous = previous;
        m_previousId = previousId;
    }


    @Override
    public JSONObject request() throws DarkmireException
    {
        JSONObject result = new JSONObject();

        result.put("type", RequestType.REQUEST_NORMAL.alias());

        JSONObject data = new JSONObject();
        
        JSONObject chat = new JSONObject();
        
        if (m_chatMessage != null) {
            chat.put("message", m_chatMessage.getMessage());
        }
        
//        if (m_previous != null) {
//            chat.put("previous", m_previous);
//        }
        
        chat.put("previous", String.format("%d", m_previous));
        
        chat.put("previous-id", m_previousId);
        
        data.put("chat", chat);
        
        result.put("data", data);

        if (m_session != null) {
            result.put("session", m_session.getId());
        } else {
            throw new DarkmireException("Not logged in any session");
        }

        return result;
    }


    @Override
    public void response(JSONObject object)
    {
        super.response(object);
        
        if (! m_status.isError())
        {
            m_serverMessages = new TreeMap<>();

            if (object.optJSONObject("data") != null)
            {
                JSONArray chat = object.getJSONObject("data").getJSONArray("chat");

                Iterator<JSONObject> iterator = chat.iterator();
                while (iterator.hasNext()) {
                    JSONObject message = iterator.next();

                    m_serverMessages.put(
                            message.getInt("id"),
                            new DarkmireChatMessage(message.getString("author"),
                                message.getString("text"),
                                message.getLong("timestamp")));
                }
            }
        }
    }

    /**
     * Získa zoznam správ servera, ktoré boli odoslané zo servera ako 
     * odpoveď. Je to mapa poradového čísla správy a správy samotnej.
     * @return zoznam správ servera
     */
    public SortedMap<Integer, DarkmireChatMessage> getServerMessages()
    {
        return m_serverMessages;
    }


    /**
     * Získa správu, ktorú obaľuje táto udalosť.
     * @return správa
     */
    public DarkmireChatMessage getChatMessage()
    {
        return m_chatMessage;
    }
}
