package darkmirec.core;

import darkmirec.events.DarkmireEvent;
import darkmirec.sessions.DarkmireSession;
import java.net.URI;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Logger;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public interface DarkmireClient {
    
    public static final String LOG_NAME = "org.brendor.darkmire";
    public static final Logger logger = Logger.getLogger(LOG_NAME);
    
    /**
     * Zapne HTTP klinenta, ktorý sa bude pripájať na danú URL.
     * @param url URL, na ktorú sa klient bude pripájať
     * @throws darkmirec.core.DarkmireException v prípade chyby
    */
    public void start(String url) throws DarkmireException;
    
    /**
     * Zastaví HTTP klienta.
     */
    public void stop();
    
    /**
     * Zaradí udalosť DarkmireEvent do poradovníka udalostí,
     * ktorý bude postupne odosielaný serveru.
     * @param e udalosť
     * @throws DarkmireException v prípade chyby
     */
    public void enqueueEvent(DarkmireEvent e) throws DarkmireException;
    
    /**
     * Vyberie z poradovníka prichádzajúcich udalostí inštanciu udalosti
     * DarkmireEvent počas istého času. Ak sa tam žiadna inštancia nenachádza,
     * vráti okamžite.
     * @param timeout časový limit
     * @return inštancia udalosti alebo null
     * @throws DarkmireException v prípade chyby
     */
    public DarkmireEvent dequeueEvent(long timeout) throws DarkmireException;
    
    /**
     * Získa inštanciu DarkmireSession podľa jej identifikátora.
     * @param id identifikátor
     * @return inštancia triedy DarkmireSession
     */
    public DarkmireSession getSession(int id);
    
    /**
     * Získa počet relácií registrovaných v klientovi.
     * @return počet relácií registrovaných v klientovi
     */
    public Integer getNumSessions();
    
    /**
     * Zistí, či je používateľ prihlásený na serveri.
     * @return true ak áno, inak false
     */
    public boolean isLogged();
    
    /**
     * Zistí, či je používateľ prihlásený ako moderátor servera
     * @return true ak áno, inak false
     */
    public boolean isModerator();
    
    /**
     * Zistí, či je používateľ prihlásený ako administrátor servera
     * @return true ak áno, inak false
     */
    public boolean isAdmin();
    
    /**
     * Získa reťazec predstavujúci cookie, ktorú server uložil v klientovi podľa
     * jej kľúča.
     * @param name kľúč cookie
     * @return cookie
     */
    public String getCookie(String name);
    
    /**
     * Získa aktuálne meno používateľa.
     * @return aktuálne meno používateľa
     */
    public String getUserName();
    
    /**
     * Získa URL servera, ku ktorému sa klient pripája. URL je adresa,
     * s ktoru bola volaná metóda start.
     * @return URL servera
     */
    public URI getServerAddress();
    
    /**
     * Získa zoznam akumulovaných Round Trip Time, podľa ktorých je možné
     * vypočítať priemernú latenciu komunikácie klient-server.
     * @return zoznam akumulovaných Round Trip Time
     */
    public ConcurrentSkipListSet<Long> getRTT();
}

