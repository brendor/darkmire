package darkmirec.core;

import darkmirec.events.DarkmireEvent;
import darkmirec.sessions.DarkmireSession;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
final class DarkmireClientImpl implements DarkmireClient {

    private DarkmireWorker m_worker = null;
    private URI m_uri = null;
    
    @Override
    public void start(String url) throws DarkmireException {
        if (m_worker != null) {
            throw new DarkmireException("Already running");
        }

        try {
            m_uri = new URI(url);

            m_worker = new DarkmireWorker(m_uri);

            Logger.getLogger(LOG_NAME)
                    .log(Level.INFO, "Starting Darkmire client with URL: {0}", url);

            m_worker.start();
            
        } catch (URISyntaxException ex)
        {
            Logger.getLogger(LOG_NAME)
                    .log(Level.INFO, "Invalid URL: {0}", url);
            
            throw new DarkmireException("Invalid URL: " + ex.getMessage());
        }
    }


    @Override
    public void stop() {
        try {
            m_worker.stop();
        } catch (DarkmireException ex) {
            Logger.getLogger(DarkmireClientImpl.class.getName()).log(Level.INFO, null, ex);
        }
        
        m_worker = null;
        m_uri = null;
    }


    @Override
    public synchronized void enqueueEvent(DarkmireEvent e) throws DarkmireException {
        
        if (! m_worker.isRunning())
        {
            throw new DarkmireException("Client not running");
        }
        
        m_worker.enqueueEvent(e);
    }


    @Override
    public synchronized DarkmireEvent dequeueEvent(long timeout) throws DarkmireException {
        
        if (m_worker == null) {
            throw new DarkmireException("Client not running");
        }
        
        try {
            return m_worker.dequeueEvent(timeout);
        } catch (InterruptedException ex) {
            throw new DarkmireException("Client interrupted", ex);
        }
    }
    
    
    @Override
    public DarkmireSession getSession(int id)
    {
        if (m_worker == null)
        {
            return null;
        }
        
        return m_worker.getSession(id);
    }

    @Override
    public Integer getNumSessions()
    {
        if (m_worker == null)
        {
            return 0;
        }
        
        return m_worker.getNumSessions();
    }
    
    
    @Override
    public boolean isLogged()
    {
        if (m_worker == null)
        {
            return false;
        }
        
        return m_worker.isLogged();
    }

    @Override
    public boolean isAdmin()
    {
        if (m_worker == null)
        {
            return false;
        }
        
        return m_worker.getPrivileges() == DarkmireEvent.UserPrivilege.USER_ADMIN;
    }

    @Override
    public boolean isModerator()
    {
        if (m_worker == null)
        {
            return false;
        }
        
        return m_worker.getPrivileges() == DarkmireEvent.UserPrivilege.USER_MODERATOR;
    }

    @Override
    public String getCookie(String name)
    {
        if (m_worker != null)
        {
            return m_worker.getCookie(name);
        }
        
        return "";
    }


    @Override
    public String getUserName()
    {
        if (m_worker != null)
        {
            return m_worker.getUserName();
        }
        
        return "";
    }

    @Override
    public URI getServerAddress()
    {
        return m_uri;
    }


    @Override
    public ConcurrentSkipListSet<Long> getRTT()
    {
        if (m_worker != null)
        {
            return m_worker.getRTT();
        }
        
        return null;
    }
}
