package darkmirec.core;

/**
 * Predstavuje výnimku v aplikácii Darkmire.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmireException extends Exception {

    /**
     * Vytvorí novú inštanciu výnimky bez detailnej správy.
     */
    public DarkmireException()
    {
    }


    /**
     * Skonštruuje inštanciu výnimky DarkmireException so
     * špecifikovanou detailnou správou.
     *
     * @param msg detailná správa
     */
    public DarkmireException(String msg)
    {
        super(msg);
    }

    /**
     * Skonštruuje inštanciu výnimky DarkmireException so
     * špecifikovanou detailnou správou a príčinou výnimky.
     *
     * @param message detailná správa
     * @param cause výnimka, ktorá spôsobila túto výnimku
     */
    public DarkmireException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
