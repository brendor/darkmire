package darkmirec.core;

import static darkmirec.core.DarkmireClientImpl.LOG_NAME;
import darkmirec.events.DarkmireAuthenticationEvent;
import darkmirec.events.DarkmireDisconnectEvent;
import darkmirec.events.DarkmireErrorEvent;
import darkmirec.events.DarkmireEvent;
import static darkmirec.events.DarkmireEvent.EventStatus.WEB_EVENT_AUTHENTICATION_ERROR;
import static darkmirec.events.DarkmireEvent.EventStatus.WEB_EVENT_AUTHENTICATION_SUCCESS;
import darkmirec.sessions.DarkmireSession;
import darkmirec.utils.DarkmireDateTime;
import darkmirec.utils.DarkmireUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

/**
 * Predstavuje pracovné vlákno, v ktorom bude prebiehať komunikácia.
 * Ide o internú triedy systému Darkmire, ktorá by nemala byť používana externe.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
class DarkmireWorker implements Runnable
{

    private Date m_date;

    private final DefaultHttpClient m_client;

    private final URI m_uri;

    protected static final int REFRESH_RATE = 100;

    protected static final int POLL_RATE = 300;
    
    protected static final int MAX_RTT_ENTRIES = 300;

    protected static final int CONNECTION_TIMEOUT_MS = 1000;

    protected static final int SO_TIMEOUT_MS = 10000;

    private long m_counter;

    private String m_userName;
    
    private DarkmireEvent.UserPrivilege m_userPrivilege = DarkmireEvent.UserPrivilege.USER_NONE;
    
    private ConcurrentSkipListSet<Long> m_rtt = new ConcurrentSkipListSet<>();

    private enum ClientState
    {

        STOPPED(0),
        STANDBY(30),
        PROCESSING_EVENT(1);

        private final int steps;


        private ClientState(int steps)
        {
            this.steps = steps;
        }


        int steps()
        {
            return this.steps;
        }
    }

    private ClientState m_state;

    private Map<Integer, DarkmireSession> m_sessions = new HashMap<>(5);

    private boolean m_logged = false;

    private final PriorityBlockingQueue<DarkmireEvent> m_incomingQueue;

    private final PriorityBlockingQueue<DarkmireEvent> m_outgoingQueue;

    private final ByteArrayOutputStream m_stream;

    private final OutputStreamWriter m_writer;

    private final ScheduledExecutorService m_scheduler = Executors.newSingleThreadScheduledExecutor();

    private RunnableScheduledFuture<?> m_task = null;


    public DarkmireWorker(URI uri)
    {

        this.m_stream = new ByteArrayOutputStream();
        this.m_writer = new OutputStreamWriter(m_stream);

        this.m_outgoingQueue = new PriorityBlockingQueue<>();
        this.m_incomingQueue = new PriorityBlockingQueue<>();

        this.m_date = new Date();

        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT_MS);
        HttpConnectionParams.setSoTimeout(httpParams, SO_TIMEOUT_MS);

        this.m_client = new DefaultHttpClient(httpParams);

        this.m_state = ClientState.STOPPED;
        this.m_counter = 0;

        this.m_uri = uri;
    }

    ResponseHandler<byte[]> m_handler = new ResponseHandler<byte[]>()
    {
        @Override
        public byte[] handleResponse(HttpResponse response)
                throws ClientProtocolException, IOException
        {
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                return EntityUtils.toByteArray(entity);
            } else {
                return null;
            }
        }
    };


    @Override
    public void run()
    {
        try {
            m_counter++;

            if (m_counter != m_state.steps) {
                return;
            }

            m_counter = 0;

            Logger.getLogger(LOG_NAME).log(Level.INFO,
                    String.format("working %s %s in %s mode (thread: %s)",
                            DarkmireDateTime.getUTCString(m_date),
                            m_uri.toString(),
                            m_state.toString(),
                            Thread.currentThread().getName()));

            try {
                communicate();
            } catch (InterruptedException exception) {
                Logger.getLogger(LOG_NAME).log(Level.INFO, "Communication interrupted: {0}", exception.getMessage());
            }

            m_date = DarkmireDateTime.getUTC();

        } catch (final RuntimeException exception) {
            Logger.getLogger(LOG_NAME).log(Level.INFO, null, exception);

            disconnect();
            
            m_outgoingQueue.add(new DarkmireErrorEvent("Runtime error (" + exception.getMessage() + ")"));

        } catch (final Exception exception) {

            Logger.getLogger(LOG_NAME).log(Level.INFO, null, exception.getMessage());

            disconnect();

            m_outgoingQueue.add(new DarkmireDisconnectEvent(exception.getMessage(), null));

        } catch (final Error ex) {
            try {
                stop();
            } catch (DarkmireException ex1) {
                Logger.getLogger(DarkmireWorker.class.getName()).log(Level.SEVERE, null, ex1);
            } finally {
                Logger.getLogger(DarkmireWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (final Throwable ex) {
            try {
                stop();
            } catch (DarkmireException ex1) {
                Logger.getLogger(DarkmireWorker.class.getName()).log(Level.SEVERE, null, ex1);
            } finally {
                Logger.getLogger(DarkmireWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    public void start()
    {
        setState(ClientState.STANDBY);

        m_task = (RunnableScheduledFuture<?>) m_scheduler.scheduleAtFixedRate(this, 0, REFRESH_RATE, TimeUnit.MILLISECONDS);
    }


    public void stop() throws DarkmireException
    {
        if (!isRunning())
        {
            return;
        }

        m_task.cancel(true);

        m_sessions.clear();

        setState(ClientState.STOPPED);
    }


    @SuppressWarnings(
            {
                "null", "ConstantConditions"
            })
    private void communicate() throws InterruptedException
    {
        boolean breakFlag = false;

        while (!m_incomingQueue.isEmpty()) {

            DarkmireEvent event = m_incomingQueue.poll(POLL_RATE, TimeUnit.MILLISECONDS);

            JSONObject request = new JSONObject();

            if (event != null) {

                Logger.getLogger(LOG_NAME).log(Level.INFO, String.format("event %s", event.getClass().toString()));

                try {

                    request.put("darkmire", event.request());

                    request.getJSONObject("darkmire").write(m_writer);

                    try {
                        m_writer.flush();

                        byte[] compress = Base64.encodeBase64(DarkmireUtils.compress(m_stream.toByteArray(), 5));

                        Logger.getLogger(LOG_NAME).log(Level.INFO, String.format("compressed data %s", new String(compress, "ascii")));

                        request.element("darkmire", new String(compress, "ascii"));

                        request.put("checksum", DarkmireUtils.Crc16CCITT(compress));

                        request.put("size", m_stream.toByteArray().length);

                    } catch (IOException exception) {
                        Logger.getLogger(DarkmireWorker.class.getName()).log(Level.INFO, ExceptionUtils.getRootCauseMessage(exception));
                    } finally {
                        m_stream.reset();
                    }

                } catch (DarkmireException exception) {
                    Logger.getLogger(DarkmireWorker.class.getName()).log(Level.WARNING,
                            ExceptionUtils.getRootCauseMessage(exception));

                    m_outgoingQueue.add(new DarkmireErrorEvent(exception.getMessage(), event));

                    continue;
                }
            } else {
//                setState(ClientState.STANDBY);
                continue;
            }

            switch (m_state) {
                case STOPPED:
                    continue;

                case STANDBY:
                    continue;

                case PROCESSING_EVENT: {
                    HttpPost post = new HttpPost(m_uri);

                    post.setHeader("http_connection", "Close");

                    request.write(m_writer);

                    try {
                        m_writer.flush();
                    } catch (IOException exception) {
                        Logger.getLogger(DarkmireWorker.class.getName()).log(Level.INFO, ExceptionUtils.getRootCauseMessage(exception));
                    }

                    post.setEntity(new ByteArrayEntity(m_stream.toByteArray()));

                    try {
                        long before = System.nanoTime();

                        byte[] response = m_client.execute(post, m_handler);
                        
                        if (m_rtt.size() >= MAX_RTT_ENTRIES)
                        {
                            m_rtt.clear();
                        }
                        
                        m_rtt.add(System.nanoTime()- before);

                        if (response != null) {
                            String parsed = new String(response, "utf-8");

                            Logger.getLogger(LOG_NAME).log(Level.INFO, parsed);

                            JSONObject responseObject = JSONObject.fromObject(parsed);

                            JSONObject darkmireObject = responseObject.optJSONObject("darkmire");

                            if (darkmireObject != null) {
                                if (darkmireObject.containsKey("type")
                                        && darkmireObject.getString("type").equals("http-server")) {
                                    disconnect();

                                    m_outgoingQueue.add(new DarkmireErrorEvent(darkmireObject.optString("data", "Unknown error"), event));

                                    break;
                                }

                            } else {
                                String darkmireEncoded = responseObject.getString("darkmire");

                                if (responseObject.containsKey("checksum")) {
                                    if (DarkmireUtils.Crc16CCITT(darkmireEncoded.getBytes("ascii")) != responseObject.getInt("checksum")) {
                                        m_outgoingQueue.add(new DarkmireErrorEvent("Checksum does not match, data corrupted", event));
                                        break;
                                    }
                                }

                                byte[] decoded = Base64.decodeBase64(darkmireEncoded);

                                try {
                                    responseObject.element("darkmire", JSONObject.fromObject(new String(DarkmireUtils.decompress(decoded, 4, decoded.length), "utf-8")));
                                } catch (DarkmireException ex) {
                                    m_outgoingQueue.add(new DarkmireErrorEvent(String.format("Error decompressing data (%s)", ex.getMessage()), event));
                                    break;
                                }

                                darkmireObject = responseObject.getJSONObject("darkmire");

                                Logger.getLogger(LOG_NAME).log(Level.INFO, responseObject.toString());
                            }

                            event.response(darkmireObject);
                            
                            DarkmireEvent.UserPrivilege privs = DarkmireEvent.UserPrivilege.fromInt(darkmireObject.optInt("privileges", 0));
                            
                            if (privs != DarkmireEvent.UserPrivilege.USER_NONE)
                            {
                                if (privs != m_userPrivilege)
                                {
                                    Logger.getLogger(LOG_NAME).log(Level.INFO, "Privileges changed from: {0} to: {1}", new Object[]{m_userPrivilege.toString(), privs.toString()});
                                }
                                
                                m_userPrivilege = privs;
                            }

                            if (event instanceof DarkmireAuthenticationEvent) {
                                DarkmireAuthenticationEvent authenticationEvent = (DarkmireAuthenticationEvent) event;

                                switch (authenticationEvent.getStatus()) {
                                    case WEB_EVENT_AUTHENTICATION_SUCCESS:

                                        m_userName = darkmireObject.optString("username");

                                        if (authenticationEvent.getSession() != null) {
                                            m_sessions.put(authenticationEvent.getId(), authenticationEvent.getSession());
                                        } else {
                                            m_logged = true;
                                        }

                                        break;
                                }
                            } else if (event instanceof DarkmireDisconnectEvent) {
                                DarkmireDisconnectEvent disconnectEvent = (DarkmireDisconnectEvent) event;

                                m_outgoingQueue.clear();
                                m_userPrivilege = DarkmireEvent.UserPrivilege.USER_NONE;

                                switch (event.getStatus()) {
                                    case WEB_EVENT_AUTHENTICATION_ERROR:
                                        m_sessions.clear();
                                        m_logged = false;
                                        break;

                                    default:
                                        if (disconnectEvent.getSession() != null) {
                                            m_sessions.clear();
                                        } else {
                                            m_logged = false;
                                        }
                                        break;
                                }

                            } else {
                                if (event.getStatus().isError())
                                {
                                    m_sessions.clear();
                                    m_logged = false;
                                    m_userPrivilege = DarkmireEvent.UserPrivilege.USER_NONE;
                                }
                            }

                            m_outgoingQueue.add(event);
                        }
                    } catch (JSONException | IOException exception) {
                        Logger.getLogger(DarkmireWorker.class.getName()).log(Level.INFO, ExceptionUtils.getRootCauseMessage(exception));

                        disconnect();

                        if (exception instanceof JSONException) {
                            m_outgoingQueue.add(new DarkmireDisconnectEvent(null));
                            m_outgoingQueue.add(new DarkmireErrorEvent("Invalid data received: " + exception.getMessage(), event));
                        } else {
                            m_outgoingQueue.add(new DarkmireDisconnectEvent(exception.getMessage(), null));
                            m_outgoingQueue.add(new DarkmireErrorEvent(ExceptionUtils.getRootCauseMessage(exception), event));
                        }

                        breakFlag = true;

                    } finally {
                        m_stream.reset();
                    }

                    break;
                }
            }

            if (m_task.isCancelled()) {
                return;
            }

            if (breakFlag) {
                break;
            }
        }

        setState(ClientState.STANDBY);
    }


    private void setState(ClientState state)
    {
        m_state = state;
        m_counter = 0;
    }


    public boolean isRunning()
    {
        return !m_task.isDone();
    }


    public synchronized void enqueueEvent(DarkmireEvent e)
    {
        m_incomingQueue.add(e);

        setState(ClientState.PROCESSING_EVENT);
    }


    public synchronized DarkmireEvent dequeueEvent(long timeout) throws InterruptedException
    {
        return m_outgoingQueue.poll(timeout, TimeUnit.MILLISECONDS);
    }


    public String getCookie(String name)
    {
        for (Cookie c : m_client.getCookieStore().getCookies()) {
            if (c.getName().equals(name)) {
                return c.getValue();
            }
        }

        return null;
    }


    private void disconnect()
    {
        m_incomingQueue.clear();
        m_outgoingQueue.clear();

        m_sessions.clear();
        m_logged = false;
        m_userPrivilege = DarkmireEvent.UserPrivilege.USER_NONE;
    }


    public DarkmireSession getSession(int id)
    {
        return m_sessions.get(id);
    }


    public Integer getNumSessions()
    {
        return m_sessions.size();
    }


    public boolean isLogged()
    {
        return m_logged;
    }


    public String getUserName() 
    {
        if (m_userName != null && !m_userName.isEmpty()) {
            return m_userName;
        }

        return "Anonymous";
    }


    public ConcurrentSkipListSet<Long> getRTT()
    {
        return m_rtt;
    }
    
    public DarkmireEvent.UserPrivilege getPrivileges()
    {
        return m_userPrivilege;
    }
}
