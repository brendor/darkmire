#include "variantmaptree.hpp"

#include "variantdelegate.hpp"

#include <QHeaderView>

#include <QDebug>


VariantMapTree::VariantMapTree(QWidget * parent) :
    QTreeWidget(parent)
{
    setItemDelegate(new VariantDelegate(this));

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    header()->setSectionResizeMode(0, QHeaderView::Stretch);
    header()->setSectionResizeMode(2, QHeaderView::Stretch);
#else
    header()->setResizeMode(0, QHeaderView::Stretch);
    header()->setResizeMode(2, QHeaderView::Stretch);
#endif
}


void VariantMapTree::addNewItem(const QString & name, const QVariant & value)
{
    QTreeWidgetItem * item = createItem(name, nullptr);

    if (value.type() == QVariant::Invalid) {
        item->setText(1, "Invalid");
    } else {
        item->setText(1, value.typeName());
    }

    item->setText(2, VariantDelegate::displayText(value));
    item->setData(2, Qt::UserRole, value);
}


QTreeWidgetItem * VariantMapTree::childAt(QTreeWidgetItem * parent, int index)
{
    if (parent)
        return parent->child(index);
    else
        return topLevelItem(index);
}


int VariantMapTree::childCount(QTreeWidgetItem * parent)
{
    if (parent)
        return parent->childCount();
    else
        return topLevelItemCount();
}


int VariantMapTree::findChild(QTreeWidgetItem * parent, const QString & text, int startIndex)
{
    for (int i = startIndex; i < childCount(parent); ++i) {
        if (childAt(parent, i)->text(0) == text)
            return i;
    }

    return -1;
}


QTreeWidgetItem * VariantMapTree::createItem(const QString & text, QTreeWidgetItem * parent)
{
    QTreeWidgetItem * item;

    if (parent)
        item = new QTreeWidgetItem(parent, nullptr);
    else
        item = new QTreeWidgetItem(this, nullptr);

    item->setText(0, text);
    item->setFlags(item->flags() | Qt::ItemIsEditable);

    return item;
}


void VariantMapTree::refresh(const QHash<QString, QVariant> & variant)
{
    disconnect(this, SIGNAL(itemChanged(QTreeWidgetItem *, int)),
               this, SLOT(updateItem(QTreeWidgetItem *)));

    qDebug() << "Refreshing properties: " << variant.count();

    for (const QString & key : variant.keys()) {

        //        qDebug() << key;

        int index = findChild(nullptr, key, 0);

        QTreeWidgetItem * item;

        if (index == -1) {
            item = createItem(key, nullptr);
        } else {
            item = childAt(nullptr, index);
        }

        QVariant value = variant.value(key);

        if (value.type() == QVariant::Invalid) {
            item->setText(1, "Invalid");
        } else {
            item->setText(1, value.typeName());
        }

        item->setText(2, VariantDelegate::displayText(value));
        item->setData(2, Qt::UserRole, value);
    }

    connect(this, SIGNAL(itemChanged(QTreeWidgetItem *, int)),
            this, SLOT(updateItem(QTreeWidgetItem *)));
}


void VariantMapTree::updateItem(QTreeWidgetItem * item)
{
    emit itemUpdated(qMakePair(item->text(0), item->data(2, Qt::UserRole)));
}

