#ifndef TEXTVIEWER_HPP
#define TEXTVIEWER_HPP

#include <QDialog>

namespace Ui {
    class TextViewer;
}

class TextViewer : public QDialog
{
        Q_OBJECT

    public:
        explicit TextViewer(const QString & text, QWidget *parent = 0);
        ~TextViewer();

    private:
        Ui::TextViewer *ui;
};

#endif // TEXTVIEWER_HPP
