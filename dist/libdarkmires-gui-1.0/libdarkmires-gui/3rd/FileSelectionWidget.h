#ifndef FileSelectionWidget_H
#define FileSelectionWidget_H

#include "ui_FileSelectionWidget.h"

#include <QMainWindow>
#include <QDir>

class QFileSystemModel;

class FileSelectionWidget : public QWidget, private Ui::FileSelectionWidget
{
        Q_OBJECT

    public:
        FileSelectionWidget(QWidget *parent = 0);

        void setPath(const QString & path);

    public slots:
        void on_listView_doubleClicked(const QModelIndex & index);
        void on_listView_clicked(const QModelIndex & index);

    protected:
        QFileSystemModel * m_dirModel;
        QFileSystemModel * m_fileModel;
    private slots:
        void on_treeView_clicked(const QModelIndex &index);
};

#endif
