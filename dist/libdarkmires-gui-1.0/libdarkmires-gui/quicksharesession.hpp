#ifndef MANAGEDREPOSITORY_HPP
#define MANAGEDREPOSITORY_HPP

#include "sessionhandler.hpp"

namespace Ui {
class QuickShare;
}

class QuickShareSession : public SessionHandler
{
        Q_OBJECT

    public:
        explicit QuickShareSession(Darkmires * const darkmires, QString session, QWidget * parent = 0);
        ~QuickShareSession();

    private:
        Ui::QuickShare * ui;

        void refreshUsers();

    public slots:
        virtual void notifyShow() override;
        virtual void notifyRemove() override;
        virtual void darkmiresEvent(Darkmires::Event event) override;

    private slots:
        void on_refreshButton_clicked();
        void on_entryTable_cellDoubleClicked(int row, int column);
};

#endif // MANAGEDREPOSITORY_HPP
