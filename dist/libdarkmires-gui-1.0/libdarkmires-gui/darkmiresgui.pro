#-------------------------------------------------
#
# Project created by QtCreator 2013-03-26T11:53:01
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = darkmiresgui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    settingstree.cpp \
    variantdelegate.cpp \
    adduserdialog.cpp \
    sessionview.cpp \
    sessionhandler.cpp \
    sessionhandlergeneral.cpp \
    variantmaptree.cpp \
    scriptedsession.cpp \
    quicksharesession.cpp \
    subversionsession.cpp \
    textviewer.cpp \
    addsessionuserdialog.cpp \
    3rd/FileSelectionWidget.cpp \
    3rd/qcustomplot.cpp

HEADERS  += mainwindow.hpp \
    settingstree.hpp \
    variantdelegate.hpp \
    adduserdialog.hpp \
    sessionview.hpp \
    sessionhandler.hpp \
    sessionhandlergeneral.hpp \
    variantmaptree.hpp \
    scriptedsession.hpp \
    quicksharesession.hpp \
    subversionsession.hpp \
    textviewer.hpp \
    addsessionuserdialog.hpp \
    3rd/FileSelectionWidget.h \
    3rd/qcustomplot.h

FORMS    += mainwindow.ui \
    adduserdialog.ui \
    sessionview.ui \
    sessionhandlergeneral.ui \
    scriptedsession.ui \
    quickshare.ui \
    subversionsession.ui \
    textviewer.ui \
    addsessionuserdialog.ui \
    3rd/FileSelectionWidget.ui

INCLUDEPATH += ../darkmires
INCLUDEPATH -= /usr/include/qt5

LIBS += -L. -L"$$_PRO_FILE_PWD_" -ldarkmires

QMAKE_CXXFLAGS += -std=c++11

# CONFIG  += qxt
# QXT     += core gui

RESOURCES += \
    resources.qrc
