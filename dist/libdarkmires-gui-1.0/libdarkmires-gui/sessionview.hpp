#ifndef SESSIONVIEW_HPP
#define SESSIONVIEW_HPP

#include <QDialog>

namespace Ui {
class SessionView;
}

class SessionView : public QDialog
{
        Q_OBJECT

    public:
        explicit SessionView(QWidget * parent = 0);
        ~SessionView();

    private:
        Ui::SessionView * ui;
};

#endif // SESSIONVIEW_HPP
