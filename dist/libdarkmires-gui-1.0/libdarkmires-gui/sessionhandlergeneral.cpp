#include "sessionhandlergeneral.hpp"
#include "ui_sessionhandlergeneral.h"

#include <QMessageBox>

#include <QSqlError>
#include <QSqlRecord>

#include "dssession.hpp"
#include "dsutils.hpp"
#include "dsconstants.hpp"

#include "addsessionuserdialog.hpp"




SessionHandlerGeneral::SessionHandlerGeneral(Darkmires * const darkmires, QString session, QWidget * parent) :
    QFrame(parent),
    ui(new Ui::SessionHandlerGeneral),
    m_session(session),
    m_sessionPtr(darkmires->getSessions().value(m_session)),
    m_darkmires(darkmires)
{
    ui->setupUi(this);

    refreshOwners();
    refreshProperties();

    connect(ui->refreshPropertiesButton, SIGNAL(clicked()),
            this, SLOT(refreshProperties()));

    connect(ui->propertyTree, SIGNAL(itemUpdated(QPair<QString, QVariant>)),
            this, SLOT(propertyChanged(QPair<QString, QVariant>)));


    switch (m_sessionPtr->getAuthentication())
    {
        case Darkmires::AuthenticationLocalDatabase:
        {
            QString uuid = m_sessionPtr->getUuid().toString();

            DsUtils::removeFirstAndLast(uuid);

            m_database = QSqlDatabase::addDatabase("QSQLITE", "GUI_" + uuid);

            if (! m_database.isValid())
            {
                qDebug("Database driver not valid!");
            }
            else
            {
                m_database.setDatabaseName(DarkSettings->value("sessions-file-path").toString() + uuid + ".db");

                qDebug() << "Opening session database... " + m_database.databaseName();

                if (m_database.open())
                {
                    m_database.exec("PRAGMA locking_mode = EXCLUSIVE");

                    qDebug() << "Session user database connection opened: " + m_database.databaseName();

                    m_usersSqlModel.reset(new QSqlTableModel(this, m_database));

                    m_usersSqlModel->setTable("users");
                    m_usersSqlModel->select();

                    m_usersSqlModel->setEditStrategy(QSqlTableModel::OnManualSubmit);

                    ui->usersTable->setModel(m_usersSqlModel.get());
                }
                else
                {
                    qDebug() << "Session user database connection could not be opened: " + m_database.lastError().text();
                }
            }

            ui->usersTab->setEnabled(true);

            break;
        }

        default:

            ui->usersTab->setEnabled(false);

            break;
    }

    ui->tabWidget->setCurrentIndex(0);
}


SessionHandlerGeneral::~SessionHandlerGeneral()
{
    delete ui;

    m_usersSqlModel.reset();

    if (m_database.isOpen())
    {
        m_database.close();

        QSqlDatabase::removeDatabase(m_database.connectionName());
    }
}


void SessionHandlerGeneral::notifyRemove()
{
    qDebug() << "Session being handled has been removed";

    m_sessionPtr.reset();

    disconnect();
}


void SessionHandlerGeneral::refreshOwners()
{
    if (! m_sessionPtr.get()) return;

    ui->ownerList->clear();

    for (const QUuid & owner : m_sessionPtr->getOwners()) {
        ui->ownerList->addItem(owner.toString());
    }
}


void SessionHandlerGeneral::refreshProperties()
{
    ui->propertyTree->refresh(m_sessionPtr->getSessionProperties());
}


void SessionHandlerGeneral::on_addOwner_clicked()
{
    if (! m_sessionPtr.get()) return;

    QUuid owner(ui->ownerEdit->text());

    if (! owner.isNull()) {
        if (m_sessionPtr->addOwner(owner)) {
            ui->ownerEdit->clear();

            refreshOwners();
        } else {
            QMessageBox::critical(this, "Darkmire", "Unable to add new user.", QMessageBox::Ok);
        }

    } else {
        QMessageBox::critical(this, "Darkmire", "Invalid UUID entered.", QMessageBox::Ok);
    }
}


void SessionHandlerGeneral::on_removeOwner_clicked()
{
    if (! m_sessionPtr.get()) return;

    if (ui->ownerList->currentItem()) {

        QUuid owner(ui->ownerList->currentItem()->text());

        if (! owner.isNull()) {
            if (m_sessionPtr->removeOwner(owner)) {
                refreshOwners();
            } else {
                QMessageBox::critical(this, "Darkmire", "Unable to remove user.", QMessageBox::Ok);
            }
        }
    } else {
        QMessageBox::information(this, "Darkmire", "No item selected.", QMessageBox::Ok);
    }
}


void SessionHandlerGeneral::propertyChanged(QPair<QString, QVariant> pair)
{
    if (! m_sessionPtr.get()) return;

    m_sessionPtr->setSessionProperty(pair.first, pair.second);
}


void SessionHandlerGeneral::on_addPropertyButton_clicked()
{
    if (ui->nameEdit->text().isEmpty()) {
        QMessageBox::critical(this, "Darkmire", "Property name must not be empty", QMessageBox::Ok);
        return;
    }

    ui->propertyTree->addNewItem(ui->nameEdit->text(), QString());

    ui->nameEdit->clear();
}


void SessionHandlerGeneral::on_removePropertyButton_clicked()
{
    if (! m_sessionPtr.get()) return;

    if (ui->propertyTree->currentIndex().isValid()) {

        QTreeWidgetItem * item = ui->propertyTree->takeTopLevelItem(ui->propertyTree->currentIndex().row());

        m_sessionPtr->removeSessionProperty(item->text(0));

        delete item;
    }
}


void SessionHandlerGeneral::on_addUserSqlButton_clicked()
{
    AddSessionUserDialog dialog(this);

    switch (dialog.exec()) {
        case QDialog::Accepted:
        {
            QSqlRecord record = m_usersSqlModel->record();

            QUuid uuid(dialog.getUuid());

            if (uuid.isNull())
            {
                QMessageBox::critical(this, "Darkmire Server", "Invalid UUID entered!");

                break;
            }

            record.setValue("session", dialog.getUuid());

            if (! m_usersSqlModel->insertRecord(-1, record))
            {
                QMessageBox::critical(this, "Darkmire Server", m_usersSqlModel->lastError().text());
            }

            break;
        }

        default:
            break;
    }
}

void SessionHandlerGeneral::on_saveUsersSqlButton_clicked()
{
    if (! m_usersSqlModel->submitAll())
    {
        qDebug() << m_usersSqlModel->lastError().text();
    }
}

void SessionHandlerGeneral::on_revertUsersSqlButton_clicked()
{
    m_usersSqlModel->revertAll();
}

void SessionHandlerGeneral::on_clearUsersSqlButton_clicked()
{
    m_usersSqlModel->removeRows(0, m_usersSqlModel->rowCount());
}

void SessionHandlerGeneral::on_removeUserSqlButton_clicked()
{
    m_usersSqlModel->removeRows(ui->usersTable->currentIndex().row(), 1);
}
