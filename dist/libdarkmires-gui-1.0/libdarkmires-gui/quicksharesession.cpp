#include "quicksharesession.hpp"
#include "ui_quickshare.h"

#include <QDebug>

#include <QMessageBox>

#include "dsuserstorage.hpp"
#include "dsquicksharesession.hpp"

#include "textviewer.hpp"

#define WAIT_FOR_USERS 1000


QuickShareSession::QuickShareSession(Darkmires * const darkmires, QString session, QWidget * parent) :
    SessionHandler(darkmires, session, parent),
    ui(new Ui::QuickShare)
{
    ui->setupUi(this);

    ui->tabWidget->insertTab(0, &m_general, SessionHandlerGeneral::TAB_WIDGET_NAME);

    ui->tabWidget->setCurrentIndex(0);

    setWindowTitle("QuickShare " + m_general.getSession());

    refreshUsers();
}


QuickShareSession::~QuickShareSession()
{
    qDebug() << "Deleting QuickShare session";

    delete ui;
}


void QuickShareSession::refreshUsers()
{
    ui->usersTable->clear();

    ui->entryTable->clearContents();
    ui->entryTable->setRowCount(0);


    if (DsUserStorage::getInstance()->tryLock(WAIT_FOR_USERS)) {

        QList<Darkmires::UserMap::key_type> users = DsUserStorage::getInstance()->getUsers();

        for (Darkmires::UserMap::key_type i : users) {

            const DsUser * user = DsUserStorage::getInstance()->get(i);

            if (user->inSession(m_general.getSession())) {
                const std::shared_ptr<DsSessionUserData> sessionUser = m_general.getDarkmires()->getSessions().value(m_general.getSession())->getUser(i);
                const DsQuickShareSessionUser * managedUserData = static_cast<DsQuickShareSessionUser *>(sessionUser.get());

                QTreeWidgetItem * item = new QTreeWidgetItem(ui->usersTable, nullptr);
                item->setText(0, QString::number(i));
                item->setText(1, user->getSessionID().toString());
                ui->usersTable->addTopLevelItem(item);

                QTreeWidgetItem * privilegesItem = new QTreeWidgetItem(item, nullptr);
                privilegesItem->setText(0, "Privileges");
                privilegesItem->setText(1, QString::number(managedUserData->getPrivileges(), 16));

                item->addChild(privilegesItem);

                QTreeWidgetItem * entriesItem = new QTreeWidgetItem(item, nullptr);
                entriesItem->setText(0, "Entries");
                entriesItem->setText(1, QString::number(managedUserData->getEntries().count()));

                item->addChild(entriesItem);


                for (int n : managedUserData->getEntries().keys())
                {
                    int row = ui->entryTable->rowCount();;
                    ui->entryTable->setRowCount(row + 1);

//                    for (int i = 0; i < ui->entryTable->rowCount(); i++)
//                    {
//                        if (user->getSessionID() == QUuid(ui->entryTable->itemAt(0, i)->text()))
//                        {
//                            row = i;
//                        }
//                    }

//                    if (row < 0)
//                    {
//                        row = ui->entryTable->rowCount();
//                        ui->entryTable->setRowCount(row + 1);
//                    }

                    const DsQuickShareSessionUser::Entry entry = managedUserData->getEntries().value(n);

                    QTableWidgetItem * userItem = new QTableWidgetItem(user->getSessionID().toString());

                    userItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

                    QTableWidgetItem * codeItem = new QTableWidgetItem(entry.code.left(40) + "...");

                    codeItem->setData(Qt::UserRole, entry.code);
                    codeItem->setData(Qt::UserRole + 1, entry.mime);
                    codeItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

                    QTableWidgetItem * mimeItem = new QTableWidgetItem(entry.mime);

                    mimeItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

                    ui->entryTable->setItem(row, 0, userItem);
                    ui->entryTable->setItem(row, 1, mimeItem);
                    ui->entryTable->setItem(row, 2, codeItem);
                }
            }
        }

        UnlockUserStorage
    }
}


void QuickShareSession::notifyShow()
{
    refreshUsers();
}


void QuickShareSession::notifyRemove()
{
    SessionHandler::notifyRemove();

    QMessageBox::critical(this, "Darkmire Server", "Session removed!", QMessageBox::Ok);
}


void QuickShareSession::darkmiresEvent(Darkmires::Event event)
{
    if (event.data().toString() == m_general.getSession())
    {
        switch (event.type())
        {
            case DarkmiresEvent::EVENT_USER_LEFT_SESSION:
            case DarkmiresEvent::EVENT_USER_JOINED_SESSION:
                refreshUsers();
                break;

            default:
                break;
        }
    }
}


void QuickShareSession::on_refreshButton_clicked()
{
    refreshUsers();
}


void QuickShareSession::on_entryTable_cellDoubleClicked(int row, int column)
{
    QTableWidgetItem * item = ui->entryTable->itemAt(row, 2);

    if (item)
    {
        qDebug() << item->data(Qt::UserRole).toString();

        TextViewer tv(item->data(Qt::UserRole).toString(), this);

        tv.exec();
    }
}
