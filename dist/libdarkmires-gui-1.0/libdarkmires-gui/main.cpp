#include "mainwindow.hpp"
#include <QApplication>

int main(int argc, char * argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QApplication::setGraphicsSystem("raster");
#endif

    QApplication a(argc, argv);

    MainWindow w;

    w.setWindowIcon(QIcon(":/resources/darkmire-icon-light.png"));

    w.show();

    return a.exec();
}
