#ifndef VARIANTMAPTREE_HPP
#define VARIANTMAPTREE_HPP

#include <QTreeWidget>

class VariantMapTree : public QTreeWidget
{
        Q_OBJECT

    public:
        explicit VariantMapTree(QWidget * parent = 0);

        void addNewItem(const QString & name, const QVariant & value);

    signals:
        void itemUpdated(QPair<QString, QVariant> pair);

    public slots:
        void refresh(const QHash<QString, QVariant> & variant);

    private slots:
        void updateItem(QTreeWidgetItem * item);

    private:
        QTreeWidgetItem * childAt(QTreeWidgetItem * parent, int index);
        int childCount(QTreeWidgetItem * parent);
        int findChild(QTreeWidgetItem * parent, const QString & text, int startIndex);
        QTreeWidgetItem * createItem(const QString & text, QTreeWidgetItem * parent);
};

#endif // VARIANTMAPTREE_HPP
