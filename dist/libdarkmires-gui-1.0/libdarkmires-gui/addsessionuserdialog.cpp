#include "addsessionuserdialog.hpp"
#include "ui_addsessionuserdialog.h"

AddSessionUserDialog::AddSessionUserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddSessionUserDialog)
{
    ui->setupUi(this);
}

AddSessionUserDialog::~AddSessionUserDialog()
{
    delete ui;
}

QString AddSessionUserDialog::getUuid() const
{
    return ui->uuidEdit->text();
}
