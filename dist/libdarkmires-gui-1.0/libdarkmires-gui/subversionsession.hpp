#ifndef SUBVERSIONSESSION_HPP
#define SUBVERSIONSESSION_HPP

#include <QWidget>

#include "sessionhandler.hpp"

namespace Ui {
    class SubversionSession;
}

class SubversionSession : public SessionHandler
{
        Q_OBJECT

    public:
        explicit SubversionSession(Darkmires * const darkmires, QString session, QWidget * parent);
        ~SubversionSession();

    private:
        Ui::SubversionSession *ui;

        // SessionHandler interface
    public slots:
        virtual void notifyShow();
        virtual void notifyRemove();
        virtual void darkmiresEvent(Darkmires::Event event);
};

#endif // SUBVERSIONSESSION_HPP
