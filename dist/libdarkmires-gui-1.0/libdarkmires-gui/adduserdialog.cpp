#include "adduserdialog.hpp"
#include "ui_adduserdialog.h"

#include <QCryptographicHash>
#include <QMessageBox>


AddUserDialog::AddUserDialog(QWidget * parent, bool allowNoPassword) :
    QDialog(parent),
    ui(new Ui::AddUserDialog),
    m_allowNoPassword(allowNoPassword)
{
    ui->setupUi(this);

    ui->passwordEdit->setMaxLength(DarkSettings->value("user/password-max-length").toInt());
//    ui->passwordAgainEdit->setMaxLength(ui->passwordEdit->maxLength());

    ui->showPassword->setChecked(false);

    on_generateButton_clicked();

    QFontMetrics fm(ui->saltEdit->font());

    resize(size().width() + fm.width(ui->saltEdit->text()) - ui->saltEdit->frameGeometry().width(), size().height());

    connect(ui->buttonBox,
            SIGNAL(accepted()),
            this,
            SLOT(accepted()));

    connect(ui->buttonBox,
            SIGNAL(rejected()),
            this,
            SLOT(reject()));

    connect(ui->regularBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(privilegesChanged()));

    connect(ui->adminBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(privilegesChanged()));

    connect(ui->moderatorBox,
            SIGNAL(clicked(bool)),
            this,
            SLOT(privilegesChanged()));

    privilegesChanged();

    ui->uuidEdit->setText(QUuid::createUuid());
}


AddUserDialog::~AddUserDialog()
{
    delete ui;
}


QString AddUserDialog::getName() const
{
    return ui->nameEdit->text();
}


QString AddUserDialog::getPassword() const
{
    return ui->hashedPassword->text();
}


Darkmires::UserPrivileges AddUserDialog::getPrivileges() const
{
    return ui->privilegesBits->text().toUInt(nullptr, 16);
}


QString AddUserDialog::getSalt() const
{
    return ui->saltEdit->text();
}


QUuid AddUserDialog::getUuid() const
{
    return QUuid(ui->uuidEdit->text());
}


QByteArray AddUserDialog::getFinal() const
{
    return QCryptographicHash::hash((getSalt() + getPassword()).toLatin1(), QCryptographicHash::Sha1).toHex();
}


void AddUserDialog::setName(const QString & name)
{
    ui->nameEdit->setText(name);
}


void AddUserDialog::setPrivileges(Darkmires::UserPrivileges privileges)
{
    ui->privilegesBits->setText(QString::number(privileges, 16));

    updatePrivileges();
}


void AddUserDialog::setSalt(const QString & salt)
{
    ui->saltEdit->setText(salt);
}


void AddUserDialog::setUuid(const QUuid & uuid)
{
    ui->uuidEdit->setText(uuid.toString());
}


void AddUserDialog::on_generateButton_clicked()
{
    ui->saltEdit->setText(
        QString::fromLatin1(QCryptographicHash::hash(
                                QString::number(qrand()).toLatin1(), QCryptographicHash::Sha1).toHex()));

    on_passwordEdit_textEdited(ui->passwordEdit->text());
}


void AddUserDialog::accepted()
{
    if (getName().isEmpty()) {
        QMessageBox::critical(this, "Error", "Username empty!", QMessageBox::Ok);
        return;
    }

    if (! m_allowNoPassword)
    {
        if (getPassword().isEmpty() || getPassword().length() < DarkSettings->value("user/password-min-length").toInt()) {
            QMessageBox::critical(this,
                                  "Error",
                                  tr("Password empty or too short! Must be between %1 and %2 characters long.")
                                  .arg(DarkSettings->value("user/password-min-length").toInt())
                                  .arg(DarkSettings->value("user/password-max-length").toInt()),
                                  QMessageBox::Ok);
            return;
        }
    }

//    if (getPassword() != ui->passwordAgainEdit->text()) {
//        QMessageBox::critical(this, "Error", "Passwords don't match!", QMessageBox::Ok);
//        return;
//    }

    accept();
}


void AddUserDialog::privilegesChanged()
{
    Darkmires::UserPrivileges privs = Darkmires::UserNone;

    if (ui->regularBox->isChecked()) {
        privs |= Darkmires::UserRegular;
    }

    if (ui->adminBox->isChecked()) {
        privs |= Darkmires::UserAdmin;

        ui->moderatorBox->setChecked(true);
    }

    if (ui->moderatorBox->isChecked()) {
        privs |= Darkmires::UserModerator;
    }

    ui->privilegesBits->setText(QString::number(privs, 16));
}


void AddUserDialog::updatePrivileges()
{
    Darkmires::UserPrivileges privs = ui->privilegesBits->text().toUInt(nullptr, 16);

    ui->regularBox->setChecked(privs & Darkmires::UserRegular);
    ui->adminBox->setChecked(privs & Darkmires::UserAdmin);
    ui->moderatorBox->setChecked(privs & Darkmires::UserModerator);
}


void AddUserDialog::on_passwordEdit_textEdited(const QString & text)
{
    if (ui->passwordEdit->text().isEmpty())
    {
        ui->hashedPassword->clear();
        ui->finalEdit->clear();
    }
    else
    {
        ui->hashedPassword->setText(QString::fromLatin1(QCryptographicHash::hash(
                                    text.toLatin1(), QCryptographicHash::Sha1).toHex()));

        ui->finalEdit->setText(QString::fromLatin1(getFinal()));
    }
}

void AddUserDialog::on_showPassword_toggled(bool checked)
{
    ui->passwordEdit->setEchoMode(checked ? QLineEdit::Normal : QLineEdit::Password);
}
