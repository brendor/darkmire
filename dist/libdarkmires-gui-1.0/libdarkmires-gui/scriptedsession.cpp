#include "scriptedsession.hpp"
#include "ui_scriptedsession.h"


ScriptedSession::ScriptedSession(Darkmires * const darkmires, QString session, QWidget * parent) :
    SessionHandler(darkmires, session, parent),
    ui(new Ui::ScriptedSession)
{
    ui->setupUi(this);

    ui->tabWidget->insertTab(0, &m_general, SessionHandlerGeneral::TAB_WIDGET_NAME);

    ui->tabWidget->setCurrentIndex(0);

    setWindowTitle("Scripted Session " + m_general.getSession());
}


ScriptedSession::~ScriptedSession()
{
    qDebug() << "Deleting scripted session";

    delete ui;
}


void ScriptedSession::notifyShow()
{

}


void ScriptedSession::notifyRemove()
{
    SessionHandler::notifyRemove();
}


void ScriptedSession::darkmiresEvent(Darkmires::Event event)
{
    switch (event.type()) {
        case Darkmires::Event::EVENT_COMMAND_RESULT:
        {
            QVariantList list = event.data().toList();

            if (list[0].toString() == m_general.getSession()) {
                ui->consoleText->append(QDateTime::currentDateTimeUtc().toString("[hh:mm:ss]: ") + list[1].toString());
            }

            break;
        }

        default:
            break;
    }
}


void ScriptedSession::on_consoleExecute_clicked()
{
    DsSessionInterface * session = m_general.getDarkmires()->getSessions().value(m_general.getSession()).get();

    if (! ui->consoleEdit->text().isEmpty()) {
        session->command(ui->consoleEdit->text());
    }

    ui->consoleEdit->clear();
}


void ScriptedSession::on_consoleEdit_returnPressed()
{
    on_consoleExecute_clicked();
}


void ScriptedSession::on_evaluateButton_clicked()
{
    DsSessionInterface * session = m_general.getDarkmires()->getSessions().value(m_general.getSession()).get();

    session->command("init");
}
