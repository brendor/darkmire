#include "subversionsession.hpp"
#include "ui_subversionsession.h"

#include <QDebug>

#include "3rd/FileSelectionWidget.h"

SubversionSession::SubversionSession(Darkmires * const darkmires, QString session, QWidget * parent) :
    SessionHandler(darkmires, session, parent),
    ui(new Ui::SubversionSession)
{
    ui->setupUi(this);

    ui->tabWidget->insertTab(0, &m_general, SessionHandlerGeneral::TAB_WIDGET_NAME);

    ui->tabWidget->setCurrentIndex(0);

    QString path = QDir().currentPath() + "/" +
                   DarkSettings->value("sessions-file-path").toString();

    ui->fileBrowser->setPath(path);

    setWindowTitle("Subversion " + m_general.getSession());

    resize(900, 650);
}


SubversionSession::~SubversionSession()
{
    delete ui;
}


void SubversionSession::notifyShow()
{

}


void SubversionSession::notifyRemove()
{
    SessionHandler::notifyRemove();
}


void SubversionSession::darkmiresEvent(Darkmires::Event event)
{
//    qDebug() << "Received event: " << event.type();
}
