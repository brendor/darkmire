#ifndef DARKMIRESWEBSERVICETHREAD_HPP
#define DARKMIRESWEBSERVICETHREAD_HPP

#include <QRunnable>

#include <QxtWebRequestEvent>

#include "darkmires.hpp"


///
/// \brief Trieda DsWebServiceThreadObject je inštanciou vyhľadávania,
/// ktoré prebieha vo vlákne ktoré predstavuje trieda DsWebServiceThread.
///
class DsWebServiceThreadObject : public QObject
{
        Q_OBJECT

    public:
        DsWebServiceThreadObject(QObject * parent = 0);

        ///
        /// \brief emitFinished je funkcia, ktorá spôsobí vyvolanie signálu dataFetched
        /// \param packet požiadavka
        ///
        void emitFinished(Darkmires::PacketPointer packet);

    signals:
        ///
        /// \brief dataFetched znamená ukončenie práce vlákna, ktoré pracovalo s týmto objektom
        /// \param packet požiadavka
        ///
        void dataFetched(Darkmires::PacketPointer packet);
};


///
/// \brief Trieda DsWebServiceThread predstavuje vlákno, v ktorom sa
/// načítajú všetky dáta prichádzajúcej požiadavky. Má to význam pri odosielaní
/// väčšieho množstva dát cez prúdový protokol TCP.
///
class DsWebServiceThread : public QRunnable
{
    public:
        explicit DsWebServiceThread(QxtWebRequestEvent * event, QObject * parent = 0);

        ///
        /// \brief run je metóda zavolaná, keď je vlákno pripravené k behu a predstavuje
        /// činnosť, ktorá sa má v tomto vlákne vykonať.
        ///
        virtual void run() override;

        ///
        /// \brief object získa objekt špecifický pre toto vlákno
        /// \return pointer na triedu DsWebServiceThreadObject
        ///
        DsWebServiceThreadObject * object();

    private:

        QxtWebRequestEvent * m_event = nullptr;

        DsWebServiceThreadObject m_object;
};

#endif // DARKMIRESWEBSERVICETHREAD_HPP
