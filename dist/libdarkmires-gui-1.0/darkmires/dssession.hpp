#ifndef DARKMIRESSESSION_HPP
#define DARKMIRESSESSION_HPP

#include <QObject>

#include <memory>

#include "darkmires.hpp"
#include "dssessioninterface.hpp"

///
/// \brief DsSession je trieda reprezentujúca reláciu aplikácie Darkmire.
/// Je to pure virtual trieda, od ktorej musí každá relácia dediť.
///
class DsSession : public QObject
{
        Q_OBJECT

    public:
        typedef QHash<int, std::shared_ptr<DsSessionUserData>> SessionUsers;

        ///
        /// \brief DsSession je konštruktor inštancie relácie.
        /// \param interface rozhranie pre túto reláciu
        ///
        explicit DsSession(DsSessionInterface * const interface);

        ///
        /// \brief ~DsSession je deštruktor relácie.
        ///
        virtual ~DsSession();

        ///
        /// \brief getChat získa zoznam konverzačných správ.
        /// \return zoznam konverzačných správ
        ///
        inline Darkmires::ChatStorage & getChat()
        {
            return m_chat;
        }

        ///
        /// @brief getThreadId získa číslo vlákna, v ktorom táto relácia beží.
        /// @return identifikátor vlákna
        /// @note Len pre účely ladenia
        ///
        inline const std::atomic_ulong & getThreadId()
        {
            return m_thread_id;
        }

        ///
        /// \brief getMode získa mód relácie.
        /// \return
        ///
        inline Darkmires::SessionMode getMode()
        {
            return m_mode;
        }

        ///
        /// \brief getSessionUser získa dáta pripojeného používateľa v tejto relácii.
        /// \param id identifikátor pripojeného používateľa
        /// \return štruktúra dát používateľa v tejto relácii
        ///
        const std::shared_ptr<DsSessionUserData> getSessionUser(int id);

        ///
        /// \brief load načíta potrebné údaje z binárnej štruktúry pri spúštaní relácie.
        /// \param in prúd binárnych dát
        ///
        virtual void load(QDataStream & in)
        {
            Q_UNUSED(in)
        }

        ///
        /// \brief save zapíše potrebné binárne dáta pri konci behu relácie.
        /// \param out prúd binárnych dát
        ///
        virtual void save(QDataStream & out)
        {
            Q_UNUSED(out)
        }

        ///
        /// \brief init inicializuje reláciu.
        /// \return true pri úspešnej inicializácii, inak false
        ///
        virtual bool init() = 0;

    signals:
        ///
        /// \brief replySignal je signál vyvolaný pri odpovedi tejto relácie na požiadavku.
        /// \param event odpoveď
        ///
        void replySignal(DsWebEvent * event);

        ///
        /// \brief sessionEvent je signál vyvolaný pri vzniku vnútornej udalosti v tejto relácii.
        /// \param event udalosť
        ///
        void sessionEvent(Darkmires::Event event);

    public slots:
        ///
        /// \brief processRequest spracuje požiadavku prichádzajúcu z predchádzajúcich vrstiev.
        /// \param packet požiadavka
        ///
        virtual void processRequest(Darkmires::PacketPointer packet) = 0;

        ///
        /// \brief addUser pridá používateľa do tejto relácie.
        /// \param user identifikátor prihláseného používateľa
        ///
        virtual void addUser(int user) = 0;

        ///
        /// \brief processCommand spracuje daný príkaz.
        /// \param command
        ///
        virtual void processCommand(QString command);

        ///
        /// \brief removeUser odstráni požívateľa z tejto relácie.
        /// \param user identifikátor prihláseného používateľa
        ///
        void removeUser(int user);

        ///
        /// \brief start spustí vlákno, v ktorom bude táto relácia bežať a spracovávať požiadavky.
        ///
        void start();

        ///
        /// \brief stop zastaví vlákno, v ktorom táto relácia beží.
        ///
        void stop();

    protected:
        DsSessionInterface * const m_interface;

        void processChat(Darkmires::PacketPointer packet, const QVariantMap & in);

        SessionUsers m_users;

        Darkmires::SessionMode m_mode = Darkmires::SessionNone;

        void sessionError(const Darkmires::PacketPointer packet, const QString & message);

        virtual void populate(std::shared_ptr<DsSessionUserData> data) = 0;

    private:
        Darkmires::ChatStorage m_chat;

        std::atomic_ulong m_thread_id;
};

#endif // DARKMIRESSESSION_HPP
