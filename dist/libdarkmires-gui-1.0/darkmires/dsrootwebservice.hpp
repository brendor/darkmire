#ifndef DARKMIRESWEBSERVICE_HPP
#define DARKMIRESWEBSERVICE_HPP

#include <QxtWebServiceDirectory>

#include <QThread>
#include <atomic>

#include <memory>

#include "darkmires.hpp"

#include "dswebservicethread.hpp"


class DsAuthenticationLayer;
class DsDataLayer;

///
/// \brief Trieda DsRootWebService dedí od QxtWebServiceDirectory technológie QxtWeb
/// a v systéme Darkmire spracováva prichádzajúce požiadavky, buď v samostatnom, alebo
/// hlavnom vlákne.
///
/// Následne tieto požiadavky kontroluje a posiela ďalej do autentifikačnej vrstvy, alebo
/// ich zahodí a odpovie chybovou hláškou.
///
class Q_DECL_HIDDEN DsRootWebService : public QxtWebServiceDirectory
{
        Q_OBJECT

    public:
        enum DarkmiresWebServicePost
        {
            POST_404,
            POST_FETCH_DATA_THREAD,
            POST_FETCH_DATA_IGNORE
        };

        enum DarkmiresWebServiceGet
        {
            GET_404,
            GET_SHOW_TEMPLATE
        };

        explicit DsRootWebService(QxtAbstractWebSessionManager * sm);

        DsRootWebService(const DsRootWebService &) = delete;

        virtual ~DsRootWebService();

        void setBehaviorPost(DarkmiresWebServicePost b)
        {
            m_behaviorPOST = b;
        }

        void setBehaviorGet(DarkmiresWebServiceGet b)
        {
            m_behaviorGET = b;
        }

        inline DsAuthenticationLayer * getAuthenticationLayer()
        {
            return m_authentication.get();
        }

        inline DsDataLayer * getDataLayer()
        {
            return m_data.get();
        }

        inline void setForceHttps(bool https)
        {
            m_forceHttps = https;
        }

        inline void setDenyExternal(bool deny)
        {
            m_denyExternal = deny;
        }

        ///
        /// \brief createPacket vytvorí paket na základe prichádzajúcej udalosti systému QxtWeb
        /// \param event udalosť
        /// \return paket systému Darkmire
        ///
        static Darkmires::PacketPointer createPacket(const QxtWebRequestEvent * event);

    protected:
        virtual void indexRequested(QxtWebRequestEvent * event) final;
        virtual void unknownServiceRequested(QxtWebRequestEvent * event, const QString & name) final;
        virtual void pageRequestedEvent(QxtWebRequestEvent * event) final;

    private:
        DarkmiresWebServiceGet m_behaviorGET = GET_404;
        DarkmiresWebServicePost m_behaviorPOST = POST_404;

        std::shared_ptr<DsDataLayer> m_data;
        std::shared_ptr<DsAuthenticationLayer> m_authentication;

        bool m_forceHttps = false;
        bool m_denyExternal = true;

        void serviceError(const QxtWebRequestEvent * event, Darkmires::WebEventType type, const QString & message);

    public slots:
        ///
        /// \brief replySlot je slot, ktorý je posledným článkom v procese odpovede, po jeho zavolaní
        /// sa odpoveď posiela webovému serveru a následne naspäť autorovi požiadavky po sieti.
        /// \param event odpoveď
        ///
        void replySlot(DsWebEvent * event);

    private slots:
        void finalizeRequest(Darkmires::PacketPointer packet);

    signals:
        void requestSignal(Darkmires::PacketPointer packet);
};

#endif // DARKMIRESWEBSERVICE_HPP
