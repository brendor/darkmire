#ifndef DARKMIRESUSERSTORAGE_HPP
#define DARKMIRESUSERSTORAGE_HPP

#include <QObject>

#include <QMutex>

#include "darkmires.hpp"

///
/// \brief Trieda DsUserStorage predstavuje schránku obsahujúce dáta a informácie
/// o pripojených používateľoch. Môže byť volaná z viacerých vlákien súčastne, preto je nutné
/// ju pred používaním zamknúť a po skončení odomnknúť.
///
class DsUserStorage : public QObject
{
        Q_OBJECT

    public:
        ///
        /// \brief getInstance získa inštanciu tejto triedy poďľa vzoru singleton.
        /// \return pointer na inštanciu tejto triedy
        ///
        static DsUserStorage * getInstance();

        ///
        /// \brief freeInstance zmaže statickú inštanciu.
        ///
        static void freeInstance();

        ///
        /// \brief allocateInstance alokuje statickú inštanciu.
        ///
        static void allocateInstance();

        ///
        /// \brief add pridá používateľa po jeho prihlásení.
        /// \param user pointer na používateľa
        ///
        void add(const Darkmires::UserPointer & user);

        ///
        /// \brief remove odstráni prihlásenéhe používateľa s daným ID.
        /// \param user ID používateľa
        ///
        void remove(int user);

        ///
        /// \brief contains zistí, či sa používateľ s daným ID nachádza v zozname.
        /// \param id ID používateľa
        /// \return true ak sa nachádza, inak false
        ///
        bool contains(int id);

        ///
        /// \brief inSession zistí, či sa používateľ s daným ID nachádza v danej relácií.
        /// \param session identifikátor relácie
        /// \param id ID používateľa
        /// \return true ak sa nachádza, false inak
        ///
        bool inSession(const QString & session, int id);

        ///
        /// \brief inAnySession zistí, či sa daný používateľ nachádza v akejkoľvek relácii.
        /// \param id ID používateľa
        /// \return true ak áno, false inak
        ///
        bool inAnySession(int id);

        ///
        /// \brief sessionCount zistí, v koľkých reláciách sa daný používateľ nachádza.
        /// \param id ID používateľa
        /// \return počet relácií
        ///
        int sessionCount(int id);

        ///
        /// \brief isUnique zistí, či sa v zozname nachádza používateľ s rovnakým menom,
        /// alebo nie.
        /// \param name meno na overenie
        /// \return true ak nie (meno je unikátne), false ak áno
        ///
        bool isUnique(const QString & name);

        ///
        /// \brief get získa pointer na inštanciu používateľa
        /// \param id ID používateľa
        /// \return pointer na inštanciu používateľa
        ///
        DsUser * get(int id);

        ///
        /// \brief getUsers získa zoznam všetkých používateľov
        /// \return pointer na zoznam všetkých používateľov
        ///
        QList<Darkmires::UserMap::key_type> getUsers();

        ///
        /// \brief hasPrivileges zistí, či má daný používateľ dané privilégiá
        /// \param id ID používateľa
        /// \param privileges privilégiá
        /// \return true ak áno, false ak nie
        ///
        bool hasPrivileges(int id, Darkmires::UserPrivileges privileges);

        ///
        /// \brief lock zamkne vnútorný mutex, čím zabráni, aby sa volania z jedného vlákna
        /// prerývali s volaním z iného vlákna.
        ///
        void lock();

        ///
        /// \brief tryLock skúsi zamknúť vnútorný mutex, ak mutex bol už zamknutý, počká timeout
        /// sekúnd na jeho odomknutie. Ak sa dovtedy neodomkne, vráti false.
        /// \param timeout čas, počas ktorého sa má čakať
        /// \return true ak sa podarilo mutex zamknúť, inak false
        ///
        bool tryLock(int timeout);

        ///
        /// \brief unlock odomkne mutex
        ///
        void unlock();

        virtual ~DsUserStorage();

    signals:
        ///
        /// \brief usersEvent signalizuje vnútornú udalosť
        /// \param event udalosť, ktorá nastala
        ///
        void usersEvent(Darkmires::Event event);

    public slots:

        ///
        /// \brief wipe slot, ktorý zmaže všetkých pripojených používateľov
        ///
        void wipe();

    private:
        explicit DsUserStorage();

        static std::unique_ptr<DsUserStorage> m_singleton;

        Darkmires::UserMap m_users;

        QMutex m_mutex;
};

#define LockUserStorage DsUserStorage::getInstance()->lock();
#define UnlockUserStorage DsUserStorage::getInstance()->unlock();

#endif // DARKMIRESUSERSTORAGE_HPP
