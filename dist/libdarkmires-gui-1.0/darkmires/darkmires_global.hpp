#ifndef DARKMIRES_GLOBAL_HPP
#define DARKMIRES_GLOBAL_HPP

#include <QtCore/qglobal.h>

#if defined(DARKMIRES_LIBRARY)
#  define DARKMIRESSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DARKMIRESSHARED_EXPORT Q_DECL_IMPORT
#endif

#define DARKMIRES_VERSION 0x000100

#define DARKMIRES_VERSION_CHECK(major, minor, patch) ((major << 16) | (minor << 8) | (patch))

#endif // DARKMIRES_GLOBAL_HPP
