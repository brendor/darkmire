Format: 3.0 (quilt)
Source: libdarkmires-gui
Binary: libdarkmires-gui1
Architecture: any
Version: 1.0-1
Maintainer: Tomáš Kováč <brendorrmt@gmail.com>
Standards-Version: 3.9.4
Build-Depends: gcc (>= 4.7.1~), debhelper (>= 9~), libdarkmires (>= 1.0~)
Package-List: 
 libdarkmires-gui1 deb web optional
Checksums-Sha1: 
 cb7b471204152530e161cb53aade09b7bb619d18 433056 libdarkmires-gui_1.0.orig.tar.xz
 14d77543e337bf724ffb2c9df94751713e7b0232 1006 libdarkmires-gui_1.0-1.debian.tar.gz
Checksums-Sha256: 
 224ae77f8d3a9260e36ea262897d27f6f259a1f2bd73a81c9dfe392253341a3f 433056 libdarkmires-gui_1.0.orig.tar.xz
 f9b5e4e98697ca9018eaca0ddc19b4c67872c5c39af9ce178d33a12110c32bcf 1006 libdarkmires-gui_1.0-1.debian.tar.gz
Files: 
 53706334104356e2524d23feddf14e03 433056 libdarkmires-gui_1.0.orig.tar.xz
 661e04b2567a7dd8c9aeacb933f492bc 1006 libdarkmires-gui_1.0-1.debian.tar.gz
