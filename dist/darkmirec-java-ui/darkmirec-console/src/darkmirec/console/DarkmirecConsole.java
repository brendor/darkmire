package darkmirec.console;

import darkmirec.core.Darkmire;
import darkmirec.core.DarkmireClient;
import darkmirec.core.DarkmireException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public class DarkmirecConsole {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        DarkmireClient client = Darkmire.createClient();

        try
        {
            client.start("http://localhost:8000");
        } catch (DarkmireException ex)
        {
            Logger.getLogger(DarkmirecConsole.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
