package org.brendor.darkmire.netbeans.utils;

import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyledDocument;

/**
 *
 * @author brendor
 */
public class GeneratorUtils
{
    public static int getRowFirstNonWhite(StyledDocument doc, int offset) throws BadLocationException
    {
        Element lineElement = doc.getParagraphElement(offset);
        int start = lineElement.getStartOffset();
        while (start + 1 < lineElement.getEndOffset()) {
            try {
                if (doc.getText(start, 1).charAt(0) != ' ') {
                    break;
                }
            } catch (BadLocationException ex) {
                throw (BadLocationException) new BadLocationException(
                        "calling getText(" + start + ", " + (start + 1)
                        + ") on doc of length: " + doc.getLength(), start).initCause(ex);
            }
            start++;
        }
        return start;
    }
    
    
    public static int getRowMostLeftWhite(StyledDocument doc, int offset)
    {
        int current = offset;
        
        while (current > 0) {
            try {
                if (doc.getText(current - 1, 1).charAt(0) != ' ') {
                    break;
                }
            } catch (BadLocationException ex) {
                return 0;
            }
            current--;
        }
        
        return offset - current;
    }


    public static int indexOfWhite(char[] line)
    {
        int i = line.length;
        while (--i > -1) {
            final char c = line[i];
            if (Character.isWhitespace(c)) {
                return i;
            }
        }
        return -1;
    }
}
