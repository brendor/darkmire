package org.brendor.darkmire.netbeans.quickshare;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.apache.commons.lang.StringUtils;
import org.brendor.darkmire.netbeans.DarkmireTopComponent;
import org.brendor.darkmire.netbeans.utils.GeneratorUtils;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.windows.WindowManager;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class QuickShareSessionCodeCompletionItem implements CompletionItem
{

    private static String m_name = "<i>Managed Repository Block</i>";

    private static String m_type = "Darkmire";

    private static ImageIcon fieldIcon =
            new ImageIcon(ImageUtilities.loadImage("org/brendor/darkmire/netbeans/graphics/darkmire-small.png"));

    private static Color fieldColor = Color.decode("0x36001C");

    private int m_caretOffset = 0;

    public static String ELEMENT_NAME = "%%darkmire-managed-repository%%";

    public static String FRIENDLY_NAME_START = "Darkmire Code Start";

    public static String FRIENDLY_NAME_END = "Darkmire Code End";


    public QuickShareSessionCodeCompletionItem(int caretOffset)
    {
        m_caretOffset = caretOffset;
    }


    public static Color getFieldColor()
    {
        return fieldColor;
    }
    
    
    @Override
    public void defaultAction(final JTextComponent component)
    {
        final StyledDocument doc = (StyledDocument) component.getDocument();

        switch (NbEditorUtilities.getMimeType(component)) {
            case "text/x-java":
                NbDocument.runAtomic(doc, new Runnable()
                {
                    @Override
                    public void run()
                    {
                        int i = GeneratorUtils.getRowMostLeftWhite(doc, component.getCaretPosition());

                        try {
                            NbDocument.insertGuarded(doc, component.getCaretPosition(),
                                    String.format("\n// <editor-fold defaultstate=\"collapsed\" desc=\"%s\">\n"
                                    + "// %s\n"
                                    + "// </editor-fold>\n\n", FRIENDLY_NAME_START, ELEMENT_NAME));

                            doc.insertString(component.getCaretPosition(), StringUtils.leftPad("", i, ' '), null);

                            NbDocument.insertGuarded(doc, component.getCaretPosition() + 1,
                                    String.format("\n// <editor-fold defaultstate=\"collapsed\" desc=\"%s\">\n"
                                    + "// %s\n"
                                    + "// </editor-fold>\n", FRIENDLY_NAME_END, ELEMENT_NAME));

                            DarkmireTopComponent top = (DarkmireTopComponent) WindowManager.getDefault().findTopComponent(DarkmireTopComponent.COMPONENET_NAME);
                            
                            if (top.getSessionManager() instanceof QuickShareSession) {
                                QuickShareSession mr = (QuickShareSession) top.getSessionManager();
                                
                                mr.setHint(NbEditorUtilities.getTopComponent(component));
                            }
                            
                        } catch (BadLocationException ex) {
                            Exceptions.printStackTrace(ex);
                        }
                    }
                });

                break;
        }
        
        Completion.get().hideAll();
    }


    @Override
    public void processKeyEvent(KeyEvent evt)
    {
    }


    @Override
    public int getPreferredWidth(Graphics g, Font defaultFont)
    {
        return CompletionUtilities.getPreferredWidth(m_name, m_type, g, defaultFont);
    }


    @Override
    public void render(Graphics g, Font defaultFont, Color defaultColor,
            Color backgroundColor, int width, int height, boolean selected)
    {
        CompletionUtilities.renderHtml(fieldIcon, m_name, m_type, g, defaultFont,
                (selected ? Color.white : fieldColor), width, height, selected);
    }


    @Override
    public CompletionTask createDocumentationTask()
    {
        return null;
    }


    @Override
    public CompletionTask createToolTipTask()
    {
        return null;
    }


    @Override
    public boolean instantSubstitution(JTextComponent component)
    {
        return true;
    }


    @Override
    public int getSortPriority()
    {
        return -1;
    }


    @Override
    public CharSequence getSortText()
    {
        return m_name;
    }


    @Override
    public CharSequence getInsertPrefix()
    {
        return "Darkmire";
    }
}
