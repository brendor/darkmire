/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
@OptionsPanelController.ContainerRegistration(id = "Darkmire",
        categoryName = "#OptionsCategory_Name_Darkmire",
        position = 700,
        iconBase = "org/brendor/darkmire/netbeans/graphics/darkmire.png",
        keywords = "#OptionsCategory_Keywords_Darkmire",
        keywordsCategory = "Darkmire")
@NbBundle.Messages(value = {"OptionsCategory_Name_Darkmire=Darkmire", "OptionsCategory_Keywords_Darkmire=darkmire"})
package org.brendor.darkmire.netbeans;

import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;
