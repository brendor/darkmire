package org.brendor.darkmire.netbeans;

import darkmirec.sessions.DarkmireSession;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public abstract class SessionPanel extends javax.swing.JPanel {
    protected DarkmireSession m_session;

    public SessionPanel(DarkmireSession session)
    {
        m_session = session;
    }
    
    public abstract void closing();
}
