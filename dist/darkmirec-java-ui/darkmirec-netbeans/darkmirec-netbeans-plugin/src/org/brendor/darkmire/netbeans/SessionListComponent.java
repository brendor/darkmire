/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.brendor.darkmire.netbeans;

import darkmirec.sessions.DarkmireQuickShareSession;
import darkmirec.sessions.DarkmireScriptedSession;
import darkmirec.sessions.DarkmireSession;
import darkmirec.sessions.DarkmireSubversionSession;
import java.awt.Color;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class SessionListComponent extends javax.swing.JPanel {

//    class MyEditorKit extends StyledEditorKit {
//
//        @Override
//        public ViewFactory getViewFactory()
//        {
//            return new StyledViewFactory();
//        }
//        
//        class CenteredBoxView extends BoxView
//        {
//            public CenteredBoxView(Element elem, int axis) {
//
//                super(elem,axis);
//            }
//            
//            @Override
//            protected void layoutMajorAxis(int targetSpan, int axis, int[] offsets, int[] spans)
//            {
//
//                super.layoutMajorAxis(targetSpan,axis,offsets,spans);
//                int textBlockHeight = 0;
//                int offset = 0;
//
//                for (int i = 0; i < spans.length; i++) {
//
//                    textBlockHeight = spans[i];
//                }
//                offset = (targetSpan - textBlockHeight) / 2;
//                for (int i = 0; i < offsets.length; i++) {
//                    offsets[i] += offset;
//                }
//            }
//        }  
//
//        class StyledViewFactory implements ViewFactory {
//
//            @Override
//            public View create(Element elem) {
//                String kind = elem.getName();
//                if (kind != null) {
//                    if (kind.equals(AbstractDocument.ContentElementName))
//                    {
//                        return new LabelView(elem);
//                    }
//                    else if (kind.equals(AbstractDocument.ParagraphElementName))
//                    {
//                        return new ParagraphView(elem);
//                    }
//                    else if (kind.equals(AbstractDocument.SectionElementName))
//                    {
//                        return new CenteredBoxView(elem, View.Y_AXIS);
//                    }
//                    else if (kind.equals(StyleConstants.ComponentElementName))
//                    {
//                        return new ComponentView(elem);
//                    }
//                    else if (kind.equals(StyleConstants.IconElementName))
//                    {
//                        return new IconView(elem);
//                    }
//                }
//
//                return new LabelView(elem);
//            }
//
//        }
//    }
    
    private DarkmireSession m_session = null;

    SessionListComponent(DarkmireSession session)
    {
        m_session = session;
        
        initComponents();
        
        nameText.setText(session.getName());
        
        this.setToolTipText(nameText.getText());
        
        switch (session.getAuthentication())
        {
            case AUTHENTICATION_NONE:
                authLabel.setText("No authentication");
                break;
                
            case AUTHENTICATION_LDAP:
                authLabel.setText("LDAP");
                break;
                
            case AUTHENTICATION_MODERATE:
                authLabel.setText("Moderation only");
                break;
                
            case AUTHENTICATION_LOCAL_DATABASE:
                authLabel.setText("Local database");
                break;
        }
        
        if (session instanceof DarkmireQuickShareSession)
        {
            typeLabel.setText("QuickShare");
        }
        else if (session instanceof DarkmireSubversionSession)
        {
            typeLabel.setText("Subversion");
        }
        else if (session instanceof DarkmireScriptedSession)
        {
            typeLabel.setText("Scripted");
            
            DarkmireScriptedSession scripted = (DarkmireScriptedSession)session;
            
            if (! scripted.getCustomName().isEmpty())
            {
                typeLabel.setText(scripted.getCustomName());
            }
        }
        else
        {
            typeLabel.setText("Unknown");
        }
        
        if (session.isOwner())
        {
            ownerIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Wheel-icon.png")));
            ownerIcon.setToolTipText("Owner");
        }
        
        nameText.setBackground(new Color(0, 0, 0, 0));
        namePanel.setBackground(new Color(0, 0, 0, 0));
        
        
        StyledDocument doc = nameText.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
        
        namePanel.setBorder(new EmptyBorder(5, 0, 5, 0));
    }

    public void setFocused(boolean focused)
    {
        setBackground(focused ? Color.lightGray : Color.white);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        ownerIcon = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        namePanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        nameText = new javax.swing.JTextPane();
        typeLabel = new javax.swing.JLabel();
        authLabel = new javax.swing.JLabel();

        org.openide.awt.Mnemonics.setLocalizedText(ownerIcon, org.openide.util.NbBundle.getMessage(SessionListComponent.class, "SessionListComponent.ownerIcon.text")); // NOI18N
        ownerIcon.setMaximumSize(new java.awt.Dimension(16, 16));
        ownerIcon.setMinimumSize(new java.awt.Dimension(16, 16));
        ownerIcon.setPreferredSize(new java.awt.Dimension(16, 16));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        namePanel.setBorder(null);

        jScrollPane2.setBorder(null);
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        jScrollPane2.setViewportBorder(null);

        nameText.setEditable(false);
        nameText.setBorder(null);
        nameText.setFont(new java.awt.Font("DejaVu Sans Condensed", 1, 12)); // NOI18N
        nameText.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jScrollPane2.setViewportView(nameText);

        javax.swing.GroupLayout namePanelLayout = new javax.swing.GroupLayout(namePanel);
        namePanel.setLayout(namePanelLayout);
        namePanelLayout.setHorizontalGroup(
            namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 109, Short.MAX_VALUE)
            .addGroup(namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE))
        );
        namePanelLayout.setVerticalGroup(
            namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
            .addGroup(namePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
        );

        jPanel1.add(namePanel);

        typeLabel.setForeground(new java.awt.Color(130, 130, 130));
        typeLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        org.openide.awt.Mnemonics.setLocalizedText(typeLabel, org.openide.util.NbBundle.getMessage(SessionListComponent.class, "SessionListComponent.typeLabel.text")); // NOI18N
        jPanel1.add(typeLabel);

        authLabel.setForeground(new java.awt.Color(130, 130, 130));
        authLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        org.openide.awt.Mnemonics.setLocalizedText(authLabel, org.openide.util.NbBundle.getMessage(SessionListComponent.class, "SessionListComponent.authLabel.text")); // NOI18N
        jPanel1.add(authLabel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ownerIcon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ownerIcon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel authLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel namePanel;
    private javax.swing.JTextPane nameText;
    private javax.swing.JLabel ownerIcon;
    private javax.swing.JLabel typeLabel;
    // End of variables declaration//GEN-END:variables
}
