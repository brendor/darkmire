package org.brendor.darkmire.netbeans.subversion;

import darkmirec.core.DarkmireException;
import darkmirec.events.DarkmireDataEvent;
import darkmirec.events.DarkmireDisconnectEvent;
import darkmirec.events.DarkmireEvent;
import darkmirec.sessions.DarkmireSubversionSession;
import darkmirec.utils.DarkmireEventHandler;
import darkmirec.utils.IDarkmireLogger;
import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListCellRenderer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.brendor.darkmire.netbeans.DarkmireTopComponent;
import org.brendor.darkmire.netbeans.FilteredListModel;
import org.brendor.darkmire.netbeans.FolderProjectFactory;
import org.brendor.darkmire.netbeans.JSearchTextField;
import org.brendor.darkmire.netbeans.SessionPanel;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;
import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.ISVNEventHandler;
import org.tmatesoft.svn.core.wc.SVNEvent;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc.admin.ISVNAdminEventHandler;
import org.tmatesoft.svn.core.wc.admin.SVNAdminEvent;
import org.tmatesoft.svn.core.wc2.ISvnOperationHandler;
import org.tmatesoft.svn.core.wc2.SvnCheckout;
import org.tmatesoft.svn.core.wc2.SvnOperation;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;
import org.tmatesoft.svn.core.wc2.SvnTarget;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class SubversionSessionAdmin extends SessionPanel implements DarkmireEventHandler
{
    public static final String SVN_ADMIN_REPO_PATH = "svn-admin-repo-path";
    
    private int m_port = SubversionSession.SUBVERSION_DEFAULT_PORT;
    
    private void logConsole(String message, IDarkmireLogger.LogPriority prio)
    {
        StyledDocument sd = subversionConsole.getStyledDocument();
        
        SimpleAttributeSet timeAttribs = new SimpleAttributeSet();
        StyleConstants.setForeground(timeAttribs, Color.YELLOW);
        StyleConstants.setBold(timeAttribs, true);
        
        try
        {
            sd.insertString(sd.getLength(), String.format("%s: ",  new Date().toString()), timeAttribs);
        }
        catch (BadLocationException ex)
        {
            Exceptions.printStackTrace(ex);
        }
        
        
        SimpleAttributeSet attribs = null;
        
        switch (prio)
        {
            case ERROR:
                attribs = new SimpleAttributeSet();
                StyleConstants.setForeground(attribs, Color.RED);
                StyleConstants.setBold(attribs, true);
                break;
                
            default:
                break;
        }
        
        try
        {
            sd.insertString(sd.getLength(), String.format("%s\n", message), attribs);
        }
        catch (BadLocationException ex)
        {
            Exceptions.printStackTrace(ex);
        }
    }
    
    
    private final ISVNEventHandler m_commonEventHandler = new ISVNAdminEventHandler()
    {
        @Override
        public void handleAdminEvent(SVNAdminEvent svnae, double d) throws SVNException
        {
            logConsole("Admin event: " + svnae.getMessage(), IDarkmireLogger.LogPriority.INFO);
        }

        @Override
        public void handleEvent(SVNEvent svne, double d) throws SVNException
        {
            logConsole("Event: " + svne.toString(), IDarkmireLogger.LogPriority.INFO);
        }

        @Override
        public void checkCancelled() throws SVNCancelException
        {
            
        }
    };
    
    private final ISvnOperationHandler m_commonOperationHandler = new ISvnOperationHandler()
    {
        @Override
        public void beforeOperation(SvnOperation<?> so) throws SVNException
        {
            logConsole("Before: " + so.toString(), IDarkmireLogger.LogPriority.INFO);
        }


        @Override
        public void afterOperationSuccess(SvnOperation<?> so) throws SVNException
        {
            logConsole("Success: " + so.toString(), IDarkmireLogger.LogPriority.INFO);
        }


        @Override
        public void afterOperationFailure(SvnOperation<?> so)
        {
            logConsole("Failure: " + so.toString(), IDarkmireLogger.LogPriority.ERROR);
        }
    };
    
    
    private final FilteredListModel.Filter m_filter = new FilteredListModel.Filter()
    {
        @Override
        public boolean accept(Object element)
        {
            if (searchField.getText().isEmpty())
            {
                return true;
            }
            
            Repository repo = (Repository)element;
            
            return repo.getAuthorName().startsWith(searchField.getText());
        }
    };
    
    private final DocumentListener m_listener = new DocumentListener()
    {
        @Override
        public void insertUpdate(DocumentEvent e)
        {
            update();
        }


        @Override
        public void removeUpdate(DocumentEvent e)
        {
            update();
        }


        @Override
        public void changedUpdate(DocumentEvent e)
        {
            update();
        }
        
        private void update()
        {
            ((FilteredListModel)repositoryList.getModel()).filter();
        }
    };


    @Override
    public void closing()
    {
        DarkmireTopComponent.get().preferences().put(SVN_ADMIN_REPO_PATH, repoRootPathField.getText());
    }
    
    private class RepositoryListRenderer implements ListCellRenderer<Repository>
    {
        @Override
        public Component getListCellRendererComponent(JList<? extends Repository> list,
                Repository value,
                int index,
                boolean isSelected,
                boolean cellHasFocus)
        {
            RepositoryListComponent component = new RepositoryListComponent(value);

            component.setFocused(cellHasFocus);

            return component;
        }

    }
    
    final SvnOperationFactory m_svnOperationFactory = new SvnOperationFactory();
    
    final FolderProjectFactory m_folderProjectFactory = new FolderProjectFactory();
    
    /**
     * Creates new form QuickShareSession
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public SubversionSessionAdmin(DarkmireSubversionSession session)
    {
        super(session);
        
        m_svnOperationFactory.setEventHandler(m_commonEventHandler);
        m_svnOperationFactory.setOperationHandler(m_commonOperationHandler);
                
        System.out.println("Subversion Admin session init: " + session.getName());
        
        initComponents();

        infoLabel.setText(m_session.getId());
        
        refreshListRequest();
        
        repositoryList.setCellRenderer(new RepositoryListRenderer());
        
        JSearchTextField sf = (JSearchTextField)searchField;
        sf.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Search16.png")));
        sf.setTextWhenNotFocused("Search...");
        sf.getDocument().addDocumentListener(m_listener);
        
        repoRootPathField.setText(DarkmireTopComponent.get().preferences().get(SVN_ADMIN_REPO_PATH, ""));
        
        repositoryListValueChanged(null);
    }
    
    
    private void refreshListRequest()
    {
        DarkmireTopComponent top = (DarkmireTopComponent) WindowManager.getDefault().findTopComponent(DarkmireTopComponent.COMPONENET_NAME);

        JSONObject data = new JSONObject();

        data.put("type", DarkmireSubversionSession.RequestType.REQUEST_LIST.code());

        try
        {
            top.enqueue(new DarkmireDataEvent(m_session, data), this);
        }
        catch (DarkmireException ex)
        {
            Exceptions.printStackTrace(ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        infoLabel = new javax.swing.JLabel();
        disconnectButton = new javax.swing.JButton();
        mainPanel = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        repositoryList = new javax.swing.JList();
        searchPanel = new javax.swing.JPanel();
        searchField = new JSearchTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        subversionConsole = new javax.swing.JTextPane();
        jPanel3 = new javax.swing.JPanel();
        changeRepoPathButton = new javax.swing.JButton();
        repoRootPathField = new org.jdesktop.xswingx.JXTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        nameField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        passwordField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        authorField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        pathField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        controlPanel = new javax.swing.JPanel();
        checkoutButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        openButton = new javax.swing.JButton();
        refreshButton = new javax.swing.JButton();

        setBorder(null);

        infoLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 14)); // NOI18N
        infoLabel.setForeground(new java.awt.Color(12, 115, 28));
        org.openide.awt.Mnemonics.setLocalizedText(infoLabel, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.infoLabel.text")); // NOI18N
        infoLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        disconnectButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Login-out-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(disconnectButton, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.disconnectButton.text")); // NOI18N
        disconnectButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                disconnectButtonActionPerformed(evt);
            }
        });

        mainPanel.setLayout(new javax.swing.BoxLayout(mainPanel, javax.swing.BoxLayout.LINE_AXIS));

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(450);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        repositoryList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                repositoryListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(repositoryList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 212;
        gridBagConstraints.ipady = 356;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel10.add(jScrollPane1, gridBagConstraints);

        searchPanel.setPreferredSize(new java.awt.Dimension(258, 25));
        searchPanel.setLayout(new java.awt.GridBagLayout());

        searchField.setText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.searchField.text")); // NOI18N
        searchField.setMinimumSize(new java.awt.Dimension(250, 25));
        searchField.setPreferredSize(new java.awt.Dimension(250, 25));
        searchPanel.add(searchField, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        jPanel10.add(searchPanel, gridBagConstraints);

        jSplitPane1.setLeftComponent(jPanel10);

        jScrollPane3.setBorder(null);
        jScrollPane3.setViewportBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel1.setBorder(null);

        subversionConsole.setEditable(false);
        subversionConsole.setBackground(new java.awt.Color(1, 24, 111));
        subversionConsole.setForeground(new java.awt.Color(2, 237, 50));
        subversionConsole.setText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.subversionConsole.text")); // NOI18N
        jScrollPane2.setViewportView(subversionConsole);

        changeRepoPathButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Folder16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(changeRepoPathButton, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.changeRepoPathButton.text")); // NOI18N
        changeRepoPathButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                changeRepoPathButtonActionPerformed(evt);
            }
        });

        repoRootPathField.setText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.repoRootPathField.text")); // NOI18N
        repoRootPathField.setToolTipText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.repoRootPathField.toolTipText")); // NOI18N
        repoRootPathField.setPrompt(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.repoRootPathField.prompt")); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(repoRootPathField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(changeRepoPathButton))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(changeRepoPathButton)
                .addComponent(repoRootPathField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel2.setLayout(new java.awt.BorderLayout(4, 4));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.jPanel5.border.title"))); // NOI18N

        nameField.setEditable(false);
        nameField.setText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.nameField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.jLabel1.text")); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nameField))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel1))
        );

        passwordField.setEditable(false);
        passwordField.setText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.passwordField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.jLabel2.text")); // NOI18N
        jLabel2.setMaximumSize(new java.awt.Dimension(36, 15));
        jLabel2.setMinimumSize(new java.awt.Dimension(36, 15));
        jLabel2.setPreferredSize(new java.awt.Dimension(36, 15));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(passwordField))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        authorField.setEditable(false);
        authorField.setText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.authorField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.jLabel3.text")); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(authorField))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(authorField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel3))
        );

        pathField.setEditable(false);
        pathField.setText(org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.pathField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.jLabel4.text")); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pathField))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(pathField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel4))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.add(jPanel5, java.awt.BorderLayout.PAGE_START);

        controlPanel.setBorder(null);
        controlPanel.setLayout(new java.awt.GridBagLayout());

        checkoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Folder_add16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(checkoutButton, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.checkoutButton.text")); // NOI18N
        checkoutButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                checkoutButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        controlPanel.add(checkoutButton, gridBagConstraints);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        controlPanel.add(jSeparator1, gridBagConstraints);

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/File_add16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(openButton, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.openButton.text")); // NOI18N
        openButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                openButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        controlPanel.add(openButton, gridBagConstraints);

        jPanel2.add(controlPanel, java.awt.BorderLayout.PAGE_END);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jScrollPane3.setViewportView(jPanel1);

        jSplitPane1.setRightComponent(jScrollPane3);

        mainPanel.add(jSplitPane1);

        refreshButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Refresh16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(refreshButton, org.openide.util.NbBundle.getMessage(SubversionSessionAdmin.class, "SubversionSessionAdmin.refreshButton.text")); // NOI18N
        refreshButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                refreshButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(disconnectButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(infoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(refreshButton))
                    .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1027, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(disconnectButton)
                    .addComponent(infoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refreshButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void disconnectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_disconnectButtonActionPerformed
    {//GEN-HEADEREND:event_disconnectButtonActionPerformed
        DarkmireTopComponent top = DarkmireTopComponent.get();

        try
        {
            top.enqueue(new DarkmireDisconnectEvent(m_session), this);
        }
        catch (DarkmireException ex)
        {
            System.out.println(ex.getMessage());
        }
    }//GEN-LAST:event_disconnectButtonActionPerformed

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_refreshButtonActionPerformed
    {//GEN-HEADEREND:event_refreshButtonActionPerformed
        refreshListRequest();
    }//GEN-LAST:event_refreshButtonActionPerformed

    private void changeRepoPathButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_changeRepoPathButtonActionPerformed
    {//GEN-HEADEREND:event_changeRepoPathButtonActionPerformed
        JFileChooser chooseDir = new JFileChooser();
        
        chooseDir.setMultiSelectionEnabled(false);
        chooseDir.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        
        if (chooseDir.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            File f = chooseDir.getSelectedFile();
            
            if (! f.exists() || ! f.canWrite() || ! f.canRead())
            {
                NotifyDescriptor nd = new NotifyDescriptor.Message("Invalid directory selected.",
                                                                   NotifyDescriptor.INFORMATION_MESSAGE);
                DialogDisplayer.getDefault().notify(nd);
                
                return;
            }
            
            repoRootPathField.setText(f.getAbsolutePath());
            
            DarkmireTopComponent.get().preferences().put(SVN_ADMIN_REPO_PATH, repoRootPathField.getText());
        }
    }//GEN-LAST:event_changeRepoPathButtonActionPerformed

    private void repositoryListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_repositoryListValueChanged
    {//GEN-HEADEREND:event_repositoryListValueChanged

        if (repositoryList.getSelectedValue() == null)
        {
            controlPanel.setEnabled(false);
            checkoutButton.setEnabled(false);
            openButton.setEnabled(false);
        }
        else
        {
            controlPanel.setEnabled(true);
            
            Repository repo = (Repository)repositoryList.getSelectedValue();
            
            updateRepositoryInfo(repo);
            
            File f = fileForRepository(repo);
        
            checkoutButton.setEnabled(! SVNWCUtil.isVersionedDirectory(f));
            openButton.setEnabled(! checkoutButton.isEnabled());
        }
        
    }//GEN-LAST:event_repositoryListValueChanged

    private void checkoutButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_checkoutButtonActionPerformed
    {//GEN-HEADEREND:event_checkoutButtonActionPerformed
        
        if (repositoryList.getSelectedValue() == null)
        {
            JOptionPane.showMessageDialog(this, "You must choose a repository to checkout!", "Darkmire", JOptionPane.WARNING_MESSAGE);
            
            return;
        }
        
        if (repoRootPathField.getText().isEmpty())
        {
            changeRepoPathButtonActionPerformed(null);
        }
        
        Repository repo = ((Repository)repositoryList.getSelectedValue());
        
        File f = fileForRepository(repo);
        
        if (SVNWCUtil.isVersionedDirectory(f))
        {
            JOptionPane.showMessageDialog(this, "Directory already under version control.", "Darkmire", JOptionPane.WARNING_MESSAGE);

            return;
        }

        URI dserver = DarkmireTopComponent.get().getServerAddress();

        try
        {
            final SvnCheckout checkout = m_svnOperationFactory.createCheckout();
            
            SVNURL url = SVNURL.create("svn", dserver.getUserInfo(), dserver.getHost(), m_port, repo.getPath(), true);

            checkout.setSingleTarget(SvnTarget.fromFile(f));
            checkout.setSource(SvnTarget.fromURL(url));
            checkout.setDepth(SVNDepth.INFINITY);
            //... other options
            checkout.run();
        }
        catch (SVNException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getErrorMessage(), "Darkmire", JOptionPane.ERROR_MESSAGE);
        }
        finally
        {
            m_svnOperationFactory.dispose();
        }
        
    }//GEN-LAST:event_checkoutButtonActionPerformed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_openButtonActionPerformed
    {//GEN-HEADEREND:event_openButtonActionPerformed
        
        if (repositoryList.getSelectedValue() == null)
        {
            return;
        }
        
        Repository repo = ((Repository)repositoryList.getSelectedValue());
        
        File f = fileForRepository(repo);
        
        if (f == null)
        {
            JOptionPane.showMessageDialog(this, "Repository format invalid.", "Darkmire", JOptionPane.WARNING_MESSAGE);
            
            return;
        }
        
        File listDir[];
        listDir = f.listFiles(new FileFilter()
        {
            @Override
            public boolean accept(File pathname)
            {
                FileObject fileObject = FileUtil.toFileObject(FileUtil.normalizeFile(pathname));
                
                boolean accept;
                
                try
                {
                    accept = m_folderProjectFactory.isProject(fileObject);
                }
                catch (IllegalArgumentException ex)
                {
                    return false;
                }
                
                return accept;
            }
        });
        
        Project[] array = new Project[listDir.length];
        
        int c = 0;
        
        for (File file : listDir)
        {
            FileObject fileObject = FileUtil.toFileObject(FileUtil.normalizeFile(file));
            
            Project p = null;

            try
            {
                p = m_folderProjectFactory.loadProject(fileObject, null);
            }
            catch (IOException ex)
            {
                JOptionPane.showMessageDialog(this, "Unable to open project: " + ex.getMessage(), "Darkmire", JOptionPane.ERROR_MESSAGE);
            }

            array[c] = p;
            
            c++;
        }
        
        OpenProjects.getDefault().open(array, true);
        
    }//GEN-LAST:event_openButtonActionPerformed

    private void updateRepositoryInfo(Repository repo)
    {
        nameField.setText(repo.getName());
        passwordField.setText(repo.getPassword());
        pathField.setText(repo.getPath());
        authorField.setText(repo.getAuthorUuid());
    }
    
    private File fileForRepository(Repository repo)
    {
        File f = new File(repoRootPathField.getText(),
                String.format("%s-%s",
                        repo.getAuthorName().isEmpty() ? "anonymous" : repo.getAuthorName(),
                        repo.getName()));
        
        if (! f.isDirectory()) return null;
        
        return f;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField authorField;
    private javax.swing.JButton changeRepoPathButton;
    private javax.swing.JButton checkoutButton;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JButton disconnectButton;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JTextField nameField;
    private javax.swing.JButton openButton;
    private javax.swing.JTextField passwordField;
    private javax.swing.JTextField pathField;
    private javax.swing.JButton refreshButton;
    private org.jdesktop.xswingx.JXTextField repoRootPathField;
    private javax.swing.JList repositoryList;
    private javax.swing.JTextField searchField;
    private javax.swing.JPanel searchPanel;
    private javax.swing.JTextPane subversionConsole;
    // End of variables declaration//GEN-END:variables


    @Override
    public void event(DarkmireEvent event)
    {
        System.out.println("Subversion Admin event: " + event.getClass().getName());
        
        if (event instanceof DarkmireDataEvent)
        {
            DarkmireDataEvent dataEvent = (DarkmireDataEvent) event;
            
            switch (event.getStatus())
            {
                case WEB_EVENT_SESSION_SUCCESS:
                {
                    if (dataEvent.getData().containsKey("repositories"))
                    {
                        JSONArray repos = dataEvent.getData().getJSONArray("repositories");
                        
                        DefaultListModel<Repository> model = new DefaultListModel<>();
                        
                        for (int i = 0; i < repos.size(); i++)
                        {
                            JSONObject r = repos.optJSONObject(i);
                            
                            if (r.isNullObject())
                            {
                                continue;
                            }
                            
                            Repository repo = new Repository(r);
                            
                            model.addElement(repo);
                        }
                        
                        FilteredListModel filteredModel = new FilteredListModel(model);
                        
                        filteredModel.setFilter(m_filter);
                        
                        repositoryList.setModel(filteredModel);
                    }
                    
                    m_port = dataEvent.getData().optInt("server-port", SubversionSession.SUBVERSION_DEFAULT_PORT);
                    
                    break;
                }
            }
        }
    }
}
