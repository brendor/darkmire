package org.brendor.darkmire.netbeans;

import darkmirec.core.Darkmire;
import darkmirec.core.DarkmireClient;
import static darkmirec.core.DarkmireClient.LOG_NAME;
import darkmirec.core.DarkmireException;
import darkmirec.events.DarkmireAuthenticationEvent;
import darkmirec.events.DarkmireChatEvent;
import darkmirec.events.DarkmireDisconnectEvent;
import darkmirec.events.DarkmireErrorEvent;
import darkmirec.events.DarkmireEvent;
import static darkmirec.events.DarkmireEvent.EventStatus.WEB_EVENT_AUTHENTICATION_ERROR;
import static darkmirec.events.DarkmireEvent.EventStatus.WEB_EVENT_AUTHENTICATION_SUCCESS;
import darkmirec.events.DarkmireSessionListEvent;
import darkmirec.events.DarkmireUserEvent;
import darkmirec.sessions.DarkmireQuickShareSession;
import darkmirec.sessions.DarkmireScriptedSession;
import darkmirec.sessions.DarkmireSession;
import darkmirec.sessions.DarkmireSubversionSession;
import darkmirec.utils.DarkmireDateTime;
import darkmirec.utils.DarkmireEventHandler;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;
import org.brendor.darkmire.netbeans.quickshare.QuickShareSession;
import org.brendor.darkmire.netbeans.quickshare.QuickShareSessionAdmin;
import org.brendor.darkmire.netbeans.scripted.ScriptedSession;
import org.brendor.darkmire.netbeans.subversion.SubversionSession;
import org.brendor.darkmire.netbeans.subversion.SubversionSessionAdmin;
import org.brendor.darkmire.netbeans.utils.ListAction;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
        dtd = "-//org.brendor.darkmire.netbeans//Darkmire//EN",
        autostore = false)
@TopComponent.Description(
        preferredID = "DarkmireTopComponent",
        iconBase = "org/brendor/darkmire/netbeans/graphics/darkmire-small.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "editor", openAtStartup = true, position = -100)
@ActionID(category = "Window", id = "org.brendor.darkmire.netbeans.DarkmireTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_DarkmireAction",
        preferredID = "DarkmireTopComponent")
@Messages(
        {
    "CTL_DarkmireAction=Darkmire",
    "CTL_DarkmireTopComponent=Darkmire",
    "HINT_DarkmireTopComponent=This is the Darkmire"
})
public final class DarkmireTopComponent extends TopComponent {

    private class SessionListRenderer implements ListCellRenderer<DarkmireSession>
    {
        @Override
        public Component getListCellRendererComponent(JList<? extends DarkmireSession> list, 
                DarkmireSession value, int index, boolean isSelected, boolean cellHasFocus)
        {
            SessionListComponent component = new SessionListComponent(value);

            component.setFocused(cellHasFocus);

            return component;
        }
    }
    
    private final DarkmireClient m_client = Darkmire.createClient();

    public static String LOGIN_USERNAME = "LOGIN_USERNAME";
    
    private Preferences m_preferences = null;

    private static final RequestProcessor rp = new RequestProcessor("event-listener", 1, true);

    private Image m_background;

    private long m_previousChatCheck;

    private int m_previousChatId = 0;
    
    private SessionPanel m_sessionManagePanel = null;

    private final int POLL_EVENT_TIMEOUT_MS = 1000;
    
    public static String COMPONENET_NAME = "DarkmireTopComponent";
    
    public static DarkmireTopComponent get()
    {
        return (DarkmireTopComponent) WindowManager.getDefault().findTopComponent(DarkmireTopComponent.COMPONENET_NAME);
    }
    
    public Preferences preferences()
    {
        return m_preferences;
    }

    public class DarkmireEventListener implements Runnable {

        private final RequestProcessor.Task m_task = rp.create(this);

        private int m_refreshCounter = 0;

        public static final int REFRESH_THRESHOLD = 3;


        public DarkmireEventListener()
        {
            System.out.println("Event listener created...");

            m_task.schedule(0);
        }


        @Override
        public void run()
        {
            assert !EventQueue.isDispatchThread();
            
            System.out.println(String.format("Waiting for events... (thread: %s)", Thread.currentThread().getName()));
            
            if (! m_client.isLogged())
            {
                showAdminPanel(false);
            }

            try {
                @SuppressWarnings("UnusedAssignment")
                 DarkmireEvent event = null;
                while ((event = m_client.dequeueEvent(POLL_EVENT_TIMEOUT_MS)) != null) {

                    if (m_client.getRTT().size() > 0)
                    {
                        latencyField.setText(String.format("%.2f", m_client.getRTT().first() / 1000000.0));
                    }
                    else
                    {
                        latencyField.setText("0");
                    }
                    
                    System.out.println(String.format("Incoming event: %s %s", event.getClass().toString(), event.getStatus().toString()));

                    if (event.getObject() != null) {
                        
                        final String error = event.getErrorMessage();
                        
                        if (error != null)
                        {
                            if (! error.isEmpty()) {
                                EventQueue.invokeLater(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        setErrorText(error);
                                    }
                                });
                            }
                        }
                        
                        try {
                            event.getObject().event(event);
                        } catch (NullPointerException ex) {
                            System.out.println("Event sender has been destroyed");
                        }
                        
                    } else {
                        if (event instanceof DarkmireErrorEvent)
                        {
                            final DarkmireErrorEvent darkmireErrorEvent = (DarkmireErrorEvent) event;

                            if (darkmireErrorEvent.getOrigin() != null)
                            {
                                if (darkmireErrorEvent.getOrigin() instanceof DarkmireSessionListEvent)
                                {
                                    EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run()
                                        {
                                            setErrorText(String.format("Error retrieving session list [%s]", darkmireErrorEvent.getErrorMessage()));
                                        }
                                    });
                                } else {
                                    EventQueue.invokeLater(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            setErrorText(darkmireErrorEvent.getErrorMessage());

                                            loginProgressBar.setIndeterminate(false);
                                        }
                                    });
                                }

                                EventQueue.invokeLater(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        loginProgressBar.setIndeterminate(false);
                                        adminProgressBar.setIndeterminate(false);
                                        
                                        sessionList.setModel(new DefaultListModel<DarkmireSession>());
                                    }
                                });

                            }
                            else
                            {
                                EventQueue.invokeLater(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        setErrorText(darkmireErrorEvent.getErrorMessage());
                                    }
                                });
                            }
                        }
                        else if (event instanceof DarkmireSessionListEvent)
                        {
                            final DarkmireSessionListEvent darkmireSessionListEvent = (DarkmireSessionListEvent) event;

                            EventQueue.invokeLater(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    DefaultListModel<DarkmireSession> model = new DefaultListModel<>();

                                    Iterator<Map.Entry<String, DarkmireSession>> iterator = darkmireSessionListEvent.getSessions().entrySet().iterator();
                                    
                                    while (iterator.hasNext())
                                    {
                                        Map.Entry<String, DarkmireSession> next = iterator.next();

                                        model.addElement(next.getValue());
                                    }

                                    FilteredListModel filteredModel = new FilteredListModel(model);
                        
                                    filteredModel.setFilter(m_filter);
                                    
                                    sessionList.setModel(filteredModel);
                                    
                                    loginProgressBar.setIndeterminate(false);
                                }
                            });
                        }
                        else if (event instanceof DarkmireAuthenticationEvent)
                        {
                            final DarkmireAuthenticationEvent darkmireAuthenticationEvent = (DarkmireAuthenticationEvent) event;

                            switch (darkmireAuthenticationEvent.getStatus())
                            {
                                case WEB_EVENT_AUTHENTICATION_ERROR:
                                    EventQueue.invokeLater(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            setErrorText(darkmireAuthenticationEvent.getErrorMessage());
                                        }
                                    });
                                    break;
                                    
                                case WEB_EVENT_AUTHENTICATION_ALREADY_AUTHENTICATED:
                                case WEB_EVENT_AUTHENTICATION_SUCCESS:
                                    EventQueue.invokeLater(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            CardLayout settingsLayout = (CardLayout) (settingsPanel.getLayout());
                                
                                            if (m_client.isAdmin() || m_client.isModerator())
                                            {
                                                settingsLayout.show(settingsPanel, "admin");
                                            }
                                            else
                                            {
                                                settingsLayout.show(settingsPanel, "regular");
                                            }
                                            
                                            refreshButtonActionPerformed(null);

                                            DarkmireSession session = m_client.getSession(0);
                                            
                                            if (session != null)
                                            {
                                                CardLayout scl = (CardLayout) (sessionsPane.getLayout());
                                                
                                                if (m_sessionManagePanel != null)
                                                {                                                    
                                                    m_sessionManagePanel.closing();
                                                    sessionsPane.remove(sessionManagePanel);
                                                    m_sessionManagePanel = null;
                                                    
                                                    refreshButtonActionPerformed(null);
                                                }
                                                
                                                if (session instanceof DarkmireQuickShareSession)
                                                {
                                                    if (session.isOwner())
                                                    {
                                                        sessionManagePanel = new QuickShareSessionAdmin((DarkmireQuickShareSession) session);
                                                    }
                                                    else
                                                    {
                                                        sessionManagePanel = new QuickShareSession((DarkmireQuickShareSession) session);
                                                    }
                                                    
                                                    
                                                    sessionManagePanel.setVisible(true);
                                                }
                                                else if (session instanceof DarkmireSubversionSession)
                                                {
                                                    if (session.isOwner())
                                                    {
                                                        sessionManagePanel = new SubversionSessionAdmin((DarkmireSubversionSession) session);
                                                    }
                                                    else
                                                    {
                                                        sessionManagePanel = new SubversionSession((DarkmireSubversionSession) session);
                                                    }
                                                    
                                                    sessionManagePanel.setVisible(true);
                                                }
                                                else if (session instanceof DarkmireScriptedSession)
                                                {
                                                    if (session.isOwner())
                                                    {
                                                        
                                                    }
                                                    else
                                                    {
                                                        sessionManagePanel = new ScriptedSession((DarkmireScriptedSession) session);
                                                    }
                                                    
                                                    sessionManagePanel.setVisible(true);
                                                }
                                                else
                                                {
                                                    System.out.println("Unknown session type: " + session.getName());
                                                }
                                                
                                                sessionsPane.add("session", sessionManagePanel);
                                                
                                                m_sessionManagePanel = (SessionPanel) sessionManagePanel;
                                            }

                                        }
                                    });
                                    break;
                            }

                            EventQueue.invokeLater(new Runnable() {
                                @Override
                                public void run()
                                {
                                    loginProgressBar.setIndeterminate(false);
                                    adminProgressBar.setIndeterminate(false);
                                }
                            });

                        } else if (event instanceof DarkmireChatEvent) {
                            final DarkmireChatEvent darkmireChatEvent = (DarkmireChatEvent) event;
                            
                            if (darkmireChatEvent.getStatus().isError())
                            {
                                EventQueue.invokeLater(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        setErrorText("Chat error: " + darkmireChatEvent.getErrorMessage());
                                    }
                                });
                                
                                continue;
                            }

                            EventQueue.invokeLater(new Runnable() {
                                @Override
                                public void run()
                                {
                                    for (Entry<Integer, DarkmireChatEvent.DarkmireChatMessage> message : darkmireChatEvent.getServerMessages().entrySet())
                                    {

                                        if (message.getKey() > m_previousChatId) {
                                            m_previousChatId = message.getKey();
                                        }

                                        StyledDocument doc = chatArea.getStyledDocument();
                                        try {
                                            doc.insertString(doc.getLength(), String.format("(%d) %s\n", message.getKey(), message.getValue().toString()), doc.getStyle("regular"));
                                        } catch (BadLocationException ex) {
                                            System.out.println(ex.getMessage());
                                        }
                                    }

                                    if (darkmireChatEvent.getChatMessage() != null) {
                                        StyledDocument doc = chatArea.getStyledDocument();
                                        try {
                                            doc.insertString(doc.getLength(), String.format("(-) %s\n", darkmireChatEvent.getChatMessage().toString()), doc.getStyle("regular"));
                                        } catch (BadLocationException ex) {
                                            System.out.println(ex.getMessage());
                                        }
                                    }
                                }
                            });
                        } else if (event instanceof DarkmireDisconnectEvent) {
                            final DarkmireDisconnectEvent darkmireDisconnectEvent = (DarkmireDisconnectEvent) event;

                            EventQueue.invokeLater(new Runnable() {
                                @Override
                                public void run()
                                {
                                    switch (darkmireDisconnectEvent.getStatus())
                                    {
                                        case WEB_EVENT_AUTHENTICATION_ERROR:
                                            if (darkmireDisconnectEvent.getErrorMessage() != null)
                                            {
                                                setErrorText("Disconnect error: " + darkmireDisconnectEvent.getErrorMessage());
                                            }
                                            else
                                            {
                                                setErrorText("Error disconnecting");
                                            }
                                            break;
                                            
                                        default:
                                            if (darkmireDisconnectEvent.getReason() == null) {
                                                if (darkmireDisconnectEvent.getSession() != null) {
                                                    setErrorText("Disconnected from session");
                                                } else {
                                                    setErrorText("Disconnected");
                                                }
                                            } else {
                                                setErrorText("Disconnected: " + darkmireDisconnectEvent.getReason());
                                            }
                                            break;
                                    }
                                }
                            });
                        }
                        else
                        {
                            final DarkmireEvent e = event;
                            
                            EventQueue.invokeLater(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    switch (e.getStatus())
                                    {
                                        case WEB_EVENT_AUTHENTICATION_ERROR:
                                        {
                                            if (e.getErrorMessage() != null)
                                            {
                                                setErrorText(e.getErrorMessage());
                                            }
                                            break;
                                        }
                                    }
                                }
                            });
                        }
                    }

                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run()
                        {
                            CardLayout scl = (CardLayout) (sessionsPane.getLayout());

                            if (m_client.getSession(0) != null) {
                                loginButton.setText("Disconnect");
                                sessionList.setEnabled(false);

                                scl.show(sessionsPane, "session");
                                
                            } else {
                                loginButton.setText("Connect");
                                sessionList.setEnabled(true);

                                scl.show(sessionsPane, "login");
                                
                                if (m_sessionManagePanel != null)
                                {
                                    m_sessionManagePanel.closing();
                                    m_sessionManagePanel = null;
                                    
                                    sessionsPane.remove(sessionManagePanel);
                                    
                                    refreshButtonActionPerformed(null);
                                }
                                
                                m_previousChatId = 0;
                            }

                            showAdminPanel(m_client.isLogged());
                        }
                    });

                    if (Thread.interrupted()) {
                        return;
                    }
                }
            } catch (DarkmireException ex) {
                System.out.println("Event fetch error: " + ex.getMessage());
            }


            if (m_refreshCounter == REFRESH_THRESHOLD) {

                m_refreshCounter = 0;

                if (m_client.getSession(0) != null) {
                    try {
                        m_client.enqueueEvent(new DarkmireChatEvent(m_client.getSession(0), null, getAndUpdateChatCheck(), m_previousChatId));
                    } catch (DarkmireException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }

                if (m_client.isLogged())
                {
                    try {
                        m_client.enqueueEvent(new DarkmireUserEvent());
                    } catch (DarkmireException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }

            } else {
                m_refreshCounter++;
            }

            m_task.schedule(0);
        }


        public void interrupt()
        {
            m_task.cancel();
        }
    }

    private DarkmireEventListener m_eventListener = null;

    private ListAction m_listAction = null;
    
    private final FilteredListModel.Filter m_filter = new FilteredListModel.Filter()
    {
        @Override
        public boolean accept(Object element)
        {
            if (sessionSearchField.getText().isEmpty())
            {
                return true;
            }
            
            DarkmireSession session = (DarkmireSession)element;
            
            return session.getName().toLowerCase().startsWith(sessionSearchField.getText().toLowerCase());
        }
    };
    
    private final DocumentListener m_listener = new DocumentListener()
    {
        @Override
        public void insertUpdate(DocumentEvent e)
        {
            update();
        }


        @Override
        public void removeUpdate(DocumentEvent e)
        {
            update();
        }


        @Override
        public void changedUpdate(DocumentEvent e)
        {
            update();
        }
        
        private void update()
        {
            if (sessionList.getModel() instanceof FilteredListModel)
            {
                ((FilteredListModel)sessionList.getModel()).filter();
            }
        }
    };


    public DarkmireTopComponent()
    {
        m_previousChatCheck = DarkmireDateTime.getUTC().getTime();
        URL url = getClass().getResource("/org/brendor/darkmire/netbeans/graphics/darkmire-splash.png");
        if (url != null) {
            m_background = Toolkit.getDefaultToolkit().createImage(url);
            Toolkit.getDefaultToolkit().prepareImage(m_background, -1, -1, null);
        }

        initComponents();
        
        JSearchTextField searchField = (JSearchTextField)sessionSearchField;
        searchField.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Search16.png")));
        searchField.setTextWhenNotFocused("Search sessions...");
        searchField.getDocument().addDocumentListener(m_listener);
        
        sessionList.setCellRenderer(new SessionListRenderer());

        setName(Bundle.CTL_DarkmireTopComponent());
        setToolTipText(Bundle.HINT_DarkmireTopComponent());
        putClientProperty(TopComponent.PROP_CLOSING_DISABLED, Boolean.TRUE);
        putClientProperty(TopComponent.PROP_MAXIMIZATION_DISABLED, Boolean.TRUE);

        Handler handler = null;
        try {
            handler = new FileHandler("test.log", false);
        } catch (IOException | SecurityException ex) {
            Exceptions.printStackTrace(ex);
        }

        Logger.getLogger(LOG_NAME).addHandler(handler);

        m_preferences = NbPreferences.forModule(GeneralPanel.class);

        m_preferences.addPreferenceChangeListener(new PreferenceChangeListener() {
            @Override
            public void preferenceChange(PreferenceChangeEvent evt)
            {
                if (evt.getKey().equals(GeneralPanel.SERVER_URL)) {
                    m_client.stop();

                    try {
                        m_client.start(m_preferences.get(GeneralPanel.SERVER_URL, GeneralPanel.DEFAULT_SERVER_URL));
                    } catch (DarkmireException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
        });


        Action displayAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (sessionList.isEnabled()) {
                    loginButtonActionPerformed(e);
                }
            }
        };

        m_listAction = new ListAction(sessionList, displayAction);
        
        
        DefaultComboBoxModel<DarkmireAuthenticationEvent.UserPrivilege> model = new DefaultComboBoxModel<>();

        model.insertElementAt(DarkmireAuthenticationEvent.UserPrivilege.USER_REGULAR, 0);
        model.insertElementAt(DarkmireAuthenticationEvent.UserPrivilege.USER_MODERATOR, 1);
        model.insertElementAt(DarkmireAuthenticationEvent.UserPrivilege.USER_ADMIN, 2);
        
        model.setSelectedItem(model.getElementAt(0));

        roleComboBox.<DarkmireAuthenticationEvent.UserPrivilege>setModel(model);
        
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex)
        {
            Exceptions.printStackTrace(ex);
        }
        
        nameField.setText(m_preferences.get(LOGIN_USERNAME, ""));
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        tabPanel = new javax.swing.JTabbedPane();
        sessionsPane = new javax.swing.JPanel();
        sessionLoginPanel = new javax.swing.JPanel();
        sessionListPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        loginButton = new javax.swing.JButton();
        sessionScrollPane = new javax.swing.JScrollPane();
        sessionList = new javax.swing.JList<DarkmireSession>();
        jPanel1 = new javax.swing.JPanel();
        refreshButton = new javax.swing.JButton();
        sessionIdLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        loginProgressBar = new javax.swing.JProgressBar();
        sessionSearchField = new JSearchTextField();
        sessionManagePanel = new javax.swing.JPanel();
        adminPanel = new javax.swing.JPanel();
        adminCardPanel = new javax.swing.JPanel();
        loginAdminPanel = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        passwordField = new javax.swing.JPasswordField();
        nameField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        roleComboBox = new javax.swing.JComboBox<DarkmireAuthenticationEvent.UserPrivilege>();
        adminLoginButton = new javax.swing.JButton();
        loggedInPanel = new javax.swing.JPanel();
        welcomeLabel = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        uuidField = new javax.swing.JTextField();
        settingsPanel = new javax.swing.JPanel();
        regularUserCard = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        adminUserPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        latencyField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        adminProgressBar = new javax.swing.JProgressBar();
        adminLogoutButton = new javax.swing.JButton();
        settingsButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        chatPane = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        chatInput = new javax.swing.JTextArea();
        chatOkButton = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        chatArea = new javax.swing.JTextPane();
        jPanel5 = new javax.swing.JPanel();
        errorLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(68, 68, 68));

        tabPanel.setMinimumSize(new java.awt.Dimension(0, 0));
        tabPanel.setPreferredSize(new java.awt.Dimension(0, 0));
        tabPanel.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                tabPanelStateChanged(evt);
            }
        });

        sessionsPane.setMinimumSize(new java.awt.Dimension(0, 0));
        sessionsPane.setPreferredSize(new java.awt.Dimension(0, 0));
        sessionsPane.setLayout(new java.awt.CardLayout());

        sessionListPanel.setOpaque(false);
        sessionListPanel.setLayout(new java.awt.GridBagLayout());

        jPanel4.setBorder(null);
        jPanel4.setPreferredSize(new java.awt.Dimension(430, 180));

        loginButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Login-in-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(loginButton, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.loginButton.text")); // NOI18N
        loginButton.setFocusCycleRoot(true);
        loginButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                loginButtonActionPerformed(evt);
            }
        });

        sessionScrollPane.setBorder(null);
        sessionScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        sessionScrollPane.setToolTipText(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.sessionScrollPane.toolTipText")); // NOI18N

        sessionList.setBorder(null);
        sessionList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        sessionList.setName(""); // NOI18N
        sessionList.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyReleased(java.awt.event.KeyEvent evt)
            {
                sessionListKeyReleased(evt);
            }
        });
        sessionScrollPane.setViewportView(sessionList);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(sessionScrollPane)
                    .addComponent(loginButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(sessionScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
        sessionListPanel.add(jPanel4, gridBagConstraints);

        jPanel1.setPreferredSize(new java.awt.Dimension(0, 0));

        refreshButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Refresh-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(refreshButton, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.refreshButton.text")); // NOI18N
        refreshButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                refreshButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(sessionIdLabel, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.sessionIdLabel.text")); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(refreshButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sessionIdLabel)
                .addContainerGap(566, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(refreshButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sessionIdLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        sessionSearchField.setText(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.sessionSearchField.text")); // NOI18N

        javax.swing.GroupLayout sessionLoginPanelLayout = new javax.swing.GroupLayout(sessionLoginPanel);
        sessionLoginPanel.setLayout(sessionLoginPanelLayout);
        sessionLoginPanelLayout.setHorizontalGroup(
            sessionLoginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sessionListPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sessionLoginPanelLayout.createSequentialGroup()
                .addGroup(sessionLoginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(sessionLoginPanelLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sessionSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(sessionLoginPanelLayout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(loginProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        sessionLoginPanelLayout.setVerticalGroup(
            sessionLoginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sessionLoginPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sessionSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sessionListPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sessionLoginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(loginProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        sessionsPane.add(sessionLoginPanel, "login");

        javax.swing.GroupLayout sessionManagePanelLayout = new javax.swing.GroupLayout(sessionManagePanel);
        sessionManagePanel.setLayout(sessionManagePanelLayout);
        sessionManagePanelLayout.setHorizontalGroup(
            sessionManagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 838, Short.MAX_VALUE)
        );
        sessionManagePanelLayout.setVerticalGroup(
            sessionManagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 416, Short.MAX_VALUE)
        );

        sessionsPane.add(sessionManagePanel, "session");

        tabPanel.addTab(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.sessionsPane.TabConstraints.tabTitle"), new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Users-icon.png")), sessionsPane); // NOI18N

        adminCardPanel.setLayout(new java.awt.CardLayout());

        loginAdminPanel.setName(""); // NOI18N
        loginAdminPanel.setLayout(new java.awt.GridBagLayout());

        jPanel7.setLayout(new java.awt.GridBagLayout());

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.jLabel1.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel7.add(jLabel1, gridBagConstraints);

        passwordField.setText(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.passwordField.text")); // NOI18N
        passwordField.setPreferredSize(new java.awt.Dimension(140, 25));
        passwordField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                passwordFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanel7.add(passwordField, gridBagConstraints);

        nameField.setText(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.nameField.text")); // NOI18N
        nameField.setPreferredSize(new java.awt.Dimension(140, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel7.add(nameField, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.jLabel2.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 10);
        jPanel7.add(jLabel2, gridBagConstraints);

        roleComboBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                roleComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        jPanel7.add(roleComboBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        loginAdminPanel.add(jPanel7, gridBagConstraints);

        adminLoginButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Login-in-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(adminLoginButton, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.adminLoginButton.text")); // NOI18N
        adminLoginButton.setPreferredSize(new java.awt.Dimension(100, 27));
        adminLoginButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                adminLoginButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
        loginAdminPanel.add(adminLoginButton, gridBagConstraints);

        adminCardPanel.add(loginAdminPanel, "login");

        welcomeLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        welcomeLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        org.openide.awt.Mnemonics.setLocalizedText(welcomeLabel, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.welcomeLabel.text")); // NOI18N

        jPanel6.setLayout(new java.awt.GridBagLayout());

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.jLabel3.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 0, 7);
        jPanel6.add(jLabel3, gridBagConstraints);

        uuidField.setEditable(false);
        uuidField.setText(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.uuidField.text")); // NOI18N
        uuidField.setEnabled(false);
        uuidField.setMinimumSize(new java.awt.Dimension(300, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 290;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel6.add(uuidField, gridBagConstraints);

        settingsPanel.setLayout(new java.awt.CardLayout());

        regularUserCard.setLayout(new java.awt.GridBagLayout());

        jLabel5.setForeground(new java.awt.Color(146, 146, 146));
        org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.jLabel5.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        regularUserCard.add(jLabel5, gridBagConstraints);

        settingsPanel.add(regularUserCard, "regular");

        adminUserPanel.setLayout(new java.awt.GridBagLayout());

        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.jLabel6.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        adminUserPanel.add(jLabel6, gridBagConstraints);

        settingsPanel.add(adminUserPanel, "admin");

        jPanel8.setLayout(new java.awt.GridBagLayout());

        latencyField.setEditable(false);
        latencyField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        latencyField.setText(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.latencyField.text")); // NOI18N
        latencyField.setEnabled(false);
        latencyField.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                latencyFieldMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 40;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel8.add(latencyField, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.jLabel4.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 0, 7);
        jPanel8.add(jLabel4, gridBagConstraints);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(settingsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(settingsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout loggedInPanelLayout = new javax.swing.GroupLayout(loggedInPanel);
        loggedInPanel.setLayout(loggedInPanelLayout);
        loggedInPanelLayout.setHorizontalGroup(
            loggedInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loggedInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(loggedInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(welcomeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        loggedInPanelLayout.setVerticalGroup(
            loggedInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loggedInPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(welcomeLabel)
                .addGap(12, 12, 12)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        adminCardPanel.add(loggedInPanel, "admin");

        adminLogoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Login-out-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(adminLogoutButton, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.adminLogoutButton.text")); // NOI18N
        adminLogoutButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                adminLogoutButtonActionPerformed(evt);
            }
        });

        settingsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Wheel-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(settingsButton, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.settingsButton.text")); // NOI18N
        settingsButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                settingsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(adminLogoutButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(settingsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(adminProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(adminProgressBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(adminLogoutButton)
                        .addComponent(settingsButton))))
        );

        javax.swing.GroupLayout adminPanelLayout = new javax.swing.GroupLayout(adminPanel);
        adminPanel.setLayout(adminPanelLayout);
        adminPanelLayout.setHorizontalGroup(
            adminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(adminCardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        adminPanelLayout.setVerticalGroup(
            adminPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(adminPanelLayout.createSequentialGroup()
                .addComponent(adminCardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tabPanel.addTab(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.adminPanel.TabConstraints.tabTitle"), new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Wheels-icon.png")), adminPanel); // NOI18N

        jPanel3.setLayout(new java.awt.GridBagLayout());

        chatInput.setColumns(20);
        chatInput.setLineWrap(true);
        chatInput.setRows(5);
        chatInput.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        chatInput.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyReleased(java.awt.event.KeyEvent evt)
            {
                chatInputKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(chatInput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 569;
        gridBagConstraints.ipady = 55;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 11, 0);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        chatOkButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Accept-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(chatOkButton, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.chatOkButton.text")); // NOI18N
        chatOkButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                chatOkButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 9;
        gridBagConstraints.ipady = 8;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 0);
        jPanel3.add(chatOkButton, gridBagConstraints);

        chatArea.setEditable(false);
        jScrollPane3.setViewportView(chatArea);

        javax.swing.GroupLayout chatPaneLayout = new javax.swing.GroupLayout(chatPane);
        chatPane.setLayout(chatPaneLayout);
        chatPaneLayout.setHorizontalGroup(
            chatPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chatPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(chatPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 814, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );
        chatPaneLayout.setVerticalGroup(
            chatPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chatPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabPanel.addTab(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.chatPane.TabConstraints.tabTitle"), new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Chat-icon.png")), chatPane); // NOI18N

        jPanel5.setPreferredSize(new java.awt.Dimension(0, 0));

        errorLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        errorLabel.setForeground(new java.awt.Color(188, 2, 2));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        org.openide.awt.Mnemonics.setLocalizedText(errorLabel, org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.errorLabel.text")); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(errorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(errorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 830, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tabPanel.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(DarkmireTopComponent.class, "DarkmireTopComponent.tabPanel.AccessibleContext.accessibleName")); // NOI18N
    }// </editor-fold>//GEN-END:initComponents

    private void chatOkButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_chatOkButtonActionPerformed
    {//GEN-HEADEREND:event_chatOkButtonActionPerformed
        if (chatInput.getText().trim().isEmpty()) {
            chatInput.setText("");
            return;
        }

        System.out.println("Sending chat message... " + DarkmireDateTime.getUTCString(new Date(m_previousChatCheck)));

        try {
            m_client.enqueueEvent(new DarkmireChatEvent(m_client.getSession(0), chatInput.getText().trim(), getAndUpdateChatCheck(), m_previousChatId));
        } catch (DarkmireException ex) {
            System.out.println(ex.getMessage());
        }

        chatInput.setText("");
    }//GEN-LAST:event_chatOkButtonActionPerformed

    private void chatInputKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_chatInputKeyReleased
    {//GEN-HEADEREND:event_chatInputKeyReleased
        if (evt.getKeyCode() == '\n')
        {
            chatOkButtonActionPerformed(null);
        }
    }//GEN-LAST:event_chatInputKeyReleased

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_refreshButtonActionPerformed
    {//GEN-HEADEREND:event_refreshButtonActionPerformed

        sessionSearchField.requestFocus();
        
        setErrorText("");

        try {
            m_client.enqueueEvent(new DarkmireSessionListEvent());
            
            loginProgressBar.setIndeterminate(true);
            
        } catch (DarkmireException ex) {
            System.out.println(ex.getMessage());
        }
    }//GEN-LAST:event_refreshButtonActionPerformed

    private void adminLoginButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_adminLoginButtonActionPerformed
    {//GEN-HEADEREND:event_adminLoginButtonActionPerformed
        
        m_preferences.put(LOGIN_USERNAME, nameField.getText());
        
        if (nameField.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(this, "Name is missing", "Darkmire", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        try {
            m_client.enqueueEvent(new DarkmireAuthenticationEvent(
                    null,
                    0,
                    nameField.getText(),
                    passwordField.getPassword(),
                    m_preferences.get(GeneralPanel.USER_NICKNAME, "Anonymous"),
                    (DarkmireAuthenticationEvent.UserPrivilege) roleComboBox.getSelectedItem()));

            adminProgressBar.setIndeterminate(true);

        } catch (DarkmireException ex) {
            setErrorText(ex.getMessage());
        }

        passwordField.setText("");

    }//GEN-LAST:event_adminLoginButtonActionPerformed

    private void tabPanelStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_tabPanelStateChanged
    {//GEN-HEADEREND:event_tabPanelStateChanged
        setErrorText("");
        
        if (tabPanel.getSelectedComponent() == adminPanel)
        {
            passwordField.requestFocus();
        }
        
    }//GEN-LAST:event_tabPanelStateChanged

    private void passwordFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_passwordFieldActionPerformed
    {//GEN-HEADEREND:event_passwordFieldActionPerformed
        adminLoginButtonActionPerformed(evt);
    }//GEN-LAST:event_passwordFieldActionPerformed

    private void adminLogoutButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_adminLogoutButtonActionPerformed
    {//GEN-HEADEREND:event_adminLogoutButtonActionPerformed
        
        if (getSessionManager() != null)
        {
            for (int i = 0; i < m_client.getNumSessions(); i++)
            {
                try
                {
                    DarkmireDisconnectEvent event = new DarkmireDisconnectEvent(m_client.getSession(i));
                    
                    event.setObject((DarkmireEventHandler) getSessionManager());

                    m_client.enqueueEvent(event);
                }
                catch (DarkmireException ex)
                {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
        
        try {
            m_client.enqueueEvent(new DarkmireDisconnectEvent(null));
        } catch (DarkmireException ex) {
            Exceptions.printStackTrace(ex);
        }
    }//GEN-LAST:event_adminLogoutButtonActionPerformed

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_loginButtonActionPerformed
    {//GEN-HEADEREND:event_loginButtonActionPerformed
        if (m_client.getSession(0) != null)
        {
            try {
                m_client.enqueueEvent(new DarkmireDisconnectEvent(m_client.getSession(0)));
            } catch (DarkmireException ex) {
                System.out.println(ex.getMessage());
            }

        } else {

            setErrorText("");

            if (sessionList.getSelectedIndex() != -1) {

                DarkmireSession session = (DarkmireSession) sessionList.getSelectedValue();

                String username = null;
                char[] password = null;

                switch (session.getAuthentication()) {
                    case AUTHENTICATION_NONE:
                        break;

                    case AUTHENTICATION_ERROR:
                        return;

                    case AUTHENTICATION_LDAP:
                        AuthenticationPanel form = new AuthenticationPanel();
                        DialogDescriptor dd = new DialogDescriptor(form, "Darkmire LDAP Login", true, null);
                        Object result = DialogDisplayer.getDefault().notify(dd);

                        if (result != NotifyDescriptor.OK_OPTION) {
                            return;
                        }

                        username = form.getUserName();
                        password = form.getPassword();

                        break;
                        
                    case AUTHENTICATION_LOCAL_DATABASE:
                        
                        if (! m_client.isLogged())
                        {
                            NotifyDescriptor nd = new NotifyDescriptor.Message("Must be logged in to join this session",
                                                                               NotifyDescriptor.INFORMATION_MESSAGE);
                            DialogDisplayer.getDefault().notify(nd);
                            
                            tabPanel.setSelectedComponent(adminPanel);
                            
                            return;
                        }
                        
                        break;
                }

                try {
                    m_client.enqueueEvent(new DarkmireAuthenticationEvent(
                            session,
                            0,
                            username,
                            password,
                            m_preferences.get(GeneralPanel.USER_NICKNAME, "Anonymous"),
                            DarkmireAuthenticationEvent.UserPrivilege.USER_REGULAR));

                    loginProgressBar.setIndeterminate(true);

                } catch (DarkmireException ex) {
                    setErrorText(ex.getMessage());
                }
            } else {
                NotifyDescriptor nd = new NotifyDescriptor.Message("Please select a session to connect to",
                        NotifyDescriptor.INFORMATION_MESSAGE);
                DialogDisplayer.getDefault().notify(nd);
            }
        }
    }//GEN-LAST:event_loginButtonActionPerformed

    private void sessionListKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_sessionListKeyReleased
    {//GEN-HEADEREND:event_sessionListKeyReleased
        if (evt.getKeyChar() == '\n')
        {
            loginButtonActionPerformed(null);
        }
    }//GEN-LAST:event_sessionListKeyReleased

    private void roleComboBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_roleComboBoxActionPerformed
    {//GEN-HEADEREND:event_roleComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_roleComboBoxActionPerformed

    private void settingsButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_settingsButtonActionPerformed
    {//GEN-HEADEREND:event_settingsButtonActionPerformed
        OptionsDisplayer.getDefault().open("Darkmire/General", true);
    }//GEN-LAST:event_settingsButtonActionPerformed

    private void latencyFieldMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_latencyFieldMouseClicked
    {//GEN-HEADEREND:event_latencyFieldMouseClicked
        
        final JFileChooser fc = new JFileChooser(); 
 
        // in response to a button click: 
        int returnVal = fc.showSaveDialog(this); 

        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            FileWriter fstream = null;
            
            try
            {
                File file = fc.getSelectedFile();
                
                fstream = new FileWriter(file);
                
                try (BufferedWriter out = new BufferedWriter(fstream))
                {
                    long c = 1;
                    
                    Iterator<Long> di = m_client.getRTT().descendingIterator();
                    
                    out.write("request (#)\n");
                    out.write("time (ms)\n");
                    
                    while (di.hasNext())
                    {
                        Long l = di.next();
                        
                        out.write(String.format("%d %f\n", c, l / 1000000.0)); // To ms
                        
                        c++;
                    }
                }
            }
            catch (IOException ex)
            {
                Exceptions.printStackTrace(ex);
            }
            finally
            {
                if (fstream != null)
                {
                    try
                    {
                        fstream.close();
                    } catch (IOException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
        }
        
    }//GEN-LAST:event_latencyFieldMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel adminCardPanel;
    private javax.swing.JButton adminLoginButton;
    private javax.swing.JButton adminLogoutButton;
    private javax.swing.JPanel adminPanel;
    private javax.swing.JProgressBar adminProgressBar;
    private javax.swing.JPanel adminUserPanel;
    private javax.swing.JTextPane chatArea;
    private javax.swing.JTextArea chatInput;
    private javax.swing.JButton chatOkButton;
    private javax.swing.JPanel chatPane;
    private javax.swing.JLabel errorLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField latencyField;
    private javax.swing.JPanel loggedInPanel;
    private javax.swing.JPanel loginAdminPanel;
    private javax.swing.JButton loginButton;
    private javax.swing.JProgressBar loginProgressBar;
    private javax.swing.JTextField nameField;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JButton refreshButton;
    private javax.swing.JPanel regularUserCard;
    private javax.swing.JComboBox roleComboBox;
    private javax.swing.JLabel sessionIdLabel;
    private javax.swing.JList sessionList;
    private javax.swing.JPanel sessionListPanel;
    private javax.swing.JPanel sessionLoginPanel;
    private javax.swing.JPanel sessionManagePanel;
    private javax.swing.JScrollPane sessionScrollPane;
    private javax.swing.JTextField sessionSearchField;
    private javax.swing.JPanel sessionsPane;
    private javax.swing.JButton settingsButton;
    private javax.swing.JPanel settingsPanel;
    private javax.swing.JTabbedPane tabPanel;
    private javax.swing.JTextField uuidField;
    private javax.swing.JLabel welcomeLabel;
    // End of variables declaration//GEN-END:variables


    public javax.swing.JPanel getSessionManager()
    {
        return m_sessionManagePanel;
    }
    
    
    @Override
    public void componentOpened()
    {
        System.out.println("Opening...");

        try {
            m_client.start(m_preferences.get(GeneralPanel.SERVER_URL, GeneralPanel.DEFAULT_SERVER_URL));
        } catch (DarkmireException ex) {
            Exceptions.printStackTrace(ex);
        }

        m_eventListener = new DarkmireEventListener();
    }


    @Override
    public void componentClosed()
    {
        System.out.println("Closing...");

        m_eventListener.interrupt();

        m_client.stop();
    }


    @Override
    protected void componentActivated()
    {
        super.componentActivated();

        sessionList.setModel(new DefaultListModel<DarkmireSession>());

        refreshButtonActionPerformed(null);

        System.out.println(sessionList.getSelectedIndex());
    }


    @Override
    protected void componentDeactivated()
    {
        super.componentDeactivated();

        setErrorText("");
        
        if (m_sessionManagePanel != null)
        {
            m_sessionManagePanel.closing();
        }
    }


    void writeProperties(java.util.Properties p)
    {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }


    void readProperties(java.util.Properties p)
    {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }


    private synchronized long getAndUpdateChatCheck()
    {
        long previous = m_previousChatCheck;

        m_previousChatCheck = DarkmireDateTime.getUTC().getTime();

        return previous;
    }


    private void setErrorText(String text)
    {
        if (text.length() > 100) {
            text = text.substring(0, 100);
        }

        errorLabel.setText(text);
    }


    public void enqueue(DarkmireEvent event, SessionPanel handler) throws DarkmireException
    {
        event.setObject((DarkmireEventHandler) handler);
        m_client.enqueueEvent(event);
    }
    
    public String getCookie(String cookie)
    {
        return m_client.getCookie(cookie);
    }
    
    private void showAdminPanel(boolean shown)
    {
        CardLayout cl = (CardLayout) (adminCardPanel.getLayout());
        
        if (shown)
        {
            cl.show(adminCardPanel, "admin");
            adminLogoutButton.setEnabled(true);
            
            welcomeLabel.setText(String.format("%s\'s user panel", m_client.getUserName()));
            uuidField.setText(m_client.getCookie("sessionID"));
        }
        else
        {
            cl.show(adminCardPanel, "login");
            adminLogoutButton.setEnabled(false);
        }
    }
    
    public URI getServerAddress()
    {
        return m_client.getServerAddress();
    }
}
