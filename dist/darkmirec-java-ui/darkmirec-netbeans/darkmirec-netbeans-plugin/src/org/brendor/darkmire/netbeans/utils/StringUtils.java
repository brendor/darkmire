package org.brendor.darkmire.netbeans.utils;

import java.util.Arrays;

/**
 *
 * @author brendor
 */
public class StringUtils
{
    public static String repeat(char c, int n)
    {
        char[] buff = new char[n];

        Arrays.fill(buff, c);

        return new String(buff);
    }
}
