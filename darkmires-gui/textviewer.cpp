#include "textviewer.hpp"
#include "ui_textviewer.h"

TextViewer::TextViewer(const QString & text, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextViewer)
{
    ui->setupUi(this);

    ui->textEdit->setText(text);
}

TextViewer::~TextViewer()
{
    delete ui;
}
