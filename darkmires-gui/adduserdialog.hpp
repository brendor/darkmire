#ifndef ADDUSERDIALOG_HPP
#define ADDUSERDIALOG_HPP

#include <QDialog>

#include "darkmires.hpp"

namespace Ui {
class AddUserDialog;
}

class AddUserDialog : public QDialog
{
        Q_OBJECT

    public:
        explicit AddUserDialog(QWidget * parent = 0, bool allowNoPassword = false);
        ~AddUserDialog();

        QString getName() const;
        QString getPassword() const;
        Darkmires::UserPrivileges getPrivileges() const;
        QString getSalt() const;
        QUuid getUuid() const;

        void setName(const QString & name);
        void setPrivileges(Darkmires::UserPrivileges privileges);
        void setSalt(const QString & salt);
        void setUuid(const QUuid & uuid);

    private slots:
        void on_generateButton_clicked();

        void accepted();

        void privilegesChanged();

        void updatePrivileges();

        void on_passwordEdit_textEdited(const QString &text);

        void on_showPassword_toggled(bool checked);

    private:
        Ui::AddUserDialog * ui;

        QByteArray getFinal() const;

        bool m_allowNoPassword = false;
};

#endif // ADDUSERDIALOG_HPP
