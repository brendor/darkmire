#include "sessionview.hpp"
#include "ui_sessionview.h"

SessionView::SessionView(QWidget * parent) :
    QDialog(parent),
    ui(new Ui::SessionView)
{
    ui->setupUi(this);
}

SessionView::~SessionView()
{
    delete ui;
}
