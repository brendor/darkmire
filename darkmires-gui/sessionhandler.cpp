#include "sessionhandler.hpp"

SessionHandler::SessionHandler(Darkmires * darkmires, QString session, QWidget * parent) :
    QDialog(parent),
    m_general(darkmires, session, this)
{
}


void SessionHandler::notifyRemove()
{
    m_general.notifyRemove();
}
