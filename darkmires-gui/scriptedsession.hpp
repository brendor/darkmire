#ifndef SCRIPTEDSESSION_HPP
#define SCRIPTEDSESSION_HPP

#include "sessionhandler.hpp"

namespace Ui {
class ScriptedSession;
}

class ScriptedSession : public SessionHandler
{
        Q_OBJECT

    public:
        explicit ScriptedSession(Darkmires * const darkmires, QString session, QWidget * parent = 0);
        virtual ~ScriptedSession();

    public slots:
        virtual void notifyShow() override;
        virtual void notifyRemove() override;
        virtual void darkmiresEvent(Darkmires::Event event) override;

    private slots:
        void on_consoleExecute_clicked();

        void on_consoleEdit_returnPressed();

        void on_evaluateButton_clicked();

    private:
        Ui::ScriptedSession * ui;
};

#endif // SCRIPTEDSESSION_HPP
