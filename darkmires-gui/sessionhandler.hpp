#ifndef SESSIONHANDLER_HPP
#define SESSIONHANDLER_HPP

#include <QDialog>

#include "sessionhandlergeneral.hpp"

class SessionHandler : public QDialog
{
        Q_OBJECT
    public:
        explicit SessionHandler(Darkmires * darkmires, QString session, QWidget * parent = 0);

        inline const QString & getSession() {
            return m_general.getSession();
        }

    signals:
        void logMessage(QString message);

    public slots:
        virtual void notifyShow() { }
        virtual void notifyRemove();
        virtual void darkmiresEvent(Darkmires::Event event) = 0;

    protected:
        SessionHandlerGeneral m_general;
};

#endif // SESSIONHANDLER_HPP
