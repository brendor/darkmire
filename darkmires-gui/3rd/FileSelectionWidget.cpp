#include <QDir>
#include <QFileSystemModel>
#include <QWidget>

#include <iostream>

#include "FileSelectionWidget.h"

FileSelectionWidget::FileSelectionWidget(QWidget *parent)
    : QWidget(parent)
{
    std::cout << "Current path: " << QDir::currentPath().toStdString() << std::endl;

    setupUi(this); // Otherwise 'listView' seems to be undefined
    
    QString path = QDir::currentPath();

    this->m_dirModel = new QFileSystemModel;
    this->m_dirModel->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs);
    this->m_dirModel->setRootPath(path);

    this->treeView->setModel(m_dirModel);
    this->treeView->setRootIndex(m_dirModel->index(path));

    m_fileModel = new QFileSystemModel;
    m_fileModel->setFilter(QDir::NoDotAndDotDot | QDir::Files);
    m_fileModel->setRootPath(path);

    this->listView->setModel(m_fileModel);
    this->listView->setRootIndex(m_fileModel->index(path));

    this->lblPath->setText(path);
    //    this->lblFile->setText(listView->currentIndex().data(QFileSystemModel::FilePathRole).toString());
}

void FileSelectionWidget::setPath(const QString & path)
{
    this->treeView->setRootIndex(m_dirModel->index(path));
}

void FileSelectionWidget::on_listView_doubleClicked(const QModelIndex & index)
{
    std::cout << listView->currentIndex().data(QFileSystemModel::FilePathRole).toString().toStdString() << std::endl;
    //std::cout << listView->currentIndex().data(QFileSystemModel::FileNameRole).toString().toStdString() << std::endl;

    if(this->m_dirModel->isDir(this->listView->currentIndex())) // A directory was selected
    {
        this->listView->setRootIndex(m_dirModel->index(listView->currentIndex().data(QFileSystemModel::FilePathRole).toString()));

        this->lblPath->setText(listView->currentIndex().data(QFileSystemModel::FilePathRole).toString());
    }
    else // A file was selected
    {
        on_listView_clicked(index);
    }
}

void FileSelectionWidget::on_listView_clicked(const QModelIndex & index)
{
//    this->lblFile->setText(listView->currentIndex().data(QFileSystemModel::FilePathRole).toString());
}

void FileSelectionWidget::on_treeView_clicked(const QModelIndex &index)
{
    QString path = m_dirModel->fileInfo(index).absoluteFilePath();
    this->listView->setRootIndex(m_fileModel->setRootPath(path));
}
