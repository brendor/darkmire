#ifndef SESSIONHANDLERGENERAL_HPP
#define SESSIONHANDLERGENERAL_HPP

#include <QFrame>

#include <memory>

#include <QSqlTableModel>

#include "darkmires.hpp"

namespace Ui {
class SessionHandlerGeneral;
}

class SessionHandlerGeneral : public QFrame
{
        Q_OBJECT

    public:
        explicit SessionHandlerGeneral(Darkmires * const darkmires, QString session, QWidget * parent = 0);
        virtual ~SessionHandlerGeneral();

        inline const QString & getSession() {
            return m_session;
        }
        inline Darkmires * getDarkmires() {
            return m_darkmires;
        }

        static constexpr const char * TAB_WIDGET_NAME = "Session";

        void notifyRemove();

    private:
        Ui::SessionHandlerGeneral * ui;

    private slots:
        void refreshOwners();
        void refreshProperties();

        void on_addOwner_clicked();

        void on_removeOwner_clicked();

        void propertyChanged(QPair<QString, QVariant> pair);

        void on_addPropertyButton_clicked();

        void on_removePropertyButton_clicked();

        void on_addUserSqlButton_clicked();

        void on_saveUsersSqlButton_clicked();

        void on_revertUsersSqlButton_clicked();

        void on_clearUsersSqlButton_clicked();

        void on_removeUserSqlButton_clicked();

    protected:
        QString m_session;

        std::shared_ptr<DsSessionInterface> m_sessionPtr;

        Darkmires * const m_darkmires;

        QSqlDatabase m_database;
        std::unique_ptr<QSqlTableModel> m_usersSqlModel;
};

#endif // SESSIONHANDLERGENERAL_HPP
