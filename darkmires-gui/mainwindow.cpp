#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

#include <QSqlError>
#include <QSqlRecord>

#include <QCryptographicHash>

#include <QPushButton>

#include <QTimer>

#include <QClipboard>

#include <QTextStream>

#include <QDesktopWidget>

#include "adduserdialog.hpp"

#include "dsuserstorage.hpp"
#include "dsutils.hpp"

#include "quicksharesession.hpp"
#include "scriptedsession.hpp"
#include "subversionsession.hpp"



std::unique_ptr<QSettings> MainWindow::m_settings;


#define WAIT_FOR_SESSION 300
#define WAIT_FOR_USERS 300

#define MAX_CONSOLE_CHARS 20000


Q_DECLARE_METATYPE(std::shared_ptr<SessionHandler>)


MainWindow::MainWindow(QWidget * parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_darkmires(Darkmires::getInstance()),
    m_refreshTimer(this)
{
    srand(QDateTime::currentDateTime().toTime_t());

    ui->setupUi(this);

    getSettings();

    m_refreshTimer.setInterval(3000);

    connect(&m_refreshTimer, SIGNAL(timeout()), this, SLOT(refreshTimer()));

    //    setWindowState(Qt::WindowMaximized);
    setGeometry(0,
                0,
                getSettings()->value("window/width", 800).toInt(),
                getSettings()->value("window/height", 600).toInt());

    connect(ui->actionSave_Log,
            SIGNAL(triggered()),
            this,
            SLOT(saveLog()));

    m_darkmires->setLoggingFunction(
    [this](const QString level, const QString msg) {

        if (! ui->loggingBox->isChecked()) return;

        QString color = "white";

        QString message = msg;

        if (! level.isEmpty()) {

            message.prepend("[" + level + "] ");

            if (level == "INFO") {
                color = "lightblue";
            } else if (level == "DEBUG") {
                color = "lightgreen";
            } else if (level == "WARNING") {
                color = "orange";
            } else if (level == "CRITICAL") {
                color = "lightred";
            } else if (level == "Error") {
                color = "red";
            }
        }

        ui->consoleEdit->moveCursor(QTextCursor::End);
        ui->consoleEdit->insertHtml("<span style='color: " + color + ";'>" + message + "<br>");
    }
    );

    m_darkmires->init();

    connect(m_darkmires,
            SIGNAL(darkmiresEvent(Darkmires::Event)),
            this,
            SLOT(darkmiresEvent(Darkmires::Event)));

    ui->sessionWidget->setCurrentIndex(0);
    ui->usersTabWidget->setCurrentIndex(0);

    on_stopButton_clicked();
    on_temporaryRadio_toggled(ui->temporaryRadio->isChecked());

    ui->envWidget->setSettingsObject(DarkSettings);
    ui->envWidget->setAutoRefresh(false);
    ui->envWidget->refresh();

    ui->forceHttpsBox->setChecked(getSettings()->value("server/https", ui->forceHttpsBox->isChecked()).toBool());
    ui->ldapBox->setChecked(getSettings()->value("server/ldap", ui->ldapBox->isChecked()).toBool());
    ui->multiThreadBox->setChecked(getSettings()->value("server/threads", ui->multiThreadBox->isChecked()).toBool());

    ui->refreshOnEventBox->setChecked(getSettings()->value("gui/refresh-on-event", ui->refreshOnEventBox->isChecked()).toBool());

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    ui->sessionTable->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->usersTable->header()->setSectionResizeMode(0, QHeaderView::Stretch);
#else
    ui->sessionTable->header()->setResizeMode(0, QHeaderView::Stretch);
    ui->usersTable->header()->setResizeMode(0, QHeaderView::Stretch);
#endif

    ui->sessionSecurityCombo->addItem("Free For All (none)", (int)Darkmires::AuthenticationNone);
    ui->sessionSecurityCombo->addItem("Local Database", (int)Darkmires::AuthenticationLocalDatabase);
    ui->sessionSecurityCombo->addItem("LDAP", (int)Darkmires::AuthenticationLDAP);

    ui->sessionModeCombo->addItem("QuickShare Session", (int)Darkmires::SessionQuickShare);
    ui->sessionModeCombo->addItem("Scripted Session", (int)Darkmires::SessionScripted);
    ui->sessionModeCombo->addItem("Subversion Session", (int)Darkmires::SessionSubversion);

    m_database = QSqlDatabase::addDatabase("QSQLITE", "GUI");

    if (! m_database.isValid()) {
        logMessage("Database driver not valid!");
    } else {
        m_database.setDatabaseName(DarkSettings->value("database-file-path").toString() + "darkmire.db");

        logMessage("Opening database... " + m_database.databaseName());

        if (m_database.open()) {
            logMessage("Database connection opened: " + m_database.databaseName());

            m_usersSqlModel.reset(new QSqlTableModel(this, m_database));

            m_usersSqlModel->setTable("users");
            m_usersSqlModel->select();

            m_usersSqlModel->setEditStrategy(QSqlTableModel::OnManualSubmit);

            ui->usersSqlTable->setModel(m_usersSqlModel.get());

        } else {
            logMessage("Database connection could not be opened: " + m_database.lastError().text());
        }
    }

    ui->actionSave_to_file->setChecked(getSettings()->value("log/write-logs").toBool());


    if (m_logTempFile.open())
    {
        m_logTempStream.setDevice(&m_logTempFile);
        m_logTempStream.setCodec("UTF-8");
    }

    if (ui->actionSave_to_file->isChecked())
    {
        m_logFile.setFileName(DarkSettings->value("log-file-path").toString() + "log_" + QDateTime::currentDateTimeUtc().toString("dd-MM-hh-mm-ss") + ".log");

        qDebug() << "Opening log file: " << m_logFile.fileName();

        if (m_logFile.open(QIODevice::WriteOnly))
        {
            m_logStream.setDevice(&m_logFile);
            m_logStream.setCodec("UTF-8");
        }
        else
        {
            qDebug() << "Log file could not be opened: " << m_logFile.fileName() << ": " << m_logFile.errorString();;
        }
    }

    connect(ui->actionConnections, SIGNAL(triggered()), this, SLOT(printConnections()));

    setupRequestProcessGraph(ui->requestPlot);
    setupCustomGraph(ui->customPlot);
}


MainWindow::~MainWindow()
{
    qDebug() << "Destroying GUI...";

    m_darkmires->stop();

    ui->envWidget->setSettingsObject(nullptr);

    getSettings()->setValue("window/width", size().width());
    getSettings()->setValue("window/height", size().height());

    getSettings()->setValue("server/https", ui->forceHttpsBox->isChecked());
    getSettings()->setValue("server/ldap", ui->ldapBox->isChecked());
    getSettings()->setValue("server/threads", ui->multiThreadBox->isChecked());

    getSettings()->setValue("gui/refresh-on-event", ui->refreshOnEventBox->isChecked());
    getSettings()->setValue("log/write-logs", ui->actionSave_to_file->isChecked());

    m_database.close();

    m_logTempFile.close();
    m_logFile.close();

    delete ui;

    Darkmires::freeInstance();
}


QSettings * MainWindow::getSettings()
{
    if (m_settings.get() == nullptr) {
        m_settings.reset(new QSettings("brendor", "darkmire-gui"));

        qDebug() << "Settings file opened: " + m_settings->fileName();
    }

    return m_settings.get();
}


void MainWindow::on_startButton_clicked()
{
    if (! ui->startButton->isEnabled()) return;

    QHostAddress address;

    if (ui->interfaceBox->currentText() == "Localhost") {
        address = QHostAddress(QHostAddress::LocalHost);
    } else if (ui->interfaceBox->currentText() == "Localhost IPv6") {
        address = QHostAddress(QHostAddress::LocalHostIPv6);
    } else if (ui->interfaceBox->currentText() == "Any") {
        address = QHostAddress(QHostAddress::Any);
    } else if (ui->interfaceBox->currentText() == "Any IPv6") {
        address = QHostAddress(QHostAddress::AnyIPv6);
    } else  {
        address.setAddress(ui->interfaceBox->currentText());
    }

    Darkmires::Protocol protocol = Darkmires::SCGI;

    if (ui->protocolBox->currentText() == "SCGI") {
        protocol = Darkmires::SCGI;
    } else if (ui->protocolBox->currentText() == "HTTP") {
        protocol = Darkmires::HTTP;
    }

    Darkmires::Flags flags = Darkmires::NoFlags;

    if (ui->forceHttpsBox->isChecked()) {
        flags |= Darkmires::FlagForceHTTPS;
    }

    if (ui->multiThreadBox->isChecked()) {
        flags |= Darkmires::FlagUseThreads;
    }

    if (ui->ldapBox->isChecked()) {
        flags |= Darkmires::FlagAllowLDAP;
    }

    if (ui->denyExternalBox->isChecked()) {
        flags |= Darkmires::FlagDenyExternal;
    }

    if (ui->compressionBox->isChecked()) {
        flags |= Darkmires::FlagCompression;
    }

    flags |= Darkmires::FlagStatistics;

    m_running = m_darkmires->start(address, ui->portEdit->text().toUInt(), protocol, flags);


    if (m_running) {
        m_serverStarted = QDateTime::currentDateTimeUtc();

        ui->startButton->setEnabled(false);

        ui->generalTab->setEnabled(false);
        ui->environmentTab->setEnabled(false);

        ui->stopButton->setEnabled(true);
        ui->restartButton->setEnabled(true);

        ui->activeUsersTab->setEnabled(true);
        ui->sessionsTab->setEnabled(true);
        ui->chatTab->setEnabled(true);

        //        ui->registeredUsersTab->setEnabled(true);

        refreshSessions();

        ui->statusBar->showMessage("Server running");
    }
}


void MainWindow::on_stopButton_clicked()
{
    m_sessionHandlers.clear();

    if (ui->stopButton->isEnabled()) {
        m_darkmires->stop();
    }

    m_running = false;

    ui->startButton->setEnabled(true);

    ui->generalTab->setEnabled(true);
    ui->environmentTab->setEnabled(true);

    ui->stopButton->setEnabled(false);
    ui->restartButton->setEnabled(false);

    ui->tabWidget->setCurrentIndex(0);
    ui->settingsTabWidget->setCurrentIndex(0);

    ui->activeUsersTab->setEnabled(false);
    ui->sessionsTab->setEnabled(false);
    ui->chatTab->setEnabled(false);

    //    ui->registeredUsersTab->setEnabled(false);

    ui->statusBar->showMessage("Server stopped");

    refreshUsers();
    refreshSessions();
}


void MainWindow::logMessage(const QString & msg)
{
    ui->consoleEdit->moveCursor(QTextCursor::End);
    ui->consoleEdit->insertHtml("<span style='color: orange;'>[GUI] " + msg + "<br>");
}


void MainWindow::darkmiresEvent(Darkmires::Event event)
{
    if (ui->refreshOnEventBox->isChecked()) {
        switch (event.type()) {
            case DarkmiresEvent::EVENT_USER_EDITED:
                m_needRefreshUsers = true;
                break;

            case DarkmiresEvent::EVENT_USER_JOINED_SESSION:
            case DarkmiresEvent::EVENT_USER_LEFT_SESSION:
            case DarkmiresEvent::EVENT_SESSION_ADDED:
            case DarkmiresEvent::EVENT_SESSION_REMOVED:
                m_needRefreshSessions = true;
                break;

            case DarkmiresEvent::EVENT_USER_JOINED:
            case DarkmiresEvent::EVENT_USER_LEFT:
                m_needRefreshUsers = true;
                m_needRefreshSessions = true;
                break;

            case DarkmiresEvent::EVENT_NEW_CHAT_MESSAGE:
                m_needRefreshChat = true;
                break;

            case DarkmiresEvent::EVENT_STATISTICS:
            {

                QVariantMap m = event.data().toMap();

                switch (m["type"].toUInt())
                {
                    case DarkmiresEvent::STATISTICS_PACKET_PROCESSING:

                        quint32 key = m["id"].toUInt();
                        double time = m["time"].toDouble() / 1000000.0; /// To ms

                        int graph = 1;

                        if (m["context"].toUInt() == Darkmires::WebEventSessionSuccess ||
                            m["context"].toUInt() == Darkmires::WebEventSessionError)
                        {
                            graph = 0;
                        }
                        else
                        {
                            break;
                        }

                        quint32 rangeStart = DsUtils::clamp<int>(key - ui->requestLimitBox->value(), 0, ui->requestLimitBox->maximum());

                        // add data to lines:
                        ui->requestPlot->graph(graph)->addData(key, time);

                        // remove data of lines that's outside visible range:
                        ui->requestPlot->graph(0)->removeDataBefore(DsUtils::clamp<int>(rangeStart - 1, 0, ui->requestLimitBox->maximum()));
//                        ui->reqProcGraph->graph(1)->removeDataBefore(DsUtils::clamp<int>(rangeStart - 1, 0, ui->requestLimitBox->maximum()));

                        // rescale value (vertical) axis to fit the current data:
                        ui->requestPlot->graph(0)->rescaleValueAxis();
//                        ui->reqProcGraph->graph(1)->rescaleValueAxis(true);

                        // make key axis range scroll with the data (at a constant range size of 8):
                        ui->requestPlot->xAxis->setRange(rangeStart, DsUtils::clamp<int>(key - 1, 0, ui->requestLimitBox->value()), Qt::AlignLeft);

                        ui->requestPlot->replot();

                        break;
                }

                break;
            }

            default:
                break;
        }
    }
}


void MainWindow::saveLog()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save Log", ".", "Text files (*.txt *.log)");

    if (! fileName.isEmpty()) {
        QFile file(fileName);

        if (file.open(QFile::WriteOnly)) {
            file.write(ui->consoleEdit->document()->toPlainText().toUtf8());

            file.close();
        }
    }
}


void MainWindow::refreshUsers()
{
    ui->usersTable->clear();

    if (! m_running) return;

    if (DsUserStorage::getInstance()->tryLock(WAIT_FOR_USERS)) {

        QList<Darkmires::UserMap::key_type> users = DsUserStorage::getInstance()->getUsers();

        for (Darkmires::UserMap::key_type i : users) {

            const DsUser * user = DsUserStorage::getInstance()->get(i);

            QTreeWidgetItem * item = new QTreeWidgetItem(ui->usersTable, 0);
            item->setText(0, QString::number(i));
            item->setText(1, user->getSessionID().toString());
            ui->usersTable->addTopLevelItem(item);

            QTreeWidgetItem * childItem = new QTreeWidgetItem(item, 0);
            childItem->setText(0, tr("Privileges"));
            childItem->setText(1, "0x" + QString::number(user->getPrivileges(), 16));
            item->addChild(childItem);

            childItem = new QTreeWidgetItem(item, childItem);
            childItem->setText(0, tr("UUID"));
            childItem->setText(1, user->getUuid().toString());
            childItem->setData(1, Qt::UserRole, user->getUuid().toString());
            item->addChild(childItem);

            childItem = new QTreeWidgetItem(item, childItem);
            childItem->setText(0, tr("Username"));
            childItem->setText(1, user->getName().isEmpty() ? "<Anonymous>" : user->getName());
            item->addChild(childItem);

            childItem = new QTreeWidgetItem(item, childItem);
            childItem->setText(0, tr("Sessions"));
            childItem->setText(1, QString::number(user->sessionCount()));
            item->addChild(childItem);

            for (const QString & session : user->getSessions())
            {
                QTreeWidgetItem * sessionItem = new QTreeWidgetItem(childItem);
                sessionItem->setText(0, session);
                childItem->addChild(sessionItem);
            }
        }

        UnlockUserStorage
    }
}


void MainWindow::refreshSessions()
{
    ui->sessionTable->clear();

    ui->chatSessionsBox->clear();

    if (! m_running) return;

    const Darkmires::SessionMap & sessions = m_darkmires->getSessions();

    for (SessionHandlers::iterator sh = m_sessionHandlers.begin(); sh != m_sessionHandlers.end();) {
        if (! sessions.contains(sh.key())) {

            if (sh.value() == m_currentSessionHandler) {
                emit notifySessionHandlerRemove();
            }

            sh = m_sessionHandlers.erase(sh);
        } else {
            ++sh;
        }
    }

    for (Darkmires::SessionMap::const_iterator i = sessions.begin(); i != sessions.end(); ++i) {

        ui->chatSessionsBox->addItem(i.key(), QDateTime::currentDateTimeUtc());

        QTreeWidgetItem * item = new QTreeWidgetItem(ui->sessionTable, 0);
        item->setText(0, i.key());

        ui->sessionTable->addTopLevelItem(item);

        QPushButton * button = new QPushButton("Edit...");
        button->setProperty("session-id", i.key());

        if (! m_sessionHandlers.contains(i.key())) {
            switch (i.value()->getMode()) {

                case Darkmires::SessionQuickShare:
                    m_sessionHandlers[i.key()] = std::shared_ptr<SessionHandler>(static_cast<SessionHandler *>(new QuickShareSession(m_darkmires, i.key(), this)));
                    break;

                case Darkmires::SessionScripted:
                    m_sessionHandlers[i.key()] = std::shared_ptr<SessionHandler>(static_cast<SessionHandler *>(new ScriptedSession(m_darkmires, i.key(), this)));
                    break;

                case Darkmires::SessionSubversion:
                    m_sessionHandlers[i.key()] = std::shared_ptr<SessionHandler>(static_cast<SessionHandler *>(new SubversionSession(m_darkmires, i.key(), this)));
                    break;

                default:
                    button->setEnabled(false);
                    break;
            }
        }

        connect(button, SIGNAL(clicked()), this, SLOT(sessionManageClicked()));

        ui->sessionTable->setItemWidget(item, 1, button);

        QTreeWidgetItem * childItem = new QTreeWidgetItem(item, 0);
        childItem->setText(0, tr("Name"));
        childItem->setText(1, i.value()->getName());
        item->addChild(childItem);

        childItem = new QTreeWidgetItem(item, childItem);
        childItem->setText(0, tr("Security"));
        childItem->setText(1, QString::number((int)i.value()->getAuthentication()));
        item->addChild(childItem);

        childItem = new QTreeWidgetItem(item, childItem);
        childItem->setText(0, tr("Mode"));
        childItem->setText(1, QString::number((int)i.value()->getMode()));
        item->addChild(childItem);

        childItem = new QTreeWidgetItem(item, childItem);
        childItem->setText(0, tr("Thread"));
        childItem->setText(1, QString::number(i.value()->getThreadId()));
        item->addChild(childItem);

        if (DsUserStorage::getInstance()->tryLock(WAIT_FOR_USERS)) {

            QList<Darkmires::UserMap::key_type> users = DsUserStorage::getInstance()->getUsers();

            childItem = new QTreeWidgetItem(item, childItem);
            childItem->setText(0, tr("Users"));
            childItem->setText(1, "Total: " + QString::number(users.count()));
            item->addChild(childItem);

            for (const Darkmires::UserMap::key_type & u : users) {
                const DsUser * user = DsUserStorage::getInstance()->get(u);

                if (user->inSession(i.key())) {
                    QTreeWidgetItem * userItem = new QTreeWidgetItem(childItem, nullptr);
                    userItem->setText(0, QString("ID"));
                    userItem->setText(1, QString::number(user->getId()));
                    childItem->addChild(userItem);
                }
            }

            UnlockUserStorage
        }

        const DsSessionInterface::SessionOwners & owners = i.value()->getOwners();

        childItem = new QTreeWidgetItem(item, childItem);
        childItem->setText(0, tr("Owners"));
        childItem->setText(1, "Total: " + QString::number(owners.count()));
        item->addChild(childItem);

        for (const QUuid & u : owners) {
            QTreeWidgetItem * ownerItem = new QTreeWidgetItem(childItem, nullptr);
            ownerItem->setText(0, QString("UUID"));
            ownerItem->setText(1, u.toString());
            childItem->addChild(ownerItem);
        }
    }
}


void MainWindow::refreshChat(const QString & session)
{
    qDebug() << "Refreshing chat for session: " << session;

    if (session != ui->chatSessionsBox->currentText()) return;

    Darkmires::ChatStorage chat = m_darkmires->getSessions().value(session)->getChat();

    qint64 previous = ui->chatSessionsBox->itemData(ui->chatSessionsBox->currentIndex()).toLongLong();

    for (Darkmires::ChatStorage::const_iterator i = chat.upperBound(previous); i != chat.end(); ++i) {

        QString user;

        const Darkmires::ChatMessage & message = i.value();

        if (i.value().user != 0) {
            if (DsUserStorage::getInstance()->contains(message.user)) {

                if (DsUserStorage::getInstance()->tryLock(WAIT_FOR_USERS)) {

                    user = DsUserStorage::getInstance()->get(message.user)->getName();

                    UnlockUserStorage

                } else {
                    user = "[UNAVAILABLE]";
                }
            } else {
                user = "[UNKNOWN]";
            }
        } else {
            user = "[SERVER]";
        }

        ui->chatText->append(QString("[%4] <b>%1</b> (%2): %3")
                             .arg(user.isEmpty() ? "Anonymous" : user)
                             .arg(message.id)
                             .arg(message.message)
                             .arg(QDateTime::fromMSecsSinceEpoch(i.key()).toString("hh:mm:ss.zzz")));
    }

    ui->chatSessionsBox->setItemData(ui->chatSessionsBox->currentIndex(), QDateTime::currentMSecsSinceEpoch());
}


QTreeWidgetItem * MainWindow::findCurrentTopLevel(QTreeWidget * widget)
{
    QTreeWidgetItem * item = widget->currentItem();

    if (item) {
        while (item->parent()) {
            item = item->parent();
        }
    }

    return item;
}

void MainWindow::setupRequestProcessGraph(QCustomPlot * graph)
{
    //ui->reqProcGraph->setInteractions();
    graph->xAxis->setRange(0, 100);
    graph->yAxis->setRange(0, 2000);
    graph->axisRect()->setupFullAxesBox();

    graph->yAxis->setNumberPrecision(2);

//    graph->plotLayout()->insertRow(0);
//    graph->plotLayout()->addElement(0, 0, new QCPPlotTitle(graph, "Request Processing"));

    graph->xAxis->setLabel("request (#)");
    graph->yAxis->setLabel("time (ms)");

    graph->legend->setVisible(false);

    graph->addGraph();
    graph->graph(0)->setName(QString("Session"));
    graph->graph(0)->setPen(QPen(Qt::blue));
    graph->graph(0)->setBrush(QBrush(QColor(200, 255, 180, 100)));
//    graph->graph(0)->setAntialiasedFill(true);
    graph->graph(0)->setLineStyle(QCPGraph::lsLine);
    //graph->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::red, 5));

//    graph->addGraph();
//    graph->graph(1)->setName(QString("Other"));
//    graph->graph(1)->setPen(QPen(Qt::green));
//    graph->graph(1)->setBrush(QBrush(QColor(240, 255, 200, 100)));
//    graph->graph(1)->setAntialiasedFill(true);
//    graph->graph(1)->setLineStyle(QCPGraph::lsLine);

    /// Add first point (request coming from ID 1)
    graph->graph(0)->addData(0, 0);
//    graph->graph(1)->addData(0, 0);

//    graph->xAxis->setTickLabelType(QCPAxis::ltDateTime);
//    graph->xAxis->setDateTimeFormat("hh:mm:ss");
//    graph->xAxis->setAutoTickStep(false);
//    graph->xAxis->setTickStep(2);
//    graph->axisRect()->setupFullAxesBox();

    connect(graph->xAxis, SIGNAL(rangeChanged(QCPRange)), graph->xAxis2, SLOT(setRange(QCPRange)));
    connect(graph->yAxis, SIGNAL(rangeChanged(QCPRange)), graph->yAxis2, SLOT(setRange(QCPRange)));
}

void MainWindow::setupCustomGraph(QCustomPlot * graph)
{
    graph->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    graph->axisRect()->setRangeZoom(Qt::Horizontal);
    graph->axisRect()->setRangeDragAxes(graph->xAxis, nullptr);

    graph->addGraph();
    graph->graph(0)->setName(QString("Graph"));
    graph->graph(0)->setPen(QPen(Qt::blue));
    graph->graph(0)->setBrush(QBrush(QColor(200, 255, 180, 100)));
}

void MainWindow::screenshot(QCustomPlot * plot)
{
    QString fileName = ui->statisticsTab->tabText(ui->statisticsTab->currentIndex()) + ".pdf";
    fileName.replace(" ", "");

    if (! plot->savePdf("screenshots/" + fileName))
    {
        logMessage("Error saving PDF file: " + fileName);
    }
}


void MainWindow::keyPressEvent(QKeyEvent * e)
{
    QMainWindow::keyPressEvent(e);

    if (e->modifiers() & Qt::ControlModifier)
    {
        ui->customPlot->axisRect()->setRangeZoom(Qt::Horizontal);
        ui->customPlot->axisRect()->setRangeDragAxes(ui->customPlot->xAxis, nullptr);
    }
    else
    {
        ui->customPlot->axisRect()->setRangeZoom(Qt::Vertical);
        ui->customPlot->axisRect()->setRangeDragAxes(nullptr, ui->customPlot->yAxis);
    }
}


void MainWindow::on_restartButton_clicked()
{
    on_stopButton_clicked();
    on_startButton_clicked();
}


void MainWindow::on_addSession_clicked()
{
    ui->sessionDateTime->setDateTime(QDateTime::currentDateTimeUtc().addSecs(60 * 60));

    ui->sessionWidget->setCurrentIndex(1);
}


void MainWindow::on_cancelSession_clicked()
{
    ui->sessionWidget->setCurrentIndex(0);
}


void MainWindow::on_refreshButton_clicked()
{
    ui->envWidget->maybeRefresh();
}


void MainWindow::on_confirmSession_clicked()
{
    Darkmires::AuthenticationType security = (Darkmires::AuthenticationType)ui->sessionSecurityCombo->itemData(ui->sessionSecurityCombo->currentIndex()).toInt();
    Darkmires::SessionMode mode = (Darkmires::SessionMode)ui->sessionModeCombo->itemData(ui->sessionModeCombo->currentIndex()).toInt();

    DsSessionInterface * session = m_darkmires->addSession(ui->sessionNameEdit->text(), security, mode, ui->sessionUuidEdit->text());

    if (session) {
        qDebug() << "Session added: " << session->getUuid().toString();

        if (ui->temporaryRadio->isChecked()) {
            session->setExpiration(ui->sessionDateTime->dateTime());
        }

        ui->sessionNameEdit->clear();
        ui->sessionUuidEdit->clear();

        switch (mode) {
            case Darkmires::SessionScripted:
                if (ui->scriptTreeWidget->currentItem())
                {
                    session->setSessionProperty("script-file", ui->scriptTreeWidget->currentItem()->toolTip(0));
                }
                break;

            default:
                break;
        }

        for (int i = 0; i < ui->sessionSettingsTable->rowCount(); i++)
        {
            QTableWidgetItem * key = ui->sessionSettingsTable->itemAt(0, i);
            QTableWidgetItem * value = ui->sessionSettingsTable->itemAt(1, i);

            session->setSessionProperty(key->text(), value->text());
        }

        if (! session->start()) {
            m_darkmires->removeSession(session->getUuid().toString(), true);

            logMessage("Unable to start session!");

        } else {
            ui->sessionWidget->setCurrentIndex(0);
        }
    }
}


void MainWindow::on_kickUserButton_clicked()
{
    QTreeWidgetItem * item = findCurrentTopLevel(ui->usersTable);

    if (item) {
        m_darkmires->kickUser(item->text(0).toInt());
    }
}


void MainWindow::on_temporaryRadio_toggled(bool checked)
{
    if (! checked) {
        ui->lifeSpanFrame->setEnabled(false);
    } else {
        ui->lifeSpanFrame->setEnabled(true);
    }
}


void MainWindow::on_removeSession_clicked()
{
    QTreeWidgetItem * item = findCurrentTopLevel(ui->sessionTable);

    if (item)
    {
        switch (QMessageBox::question(this, "Darkmire", "Really delete this session?", QMessageBox::Ok | QMessageBox::Cancel))
        {
            case QMessageBox::Ok:
                m_darkmires->removeSession(item->text(0), ui->sessionDeleteBox->isChecked());
                break;

            default:
                break;
        }
    }
}


void MainWindow::on_chatSessionsBox_currentIndexChanged(const QString & text)
{
    if (! text.isEmpty()) {

        ui->chatText->clear();

        for (int i = 0; i < ui->chatSessionsBox->count(); i++)
        {
            ui->chatSessionsBox->setItemData(i, m_serverStarted);
        }

        refreshChat(text);
    }
}


void MainWindow::on_addUserSqlButton_clicked()
{
    AddUserDialog dialog(this);

    switch (dialog.exec()) {
        case QDialog::Accepted:
        {
            QSqlRecord record = m_usersSqlModel->record();

            record.setValue("name", dialog.getName());
            record.setValue("password", dialog.getPassword());
            record.setValue("privileges", dialog.getPrivileges());
            record.setValue("salt", dialog.getSalt());
            record.setValue("session", dialog.getUuid().toString());

            if (! m_usersSqlModel->insertRecord(-1, record))
            {
                logMessage(m_usersSqlModel->lastError().text());
            }

            break;
        }

        default:
            break;
    }
}


void MainWindow::on_saveUsersSqlButton_clicked()
{
    QFont font = ui->saveUsersSqlButton->font();
    font.setBold(false);

    if (! m_usersSqlModel->submitAll())
    {
        logMessage(m_usersSqlModel->lastError().text());
        font.setBold(true);
    }

    ui->saveUsersSqlButton->setFont(font);
}


void MainWindow::on_revertUsersSqlButton_clicked()
{
    m_usersSqlModel->revertAll();
}


void MainWindow::on_clearUsersSqlButton_clicked()
{
    m_usersSqlModel->removeRows(0, m_usersSqlModel->rowCount());

    QFont font = ui->saveUsersSqlButton->font();
    font.setBold(true);
    ui->saveUsersSqlButton->setFont(font);
}


void MainWindow::on_removeUserSqlButton_clicked()
{
    m_usersSqlModel->removeRows(ui->usersSqlTable->currentIndex().row(), 1);

    QFont font = ui->saveUsersSqlButton->font();
    font.setBold(true);
    ui->saveUsersSqlButton->setFont(font);
}


void MainWindow::on_chatOkButton_clicked()
{
    Darkmires::ChatMessage message;

    message.message = ui->chatEdit->toPlainText().simplified();
    message.user = 0;

    m_darkmires->chatMessage(ui->chatSessionsBox->currentText(), std::move(message));

    ui->chatEdit->clear();
    ui->chatEdit->setFocus();

    on_refreshChatButton_clicked();
}


void MainWindow::on_chatEdit_textChanged()
{
    if (!(Qt::ShiftModifier & QApplication::keyboardModifiers()) && ui->chatEdit->toPlainText().endsWith('\n')) {
        on_chatOkButton_clicked();
    }
}


void MainWindow::on_loggingBox_toggled(bool checked)
{
    if (checked) {
        logMessage("[LOGGING ON]");
    } else {
        logMessage("[LOGGING OFF]");
    }
}


void MainWindow::on_refreshSessionsButton_clicked()
{
    refreshSessions();
}


void MainWindow::on_refreshUsersButton_clicked()
{
    refreshUsers();
}


void MainWindow::on_refreshChatButton_clicked()
{
    if (! ui->chatSessionsBox->currentText().isEmpty()) {
        refreshChat(ui->chatSessionsBox->currentText());
    }
}


void MainWindow::sessionManageClicked()
{
    QPushButton * button = static_cast<QPushButton *>(sender());

    if (m_currentSessionHandler) {
        m_currentSessionHandler->disconnect();
    }

    m_currentSessionHandler = m_sessionHandlers.value(button->property("session-id").toString());

    if (m_currentSessionHandler) {
        connect(m_currentSessionHandler.get(), SIGNAL(finished(int)), this, SLOT(sessionManagerClosed()));
        connect(m_currentSessionHandler.get(), SIGNAL(logMessage(QString)), this, SLOT(logMessage(QString)));

        connect(this, SIGNAL(notifySessionHandlerRemove()), m_currentSessionHandler.get(), SLOT(notifyRemove()));

        connect(m_darkmires,
                SIGNAL(darkmiresEvent(Darkmires::Event)),
                m_currentSessionHandler.get(),
                SLOT(darkmiresEvent(Darkmires::Event)));

        m_currentSessionHandler->notifyShow();

        m_currentSessionHandler->open();
    }
}


void MainWindow::sessionAddOwnerClicked()
{
}


void MainWindow::on_refreshOnEventBox_toggled(bool checked)
{
    if (checked) {
        m_refreshTimer.start();
    } else {
        m_refreshTimer.stop();
    }
}


void MainWindow::refreshTimer()
{
    if (m_needRefreshSessions) {
        on_refreshSessionsButton_clicked();
    }

    if (m_needRefreshUsers) {
        on_refreshUsersButton_clicked();
    }

    if (m_needRefreshChat) {
        on_refreshChatButton_clicked();
    }

    m_needRefreshChat = false;
    m_needRefreshSessions = false;
    m_needRefreshUsers = false;
}


void MainWindow::refreshScripts()
{
    ui->scriptTreeWidget->clear();
    ui->translationsTreeWidget->clear();

    for (const QString & key : m_darkmires->getScripts().keys()) {

        const Darkmires::ScriptInfo & script = m_darkmires->getScripts().value(key);

        QTreeWidgetItem * item = new QTreeWidgetItem(ui->scriptTreeWidget);

        item->setText(0, script.script.fileName());
        item->setToolTip(0, script.script.absoluteFilePath());
        item->setData(0, Qt::UserRole, key);

        ui->scriptTreeWidget->addTopLevelItem(item);
    }
}


void MainWindow::sessionManagerClosed()
{
    m_currentSessionHandler.reset();
}


void MainWindow::on_clearLogButton_clicked()
{
    qDebug() << "Console dumped to temporary file: " << m_logTempFile.fileName();

    m_logTempStream << ui->consoleEdit->document()->toPlainText().toUtf8();

    if (m_logFile.isOpen())
    {
        qDebug() << "Console dumped to log file: " << m_logFile.fileName();

        m_logStream << ui->consoleEdit->document()->toPlainText().toUtf8();
    }

    ui->consoleEdit->clear();
}


void MainWindow::on_usersSqlTable_doubleClicked(const QModelIndex & index)
{
    AddUserDialog dialog(this, true);

    dialog.setName(m_usersSqlModel->index(index.row(), 1).data().toString());
    dialog.setPrivileges(m_usersSqlModel->index(index.row(), 2).data().toUInt());
    dialog.setSalt(m_usersSqlModel->index(index.row(), 4).data().toString());
    dialog.setUuid(QUuid(m_usersSqlModel->index(index.row(), 5).data().toString()));

    switch (dialog.exec()) {
        case QDialog::Accepted:
        {
            QSqlRecord record = m_usersSqlModel->record();

            if (! dialog.getPassword().isEmpty())
            {
                QByteArray password = QCryptographicHash::hash((dialog.getSalt() + dialog.getPassword()).toLatin1(), QCryptographicHash::Sha1).toHex();
                record.setValue("salt", dialog.getSalt());
                record.setValue("password", QString::fromLatin1(password));
            }
            else
            {
                record.setValue("password", m_usersSqlModel->index(index.row(), 3).data().toString());
                record.setValue("salt", m_usersSqlModel->index(index.row(), 4).data().toString());
            }

            record.setValue("id", m_usersSqlModel->index(index.row(), 0).data().toULongLong());
            record.setValue("name", dialog.getName());
            record.setValue("privileges", dialog.getPrivileges());
            record.setValue("session", dialog.getUuid().toString());

            if (! m_usersSqlModel->setRecord(index.row(), record))
            {
                logMessage(m_usersSqlModel->lastError().text());
            }

            QFont font = ui->saveUsersSqlButton->font();
            font.setBold(m_usersSqlModel->isDirty(index));
            ui->saveUsersSqlButton->setFont(font);

            break;
        }

        default:
            break;
    }
}


void MainWindow::on_usersTable_itemDoubleClicked(QTreeWidgetItem * item, int)
{
    if (! item->data(1, Qt::UserRole).isNull()) {
        QApplication::clipboard()->setText(item->data(1, Qt::UserRole).toString());

        QMessageBox::information(this, "Darkmires", "User UUID copied to clipboard.", QMessageBox::Ok);
    }
}


void MainWindow::on_copyUuidButton_clicked()
{
    if (ui->usersTable->currentItem()) {
        on_usersTable_itemDoubleClicked(ui->usersTable->currentItem(), ui->usersTable->currentColumn());
    }
}


void MainWindow::on_sessionModeCombo_currentIndexChanged(int index)
{
    switch ((Darkmires::SessionMode)ui->sessionModeCombo->itemData(index).toInt()) {
        case Darkmires::SessionScripted:
            refreshScripts();
            ui->sessionSettingsWidget->setCurrentWidget(ui->scriptedSessionSettingsPage);
            break;

        default:
            ui->sessionSettingsWidget->setCurrentWidget(ui->defaultSessionSettingsPage);
            break;
    }
}


void MainWindow::on_scriptTreeWidget_currentItemChanged(QTreeWidgetItem * current, QTreeWidgetItem * previous)
{
    Q_UNUSED(previous)

    if (! current) return;

    ui->translationsTreeWidget->clear();

    const Darkmires::ScriptInfo & info = m_darkmires->getScripts().value(current->data(0, Qt::UserRole).toString());

    ui->translationsTreeWidget->addTopLevelItem(new QTreeWidgetItem(ui->translationsTreeWidget, QStringList("Default")));

    for (const QString & locale : info.translations.keys()) {
        QTreeWidgetItem * item = new QTreeWidgetItem(ui->translationsTreeWidget);

        item->setText(0, locale);

        item->setText(1, info.translations.value(locale).fileName());
        item->setToolTip(1, info.translations.value(locale).filePath());

        ui->translationsTreeWidget->addTopLevelItem(item);
    }
}


void MainWindow::on_consoleEdit_textChanged()
{
    if (ui->consoleEdit->document()->characterCount() > MAX_CONSOLE_CHARS) {
        on_clearLogButton_clicked();
    }
}

void MainWindow::printConnections()
{
    logMessage("### Connections ###");

    foreach (const QString & connection, QSqlDatabase::connectionNames())
    {
        logMessage(connection);
    }

    logMessage("###################");
}

void MainWindow::on_sessionSettingAdd_clicked()
{
    QTableWidgetItem * item = new QTableWidgetItem();

    ui->sessionSettingsTable->setItem(ui->sessionSettingsTable->rowCount(), 0, item);

    item = new QTableWidgetItem();

    ui->sessionSettingsTable->setItem(ui->sessionSettingsTable->rowCount(), 1, item);

    ui->sessionSettingsTable->setRowCount(ui->sessionSettingsTable->rowCount() + 1);
}

void MainWindow::on_sessionSettingRemove_clicked()
{
    if (ui->sessionSettingsTable->currentRow() >= 0)
    {
        ui->sessionSettingsTable->removeRow(ui->sessionSettingsTable->currentRow());
    }
}


void MainWindow::on_clearGraphsButton_clicked()
{
    double key1 = (ui->requestPlot->graph(0)->data()->end() - 1).key();
    ui->requestPlot->graph(0)->removeData(0, key1);

//    double key2 = (ui->reqProcGraph->graph(1)->data()->end() - 1).key();
//    ui->reqProcGraph->graph(1)->removeData(0, key2);

    ui->requestPlot->xAxis->setRange(key1, key1 + ui->requestLimitBox->value(), Qt::AlignRight);
    ui->requestPlot->yAxis->setRange(0, 2000);


    ui->requestPlot->rescaleAxes();
    ui->requestPlot->replot();
}

void MainWindow::on_screenshotButton_clicked()
{
    if (ui->statisticsTab->currentWidget() == ui->requestsTab)
    {
        screenshot(ui->requestPlot);
    }
    else if (ui->statisticsTab->currentWidget() == ui->customTab)
    {
        screenshot(ui->customPlot);
    }
}

void MainWindow::on_loadCustomPlotButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open plot", ".", "Plot files (*.plot)");

    QFile f(fileName);

    if (f.open(QFile::ReadOnly))
    {
        ui->customPlot->graph(0)->clearData();

        QTextStream s(&f);

        QString x, y;
        double key, value;

        x = s.readLine(75);
        y = s.readLine(75);

        ui->customPlot->xAxis->setLabel(x);
        ui->customPlot->yAxis->setLabel(y);

        while (! s.atEnd())
        {
            s >> key >> value;

            ui->customPlot->graph(0)->addData(key, value);
        }

//        ui->requestPlot->graph(0)->rescaleValueAxis();
//        ui->requestPlot->xAxis->setRange(0, value);


        ui->requestPlot->rescaleAxes();
        ui->requestPlot->replot();

        f.close();
    }
}
