#ifndef ADDSESSIONUSERDIALOG_HPP
#define ADDSESSIONUSERDIALOG_HPP

#include <QDialog>

namespace Ui {
    class AddSessionUserDialog;
}

class AddSessionUserDialog : public QDialog
{
        Q_OBJECT

    public:
        explicit AddSessionUserDialog(QWidget *parent = 0);
        ~AddSessionUserDialog();

        QString getUuid() const;

    private:
        Ui::AddSessionUserDialog *ui;
};

#endif // ADDSESSIONUSERDIALOG_HPP
