#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include "darkmires.hpp"

#include <memory>

#include <QTreeWidgetItem>

#include <QSqlDatabase>
#include <QSqlTableModel>

#include <QTemporaryFile>

#include "sessionhandler.hpp"

#include "3rd/qcustomplot.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT
    public:
        explicit MainWindow(QWidget * parent = 0);
        virtual ~MainWindow();

        static QSettings * getSettings();

    public slots:
        void logMessage(const QString & msg);

        void darkmiresEvent(Darkmires::Event event);

    private slots:
        void saveLog();

        void on_startButton_clicked();
        void on_stopButton_clicked();
        void on_restartButton_clicked();
        void on_addSession_clicked();
        void on_cancelSession_clicked();
        void on_refreshButton_clicked();
        void on_confirmSession_clicked();
        void on_kickUserButton_clicked();
        void on_temporaryRadio_toggled(bool checked);
        void on_removeSession_clicked();
        void on_chatSessionsBox_currentIndexChanged(const QString & text);
        void on_addUserSqlButton_clicked();
        void on_saveUsersSqlButton_clicked();
        void on_revertUsersSqlButton_clicked();
        void on_clearUsersSqlButton_clicked();
        void on_removeUserSqlButton_clicked();
        void on_chatOkButton_clicked();
        void on_chatEdit_textChanged();
        void on_loggingBox_toggled(bool checked);
        void on_refreshSessionsButton_clicked();
        void on_refreshUsersButton_clicked();
        void on_refreshChatButton_clicked();
        void on_refreshOnEventBox_toggled(bool checked);
        void on_clearLogButton_clicked();
        void on_usersSqlTable_doubleClicked(const QModelIndex & index);

        void sessionManageClicked();
        void sessionAddOwnerClicked();

        void refreshTimer();

        void refreshScripts();

        void sessionManagerClosed();

        void on_usersTable_itemDoubleClicked(QTreeWidgetItem * item, int);

        void on_copyUuidButton_clicked();

        void on_sessionModeCombo_currentIndexChanged(int index);

        void on_scriptTreeWidget_currentItemChanged(QTreeWidgetItem * current, QTreeWidgetItem * previous);

        void on_consoleEdit_textChanged();

        void printConnections();

        void on_sessionSettingAdd_clicked();

        void on_sessionSettingRemove_clicked();

        void on_clearGraphsButton_clicked();

        void on_screenshotButton_clicked();

        void on_loadCustomPlotButton_clicked();

    private:
        Ui::MainWindow * ui;

        Darkmires * m_darkmires;

        bool m_running = false;

        void refreshUsers();
        void refreshSessions();
        void refreshChat(const QString & session);

        QTreeWidgetItem * findCurrentTopLevel(QTreeWidget * widget);

        static std::unique_ptr<QSettings> m_settings;

        QDateTime m_serverStarted;

        QSqlDatabase m_database;
        std::unique_ptr<QSqlTableModel> m_usersSqlModel;

        QTimer m_refreshTimer;

        bool m_needRefreshSessions = false;
        bool m_needRefreshUsers = false;
        bool m_needRefreshChat = false;

        std::shared_ptr<SessionHandler> m_currentSessionHandler;


        typedef QHash<QString, std::shared_ptr<SessionHandler>> SessionHandlers;

        SessionHandlers m_sessionHandlers;

        QTemporaryFile m_logTempFile;
        QFile m_logFile;

        QTextStream m_logTempStream;
        QTextStream m_logStream;

        void setupRequestProcessGraph(QCustomPlot * graph);
        void setupCustomGraph(QCustomPlot * graph);

        void screenshot(QCustomPlot * plot);

    signals:
        void notifySessionHandlerRemove();

    public:
        void keyPressEvent(QKeyEvent * e);

};

#endif // MAINWINDOW_HPP
