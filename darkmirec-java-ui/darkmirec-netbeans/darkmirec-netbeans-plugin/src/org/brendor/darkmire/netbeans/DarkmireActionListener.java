package org.brendor.darkmire.netbeans;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@ActionID(
    category = "Darkmire",
id = "org.brendor.darkmire.netbeans.DarkmireActionListener")
@ActionRegistration(
    iconBase = "org/brendor/darkmire/netbeans/graphics/darkmire.png",
displayName = "#CTL_DarkmireActionListener")
@ActionReferences({
    @ActionReference(path = "Menu/Versioning", position = -10, separatorBefore = -60, separatorAfter= 60),
    @ActionReference(path = "Toolbars/File", position = 700),
    @ActionReference(path = "Shortcuts", name = "DS-D")
})
@Messages("CTL_DarkmireActionListener=Darkmire")
public final class DarkmireActionListener implements ActionListener {

    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        TopComponent top = DarkmireTopComponent.get();
        
        if (top == null)
        {
            top = new DarkmireTopComponent();            
        }
        
        top.openAtTabPosition(-1);
        top.requestActive();
    }
}
