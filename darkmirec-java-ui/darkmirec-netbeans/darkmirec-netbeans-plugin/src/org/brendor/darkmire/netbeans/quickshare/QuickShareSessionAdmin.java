package org.brendor.darkmire.netbeans.quickshare;

import org.brendor.darkmire.netbeans.SessionPanel;
import darkmirec.core.DarkmireException;
import darkmirec.events.DarkmireDataEvent;
import darkmirec.events.DarkmireDisconnectEvent;
import darkmirec.events.DarkmireEvent;
import darkmirec.sessions.DarkmireQuickShareSession;
import darkmirec.utils.DarkmireEventHandler;
import darkmirec.utils.DarkmireUtils;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import javax.activation.DataHandler;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.brendor.darkmire.netbeans.DarkmireTopComponent;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.openide.util.Exceptions;


/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class QuickShareSessionAdmin extends SessionPanel implements DarkmireEventHandler
{


    @Override
    public void closing()
    {
        
    }
    
    private class EntryListDrop implements DropTargetListener
    {
        @Override
        public void dragEnter(DropTargetDragEvent dtde)
        {

        }

        @Override
        public void dragOver(DropTargetDragEvent dtde)
        {

        }

        @Override
        public void dropActionChanged(DropTargetDragEvent dtde)
        {

        }

        @Override
        public void dragExit(DropTargetEvent dte)
        {

        }

        @Override
        public void drop(DropTargetDropEvent dtde)
        {
            Transferable tr = dtde.getTransferable();
            
            for (DataFlavor flavor : tr.getTransferDataFlavors())
            {
                if ("darkmire/quick-entry".equals(flavor.getMimeType()))
                {
                    try {
                        Entry e = (Entry) tr.getTransferData(flavor);

                        dtde.acceptDrop(DnDConstants.ACTION_LINK);

                        m_TextEditor.setText(e.getCode());
                        
                        for (int i = 0; i < typeBox.getModel().getSize(); i++)
                        {
                            if (((String) typeBox.getModel().getElementAt(i)).equals(e.getMimetype()))
                            {
                                typeBox.setSelectedIndex(i);
                                
                                break;
                            }
                        }
                        
                        entryList.setSelectedValue(e, true);
                        
                        statusLabel.setText("Entry from: " + e.getAuthor());

                        dtde.dropComplete(true);

                    } catch (UnsupportedFlavorException e) {

                    } catch (IOException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
            
            
        }
    }
    
    
    private class EntryListRenderer implements ListCellRenderer<Entry>
    {
        @Override
        public Component getListCellRendererComponent(JList<? extends Entry> list,
                Entry value,
                int index,
                boolean isSelected,
                boolean cellHasFocus)
        {
            EntryListComponent component = new EntryListComponent(value);

            component.setFocused(cellHasFocus);

            return component;
        }

    }
    
    private class EntryListDrag implements DragGestureListener, DragSourceListener
    {
        @Override
        public void dragGestureRecognized(DragGestureEvent dge)
        {
            JList list = (JList) dge.getComponent();
            
            if (! list.isSelectionEmpty())
            {
                Entry entry = (Entry) list.getSelectedValue();

                Transferable transferable = new DataHandler(entry, "darkmire/quick-entry");

                m_dragSource.startDrag(dge, DragSource.DefaultLinkDrop, transferable, this);
            }
        }


        @Override
        public void dragEnter(DragSourceDragEvent dsde)
        {

        }


        @Override
        public void dragOver(DragSourceDragEvent dsde)
        {

        }


        @Override
        public void dropActionChanged(DragSourceDragEvent dsde)
        {

        }


        @Override
        public void dragExit(DragSourceEvent dse)
        {
            
        }


        @Override
        public void dragDropEnd(DragSourceDropEvent dsde)
        {

        }
    }
    
    private RSyntaxTextArea m_TextEditor = null;
    
    private DropTarget m_dropTarget = null;
    private DragSource m_dragSource = new DragSource();
    
    private EntryListDrag m_dragListener = new EntryListDrag();
    private EntryListDrop m_dropListener = new EntryListDrop();
    
    /**
     * Creates new form QuickShareSession
     */
    public QuickShareSessionAdmin(DarkmireQuickShareSession session)
    {
        super(session);
        
        m_TextEditor = new RSyntaxTextArea(20, 60);
        m_TextEditor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
        m_TextEditor.setCodeFoldingEnabled(true);
        m_TextEditor.setAntiAliasingEnabled(true);
        
        initComponents();
        
        for (Field s : SyntaxConstants.class.getFields())
        {
            if (Modifier.isStatic(s.getModifiers()) &&
                s.getType() == String.class)
            {
                try {
                    typeBox.addItem((String) s.get(null));
                } catch (IllegalArgumentException | IllegalAccessException ex)
                {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
        
        m_TextEditor.setDragEnabled(false);
        m_TextEditor.setDropTarget(m_dropTarget);
        
        entryList.setDragEnabled(false);
        entryList.setDropMode(DropMode.USE_SELECTION);
        
        m_dragSource.createDefaultDragGestureRecognizer(entryList, DnDConstants.ACTION_LINK, m_dragListener);
        
        m_dropTarget = new DropTarget(m_TextEditor, m_dropListener);
        
        scrollPane.setViewportView(m_TextEditor);
        scrollPane.setFoldIndicatorEnabled(true);
        scrollPane.setLineNumbersEnabled(true);

        infoLabel.setText(m_session.getId());
        
        entryListScroller.setSize(350, entryList.getHeight());
        
        entryList.setCellRenderer(new EntryListRenderer());
        
        refreshButtonActionPerformed(null);
    }   


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        infoLabel = new javax.swing.JLabel();
        disconnectButton = new javax.swing.JButton();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        sendButton = new javax.swing.JButton();
        scrollPane = new org.fife.ui.rtextarea.RTextScrollPane();
        typeBox = new javax.swing.JComboBox();
        statusLabel = new javax.swing.JLabel();
        scrollPane1 = new org.fife.ui.rtextarea.RTextScrollPane();
        jPanel3 = new javax.swing.JPanel();
        entryListScroller = new javax.swing.JScrollPane();
        entryList = new javax.swing.JList<Entry>();
        refreshButton = new javax.swing.JButton();

        infoLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 14)); // NOI18N
        infoLabel.setForeground(new java.awt.Color(12, 115, 28));
        org.openide.awt.Mnemonics.setLocalizedText(infoLabel, org.openide.util.NbBundle.getMessage(QuickShareSessionAdmin.class, "QuickShareSession.infoLabel.text")); // NOI18N
        infoLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        disconnectButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Login-out-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(disconnectButton, org.openide.util.NbBundle.getMessage(QuickShareSessionAdmin.class, "QuickShareSession.disconnectButton.text")); // NOI18N
        disconnectButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                disconnectButtonActionPerformed(evt);
            }
        });

        sendButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Save-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(sendButton, org.openide.util.NbBundle.getMessage(QuickShareSessionAdmin.class, "QuickShareSession.sendButton.text")); // NOI18N
        sendButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                sendButtonActionPerformed(evt);
            }
        });

        typeBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                typeBoxActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(statusLabel, org.openide.util.NbBundle.getMessage(QuickShareSessionAdmin.class, "QuickShareSession.statusLabel.text")); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(sendButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(typeBox, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(sendButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(typeBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
        );

        jSplitPane2.setLeftComponent(jPanel2);

        entryListScroller.setPreferredSize(new java.awt.Dimension(250, 130));

        entryList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        entryList.setDragEnabled(true);
        entryListScroller.setViewportView(entryList);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(entryListScroller, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(entryListScroller, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jSplitPane2.setRightComponent(jPanel3);

        refreshButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Refresh-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(refreshButton, "Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                refreshButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSplitPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(disconnectButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(infoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(refreshButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(disconnectButton)
                    .addComponent(infoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refreshButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void typeBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_typeBoxActionPerformed
    {//GEN-HEADEREND:event_typeBoxActionPerformed
        m_TextEditor.setSyntaxEditingStyle(typeBox.getSelectedItem().toString());
    }//GEN-LAST:event_typeBoxActionPerformed

    private void disconnectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_disconnectButtonActionPerformed
    {//GEN-HEADEREND:event_disconnectButtonActionPerformed
        DarkmireTopComponent top = DarkmireTopComponent.get();

        try
        {
            top.enqueue(new DarkmireDisconnectEvent(m_session), this);
        }
        catch (DarkmireException ex)
        {
            System.out.println(ex.getMessage());
        }
    }//GEN-LAST:event_disconnectButtonActionPerformed

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_sendButtonActionPerformed
    {//GEN-HEADEREND:event_sendButtonActionPerformed
        DarkmireTopComponent top = DarkmireTopComponent.get();

        JSONObject data = new JSONObject();

        JSONObject entry = new JSONObject();

        entry.put("code", DarkmireUtils.quote(m_TextEditor.getText()));
        entry.put("mime", m_TextEditor.getSyntaxEditingStyle());

        data.put("entry", entry);
        data.put("type", DarkmireQuickShareSession.RequestType.REQUEST_ENTRY.code());

        try {
            top.enqueue(new DarkmireDataEvent(m_session, data), this);
        } catch (DarkmireException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        refreshButtonActionPerformed(null);
        
    }//GEN-LAST:event_sendButtonActionPerformed

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_refreshButtonActionPerformed
    {//GEN-HEADEREND:event_refreshButtonActionPerformed
        DarkmireTopComponent top = DarkmireTopComponent.get();

        JSONObject data = new JSONObject();

        data.put("type", DarkmireQuickShareSession.RequestType.REQUEST_ENTRY_LIST.code());

        try
        {
            top.enqueue(new DarkmireDataEvent(m_session, data), this);
        }
        catch (DarkmireException ex)
        {
            Exceptions.printStackTrace(ex);
        }
    }//GEN-LAST:event_refreshButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton disconnectButton;
    private javax.swing.JList entryList;
    private javax.swing.JScrollPane entryListScroller;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JButton refreshButton;
    private org.fife.ui.rtextarea.RTextScrollPane scrollPane;
    private org.fife.ui.rtextarea.RTextScrollPane scrollPane1;
    private javax.swing.JButton sendButton;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JComboBox typeBox;
    // End of variables declaration//GEN-END:variables


    @Override
    public void event(DarkmireEvent event)
    {
        System.out.println("QuickShare admin event: " + event.getClass().getName());
        
        if (event instanceof DarkmireDataEvent)
        {
            JSONObject data = ((DarkmireDataEvent)event).getData();
            
            if (data != null &&
                data.containsKey("entries"))
            {
                JSONArray entries = data.getJSONArray("entries");
                
                DefaultListModel<Entry> model = new DefaultListModel<>();
                
                for (int i = 0; i < entries.size(); i++)
                {
                    model.addElement(new Entry(entries.getJSONObject(i)));
                }
                
                entryList.setModel(model);
            }
            else
            {
                java.util.Date date = new java.util.Date();
                Timestamp timestamp = new Timestamp(date.getTime());

                switch (event.getStatus())
                {
                    case WEB_EVENT_SESSION_SUCCESS:
                        statusLabel.setText("Sent successfully! (" + timestamp.toString() + ")");
                        break;
                        
                    default:
                        statusLabel.setText("Error sending entry: " + event.getErrorMessage());
                        break;
                }
            }
        }
    }
}
