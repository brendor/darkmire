package org.brendor.darkmire.netbeans.completion;

//import org.brendor.darkmire.netbeans.managedrepository.ManagedRepositoryCodeCompletionItem;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.spi.editor.completion.CompletionProvider;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
@MimeRegistration(mimeType = "text/x-java", position = 101, service = CompletionProvider.class)
public class JavaCompletionProvider implements CompletionProvider {

    @Override
    public CompletionTask createTask(int queryType, JTextComponent component)
    {
        if (queryType != CompletionProvider.COMPLETION_QUERY_TYPE) {
            return null;
        }

        return new AsyncCompletionTask(new AsyncCompletionQuery() {
            @Override
            protected void query(CompletionResultSet resultSet, Document doc, int caretOffset)
            {
//                resultSet.addItem(new ManagedRepositoryCodeCompletionItem(caretOffset));

                resultSet.finish();
            }
        });
    }


    @Override
    public int getAutoQueryTypes(JTextComponent component, String typedText)
    {
        return 0;
    }
}
