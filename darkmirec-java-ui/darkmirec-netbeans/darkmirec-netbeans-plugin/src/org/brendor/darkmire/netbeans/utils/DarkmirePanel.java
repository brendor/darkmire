package org.brendor.darkmire.netbeans.utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmirePanel extends JPanel
{
    private TexturePaint m_painter = null;
    
    
    public DarkmirePanel(Image img)
    {
        if (img != null) {
            setupImage(img);
        }
    }
    
    
    private void setupImage(Image img)
    {
        int w = img.getWidth(null);
        int h = img.getHeight(null);
        
        BufferedImage buff = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB_PRE);
        
        Graphics2D g = buff.createGraphics();
        g.drawImage(img, 0, 0, null);
        m_painter = new TexturePaint(buff, new Rectangle(0, 0, w, h));
        
        g.dispose();
    }


    @Override
    protected void paintComponent(Graphics g)
    {
        if (m_painter != null) {
            int w = getWidth();
            int h = getHeight();
            
            Insets in = getInsets();

            int x = getX() + in.left;
            int y = getY() + in.top;
            w = w - in.left - in.right;
            h = h - in.top - in.bottom;

            if (w >= 0 && h >= 0) {
                Graphics2D g2d = (Graphics2D) g;
                
                Paint pt = g2d.getPaint();
                
                g2d.setPaint(m_painter);
                
//                g2d.fillRect(x + (w - m_painter.getImage().getWidth()), y + (h - m_painter.getImage().getHeight()), w, h);
                g2d.fillRect(x, y, w, h);
                
                g2d.setPaint(pt);
            }
        }
        
        super.paintComponent(g);
    }
}
