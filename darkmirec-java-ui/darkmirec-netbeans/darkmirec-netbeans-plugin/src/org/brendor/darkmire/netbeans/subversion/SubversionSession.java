package org.brendor.darkmire.netbeans.subversion;

import darkmirec.core.DarkmireException;
import darkmirec.events.DarkmireDataEvent;
import darkmirec.events.DarkmireDisconnectEvent;
import darkmirec.events.DarkmireEvent;
import darkmirec.utils.DarkmireEventHandler;
import darkmirec.sessions.DarkmireSubversionSession;
import java.io.File;
import java.net.URI;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import net.sf.json.JSONObject;
import org.brendor.darkmire.netbeans.DarkmireTopComponent;
import org.brendor.darkmire.netbeans.SessionPanel;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;
import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.wc.ISVNEventHandler;
import org.tmatesoft.svn.core.wc.SVNEvent;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc.admin.ISVNAdminEventHandler;
import org.tmatesoft.svn.core.wc.admin.SVNAdminEvent;
import org.tmatesoft.svn.core.wc2.SvnCheckout;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;
import org.tmatesoft.svn.core.wc2.SvnTarget;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class SubversionSession extends SessionPanel implements DarkmireEventHandler
{
    public static final int SUBVERSION_DEFAULT_PORT = 3690;
    
    private final ISVNEventHandler m_commonEventHandler = new ISVNAdminEventHandler()
    {
        @Override
        public void handleAdminEvent(SVNAdminEvent svnae, double d) throws SVNException
        {
            
        }

        @Override
        public void handleEvent(SVNEvent svne, double d) throws SVNException
        {
            
        }

        @Override
        public void checkCancelled() throws SVNCancelException
        {
            
        }
    };
    
    final SvnOperationFactory m_svnOperationFactory = new SvnOperationFactory();
    
    private String m_path;
    private int m_port;
    private String m_name;
    private String m_password;
    
    
    /**
     * Creates new form QuickShareSession
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public SubversionSession(DarkmireSubversionSession session)
    {
        super(session);
        
        m_svnOperationFactory.setEventHandler(m_commonEventHandler);
                
        System.out.println("Subversion session init: " + session.getName());
        
        initComponents();

        infoLabel.setText(m_session.getId());
        
        DarkmireTopComponent top = (DarkmireTopComponent) WindowManager.getDefault().findTopComponent(DarkmireTopComponent.COMPONENET_NAME);

        JSONObject data = new JSONObject();

        data.put("type", DarkmireSubversionSession.RequestType.REQUEST_STATUS.code());

        try
        {
            top.enqueue(new DarkmireDataEvent(m_session, data), this);
        }
        catch (DarkmireException ex)
        {
            Exceptions.printStackTrace(ex);
        }
        
        StyledDocument doc = infoTextPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
        
        showButtonActionPerformed(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        infoLabel = new javax.swing.JLabel();
        disconnectButton = new javax.swing.JButton();
        mainPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        infoTextPane = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();
        passwordField = new javax.swing.JPasswordField();
        showButton = new javax.swing.JToggleButton();
        createRepoButton = new javax.swing.JButton();
        checkoutButton = new javax.swing.JButton();
        pathField = new javax.swing.JTextField();

        infoLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 14)); // NOI18N
        infoLabel.setForeground(new java.awt.Color(12, 115, 28));
        org.openide.awt.Mnemonics.setLocalizedText(infoLabel, org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.infoLabel.text")); // NOI18N
        infoLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        disconnectButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Login-out-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(disconnectButton, org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.disconnectButton.text")); // NOI18N
        disconnectButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                disconnectButtonActionPerformed(evt);
            }
        });

        mainPanel.setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.jLabel1.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 24;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel1.add(jLabel1, gridBagConstraints);

        nameField.setEditable(false);
        nameField.setText(org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.nameField.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 289;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        jPanel1.add(nameField, gridBagConstraints);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.jLabel2.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 5);
        jPanel1.add(jLabel2, gridBagConstraints);

        jScrollPane3.setBorder(null);
        jScrollPane3.setViewportBorder(null);
        jScrollPane3.setMinimumSize(new java.awt.Dimension(81, 100));
        jScrollPane3.setPreferredSize(new java.awt.Dimension(81, 100));

        infoTextPane.setEditable(false);
        infoTextPane.setBorder(null);
        infoTextPane.setText(org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.infoTextPane.text")); // NOI18N
        infoTextPane.setEnabled(false);
        infoTextPane.setMinimumSize(new java.awt.Dimension(87, 300));
        jScrollPane3.setViewportView(infoTextPane);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 0);
        jPanel1.add(jScrollPane3, gridBagConstraints);

        passwordField.setEditable(false);
        passwordField.setText(org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.passwordField.text")); // NOI18N
        passwordField.setEchoChar('\u0000');

        showButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Lock16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(showButton, org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.showButton.text")); // NOI18N
        showButton.setToolTipText(org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.showButton.toolTipText")); // NOI18N
        showButton.setMaximumSize(new java.awt.Dimension(28, 25));
        showButton.setMinimumSize(new java.awt.Dimension(28, 25));
        showButton.setPreferredSize(new java.awt.Dimension(28, 25));
        showButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(passwordField)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(showButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(showButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 66;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        mainPanel.add(jPanel1, gridBagConstraints);

        createRepoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/File-add-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(createRepoButton, org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.createRepoButton.text")); // NOI18N
        createRepoButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                createRepoButtonActionPerformed(evt);
            }
        });

        checkoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Folder16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(checkoutButton, org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.checkoutButton.text")); // NOI18N
        checkoutButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                checkoutButtonActionPerformed(evt);
            }
        });

        pathField.setEditable(false);
        pathField.setText(org.openide.util.NbBundle.getMessage(SubversionSession.class, "SubversionSession.pathField.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pathField)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(disconnectButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(infoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(createRepoButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(checkoutButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(createRepoButton)
                    .addComponent(checkoutButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pathField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(disconnectButton, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(infoLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void disconnectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_disconnectButtonActionPerformed
    {//GEN-HEADEREND:event_disconnectButtonActionPerformed
        DarkmireTopComponent top = DarkmireTopComponent.get();

        try
        {
            top.enqueue(new DarkmireDisconnectEvent(m_session), this);
        }
        catch (DarkmireException ex)
        {
            System.out.println(ex.getMessage());
        }
    }//GEN-LAST:event_disconnectButtonActionPerformed

    private void createRepoButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_createRepoButtonActionPerformed
    {//GEN-HEADEREND:event_createRepoButtonActionPerformed
        DarkmireTopComponent top = (DarkmireTopComponent) WindowManager.getDefault().findTopComponent(DarkmireTopComponent.COMPONENET_NAME);

        JSONObject data = new JSONObject();

        data.put("type", DarkmireSubversionSession.RequestType.REQUEST_CREATE.code());

        try {
            top.enqueue(new DarkmireDataEvent(m_session, data), this);
        } catch (DarkmireException ex) {
            Exceptions.printStackTrace(ex);
        }
    }//GEN-LAST:event_createRepoButtonActionPerformed

    private void checkoutButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_checkoutButtonActionPerformed
    {//GEN-HEADEREND:event_checkoutButtonActionPerformed
        
        JFileChooser chooseDir = new JFileChooser();
        
        chooseDir.setMultiSelectionEnabled(false);
        chooseDir.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        
        if (chooseDir.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            File f = chooseDir.getSelectedFile();
            
            if (! f.exists() || ! f.canWrite() || ! f.canRead())
            {
                NotifyDescriptor nd = new NotifyDescriptor.Message("Invalid directory selected.",
                                                                   NotifyDescriptor.INFORMATION_MESSAGE);
                DialogDisplayer.getDefault().notify(nd);
                
                return;
            }
            
            if (SVNWCUtil.isVersionedDirectory(f))
            {
                JOptionPane.showMessageDialog(this, "Directory already under version control.", "Darkmire", JOptionPane.WARNING_MESSAGE);
                
                return;
            }
            
            URI dserver = DarkmireTopComponent.get().getServerAddress();
            
            try
            {
                final SvnCheckout checkout = m_svnOperationFactory.createCheckout();
                
                SVNURL url = SVNURL.create("svn", dserver.getUserInfo(), dserver.getHost(), m_port, m_path, true);
                
                checkout.setSingleTarget(SvnTarget.fromFile(f));
                checkout.setSource(SvnTarget.fromURL(url));
                checkout.setDepth(SVNDepth.INFINITY);
                //... other options
                checkout.run();
            }
            catch (SVNException ex)
            {
                JOptionPane.showMessageDialog(this, ex.getErrorMessage(), "Darkmire", JOptionPane.ERROR_MESSAGE);
            }
            finally
            {
                m_svnOperationFactory.dispose();
            }
        }
    }//GEN-LAST:event_checkoutButtonActionPerformed

    private void showButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showButtonActionPerformed
    {//GEN-HEADEREND:event_showButtonActionPerformed
        if (! showButton.isSelected())
        {
            passwordField.setEchoChar('\u0000');
            showButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Unlock16.png")));
        }
        else
        {
            passwordField.setEchoChar('*');
            showButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Lock16.png")));
        }
    }//GEN-LAST:event_showButtonActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton checkoutButton;
    private javax.swing.JButton createRepoButton;
    private javax.swing.JButton disconnectButton;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JTextPane infoTextPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JTextField nameField;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JTextField pathField;
    private javax.swing.JToggleButton showButton;
    // End of variables declaration//GEN-END:variables


    @Override
    public void event(DarkmireEvent event)
    {
        System.out.println("Subversion event: " + event.getClass().getName());
        
        if (event instanceof DarkmireDataEvent)
        {
            DarkmireDataEvent dataEvent = (DarkmireDataEvent) event;
            
            switch (event.getStatus())
            {
                case WEB_EVENT_SESSION_SUCCESS:
                {
                    if (dataEvent.getData().containsKey("status"))
                    {
                        JSONObject status = dataEvent.getData().getJSONObject("status");
                        
                        createRepoButton.setEnabled(! status.getBoolean("open"));
                        
                        if (createRepoButton.isEnabled())
                        {
                            pathField.setText("Repository not created yet!");
                        }
                        else
                        {
                            m_path = status.getString("path");
                            m_port = status.optInt("server-port", SUBVERSION_DEFAULT_PORT);
                            m_name = status.getString("name");
                            m_password = status.getString("password");
                            
                            pathField.setText(m_path);
                            nameField.setText(m_name);
                            passwordField.setText(m_password);
                            
                            ISVNAuthenticationManager auth = SVNWCUtil.createDefaultAuthenticationManager(m_name, m_password);
                            
                            m_svnOperationFactory.setAuthenticationManager(auth);
                        }
                    }
                    break;
                }
            }
        }
    }


    @Override
    public void closing()
    {
        
    }
}
