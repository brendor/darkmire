package org.brendor.darkmire.netbeans.scripted;

import darkmirec.core.DarkmireException;
import darkmirec.events.DarkmireDataEvent;
import darkmirec.events.DarkmireDisconnectEvent;
import darkmirec.events.DarkmireEvent;
import darkmirec.scripts.DarkmireScriptEvent;
import darkmirec.sessions.DarkmireScriptedSession;
import darkmirec.utils.DarkmireEventHandler;
import darkmirec.utils.IDarkmireLogger;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.brendor.darkmire.netbeans.DarkmireTopComponent;
import org.brendor.darkmire.netbeans.SessionPanel;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class ScriptedSession extends SessionPanel implements DarkmireEventHandler
{
    private RSyntaxTextArea m_TextEditor = null;
    
    private IDarkmireLogger.LogPriority m_logLevel = IDarkmireLogger.LogPriority.DEBUG;
    private boolean m_loggingEnabled = true;

    private final RunnableScheduledFuture<?> m_task;

    private final ScheduledExecutorService m_scheduler = Executors.newSingleThreadScheduledExecutor();
    
    private ScriptedSession m_this = null;
    
    private final Runnable m_job = new Runnable()
    {
        @Override
        public void run()
        {
            EventQueue.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    DarkmireTopComponent top = DarkmireTopComponent.get();
            
                    JSONObject data = new JSONObject();

                    DarkmireScriptedSession session = ((DarkmireScriptedSession)m_session);
                    
                    data.put("events", session.getEvents());
                    data.put("type", DarkmireScriptedSession.RequestType.REQUEST_TRIGGER_EVENTS.code());

                    try
                    {
                        top.enqueue(new DarkmireDataEvent(m_session, data), m_this);
                        
                        session.wipeEvents();
                    }
                    catch (DarkmireException ex)
                    {
                        Exceptions.printStackTrace(ex);
                    }
                }
            });
        }
    };
    
    private final long REFRESH_RATE = 2000;
    
    /**
     * Creates new form QuickShareSession
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public ScriptedSession(DarkmireScriptedSession session)
    {
        super(session);
              
        m_this = this;
        
        m_TextEditor = new RSyntaxTextArea(20, 60);
        m_TextEditor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        m_TextEditor.setCodeFoldingEnabled(true);
        m_TextEditor.setAntiAliasingEnabled(true);
        
        System.out.println("Scripted session init: " + session.getName());
        
        initComponents();

        infoLabel.setText(m_session.getId());
        
        DarkmireTopComponent top = (DarkmireTopComponent) WindowManager.getDefault().findTopComponent(DarkmireTopComponent.COMPONENET_NAME);

        JSONObject data = new JSONObject();

        data.put("type", DarkmireScriptedSession.RequestType.REQUEST_FETCH_CLIENT_SCRIPT.code());

        try
        {
            top.enqueue(new DarkmireDataEvent(m_session, data), this);
        }
        catch (DarkmireException ex)
        {
            Exceptions.printStackTrace(ex);
        }
        
        scrollPane.setViewportView(m_TextEditor);
        scrollPane.setFoldIndicatorEnabled(true);
        scrollPane.setLineNumbersEnabled(true);
        
        m_task = (RunnableScheduledFuture<?>) m_scheduler.scheduleAtFixedRate(m_job, REFRESH_RATE, REFRESH_RATE, TimeUnit.MILLISECONDS);
        
        ((DarkmireScriptedSession)m_session).setLogger(new IDarkmireLogger()
        {
            @Override
            public void log(final String string, final IDarkmireLogger.LogPriority lp)
            {
                EventQueue.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        logConsole(string, lp);
                    }
                });
            }


            @Override
            public void log(final String string)
            {
                EventQueue.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        logConsole(string, LogPriority.INFO);
                    }
                });
            }
        });
        
        logComboBox.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                m_loggingEnabled = true;
                
                switch (e.getItem().toString())
                {
                    case "Debug": m_logLevel = IDarkmireLogger.LogPriority.DEBUG; break;
                    case "Information": m_logLevel = IDarkmireLogger.LogPriority.INFO; break;
                    case "Warning": m_logLevel = IDarkmireLogger.LogPriority.WARNING; break;
                    case "Error": m_logLevel = IDarkmireLogger.LogPriority.ERROR; break;
                    case "Nothing": m_loggingEnabled = false; break;
                    default: m_logLevel = IDarkmireLogger.LogPriority.DEBUG; break;
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        infoLabel = new javax.swing.JLabel();
        disconnectButton = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        scrollPane = new org.fife.ui.rtextarea.RTextScrollPane();
        scriptPane = new org.fife.ui.rtextarea.RTextArea();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        consolePane = new javax.swing.JTextPane();
        logComboBox = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        clearButton = new javax.swing.JButton();
        evaluateButton = new javax.swing.JButton();

        infoLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 14)); // NOI18N
        infoLabel.setForeground(new java.awt.Color(12, 115, 28));
        org.openide.awt.Mnemonics.setLocalizedText(infoLabel, org.openide.util.NbBundle.getMessage(ScriptedSession.class, "ScriptedSession.infoLabel.text")); // NOI18N
        infoLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        disconnectButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/Login-out-icon.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(disconnectButton, org.openide.util.NbBundle.getMessage(ScriptedSession.class, "ScriptedSession.disconnectButton.text")); // NOI18N
        disconnectButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                disconnectButtonActionPerformed(evt);
            }
        });

        jSplitPane1.setDividerLocation(300);

        scrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        scriptPane.setColumns(20);
        scriptPane.setRows(5);
        scrollPane.setViewportView(scriptPane);

        jSplitPane1.setLeftComponent(scrollPane);

        consolePane.setEditable(false);
        consolePane.setBackground(new java.awt.Color(1, 24, 111));
        consolePane.setForeground(new java.awt.Color(23, 204, 34));
        jScrollPane2.setViewportView(consolePane);

        logComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Debug", "Information", "Warning", "Error", "Nothing" }));
        logComboBox.setSelectedIndex(1);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(ScriptedSession.class, "ScriptedSession.jLabel1.text")); // NOI18N

        clearButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Delete16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(clearButton, org.openide.util.NbBundle.getMessage(ScriptedSession.class, "ScriptedSession.clearButton.text")); // NOI18N
        clearButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                clearButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(clearButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 287, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 509, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(clearButton)))
        );

        jSplitPane1.setRightComponent(jPanel1);

        evaluateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/brendor/darkmire/netbeans/graphics/png/Tools16.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(evaluateButton, org.openide.util.NbBundle.getMessage(ScriptedSession.class, "ScriptedSession.evaluateButton.text")); // NOI18N
        evaluateButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                evaluateButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(disconnectButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(infoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(evaluateButton))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSplitPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(disconnectButton)
                    .addComponent(evaluateButton)
                    .addComponent(infoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void disconnectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_disconnectButtonActionPerformed
    {//GEN-HEADEREND:event_disconnectButtonActionPerformed
        DarkmireTopComponent top = DarkmireTopComponent.get();

        try
        {
            top.enqueue(new DarkmireDisconnectEvent(m_session), this);
        }
        catch (DarkmireException ex)
        {
            System.out.println(ex.getMessage());
        }
    }//GEN-LAST:event_disconnectButtonActionPerformed

    
    private void evaluateButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_evaluateButtonActionPerformed
    {//GEN-HEADEREND:event_evaluateButtonActionPerformed
        ((DarkmireScriptedSession)m_session).initialize(m_TextEditor.getText());
    }//GEN-LAST:event_evaluateButtonActionPerformed

    
    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_clearButtonActionPerformed
    {//GEN-HEADEREND:event_clearButtonActionPerformed
        consolePane.setText("");
    }//GEN-LAST:event_clearButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clearButton;
    private javax.swing.JTextPane consolePane;
    private javax.swing.JButton disconnectButton;
    private javax.swing.JButton evaluateButton;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JComboBox logComboBox;
    private org.fife.ui.rtextarea.RTextArea scriptPane;
    private org.fife.ui.rtextarea.RTextScrollPane scrollPane;
    // End of variables declaration//GEN-END:variables


    private void logConsole(String message, IDarkmireLogger.LogPriority prio)
    {
        if (! m_loggingEnabled || prio.code() < m_logLevel.code())
        {
            return;
        }
        
        StyledDocument sd = consolePane.getStyledDocument();
        
        SimpleAttributeSet timeAttribs = new SimpleAttributeSet();
        StyleConstants.setForeground(timeAttribs, Color.YELLOW);
        StyleConstants.setBold(timeAttribs, true);
        
        try
        {
            sd.insertString(sd.getLength(), String.format("%s: ",  new Date().toString()), timeAttribs);
        }
        catch (BadLocationException ex)
        {
            Exceptions.printStackTrace(ex);
        }
        
        
        SimpleAttributeSet attribs = new SimpleAttributeSet();
        
        switch (prio)
        {
            case INFO:
                StyleConstants.setForeground(attribs, Color.WHITE);
                break;
                
            case DEBUG:
                StyleConstants.setForeground(attribs, Color.GREEN);
                break;
            
            case WARNING:
                StyleConstants.setForeground(attribs, Color.YELLOW);
                break;
                
            case ERROR:
                StyleConstants.setForeground(attribs, Color.RED);
                StyleConstants.setBold(attribs, true);
                break;
                
            default:
                throw new AssertionError(prio.name());
        }
        
        try
        {
            sd.insertString(sd.getLength(), String.format("%s\n", message), attribs);
        }
        catch (BadLocationException ex)
        {
            Exceptions.printStackTrace(ex);
        }
    }
    
    
    @Override
    public void event(DarkmireEvent event)
    {
        System.out.println("Scripted event: " + event.getClass().getName());
        
        if (event instanceof DarkmireDataEvent)
        {
            DarkmireDataEvent dataEvent = (DarkmireDataEvent) event;
            
            switch (event.getStatus())
            {
                case WEB_EVENT_SESSION_SUCCESS:
                {
                    if (dataEvent.getData().containsKey("client-script"))
                    {
                        String script = dataEvent.getData().getString("client-script");
                        
                        m_TextEditor.setText(script);
                        
                        evaluateButtonActionPerformed(null);
                    }
                    
                    if (dataEvent.getData().containsKey("client-events"))
                    {
                        JSONArray events = dataEvent.getData().getJSONArray("client-events");
                        
                        for (int i = 0; i < events.size(); i++)
                        {
                            JSONObject o = events.optJSONObject(i);
                            
                            DarkmireScriptEvent e = new DarkmireScriptEvent();
                            e.setJson(o.toString());
                        
                            ((DarkmireScriptedSession)m_session).enqueue(e);
                        }
                    }
                    
                    break;
                }
            }
        }
    }


    @Override
    public void closing()
    {
        ((DarkmireScriptedSession)m_session).close();
        
        m_task.cancel(true);
    }
}
