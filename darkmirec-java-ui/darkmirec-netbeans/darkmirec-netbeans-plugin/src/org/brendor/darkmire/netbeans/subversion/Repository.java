
package org.brendor.darkmire.netbeans.subversion;

import net.sf.json.JSONObject;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;

/**
 *
 * @author brendor
 */
public class Repository
{
    private final int m_id;
    private final String m_authorUuid;
    private final String m_authorName;
    private final String m_name;
    private final String m_password;
    private final String m_path;
    
    final SvnOperationFactory m_svnOperationFactory = new SvnOperationFactory();
    
    public Repository(JSONObject o)
    {
        m_id = o.optInt("id");
        m_authorUuid = o.optString("uuid");
        m_name = o.optString("name");
        m_password = o.optString("password");
        m_path = o.optString("path");
        m_authorName = o.optString("nick");
        
        ISVNAuthenticationManager auth = SVNWCUtil.createDefaultAuthenticationManager(m_name, m_password);
        m_svnOperationFactory.setAuthenticationManager(auth);
    }


    public int getId()
    {
        return m_id;
    }


    public String getAuthorUuid()
    {
        return m_authorUuid;
    }
    
    public String getAuthorName()
    {
        return m_authorName;
    }


    public String getName()
    {
        return m_name;
    }


    public String getPassword()
    {
        return m_password;
    }


    public String getPath()
    {
        return m_path;
    }


    @Override
    public String toString()
    {
        return m_authorUuid;
    }
    
    public SvnOperationFactory svnFactory()
    {
        return m_svnOperationFactory;
    }
}
