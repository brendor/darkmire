/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.brendor.darkmire.netbeans.quickshare;

import java.util.Date;
import net.sf.json.JSONObject;
import darkmirec.utils.DarkmireDateTime;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class Entry
{
    private final String m_author;
    private final Date m_date;
    private final String m_mime;
    private final String m_code;
    private final String m_uuid;


    public Entry(JSONObject object)
    {
        m_author = object.optString("author", "Anonymous");
        
        m_date = DarkmireDateTime.toUTC(object.getString("time"));
        
        m_mime = object.getString("mime");
        
        m_code = object.getString("code");
        
        m_uuid = object.getString("uuid");
    }


    @Override
    public String toString()
    {
        return m_author;
    }


    public String getAuthor()
    {
        return m_author;
    }


    public Date getDate()
    {
        return m_date;
    }


    public String getCode()
    {
        return m_code;
    }


    public String getMimetype()
    {
        return m_mime;
    }


    public String getUuid()
    {
        return m_uuid;
    }
}
