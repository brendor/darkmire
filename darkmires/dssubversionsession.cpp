/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dssubversionsession.hpp"

#include <QSqlError>

#include <QDir>

#include "dsloggingengine.hpp"

#include "dsutils.hpp"
#include "dsconstants.hpp"

#include "dsuserstorage.hpp"

#include "dswebevent.hpp"

#include "dsauthenticationlayer.hpp"

#include <svnqt/repoparameter.h>
#include <svnqt/exception.h>


namespace
{
    enum SubversionRequestType : unsigned int
    {
        RepositoryRequestNormal = 0,
        RepositoryRequestCreate = 1,
        RepositoryRequestStatus = 2,
        RepositoryRequestList = 3,
    };

    static const QString UserQuerySelect = "SELECT id, session, data FROM users WHERE session IS :uuid";
    static const QString UserQuerySelectAll = "SELECT id, session, data FROM users";
    static const QString UserUpdateData = "UPDATE users SET data=:data WHERE session IS :uuid";
}

DsSubversionSession::DsSubversionSession(DsSessionInterface * const interface) :
    DsSession(interface)
{
    m_mode = Darkmires::SessionSubversion;
}


DsSubversionSession::~DsSubversionSession()
{
    qxtLog->debug() << "Deleting subversion session";

    if (m_database.isOpen())
    {
        m_database.close();

        QSqlDatabase::removeDatabase(m_database.connectionName());
    }
}


void DsSubversionSession::load(QDataStream & in)
{
}


void DsSubversionSession::save(QDataStream & out)
{
}


bool DsSubversionSession::init()
{
    QString uuid = m_interface->m_uuid.toString();

    DsUtils::removeFirstAndLast(uuid);

    QString nameOld = DsConstants::SQL_DATABASE_SESSION_PREFIX + uuid;
    QString nameNew = DsConstants::SQL_DATABASE_SESSION_THREAD_PREFIX + uuid;

    m_database = QSqlDatabase::cloneDatabase(QSqlDatabase::database(nameOld, false), nameNew);

    if (m_database.open())
    {
        qxtLog->info() << "Subversion session database opened!";

        m_userQuerySelect = QSqlQuery(m_database);

        if (! m_userQuerySelect.prepare(UserQuerySelect))
        {
            qxtLog->error() << tr("Database query could not be prepared: ") + m_userQuerySelect.lastError().text();
        }

        m_userQueryUpdate = QSqlQuery(m_database);

        if (! m_userQueryUpdate.prepare(UserUpdateData))
        {
            qxtLog->error() << tr("Database query could not be prepared: ") + m_userQueryUpdate.lastError().text();
        }
    }
    else
    {
        qxtLog->error() << "Error opening Subversion session database: " << m_database.lastError().text();

        return false;
    }

    Q_ASSERT(m_interface->getAuthentication() == Darkmires::AuthenticationLocalDatabase ||
             m_interface->getAuthentication() == Darkmires::AuthenticationLDAP);

    return true;
}


void DsSubversionSession::processRequest(Darkmires::PacketPointer packet)
{
    QMutexLocker mutex(&m_interface->m_mutex);

    if (! m_users.contains(packet->event->sessionID))
    {
        return sessionError(packet, "User not logged in session");
    }

    QVariantMap in = packet->json.toMap().value(DsConstants::JSON_DARKMIRE_ELEMENT)
                                 .toMap().value(DsConstants::JSON_DATA_ELEMENT).toMap();

    DsSubversionSessionUserData * userData =
            static_cast<DsSubversionSessionUserData *>(m_users[packet->event->sessionID].get());

    Darkmires::WebEventType status = Darkmires::WebEventSessionSuccess;

    switch ((SubversionRequestType)in.value("type").toUInt())
    {
        case RepositoryRequestList:
        {
            QSqlQuery query(m_database);

            if (! query.exec(UserQuerySelectAll))
            {
                status = Darkmires::WebEventSessionError;

                qxtLog->error() << query.lastError().text();

                query.finish();

                break;
            }

            query.first();

            QVariantList repos;

            while (query.isValid())
            {
                QVariantMap repo;

                repo["id"] = query.value(0).toInt();
                repo["uuid"] = query.value(1).toString();

                QDataStream stream(query.value(2).toByteArray());

                QString name;
                QString password;

                stream >> name;
                stream >> password;

                repo["name"] = name;
                repo["password"] = password;
                repo["path"] = getRepositoryPath(name);
                repo["nick"] = Darkmires::getInstance()->authentication()->searchUserByUuid(QUuid(query.value(1).toString()));

                repos.append(repo);

                query.next();
            }

            query.finish();

            packet->data["repositories"] = repos;
            packet->data["server-port"] = DarkSettings->value("svn-server-port", 3690);

            break;
        }


        case RepositoryRequestNormal: /// For chat etc.
        {
            processChat(packet, in);

            break;
        }

        case RepositoryRequestStatus:
        {
            appendInfo(packet, userData);

            break;
        }

        case RepositoryRequestCreate:
        {
            QString path = getRepositoryPath(userData->data().name);

            QDir().mkpath(path);

            packet->data["path"] = path;

            if (! userData->createOpen(path))
            {
                status = Darkmires::WebEventSessionError;
            }
            else
            {
                //                if (QFile::exists(path + "/conf/svnserve.conf"))
                //                {
                //                    QFile::remove(path + "/conf/svnserve.conf");
                //                }

                //                if (! QFile::copy("://data/subversion/svnserve.conf", path + "/conf/svnserve.conf"))
                //                {
                //                    qxtLog->error() << "Error copying svnserve.conf file!";
                //                }


                QFile svnserve(path + "/conf/svnserve.conf");

                if (svnserve.open(QIODevice::WriteOnly | QIODevice::Text))
                {
                    QTextStream stream(&svnserve);

                    //                    QString content = stream.readAll();

                    QFile inputFile("://data/subversion/svnserve.conf");

                    if (inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        QTextStream inputStream(&inputFile);

                        stream.seek(0);
                        stream << inputStream.readAll();

                        inputFile.close();
                    }

                    svnserve.close();
                }

                QFile pwd(path + "/conf/passwd");

                if (pwd.open(QIODevice::Append | QIODevice::Text))
                {
                    QTextStream pwdStream(&pwd);

                    const DsSubversionSessionUserData::SubversionUserData & data = userData->data();

                    Q_ASSERT(! data.name.isEmpty());
                    Q_ASSERT(! data.password.isEmpty());

                    pwdStream << data.name + "=" + data.password + "\n";

                    pwd.close();
                }
                else
                {
                    qxtLog->error() << "Error writing subversion passwd file!";
                }

                appendInfo(packet, userData);
            }

            break;
        }

        default:
            return sessionError(packet, "Invalid request type");
    }

    emit replySignal(DsWebEvent::construct(packet, status, packet->data));
}


void DsSubversionSession::addUser(int user)
{
    LockUserStorage
    DsUser * u = DsUserStorage::getInstance()->get(user);
    UnlockUserStorage

    if (u->getUuid().isNull())
    {
        return;
    }

    DsSubversionSessionUserData * dataPtr = new DsSubversionSessionUserData(user);

    dataPtr->update(u->getUuid(), m_userQueryUpdate, m_userQuerySelect);

    std::shared_ptr<DsSessionUserData> data(dataPtr);

    m_users[user] = data;

    dataPtr->open(getRepositoryPath(dataPtr->data().name));

    emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_USER_JOINED_SESSION, m_interface->m_uuid.toString()));
}


void DsSubversionSession::processCommand(QString command)
{

}


void DsSubversionSession::populate(std::shared_ptr<DsSessionUserData> data)
{

}


QString DsSubversionSession::getRepositoryPath(const QString & userName)
{
    QString name = m_interface->m_uuid.toString();
    DsUtils::removeFirstAndLast(name);

//    QString userName = user->repositoryId().toString();
//    DsUtils::removeFirstAndLast(userName);

    QString path = QDir().currentPath() + "/" +
                   DarkSettings->value("sessions-file-path").toString() + name + "/" +
                   DarkSettings->value("subversion-file-path").toString() + userName;

    path = QDir::cleanPath(path);

    return path;
}


void DsSubversionSession::appendInfo(Darkmires::PacketPointer packet, DsSubversionSessionUserData * userData)
{
    QVariantMap status;

    status["open"] = userData->isOpen();
    status["path"] = userData->path();
    status["server-port"] = DarkSettings->value("svn-server-port", 3690);
    status["name"] = userData->data().name;
    status["password"] = userData->data().password;

    packet->data["status"] = status;
}


DsSubversionSessionUserData::DsSubversionSessionUserData(int user) :
    DsSessionUserData(user),
    m_listener(this)
{
    m_repository = new svn::repository::Repository(&m_listener);
}


DsSubversionSessionUserData::~DsSubversionSessionUserData()
{
    qxtLog->debug() << "Deleting subversion session user data...";

    if (m_repository) delete m_repository;
}


svn::repository::Repository * DsSubversionSessionUserData::getRepository() const
{
    return m_repository;
}


bool DsSubversionSessionUserData::isOpen() const
{
    return m_isOpen;
}

QString DsSubversionSessionUserData::path() const
{
    return m_path.absolutePath();
}

const DsSubversionSessionUserData::SubversionUserData &DsSubversionSessionUserData::data() const
{
    return m_data;
}

const QUuid & DsSubversionSessionUserData::repositoryId() const
{
    return m_repositoryId;
}

void DsSubversionSessionUserData::update(const QUuid & uuid, QSqlQuery & update, QSqlQuery & select)
{
    Q_ASSERT(! update.isActive());
    Q_ASSERT(! select.isActive());

    select.bindValue(":uuid", uuid.toString(), QSql::Out);

    bool needsToUpdate = false;

    if (select.exec() && select.first() && select.isValid())
    {
        QByteArray raw = select.value(2).toByteArray();

        select.finish();

        if (raw.isEmpty())
        {
            needsToUpdate = true;
        }
        else
        {
            QDataStream stream(raw);

    //        SubversionUserData * data = static_cast<SubversionUserData *>((void *)raw.constData());
    //        m_data.name = data->name;
    //        m_data.password = data->password;

            stream >> m_data.name;
            stream >> m_data.password;
        }
    }

    if (needsToUpdate)
    {
        m_repositoryId = QUuid::createUuid();

        QString name = m_repositoryId.toString();
        DsUtils::removeFirstAndLast(name);

        m_data.name = name;
        m_data.password = QString::number(qrand()) + QString::number(qrand());

//        QByteArray raw = QByteArray::fromRawData((const char *)&m_data, sizeof(m_data));

        QByteArray raw;
        QDataStream stream(&raw, QIODevice::ReadWrite);

        stream << m_data.name;
        stream << m_data.password;

        update.bindValue(":data", raw, QSql::In | QSql::Binary);
        update.bindValue(":uuid", uuid.toString(), QSql::In);

        if (! update.isValid())
        {
            qxtLog->error() << "Update query invalid!";
        }

        if (! update.exec())
        {
            qxtLog->error() << "Error updating user data: " << update.lastError().text();
        }

        update.finish();
    }
}


bool DsSubversionSessionUserData::open(const QString & path)
{
    m_isOpen = false;
    m_path = QDir();

    try
    {
        m_repository->Open(path);

        m_isOpen = true;

        m_path = QDir(path);
    }
    catch (svn::ClientException e)
    {
        qxtLog->error("[SVN Exception]: " + e.msg());
    }

    return m_isOpen;
}

bool DsSubversionSessionUserData::createOpen(const QString & path)
{
    svn::repository::CreateRepoParameter params;

    params.path(path);
//    params.pre16_compat(true);
//    params.pre15_compat(true);
//    params.pre14_compat(true);

    m_isOpen = false;
    m_path = QDir();

    try
    {
        m_repository->CreateOpen(params);

        m_isOpen = true;

        m_path = QDir(path);
    }
    catch (svn::ClientException e)
    {
        qxtLog->error("[SVN Exception]: " + e.msg());
    }

    return m_isOpen;
}

DsSubversionSessionUserData::DsSubversionRepositoryListener::DsSubversionRepositoryListener(DsSubversionSessionUserData * context) :
    RepositoryListener(),
    m_context(context)
{

}


void DsSubversionSessionUserData::DsSubversionRepositoryListener::sendWarning(const QString & message)
{
    qxtLog->warning() << "[SVN] " << message;
}


void DsSubversionSessionUserData::DsSubversionRepositoryListener::sendError(const QString &message)
{
    qxtLog->error() << "[SVN] " << message;
}


bool DsSubversionSessionUserData::DsSubversionRepositoryListener::isCanceld()
{
    return ! m_context->m_running;
}
