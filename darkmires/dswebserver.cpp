/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dswebserver.hpp"

#include "dsrootwebservice.hpp"
#include "dsscgiserverconnector.hpp"

#include "dsauthenticationlayer.hpp"
#include "dsdatalayer.hpp"
#include "dsuserstorage.hpp"

#include <QxtWebContent>
#include <QxtLogger>


DsWebServer::DsWebServer() :
    m_rootService(nullptr),
    m_scgiConnector(nullptr)
{
    QxtHttpSessionManager(this);

    m_rootService = std::unique_ptr<DsRootWebService>(new DsRootWebService(this));

    connect(m_rootService->getAuthenticationLayer(),
            SIGNAL(authenticationEvent(Darkmires::Event)),
            this,
            SLOT(innerEvent(Darkmires::Event)));

    connect(m_rootService->getDataLayer(),
            SIGNAL(dataEvent(Darkmires::Event)),
            this,
            SLOT(innerEvent(Darkmires::Event)));

    m_rootService->setBehaviorGet(DsRootWebService::GET_SHOW_TEMPLATE);
    m_rootService->setBehaviorPost(DsRootWebService::POST_FETCH_DATA_IGNORE);
}


void DsWebServer::innerEvent(Darkmires::Event event)
{
    emit serverEvent(std::move(event));
}


DsWebServer::~DsWebServer()
{
    DsUserStorage::getInstance()->wipe();
}


bool DsWebServer::init(const QHostAddress & host, quint16 port, Connector connector)
{
    setPort(port);
    setListenInterface(host);

    switch (connector)
    {
        case QxtHttpSessionManager::HttpServer:
            setConnector(QxtHttpSessionManager::HttpServer);
            break;

        case QxtHttpSessionManager::Scgi:
            m_scgiConnector.reset(new DsScgiServerConnector(this));

            setConnector(m_scgiConnector.get());
            break;

        default:
            qxtLog->error() << "Unknown service!";
            return false;
    }

    setStaticContentService(m_rootService.get());

    qxtLog->write() << "Server initialized with host [" + host.toString() + "] port [" + QString::number(port) + "]";

    return true;
}


void DsWebServer::addService(QString uri, DsRootWebService * service)
{
    m_rootService->addService(uri, service);
}


void DsWebServer::incomingRequest(quint32 requestID,
                                  const QHttpRequestHeader & header,
                                  QxtWebContent * deviceContent)
{
    qxtLog->write() << "[REQUEST INCOMING]";
    qxtLog->info() << "Request: " << requestID;
    qxtLog->info() << "Method: " << header.method();
    qxtLog->info() << "URI: " << header.path();

    QxtHttpSessionManager::incomingRequest(requestID, header, deviceContent);
}


int DsWebServer::newSession()
{
    return QxtHttpSessionManager::newSession();
}

