#-------------------------------------------------
#
# Project created by QtCreator 2013-03-26T12:01:17
#
#-------------------------------------------------

QT       += network sql script

QT       -= gui

TARGET = darkmires
TEMPLATE = lib

DEFINES += DARKMIRES_LIBRARY

SOURCES += darkmires.cpp \
    darkmiresimpl.cpp \
    darkmiresevent.cpp \
    dsauthenticationlayer.cpp \
    dswebservicethread.cpp \
    dswebserver.cpp \
    dswebevent.cpp \
    dsuserstorage.cpp \
    dsuser.cpp \
    dssessionuserdata.cpp \
    dssessioninterface.cpp \
    dssession.cpp \
    dsscgiserverconnector.cpp \
    dsrootwebservice.cpp \
    dsloggingengine.cpp \
    dsldapthread.cpp \
    dsldap.cpp \
    dsdatalayer.cpp \
    dsscriptedsession.cpp \
    dsutils.cpp \
    dsquicksharesession.cpp \
    dssubversionsession.cpp

HEADERS += darkmires.hpp\
        darkmires_global.hpp \
    darkmiresimpl.hpp \
    darkmiresevent.hpp \
    dsauthenticationlayer.hpp \
    dswebservicethread.hpp \
    dswebserver.hpp \
    dswebevent.hpp \
    dsuserstorage.hpp \
    dssessionuserdata.hpp \
    dssessioninterface.hpp \
    dssession.hpp \
    dsuser.hpp \
    dsscgiserverconnector.hpp \
    dsrootwebservice.hpp \
    dsloggingengine.hpp \
    dsldapthread.hpp \
    dsldap.hpp \
    dsdatalayer.hpp \
    dsconstants.hpp \
    dsscriptedsession.hpp \
    dsutils.hpp \
    dsquicksharesession.hpp \
    dssubversionsession.hpp

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

INCLUDEPATH += /usr/include/qxt/QxtCore \
               /usr/include/qxt/QxtWeb \
               /usr/include/subversion-1 \
               /usr/include/apr-1.0

INCLUDEPATH -= /usr/include/qt5

unix:LIBS += -L/usr/lib -lldap -lsvnqt

CONFIG  += qxt
QXT     += core web

QXT     -= gui

QMAKE_CXXFLAGS += -std=c++11

RESOURCES += \
    html.qrc
