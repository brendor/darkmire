/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsldap.hpp"

#include <QxtLogger>
#include <QxtWebRequestEvent>

DsLDAP::DsLDAP(QObject * parent) :
    QObject(parent)
{
    qRegisterMetaType<DsAuthenticationLayer::SearchResult>("DsAuthenticationLayer::SearchResult");
}


void DsLDAP::search(const Darkmires::PacketPointer & packet, const QString & name)
{
    std::unique_ptr<DsLDAPThread> thread(new DsLDAPThread(this));

    m_searches[packet->event->sessionID] = packet;

    thread->m_id = packet->event->sessionID;
    thread->m_name = DsLDAP::escapeName(name);

    connect(thread.get(),
            SIGNAL(finished()),
            this,
            SLOT(threadFinished()));

    connect(thread.get(),
            SIGNAL(searchFinished(int, DsAuthenticationLayer::SearchResult)),
            this,
            SLOT(threadSearchDone(int, DsAuthenticationLayer::SearchResult)));

    m_threads.push_back(std::move(thread));

    m_threads.back()->start();
}


QString DsLDAP::escapeName(const QString & name)
{
    QString final;

    for (char c : name.toUtf8())
    {
        switch (c)
        {
            case '\\':
                final.append("\\5c");
                break;
            case '*':
                final.append("\\2a");
                break;
            case '(':
                final.append("\\28");
                break;
            case ')':
                final.append("\\29");
                break;
            case '\u0000':
                final.append("\\00");
                break;
            default:
                final.append(c);
        }
    }

    return final;
}


void DsLDAP::threadSearchDone(int id, DsAuthenticationLayer::SearchResult result)
{
    emit searchFinished(m_searches[id], result);

    m_searches.remove(id);
}


void DsLDAP::threadFinished()
{
    qxtLog->debug() << "[LDAP] Thread finished. Pending searches: " + QString::number(m_searches.size());

    for (std::vector<DarkmiresLDAPThreadUnique>::iterator it = m_threads.begin(); it != m_threads.end(); it++)
    {
        if (it->get()->isFinished())
        {
            m_searches.remove(it->get()->m_id);

            it = m_threads.erase(it);
        }

        if (it == m_threads.end())
        {
            break;
        }
    }

    qxtLog->debug() << "[LDAP] After clear: " + QString::number(m_searches.size());
}
