/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsrootwebservice.hpp"

#include <QIODevice>

#include <QxtWebRedirectEvent>
#include <QxtWebContent>
#include <QxtHtmlTemplate>
#include <QxtLogger>
#include <QxtJSON>

#include <QHash>
#include <QHostAddress>

#include <QThreadPool>

#include "dsauthenticationlayer.hpp"
#include "dsdatalayer.hpp"

#include "dswebevent.hpp"

#include "dsconstants.hpp"


#define THREAD_TIMEOUT 5000


DsRootWebService::DsRootWebService(QxtAbstractWebSessionManager * sm) :
    QxtWebServiceDirectory(sm, nullptr),
    m_data(new DsDataLayer()),
    m_authentication(new DsAuthenticationLayer(m_data))
{
    connect(m_authentication.get(), SIGNAL(replySignal(DsWebEvent *)),
            this, SLOT(replySlot(DsWebEvent *)));

    connect(this, SIGNAL(requestSignal(Darkmires::PacketPointer)),
            m_authentication.get(), SLOT(requestSlot(Darkmires::PacketPointer)));

    connect(m_data.get(), SIGNAL(replySignal(DsWebEvent *)),
            m_authentication.get(), SLOT(replySlot(DsWebEvent *)));

    connect(m_authentication.get(), SIGNAL(requestSignal(Darkmires::PacketPointer)),
            m_data.get(), SLOT(requestSlot(Darkmires::PacketPointer)));
}


void DsRootWebService::serviceError(const QxtWebRequestEvent * event, Darkmires::WebEventType type, const QString & message)
{
    postEvent(DsWebEvent::construct(createPacket(event), type, message));
}


DsRootWebService::~DsRootWebService()
{
    QThreadPool::globalInstance()->waitForDone(THREAD_TIMEOUT);

//    for (std::vector<DarkmiresWebServiceThreadUnique>::iterator it = m_threads.begin(); it != m_threads.end(); it++)
//    {

//        disconnect(it->get(),
//                   SIGNAL(finished()),
//                   this,
//                   SLOT(threadFinished()));

//        if (! it->get()->isFinished())
//        {
//            qxtLog->warning() << "[THREAD] Waiting for a fetch thread to finish...";

//            if (! it->get()->wait(THREAD_TIMEOUT))
//            {
//                qxtLog->error() << "[THREAD] Fetch thread failed to finish, terminating...";

//                it->get()->terminate();
//            }
//        }

//        it = m_threads.erase(it);

//        if (it == m_threads.end())
//        {
//            break;
//        }
//    }

    m_data.reset();
    m_authentication.reset();
}


Darkmires::PacketPointer DsRootWebService::createPacket(const QxtWebRequestEvent * event)
{
    Darkmires::PacketPointer packet(new Darkmires::PacketData);

    QByteArray bytes = event->content->readAll();

    if (DarkSettings->value("log/contents").toBool())
    {
        qxtLog->debug() << bytes;
    }

    packet->json = QxtJSON::parse(bytes);
    packet->event = event;
    packet->sessionID = QUuid(event->cookies.value(DarkSettings->value("session-id", DsConstants::DEFAULT_SESSION_ID).toString()));
    packet->time.start();

    return std::move(packet);
}


void DsRootWebService::indexRequested(QxtWebRequestEvent * event)
{
    QHostAddress host(event->remoteAddress);

    qxtLog->info() << "FROM: " + host.toString();

    if (m_denyExternal)
    {
        if (host != QHostAddress::LocalHost &&
            host != QHostAddress::LocalHostIPv6)
        {
            return postEvent(new QxtWebErrorEvent(event->sessionID,
                                                  event->requestID,
                                                  405,
                                                  "Direct external connections not permitted"));
        }
    }


    if (DarkSettings->value("log/headers").toBool())
    {
        qxtLog->debug() << "[HEADERS]";

        for (auto i = event->headers.constBegin(); i != event->headers.constEnd(); ++i)
        {
            qxtLog->debug() << i.key() << ": " << i.value();
        }
    }

    if (DarkSettings->value("log/cookies").toBool())
    {
        qxtLog->debug() << "[COOKIES]";

        for (auto i = event->cookies.constBegin(); i != event->cookies.constEnd(); ++i)
        {
            qxtLog->debug() << i.key() << ": " << i.value();
        }
    }

    if (m_forceHttps)
    {
        if (! event->headers.contains("https", "on"))
        {
            serviceError(event, Darkmires::WebBadRequest, "Use of encrypted connection (HTTPS) required");
            return;
        }
    }

    QString method = event->method.toLower();

    if (method == "post")
    {

        switch (m_behaviorPOST)
        {
            case POST_404:
            {
                serviceError(event, Darkmires::WebNotFound, "File not Found");
                break;
            }

            case POST_FETCH_DATA_THREAD:
            {
                qxtLog->debug() << "[THREAD] Initiating threaded fetch in thread: " + QString::number((long long int)QThread::currentThreadId());

                DsWebServiceThread * thread = new DsWebServiceThread(event, this);

                connect(thread->object(),
                        SIGNAL(dataFetched(Darkmires::PacketPointer)),
                        this,
                        SLOT(finalizeRequest(Darkmires::PacketPointer)));

//                connect(thread,
//                        SIGNAL(finished()),
//                        this,
//                        SLOT(threadFinished()));

//                m_threads.push_back(std::move(thread));
//                m_threads.back()->start();

                QThreadPool::globalInstance()->start(thread);

                break;
            }

            case POST_FETCH_DATA_IGNORE:
            {
                event->content->ignoreRemainingContent();

                finalizeRequest(std::move(createPacket(event)));

                break;
            }
        }

    }
    else if (method == "get")
    {

        switch (m_behaviorGET)
        {
            case GET_404:
            {
                serviceError(event, Darkmires::WebNotFound, "File Not Found");
                break;
            }

            case GET_SHOW_TEMPLATE:
            {
                QxtHtmlTemplate temp;

                if (temp.open(":/data/welcome.html"))
                {

                    temp["session"] = QString::number(event->sessionID);

                    if (event->headers.count("https"))
                    {
                        temp["https"] = * event->headers.find("https");
                    }
                    else
                    {
                        temp["https"] = "off";
                    }

                    postEvent(new QxtWebPageEvent(event->sessionID,
                                                  event->requestID,
                                                  temp.render().toUtf8()));
                }
                else
                {
                    serviceError(event, Darkmires::WebInternalError, "Internal Server Error");
                }

                break;
            }
        }

    }
    else
    {
        return serviceError(event, Darkmires::WebServiceNotAllowed, "Method Not Allowed");
    }
}


void DsRootWebService::unknownServiceRequested(QxtWebRequestEvent * event, const QString & name)
{
    serviceError(event, Darkmires::WebNotFound, QString("404 - (Service) Not Found (" + name + ")").toUtf8());
}


void DsRootWebService::pageRequestedEvent(QxtWebRequestEvent * event)
{
    QxtWebServiceDirectory::pageRequestedEvent(event);
}


void DsRootWebService::replySlot(DsWebEvent * event)
{
    postEvent(event);
}


//void DsRootWebService::threadFinished()
//{
//    qxtLog->debug() << "[THREAD] Finished";

//    for (std::vector<DarkmiresWebServiceThreadUnique>::iterator it = m_threads.begin(); it != m_threads.end(); it++)
//    {
//        if (it->get()->isFinished())
//        {
//            it = m_threads.erase(it);
//        }

//        if (it == m_threads.end())
//        {
//            break;
//        }
//    }
//}


void DsRootWebService::finalizeRequest(Darkmires::PacketPointer packet)
{
    if (packet->json.isNull())
    {
        serviceError(packet->event, Darkmires::WebBadRequest, "Bad Request");

        return;
    }

    emit requestSignal(std::move(packet));
}
