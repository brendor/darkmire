#ifndef DARKMIRESWEBSERVER_HPP
#define DARKMIRESWEBSERVER_HPP

#include <QxtHttpSessionManager>

class DsRootWebService;
class DsScgiServerConnector;

#include <memory>

#include "darkmires.hpp"

///
/// \brief Trieda DsWebServer predstavuje v technológii QxtWeb server,
/// ktorý ponúka nejakú množinu služieb.
/// V aplikácií Darkmire je táto služba len jedna.
///
class Q_DECL_HIDDEN DsWebServer : public QxtHttpSessionManager
{
        Q_OBJECT

    public:
        explicit DsWebServer();
        DsWebServer(const DsWebServer &) = delete;

        virtual ~DsWebServer();

        ///
        /// \brief init inicializuje server
        /// \param host adresa rozhrania, na ktorej bude server bežať. Typicky IADDR_ANY
        /// \param port port, na ktorom bude server bežať
        /// \param connector inštancia spájača. V aplikácii Darkmire inštancia triedy DsScgiServerConnector
        /// \return true pri úspešnej inicializácii, inak false
        ///
        bool init(const QHostAddress & host, quint16 port, Connector connector);

        ///
        /// \brief addService pridá službu, ktorej budú delegované požiadavky na danú URL adresu.
        /// V aplikácii Darkmire je to typicky adresa "/", teda koreňový adresár.
        /// \param uri adresa
        /// \param service služba, ktorej budú delegované požiadavky relatívne k danej adrese
        ///
        void addService(QString uri, DsRootWebService * service);

        inline DsRootWebService * getRoot()
        {
            return m_rootService.get();
        }

    signals:
        void postReceived(QByteArray content);
        void serverEvent(Darkmires::Event event);

    protected:
        virtual void incomingRequest(quint32 requestID, const QHttpRequestHeader & header, QxtWebContent * deviceContent) final;
        virtual int newSession() final;

    private:
        std::unique_ptr<DsRootWebService> m_rootService;
        std::unique_ptr<DsScgiServerConnector> m_scgiConnector;

    private slots:
        void innerEvent(Darkmires::Event event);
};

#endif // DARKMIRESWEBSERVER_HPP
