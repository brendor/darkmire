/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsauthenticationlayer.hpp"

#include <QxtLogger>
#include <QxtWebContent>
#include <QxtHtmlTemplate>

#include <QDebug>

#include <QxtJSON>

#include <QSqlError>
#include <QSqlRecord>

#include <QCryptographicHash>

#include <QDir>


#include "darkmiresimpl.hpp"
#include "dsconstants.hpp"

#include "dsldap.hpp"

#include "dsdatalayer.hpp"

#include "dswebevent.hpp"
#include "dsuserstorage.hpp"
#include "dsutils.hpp"


namespace
{
    static const QString UserQuerySelectByName = "SELECT name, password, salt, privileges, id, session FROM users WHERE name IS :name";
    static const QString UserQuerySelectBySession = "SELECT name, privileges FROM users WHERE session IS :session";
    static const QString UserQueryInsert = "INSERT INTO users (name, password, salt, privileges, id, session) VALUES (?, ?, ?, ?, ?, ?)";
    static const QString UserQueryUpdateSession = "UPDATE users SET session = :session WHERE name = :name";
    static const QString UserQueryUpdatePassword = "UPDATE users SET password = :password WHERE name = :name";
}


DsAuthenticationLayer::AuthenticationMap DsAuthenticationLayer::AuthenticationAliases[] =
{
    {"ldap",           Darkmires::AuthenticationLDAP},
    {"none",           Darkmires::AuthenticationNone},
    {"local",          Darkmires::AuthenticationLocalDatabase},
    {nullptr,          Darkmires::AuthenticationError}
};


DsAuthenticationLayer::DsAuthenticationLayer(const std::weak_ptr<DsDataLayer> & data, QObject * parent) :
    QObject(parent),
    m_data(data),
    m_ldap(new DsLDAP(this))
{
    m_database = QSqlDatabase::database(DsConstants::SQL_DATABASE_CONNECTION_NAME, false);

    if (! m_database.isValid())
    {
        m_database = QSqlDatabase::addDatabase("QSQLITE", DsConstants::SQL_DATABASE_CONNECTION_NAME);
    }

    if (! m_database.isValid())
    {
        qxtLog->error() << tr("Database driver not valid!");
    }
    else
    {
        m_database.setDatabaseName(DarkSettings->value("database-file-path").toString() + "darkmire.db");

        if (m_database.open())
        {
            qxtLog->info() << tr("Database connection opened: ") + m_database.databaseName();

            m_userQuerySelect = QSqlQuery(m_database);

            if (! m_userQuerySelect.prepare(UserQuerySelectByName))
            {
                qxtLog->error() << tr("Database query could not be prepared: ") + m_userQuerySelect.lastError().text();
            }

//            m_userQueryUpdate = QSqlQuery(m_database);

//            if (! m_userQueryUpdate.prepare(UserQueryUpdateSession))
//            {
//                qxtLog->error() << tr("Database session update query could not be prepared: ") + m_userQueryUpdate.lastError().text();
//            }
        }
        else
        {
            qxtLog->error() << tr("Database connection could not be opened: ") + m_database.lastError().text();
        }
    }

    connect(m_ldap.get(),
            SIGNAL(searchFinished(Darkmires::PacketPointer, DsAuthenticationLayer::SearchResult)),
            this,
            SLOT(searchFinished(Darkmires::PacketPointer, DsAuthenticationLayer::SearchResult)));
}


void DsAuthenticationLayer::authenticationError(const Darkmires::PacketPointer packet, const QString & message, Darkmires::WebEventType type)
{
    emit replySignal(DsWebEvent::construct(packet,
                                           type,
                                           message));
}


void DsAuthenticationLayer::authenticationSuccess(const Darkmires::PacketPointer & packet)
{
    qxtLog->debug() << tr("[AUTHENTICATION] Success");

    LockUserStorage

    DsUser * user = DsUserStorage::getInstance()->get(packet->event->sessionID);

    emit replySignal(DsWebEvent::construct(packet,
                                           Darkmires::WebEventAuthenticationSuccess,
                                           user->getSessionID().toByteArray().data(),
                                           user->getPrivileges()));

    UnlockUserStorage
}


void DsAuthenticationLayer::authenticateUser(const Darkmires::PacketPointer & packet, Darkmires::UserPrivileges privileges)
{
    if (privileges & Darkmires::UserAdmin)
    {
        privileges |= Darkmires::UserModerator;
    }

    if (privileges & Darkmires::UserModerator)
    {
        privileges |= Darkmires::UserRegular;
    }

    if (DsUserStorage::getInstance()->contains(packet->event->sessionID))
    {

        LockUserStorage
        DsUser * user = DsUserStorage::getInstance()->get(packet->event->sessionID);
        user->addPrivileges(privileges);
        user->setSessionID(packet->sessionID);
        user->setUuid(packet->uuid);
        UnlockUserStorage

        emit authenticationEvent(DarkmiresEvent::EVENT_USER_EDITED);

        return;
    }

    std::shared_ptr<DsUser> user(new DsUser(packet->event->sessionID, this));

    QVariantMap root = packet->json.toMap().value(DsConstants::JSON_DARKMIRE_ELEMENT).toMap();
    QVariantMap data = root.value(DsConstants::JSON_DATA_ELEMENT).toMap();

    if (! packet->uuid.isNull())
    {
        qxtLog->debug() << QString("Adding registered user to the list of immediate users: %1 (%2)")
                             .arg(packet->event->sessionID).arg(packet->uuid.toString());
    }

    user->setSessionID(packet->sessionID);
    user->setUuid(packet->uuid);
    user->setPrivileges(privileges);

    user->setWantsChat(data.value("want-chat", user->wantsChat()).toBool());

    QString name = DsAuthenticationLayer::escapeName(data.value(DsConstants::JSON_USERNAME_ELEMENT).toString());

    if (! name.isEmpty())
    {
        user->setName(name);

//        if (DsUserStorage::getInstance()->isUnique(name))
//        {
//            user->setName(name);
//        }
    }

    connect(user.get(),
            SIGNAL(timeout(int)),
            this,
            SLOT(removeUser(int)));

    DsUserStorage::getInstance()->add(user);
}


bool DsAuthenticationLayer::authenticateSession(const Darkmires::PacketPointer & packet)
{
    authenticateUser(packet, Darkmires::UserRegular);

    return m_data.lock()->addUser(packet);
}


void DsAuthenticationLayer::searchFinished(const Darkmires::PacketPointer packet, DsAuthenticationLayer::SearchResult result)
{
    if (result.success)
    {
        if (authenticateSession(packet))
        {
            return authenticationSuccess(packet);
        }
        else
        {
            return authenticationError(packet, tr("Unable to authenticate user in session"));
        }
    }
    else
    {
        qxtLog->error() << tr("[AUTHENTICATION] Failed to authenticate: ") + QString::number(packet->event->sessionID) + " (" + result.error + ")";

        return authenticationError(packet, result.error);
    }
}


DsAuthenticationLayer::~DsAuthenticationLayer()
{
    if (m_database.isOpen())
    {
        m_database.close();

        QSqlDatabase::removeDatabase(m_database.databaseName());
    }
}


Darkmires::AuthenticationType DsAuthenticationLayer::recognizeAuthenticationType(const QString & str)
{
    for (int i = 0; DsAuthenticationLayer::AuthenticationAliases[i].id != nullptr; i++)
    {
        if (str == DsAuthenticationLayer::AuthenticationAliases[i].id)
        {
            return DsAuthenticationLayer::AuthenticationAliases[i].type;
        }
    }

    return Darkmires::AuthenticationError;
}


QString DsAuthenticationLayer::authenticationTypeToString(Darkmires::AuthenticationType type)
{
    for (int i = 0; DsAuthenticationLayer::AuthenticationAliases[i].id != nullptr; i++)
    {
        if (type == DsAuthenticationLayer::AuthenticationAliases[i].type)
        {
            return DsAuthenticationLayer::AuthenticationAliases[i].id;
        }
    }

    return QString();
}


QString DsAuthenticationLayer::escapeName(const QString & name)
{
    QString final;

    for (const QChar & c : name)
    {
        if (c.isLetterOrNumber())
        {
            final.append(c);
        }
    }

    return final;
}


QString DsAuthenticationLayer::searchUserByUuid(const QUuid & uuid)
{
    QSqlQuery query(m_database);

    if (! query.prepare(UserQuerySelectBySession))
    {
        Q_ASSERT(false);

        return QString();
    }

    query.bindValue(":session", uuid.toString());

    if (! query.exec())
    {
        Q_ASSERT(false);

        return QString();
    }

    if (query.first())
    {
        return query.value(0).toString();
    }

    return QString();
}


void DsAuthenticationLayer::requestSlot(Darkmires::PacketPointer packet)
{
    if (! packet->json.canConvert(QVariant::Map))
    {
        return authenticationError(packet, tr("Invalid JSON format"));
    }


    if (Darkmires::getInstance()->hasFlag(Darkmires::FlagCompression))
    {
        QVariantMap root = packet->json.toMap();

        if (root.contains(DsConstants::JSON_TYPE_ELEMENT))
        {
            switch (Darkmires::recognizeRequest(root.value(DsConstants::JSON_TYPE_ELEMENT).toString()))
            {
                case Darkmires::RequestServerInfo:
                {
                    QVariantMap info;

                    info["flags"] = Darkmires::getInstance()->flags();
                    info["name"] = DsConstants::APPLICATION_NAME;
                    info["version"] = DsConstants::APPLICATION_VERSION;

                    emit replySignal(DsWebEvent::construct(packet,
                                                           Darkmires::WebEventNormal,
                                                           info,
                                                           Darkmires::UserNone,
                                                           true));
                    return;
                }

                default:
                    break;

            }
        }

        if (root.contains(DsConstants::JSON_DARKMIRE_ELEMENT))
        {

            QByteArray compressedRaw = root.value(DsConstants::JSON_DARKMIRE_ELEMENT).toString().toAscii();

            if (root.contains(DsConstants::JSON_CHECKSUM_ELEMENT))
            {
                if (DsUtils::Crc16CCITT(compressedRaw.constData(), compressedRaw.size()) != root.value(DsConstants::JSON_CHECKSUM_ELEMENT).toInt())
                {
                    return authenticationError(packet, "Data corrupt", Darkmires::WebBadRequest);
                }
            }
            else
            {
                qxtLog->warning() << "Request is missing the checksum!";
            }

            qxtLog->info() << "Compressed: " << QString::fromAscii(compressedRaw.constData());

            QByteArray compressed = QByteArray::fromBase64(compressedRaw);

            QByteArray decompressed = DsUtils::decompress(compressed, root.value(DsConstants::JSON_ORIGINAL_SIZE_ELEMENT).toInt());

            QString decompressedString = QString::fromUtf8(decompressed.constData(), decompressed.size());

            qxtLog->info() << "Decompressed: " << decompressedString;

            qxtLog->info() << "Compression ratio: " << (float)decompressed.size() / (float)compressed.size();

            root[DsConstants::JSON_DARKMIRE_ELEMENT] = QxtJSON::parse(decompressedString);

            packet->json = root;
        }
    }

    QVariantMap root = packet->json.toMap().value(DsConstants::JSON_DARKMIRE_ELEMENT).toMap();

    if (! root.contains(DsConstants::JSON_TYPE_ELEMENT))
    {
        return authenticationError(packet, tr("Malformed JSON"));
    }

    packet->session = root.value(DsConstants::JSON_SESSION_ELEMENT).toString();

    packet->type = Darkmires::recognizeRequest(root.value(DsConstants::JSON_TYPE_ELEMENT).toString());

    LockUserStorage
    {
        DsUser * user = DsUserStorage::getInstance()->get(packet->event->sessionID);

        if (user)
        {
            packet->uuid = user->getUuid();
        }
    }
    UnlockUserStorage

    qxtLog->info() << "Request user UUID: " << packet->uuid.toString();

    switch (packet->type)
    {

        case Darkmires::RequestListSessions:

            emit requestSignal(packet);

            break;

        case Darkmires::RequestNormal: // Normal
        {
            if (! packet->event->cookies.contains(DsConstants::SESSION_ID))
            {
                return authenticationError(packet, tr("Cookies not present"));
            }


            if (packet->session.isEmpty() ||
                ! root.contains(DsConstants::JSON_DATA_ELEMENT))
            {
                return authenticationError(packet, tr("Malformed JSON"));
            }


            if (! m_data.lock()->getSessions().contains(packet->session))
            {
                return authenticationError(packet, tr("Session not found"));
            }

            if (! DsUserStorage::getInstance()->contains(packet->event->sessionID))
            {
                return authenticationError(packet, tr("Not authenticated"));
            }

            if (! DsUserStorage::getInstance()->inSession(packet->session, packet->event->sessionID))
            {
                return authenticationError(packet, tr("Not authenticated in session"));
            }

            LockUserStorage
            DsUserStorage::getInstance()->get(packet->event->sessionID)->resetTimer();
            UnlockUserStorage

            emit requestSignal(packet);

            break;
        }


        case Darkmires::RequestDisconnect: /*! Disconnect */
        {
            if (! packet->event->cookies.contains(DsConstants::SESSION_ID))
            {
                return authenticationError(packet, tr("Authentication layer error"));
            }

            if (! DsUserStorage::getInstance()->contains(packet->event->sessionID))
            {
                return authenticationError(packet, tr("Not authenticated"));
            }

            /// No session means log out if logged in
            if (! root.contains(DsConstants::JSON_SESSION_ELEMENT))
            {
                if (packet->uuid.isNull())
                {
                    return authenticationError(packet, tr("Disconnecting but not logged in"));
                }

                qxtLog->info() << tr("[AUTHENTICATION] Disconnecting registered user: %1").arg(QString::number(packet->event->sessionID));

                LockUserStorage

                DsUser * user = DsUserStorage::getInstance()->get(packet->event->sessionID);

                if (DsUserStorage::getInstance()->hasPrivileges(packet->event->sessionID, Darkmires::UserModerator))
                {

                    user->removePrivileges(Darkmires::UserModerator | Darkmires::UserAdmin);

                }

                user->setUuid(QUuid());
                packet->uuid = QUuid();

                UnlockUserStorage

                emit replySignal(DsWebEvent::construct(packet,
                                                       Darkmires::WebEventAuthenticationDisconnect,
                                                       QVariant()));
            }
            else
            {
                qxtLog->info() << tr("[AUTHENTICATION] Disconnecting user %1 from session %2")
                               .arg(QString::number(packet->event->sessionID), root.value(DsConstants::JSON_SESSION_ELEMENT).toString());

                m_data.lock()->removeUser(packet->event->sessionID, root.value(DsConstants::JSON_SESSION_ELEMENT).toString());


                emit replySignal(DsWebEvent::construct(packet,
                                                       Darkmires::WebEventAuthenticationDisconnect,
                                                       DsUserStorage::getInstance()->sessionCount(packet->event->sessionID)));

            }

            /// Remove user if he is not logged in and not in any session
            if (! DsUserStorage::getInstance()->inAnySession(packet->event->sessionID) &&
                packet->uuid.isNull())
            {
                removeUser(packet->event->sessionID);
            }

            emit authenticationEvent(DarkmiresEvent::EVENT_USER_EDITED);

            /* Remove user if he is not at least moderator and does not belong to any session after the disconnect */
//            if (! DsUserStorage::getInstance()->inAnySession(packet->event->sessionID) &&
//                ! DsUserStorage::getInstance()->hasPrivileges(packet->event->sessionID, Darkmires::UserModerator))
//            {
//                removeUser(packet->event->sessionID);
//            }

            break;
        }

        case Darkmires::RequestUserModify: /*! Authenticate */
        {
            if (! root.contains(DsConstants::JSON_DATA_ELEMENT))
            {
                return authenticationError(packet, tr("Malformed JSON (data field missing)"));
            }

            if (! DsUserStorage::getInstance()->contains(packet->event->sessionID) ||
                packet->uuid.isNull())
            {
                return authenticationError(packet, tr("User not logged in"));
            }

            QString pass = root.value("data").toMap().value("new-password").toString();

            if (pass.isEmpty())
            {
                return authenticationError(packet, tr("Invalid request"));
            }

            QString salt;

            if (searchSQL(packet, Darkmires::UserNone, salt))
            {
                QSqlQuery query(m_database);

                if (! query.prepare(UserQueryUpdatePassword))
                {
                    Q_ASSERT(false);

                    return authenticationError(packet, tr("Internal error"));
                }

                query.bindValue(":name", root.value("data").toMap().value("name").toString(), QSql::In);

                QString newPass = QString::fromAscii(QCryptographicHash::hash((salt + pass).toAscii(),
                                                                              QCryptographicHash::Sha1).toHex());

                query.bindValue(":password", newPass, QSql::In);

                if (! query.exec())
                {
                    Q_ASSERT(false);

                    return authenticationError(packet, tr("Internal error"));
                }
            }

            break;
        }

        case Darkmires::RequestAuthenticate: /*! Authenticate */
        {
            if (! root.contains(DsConstants::JSON_DATA_ELEMENT))
            {
                return authenticationError(packet, tr("Malformed JSON (data field missing)"));
            }

            Darkmires::AuthenticationType authType = DsAuthenticationLayer::recognizeAuthenticationType(root.value("data").toMap().value("type").toString());

            if (packet->session.isEmpty())
            {
                Darkmires::UserPrivileges privs = root.value("data").toMap().value("privileges").toUInt();

                if (privs == Darkmires::UserNone)
                {
                    return authenticationError(packet, tr("Invalid privileges requested"));
                }

                if (DsUserStorage::getInstance()->contains(packet->event->sessionID) &&
                    DsUserStorage::getInstance()->hasPrivileges(packet->event->sessionID, privs))
                {
                    if (! packet->uuid.isNull())
                    {
                        return authenticationError(packet, tr("Already authenticated with these privileges"),
                                                   Darkmires::WebEventAuthenticationAlreadyAuthenticated);
                    }
                }

                QString salt;

                if (searchSQL(packet, privs, salt))
                {
                    authenticateUser(packet, Darkmires::UserRegular | privs);

                    authenticationSuccess(packet);
                }
            }
            else if (! m_data.lock()->getSessions().count(packet->session))
            {

                qxtLog->debug() << tr("Session requested by user not found: ") + packet->session;

                return authenticationError(packet, tr("Session not found"));

            }
            else
            {

                const std::shared_ptr<DsSessionInterface> & sessionPtr = m_data.lock()->getSessions().value(packet->session);

                if (DsUserStorage::getInstance()->contains(packet->event->sessionID))
                {

                    if (! packet->event->cookies.contains(DsConstants::SESSION_ID))
                    {
                        return authenticationError(packet, tr("Authentication layer error"));
                    }

                    LockUserStorage
                    DsUserStorage::getInstance()->get(packet->event->sessionID)->resetTimer();
                    UnlockUserStorage

                    if (DsUserStorage::getInstance()->inSession(packet->session, packet->event->sessionID))
                    {
                        return authenticationError(packet, tr("Already authenticated in session"),
                                                   Darkmires::WebEventAuthenticationError);
                    }
                }

                if (authType != sessionPtr->getAuthentication())
                {
                    return authenticationError(packet, tr("Session authentication type mismatch/error"));
                }

                switch (authType)
                {
                    case Darkmires::AuthenticationLDAP:
                    {
                        if (m_ldap_allowed)
                        {
                            m_ldap->search(packet, root.value(DsConstants::JSON_DATA_ELEMENT).toMap().value(DsConstants::JSON_NAME_ELEMENT).toString());
                        }
                        else
                        {
                            return authenticationError(packet, tr("Authentication via LDAP not allowed"));
                        }

                        break;
                    }

                    default:

                        if (authenticateSession(packet))
                        {
                            authenticationSuccess(packet);
                        }
                        else
                        {
                            authenticationError(packet, tr("Unable to authenticate user in session"));
                        }

                        break;
                }
            }

            break;
        }

        case Darkmires::RequestModerate:
        {
            if (! packet->event->cookies.contains(DsConstants::SESSION_ID))
            {
                return authenticationError(packet, tr("Cookies not present"));
            }

            if (! DsUserStorage::getInstance()->contains(packet->event->sessionID))
            {
                return authenticationError(packet, tr("Not authenticated"));
            }

            if (packet->uuid.isNull())
            {
                return authenticationError(packet, tr("Not logged in"));
            }

            if (DsUserStorage::getInstance()->hasPrivileges(packet->event->sessionID, Darkmires::UserModerator))
            {

            }

            LockUserStorage

            DsUser * user = DsUserStorage::getInstance()->get(packet->event->sessionID);
            user->resetTimer();

            emit replySignal(DsWebEvent::construct(packet,
                                                   Darkmires::WebEventNormal,
                                                   QVariant(),
                                                   user->getPrivileges()));
            UnlockUserStorage

            break;
        }

        default:
            return authenticationError(packet, tr("Invalid JSON format"));
    }
}


void DsAuthenticationLayer::replySlot(DsWebEvent * event)
{
    emit replySignal(event);
}


void DsAuthenticationLayer::kickUser(int id)
{
    removeUser(id);
}


void DsAuthenticationLayer::removeUser(int id)
{
    qxtLog->info() << tr("[AUTHENTICATION] Removing user: ") + QString::number(id);

    m_data.lock()->removeUser(id);

    DsUserStorage::getInstance()->remove(id);
}


bool DsAuthenticationLayer::searchSQL(const Darkmires::PacketPointer & packet, Darkmires::UserPrivileges privs, QString & saltOut)
{
    QVariantMap root = packet->json.toMap().value(DsConstants::JSON_DARKMIRE_ELEMENT).toMap();

    m_userQuerySelect.bindValue(":name", root.value("data").toMap().value("name").toString(), QSql::Out);

    if (! m_userQuerySelect.exec())
    {
        m_userQuerySelect.finish();

        authenticationError(packet, tr("Database lookup error"));

        return false;
    }

    if (! m_userQuerySelect.first())
    {
        m_userQuerySelect.finish();

        authenticationError(packet, tr("User not found"));

        return false;
    }

    if (! m_userQuerySelect.isValid())
    {
        qxtLog->error() << tr("Database query invalid: ") + m_userQuerySelect.lastError().text();

        m_userQuerySelect.finish();

        authenticationError(packet, tr("User query error"));

        return false;
    }

    if (privs != Darkmires::UserNone)
    {
        if (!(m_userQuerySelect.value(3).toUInt() & privs))
        {
            m_userQuerySelect.finish();

            authenticationError(packet, tr("Insufficient privileges"));

            return false;
        }
    }

    QString salt = m_userQuerySelect.value(2).toString();

    QString passwordIncoming = root.value("data").toMap().value("password").toString();
    QString hashIncoming = QString::fromAscii(QCryptographicHash::hash((salt + passwordIncoming).toAscii(), QCryptographicHash::Sha1).toHex());

    QString hashKnown = m_userQuerySelect.value(1).toString();

    qxtLog->debug() << tr("Password hashes: %1 : %2").arg(hashKnown).arg(hashIncoming);

    if (hashKnown != hashIncoming)
    {
        m_userQuerySelect.finish();

        authenticationError(packet, tr("Invalid password"));

        return false;
    }

    quint64 database = m_userQuerySelect.value(4).toULongLong();

    QUuid session(m_userQuerySelect.value(5).toString());

    qxtLog->info() << "Registering user with UUID: " << session.toString();

    packet->uuid = session;

    if (packet->database)
    {
        qxtLog->debug() << tr("Rewriting database id from %1 to %2").arg(packet->database).arg(database);
    }

    packet->database = database;

    m_userQuerySelect.finish();

    saltOut = salt;

    return true;
}
