#ifndef DARKMIRESSESSIONUSERDATA_HPP
#define DARKMIRESSESSIONUSERDATA_HPP

#include "darkmires_global.hpp"

#include <memory>

#include "darkmires.hpp"

///
/// \brief DsSessionUserData je trieda reprezentujúca dáta používateľa
/// pripojeného k niektorej relácii.
///
class DARKMIRESSHARED_EXPORT DsSessionUserData : public QObject
{
        Q_OBJECT

        /// \brief Identifikátor pre skripty
        Q_PROPERTY(int id READ getUser)

    public:
        ///
        /// \brief DsSessionUserData je konštruktor tejto triedy.
        /// \param user Identifikátor pripojeného používateľa
        ///
        explicit DsSessionUserData(int user);

        ///
        /// \brief ~DsSessionUserData je deštruktor tejto triedy.
        ///
        virtual ~DsSessionUserData();

        ///
        /// \brief getUser získa používateľa, ktorému sú tieto údaje priradené.
        /// \return
        ///
        inline int getUser()
        {
            return m_user;
        }

    public slots:
        ///
        /// \brief hasPrivilege zistí, či používateľ, ktorému tieto údaje patria má dané privilégiá.
        /// \param privilege privilégiá
        /// \return boolovska hodnota
        ///
        inline bool hasPrivilege(unsigned int privilege) const
        {
            return m_privileges & privilege;
        }

        ///
        /// \brief addPrivilege pridá privilégiá používateľovi.
        /// \param privilege privilégiá na pridanie
        ///
        inline void addPrivilege(unsigned int privilege)
        {
            m_privileges |= privilege;
        }

        ///
        /// \brief removePrivilege odstraní dané privilégiá tohto používateľa.
        /// \param privilege bitová maska privilégií
        ///
        inline void removePrivilege(unsigned int privilege)
        {
            m_privileges &= ~privilege;
        }

        ///
        /// \brief getPrivileges získa bitovú masku definujúcu privilégiá.
        /// \return bitová maska privilégií
        ///
        inline unsigned int getPrivileges() const
        {
            return m_privileges;
        }

    protected:
        int m_user;

        unsigned int m_privileges = 0x00;
};

#endif // DARKMIRESSESSIONUSERDATA_HPP
