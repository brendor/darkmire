#ifndef DSMODELELEMENT_HPP
#define DSMODELELEMENT_HPP

#include <QVariant>

class DsModelElement
{
    public:
        DsModelElement();

        inline const QString & getLastError() { return m_error; }

    protected:
        virtual bool make(const QVariant & variant) = 0;

        QString m_error;
};

#endif // DSMODELELEMENT_HPP
