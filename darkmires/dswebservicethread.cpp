/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dswebservicethread.hpp"

#include <QxtLogger>
#include <QxtWebContent>
#include <QxtJSON>


#include "dsrootwebservice.hpp"


DsWebServiceThread::DsWebServiceThread(QxtWebRequestEvent * event, QObject * parent) :
    m_event(event),
    m_object(parent)
{
}


void DsWebServiceThread::run()
{
    try
    {
        qxtLog->debug() << "[THREAD] Starting in thread: " + QString::number((long long int)QThread::currentThreadId());

        if (m_event)
        {
            m_event->content->waitForAllContent();

            m_object.emitFinished(std::move(DsRootWebService::createPacket(m_event)));
        }

        qxtLog->debug() << "[THREAD] Done";

    }
    catch (const std::exception & e)
    {
        qxtLog->debug() << QString("[THREAD] Error: ") + e.what();
    }
}


DsWebServiceThreadObject * DsWebServiceThread::object()
{
    return &m_object;
}


DsWebServiceThreadObject::DsWebServiceThreadObject(QObject * parent) :
    QObject(parent)
{

}

void DsWebServiceThreadObject::emitFinished(Darkmires::PacketPointer packet)
{
    emit dataFetched(packet);
}
