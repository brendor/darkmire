/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsuser.hpp"

#include "dsconstants.hpp"
#include "darkmires.hpp"

#include <QxtLogger>


DsUser::DsUser(int id, QObject * parent) :
    QObject(parent)
{
    m_id = id;

    unsigned int timeout = DarkSettings->value("user/timeout").toUInt();

    if (timeout)
    {
        m_timer.setSingleShot(true);

        connect(&m_timer,
                SIGNAL(timeout()),
                this,
                SLOT(timerEvent()));

        m_timer.start(1000 * timeout);
    }
}


DsUser::~DsUser()
{
    qxtLog->debug() << "Deleting user...";
}


void DsUser::resetTimer()
{
    if (m_timer.isActive())
    {
        m_timer.stop();

        int timeout = DarkSettings->value("user/timeout").toUInt();

        const Darkmires::SessionMap & sessions = Darkmires::getInstance()->getSessions();

        for (const QString & session : m_sessions)
        {

            if (! sessions.contains(session))
            {
                qxtLog->error() << QString("Session %1 which user is in does not exist!").arg(session);
                continue;
            }

            if (sessions.value(session)->getSessionProperty(DsConstants::SESSION_TIMEOUT, timeout).toInt() > timeout)
            {
                timeout = sessions.value(session)->getSessionProperty(DsConstants::SESSION_TIMEOUT).toInt();
            }
        }

        m_timer.start(1000 * timeout);
    }
}


void DsUser::timerEvent()
{
    emit timeout(m_id);
}


bool DsUser::inSession(const QString & session) const
{
    return m_sessions.contains(session);
}


void DsUser::addSession(const QString & session)
{
    m_sessions.append(session);
}


void DsUser::removeSession(const QString & session)
{
    m_sessions.removeOne(session);
}


int DsUser::sessionCount() const
{
    return m_sessions.count();
}


void DsUser::enqueueMessage(Darkmires::UserMessage && message)
{
    m_messages.enqueue(std::move(message));
}


void DsUser::processMessages(Darkmires::PacketPointer packet)
{
    QVariantList messages;

    while (! m_messages.empty())
    {
        Darkmires::UserMessage msg = m_messages.dequeue();

        QVariantMap message;

        message["text"] = msg.text;

        messages.append(message);
    }

    packet->data["messages"] = messages;
}
