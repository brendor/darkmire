/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dssessioninterface.hpp"

#include <QxtLogger>

#include <QSqlError>
#include <QSqlRecord>

#include "dssessionuserdata.hpp"

#include "dsquicksharesession.hpp"
#include "dsscriptedsession.hpp"
#include "dssubversionsession.hpp"

#include "dsuserstorage.hpp"

#include "dssessionuserdata.hpp"

#include "dswebevent.hpp"

#include "dsconstants.hpp"
#include "dsutils.hpp"


namespace
{
    static const QString UserQuery = "SELECT session FROM users WHERE session IS :name";
}


void DsSessionInterface::DarkmiresSessionDeleter::operator()(DsSession * p)
{
    delete p;
}


DsSessionInterface::DsSessionInterface(const QString & id) :
    QObject(nullptr),
    m_mutex(QMutex::Recursive)
{
    qRegisterMetaType<Darkmires::UserPointer>("Darkmires::UserPointer");

    if (! id.isEmpty())
    {
        m_uuid = QUuid(id);

        if (m_uuid.isNull())
        {
            qxtLog->warning() << "Creating session with invalid id (generating new)";

            m_uuid = QUuid::createUuid();
        }
    }
    else
    {
        m_uuid = QUuid::createUuid();
    }
}


DsSessionInterface::~DsSessionInterface()
{
    qxtLog->info() << "Deleting session interface";

    deinit();
}


bool DsSessionInterface::init()
{
    if (m_session)
    {
        qxtLog->debug() << "Session already initialized!";
        return false;
    }

    qxtLog->debug() << QString("Initializing session (%1) in thread: %2").arg(m_name).arg(QString::number(QThread::currentThreadId()));

    switch (m_mode)
    {
        case Darkmires::SessionQuickShare:
            /* NOTE: "this" here is not used as a parent of QObject */

            switch (m_authentication)
            {
                case Darkmires::AuthenticationNone:
                    break;

                default:
                    qxtLog->error() << "Invalid authentication mode for QuickShare session!";
                    return false;
            }

            m_session.reset(new DsQuickShareSession(this));
            break;

        case Darkmires::SessionScripted:
            m_session.reset(new DsScriptedSession(this));
            break;

        case Darkmires::SessionSubversion:

            switch (m_authentication)
            {
                case Darkmires::AuthenticationLocalDatabase:
                    break;

                default:
                    qxtLog->error() << "Invalid authentication mode for subversion session!";
                    return false;
            }

            m_session.reset(new DsSubversionSession(this));
            break;

        default:
            qxtLog->debug() << "Invalid session mode!";
            return false;
    }

    QString uuid = m_uuid.toString();
    DsUtils::removeFirstAndLast(uuid);
    QString name = DsConstants::SQL_DATABASE_SESSION_PREFIX + uuid;

    switch (m_authentication)
    {
        case Darkmires::AuthenticationLocalDatabase:
        {
            m_database = QSqlDatabase::database(name, false);

            if (! m_database.isValid())
            {
                m_database = QSqlDatabase::addDatabase("QSQLITE", name);
            }

            if (! m_database.isValid())
            {
                qxtLog->error() << tr("Database driver not valid!");
            }
            else
            {
                m_database.setDatabaseName(DarkSettings->value("sessions-file-path").toString() + uuid + ".db");

                if (m_database.open())
                {
                    qxtLog->info() << tr("Session user database connection opened: ") + m_database.databaseName();
                }
                else
                {
                    qxtLog->error() << tr("Session user database connection could not be opened: ") + m_database.lastError().text();
                }
            }

            break;
        }

        default:
            break;
    }

    return true;
}


bool DsSessionInterface::start()
{
    if (m_thread.isRunning())
    {
        qxtLog->debug() << "Session already started!";
        return false;
    }

    if (! m_session->init())
    {
        qxtLog->debug() << "Session could not be initialized!";
        return false;
    }

    QObject::connect(this,
                     SIGNAL(sessionAddUser(int)),
                     m_session.get(),
                     SLOT(addUser(int)));

    QObject::connect(this,
                     SIGNAL(sessionRemoveUser(int)),
                     m_session.get(),
                     SLOT(removeUser(int)));

    QObject::connect(this,
                     SIGNAL(sessionProcessRequest(Darkmires::PacketPointer)),
                     m_session.get(),
                     SLOT(processRequest(Darkmires::PacketPointer)));

    QObject::connect(this,
                     SIGNAL(sessionProcessCommand(QString)),
                     m_session.get(),
                     SLOT(processCommand(QString)));

    QObject::connect(m_session.get(),
                     SIGNAL(replySignal(DsWebEvent *)),
                     this,
                     SLOT(replySlot(DsWebEvent *)));

    QObject::connect(m_session.get(),
                     SIGNAL(sessionEvent(Darkmires::Event)),
                     this,
                     SLOT(eventSlot(Darkmires::Event)));

    QObject::connect(&m_thread,
                     SIGNAL(started()),
                     m_session.get(),
                     SLOT(start()));

    QObject::connect(&m_thread,
                     SIGNAL(finished()),
                     m_session.get(),
                     SLOT(stop()));

    m_session->moveToThread(&m_thread);

    m_thread.start();

    return true;
}


void DsSessionInterface::setSessionProperty(const QString & name, const QVariant & value)
{
    m_properties[name] = value;
}


const QVariant DsSessionInterface::getSessionProperty(const QString & name, const QVariant & def)
{
    return m_properties.value(name, def);
}


int DsSessionInterface::removeSessionProperty(const QString & name)
{
    return m_properties.remove(name);
}


void DsSessionInterface::command(QString command)
{
    emit sessionProcessCommand(command);
}


void DsSessionInterface::deinit()
{
    if (! m_session)
    {
        return;
    }

    m_session->disconnect();

    m_thread.quit();

    m_thread.wait(DarkSettings->value("thread-wait", DsConstants::WAIT_FOR_THREAD).toInt());

    m_session.reset();

    if (m_thread.isRunning())
    {
        m_thread.terminate();
    }

    if (m_database.isOpen())
    {
        m_database.close();

        QSqlDatabase::removeDatabase(m_database.connectionName());
    }
}


QString DsSessionInterface::escapeName(const QString & name)
{
    QString final;

    for (const QChar & c : name)
    {

        if (c.isLetterOrNumber())
        {
            final.append(c);
        }
        else
        {
            switch (c.toAscii())
            {
                case ' ':
                    final.append("_");
                    break;

                default:
                    break;
            }
        }
    }

    return final;
}


void DsSessionInterface::request(Darkmires::PacketPointer packet)
{
    if (m_mutex.tryLock(DarkSettings->value("session-wait", DsConstants::WAIT_FOR_SESSION).toInt()))
    {
        m_mutex.unlock();

        if (m_owners.contains(packet->uuid))
        {
            packet->data["owner"] = true;
        }

        emit sessionProcessRequest(packet);

    }
    else
    {
        replySlot(DsWebEvent::construct(packet, Darkmires::WebEventSessionError, "Session busy"));
    }
}


bool DsSessionInterface::addUser(const Darkmires::PacketPointer & packet)
{
    if (authenticateUser(packet))
    {
        LockUserStorage
        DsUserStorage::getInstance()->get(packet->event->sessionID)->addSession(m_uuid.toString());
        UnlockUserStorage

        emit sessionAddUser(packet->event->sessionID);

        return true;
    }

    return false;
}


void DsSessionInterface::removeUser(int id)
{
    LockUserStorage

    DsUserStorage::getInstance()->get(id)->removeSession(m_uuid.toString());

    if (m_owners.contains(DsUserStorage::getInstance()->get(id)->getSessionID()))
    {
        qxtLog->info() << "Session owner leaving his session.";
    }

    UnlockUserStorage

    emit sessionRemoveUser(id);
}



Darkmires::ChatStorage DsSessionInterface::getChat()
{
    QMutexLocker mutex(&m_mutex);

    return m_session->getChat();
}


const std::atomic_ulong & DsSessionInterface::getThreadId() const
{
    return m_session->getThreadId();
}


void DsSessionInterface::chatMessage(Darkmires::ChatMessage && message)
{
    if (! m_properties.value("allow-chat", false).toBool())
    {
        return;
    }

    QMutexLocker mutex(&m_mutex);

    message.id = (m_session->getChat().size() ? ((m_session->getChat().end() - 1).value().id + 1) : 1);
    message.time = QDateTime::currentDateTimeUtc();

    if (! message.message.isEmpty())
    {
        m_session->getChat()[message.time.toMSecsSinceEpoch()] = std::move(message);

        emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_NEW_CHAT_MESSAGE, m_uuid.toString()));
    }
}


const std::shared_ptr<DsSessionUserData> DsSessionInterface::getUser(int id)
{
    return m_session->getSessionUser(id);
}


bool DsSessionInterface::addOwner(const QUuid & owner)
{
    QMutexLocker mutex(&m_mutex);

    if (! m_owners.contains(owner))
    {
        m_owners.append(owner);
    }
    else
    {
        qxtLog->warning() << "User " << owner.toString() << " is already an owner of this session: " << m_name;

        return false;
    }

    return true;
}


bool DsSessionInterface::removeOwner(const QUuid & owner)
{
    QMutexLocker mutex(&m_mutex);

    return m_owners.removeOne(owner);
}


void DsSessionInterface::replySlot(DsWebEvent * event)
{
    emit replySignal(event);
}


void DsSessionInterface::eventSlot(Darkmires::Event event)
{
    emit sessionEvent(std::move(event));
}


void DsSessionInterface::save(QDataStream & stream)
{
    QMutexLocker mutex(&m_mutex);

    stream << m_name;
    stream << m_mode;
    stream << m_authentication;
    stream << m_owners;

    stream << m_properties;

    m_session->save(stream);
}


DsSessionInterface * DsSessionInterface::create(QDataStream & stream, const QString & id)
{
    DsSessionInterface * session = new DsSessionInterface(id);

    QString name;
    stream >> name;

    session->m_name = escapeName(name);

    int mode;
    stream >> mode;

    session->m_mode = (Darkmires::SessionMode)mode;

    int authentication;
    stream >> authentication;

    session->m_authentication = (Darkmires::AuthenticationType)authentication;

    stream >> session->m_owners;

    stream >> session->m_properties;

    if (! session->init())
    {
        delete session;
        return nullptr;
    }

    session->m_session->load(stream);

    return session;
}


DsSessionInterface * DsSessionInterface::create(const QString & name,
        Darkmires::AuthenticationType authentication,
        Darkmires::SessionMode mode,
        const QString & id)
{
    if (name.isEmpty())
    {
        qxtLog->error() << tr("[SESSION] Invalid name!");
        return nullptr;
    }

    if (! id.isEmpty())
    {
        if (QUuid(id).isNull())
        {
            qxtLog->error() << tr("[SESSION] Invalid UUID!");
            return nullptr;
        }
    }

    DsSessionInterface * session = new DsSessionInterface(id);

    session->m_name = escapeName(name);
    session->m_authentication = authentication;
    session->m_mode = mode;

    if (! session->init())
    {
        delete session;
        return nullptr;
    }

    return session;
}


bool DsSessionInterface::isOwner(int id)
{
    LockUserStorage
    QUuid uuid = DsUserStorage::getInstance()->get(id)->getSessionID();
    UnlockUserStorage

    if (uuid.isNull())
    {
        return false;
    }

    return isOwner(uuid);
}


bool DsSessionInterface::isOwner(const QUuid & uuid)
{
    return m_owners.contains(uuid);
}


bool DsSessionInterface::authenticateUser(const Darkmires::PacketPointer & packet)
{
    switch (m_authentication)
    {
        case Darkmires::AuthenticationNone:
            return true;

        case Darkmires::AuthenticationLDAP:
            return false;

        case Darkmires::AuthenticationLocalDatabase:
        {
            /// Only accept registered users
            if (packet->uuid.isNull())
            {
                return false;
            }

            QSqlQuery query(m_database);

            if (! query.prepare(UserQuery))
            {
                qxtLog->warning() << "Error preparing query!";

                return false;
            }

            query.bindValue(":name", packet->uuid.toString(), QSql::Out);

            if (! query.exec())
            {
                return false;
            }

            if (! query.first())
            {
                return false;
            }

            if (! query.isValid())
            {
                qxtLog->error() << tr("Database query invalid: ") + query.lastError().text();

                return false;
            }

            qxtLog->debug() << "Locally authenticated user: " << packet->event->sessionID << " in session " << m_name;

            return true;
        }

        default:
            break;
    }

    return false;
}
