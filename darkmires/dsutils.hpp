#ifndef DSUTILS_HPP
#define DSUTILS_HPP

#include <QByteArray>

namespace DsUtils
{
    ///
    /// \brief Crc16CCITT vypočíta hash podľa špecifikácie CRC16 CITT
    /// \param data dáta
    /// \param len dĺžka dát
    /// \return celočíselný hash
    ///
    int Crc16CCITT(const char * data, int len);

    ///
    /// \brief compress stlačí dáta
    /// \param bytes dáta
    /// \param level úroveň stlačenia v rozsahu 0-10, -1 pre preferované stlačenie
    /// \return stlačené dáta
    ///
    QByteArray compress(const QByteArray & bytes, int level = -1);

    ///
    /// \brief decompress rozbalí dáta
    /// \param bytes stlačené dáta
    /// \param originalSize veľkosť pred stlačením
    /// \return rozbalené dáta
    ///
    QByteArray decompress(QByteArray & bytes, quint32 originalSize);

    ///
    /// \brief removeFirstAndLast zmaže prvý a posledný znak z reťazca
    /// \param string referencia na reťazec
    ///
    void removeFirstAndLast(QString & string);

    ///
    /// \brief removeDir zmaže zložku
    /// \param dirName cesta k zložke
    /// \return true pri úspešnom zmazaní, inak false
    ///
    bool removeDir(const QString & dirName);

    ///
    /// \brief escapeJson odstráni z reťazca v JSON notácii nepodporované znaky
    /// \param string reťazec v JSON notácii
    /// \return reťazec bez nepovolených znakov
    ///
    QString escapeJson(const QString & string);

    template <typename T>
    T clamp(const T & value, const T & low, const T & high)
    {
      return value < low ? low : (value > high ? high : value);
    }

}

#endif // DSUTILS_HPP
