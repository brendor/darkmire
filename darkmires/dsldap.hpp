#ifndef DARKMIRESLDAP_HPP
#define DARKMIRESLDAP_HPP

#include <QObject>

#include <memory>

#include <QThread>

#include "dsldapthread.hpp"

///
/// \brief DsLDAP je trieda reprezentujúca rozhranie ku LDAP knižnici.
///
class DsLDAP : public QObject
{
        Q_OBJECT

    public:
        ///
        /// \brief DsLDAP je konštruktor tejto triedy.
        /// \param parent pointer na rodičovský objekt
        ///
        explicit DsLDAP(QObject * parent = 0);

        ///
        /// \brief search vyhľadá prostredníctvom protokolu LDAP, či sa používateľ odosielajúci
        /// danú požiadavku nachádza v databáze a má dostatočné právomoci
        /// \param packet požiadavka
        /// \param name meno používateľa v databáze LDAP
        ///
        void search(const Darkmires::PacketPointer & packet, const QString & name);

        ///
        /// \brief escapeName odstráni z mena zakázané znaky
        /// \param name meno
        /// \return meno bez zakázaných znakov
        ///
        static QString escapeName(const QString & name);

    signals:
        ///
        /// \brief searchFinished signál vyvolaný pri ukončení hľadania v databáze
        /// \param packet požiadavka, s ktorou bola volaná funkcia search
        /// \param result výsledok hľadania
        /// \sa search
        ///
        void searchFinished(Darkmires::PacketPointer packet, DsAuthenticationLayer::SearchResult result);

    private slots:
        void threadSearchDone(int id, DsAuthenticationLayer::SearchResult result);
        void threadFinished();

    private:
        typedef std::unique_ptr<DsLDAPThread> DarkmiresLDAPThreadUnique;

        std::vector<DarkmiresLDAPThreadUnique> m_threads;

        QHash<int, Darkmires::PacketPointer> m_searches;
};

#endif // DARKMIRESLDAP_HPP
