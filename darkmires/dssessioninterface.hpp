#ifndef DARKMIRESSESSION_H
#define DARKMIRESSESSION_H

#include "darkmires_global.hpp"

#include <QObject>
#include <QUuid>
#include <QDateTime>
#include <QMutex>
#include <QThread>

#include <QSqlDatabase>
#include <QSqlQuery>

#include <atomic>

#include "darkmires.hpp"

class DsSession;
class DsQuickShareSession;
class DsScriptedSession;
class DsSubversionSession;

class DsSessionUserData;

class QxtWebEvent;

///
/// \brief Trieda DsSessionInterface obaľuje reláciu bežiacu v samostatnom vlákne a poskytuje
/// vláknovo bezpečný prístup k nej.
///
class DARKMIRESSHARED_EXPORT DsSessionInterface : public QObject
{
        Q_OBJECT

    public:
        typedef QList<QUuid> SessionOwners;
        typedef QHash<QString, QVariant> SessionProperties;

        virtual ~DsSessionInterface();

        ///
        /// \brief escapeName odstráni z názvu relácie nepovolené znaky.
        /// \param name meno relácie
        /// \return meno relácie bez nepovolených znakov
        ///
        static QString escapeName(const QString & name);

        ///
        /// \brief getUuid získa jedinečný identifikátor reácie.
        /// \return jedinečný identifikátor reácie
        ///
        inline const QUuid & getUuid() const
        {
            return m_uuid;
        }

        inline const QString & getName() const
        {
            return m_name;
        }

        inline Darkmires::AuthenticationType getAuthentication() const
        {
            return m_authentication;
        }

        inline Darkmires::SessionMode getMode() const
        {
            return m_mode;
        }

        inline const QDateTime & getExpiration() const
        {
            return m_expiration;
        }

        inline void setExpiration(const QDateTime & expiration)
        {
            m_expiration = expiration;
        }

        ///
        /// \brief request pošle relácii požiadavku.
        /// \param packet požiadavka
        ///
        void request(Darkmires::PacketPointer packet);

        ///
        /// \brief addUser pridá používateľa do relácie, ak mu to práva a nastavenia
        /// relácie umožnujú.
        /// \param packet požiadavka obsahujúca údaje o používateľovi
        /// \return true ak bol pridaný, inak false
        ///
        bool addUser(const Darkmires::PacketPointer & packet);

        ///
        /// \brief removeUser odstráni používateľa z relácie.
        /// \param id identifikátor používateľa
        ///
        void removeUser(int id);

        ///
        /// \brief getChat získa zoznam správ v danej relácie.
        /// \return zoznam správ v danej relácie
        ///
        Darkmires::ChatStorage getChat();

        ///
        /// \brief getThreadId získa identifikátor vlákna v ktorom beží relácia.
        /// \return identifikátor vlákna v ktorom beží relácia
        ///
        const std::atomic_ulong & getThreadId() const;

        ///
        /// \brief chatMessage pošle správu relácii
        /// \param message správa
        ///
        void chatMessage(Darkmires::ChatMessage && message);

        ///
        /// \brief getUser získa dáta používateľa s identifikátorom ID.
        /// \param id identifikátor používateľa
        /// \return pointer na dáta používateľa
        ///
        const std::shared_ptr<DsSessionUserData> getUser(int id);

        ///
        /// \brief addOwner pridá vlastníka relácie.
        /// \param owner jedinečný identifikátor vlastníka relácie
        /// \return true pri úspešnom pridaní, inak false
        ///
        bool addOwner(const QUuid & owner);

        ///
        /// \brief removeOwner zmaže vlastníka relácie.
        /// \param owner jedinečný identifikátor vlastníka relácie
        /// \return true pri úspešnom odstránení, inak false
        ///
        bool removeOwner(const QUuid & owner);

        ///
        /// \brief isOwner zistí, či používateľ s daným ID je vlastníkom.
        /// \param id ID používateľa
        /// \return true ak je vlastníkom, inak false
        ///
        bool isOwner(int id);

        ///
        /// \brief isOwner zistí, či používateľ s daným UUID je vlastníkom.
        /// \param uuid jedinečný identifikátor používateľa
        /// \return true ak je vlastníkom, inak false
        ///
        bool isOwner(const QUuid & uuid);

        ///
        /// \brief getOwners získa zoznam vlastníkov.
        /// \return referencia na zoznam vlastníkov
        ///
        inline const SessionOwners & getOwners() const
        {
            return m_owners;
        }

        ///
        /// \brief save uloží dáta relácie do dátového prúdu.
        /// \param stream dátový prúd
        ///
        void save(QDataStream & stream);

        ///
        /// \brief create vytvorí reláciu podľa dát z dátového prúdu
        /// \param stream dátový prúd
        /// \param id požadovaný identifikátor
        /// \return pointer na novú reláciu, ktorý musí volajúci zmazať
        ///
        static DsSessionInterface * create(QDataStream & stream,
                                           const QString & id);

        ///
        /// \brief create vytvorí reláciu na základe špecifikovaných argumentov
        /// \param name meno relácie
        /// \param authentication typ autentifikácie
        /// \param mode typ relácie
        /// \param id požadovaný identifikátor
        /// \return pointer na novú reláciu, ktorý musí volajúci zmazať
        ///
        static DsSessionInterface * create(const QString & name,
                                           Darkmires::AuthenticationType authentication,
                                           Darkmires::SessionMode mode,
                                           const QString & id = QString());

        ///
        /// \brief start začne vlákno relácie obalené touto triedou.
        /// \return true pri úspešnom začatí vlákna, inak false
        ///
        bool start();

        void setSessionProperty(const QString & name, const QVariant & value);
        const QVariant getSessionProperty(const QString & name, const QVariant & def = QVariant());
        int removeSessionProperty(const QString & name);

        inline const SessionProperties & getSessionProperties() const
        {
            return m_properties;
        }

        ///
        /// \brief command spracuje príkaz špecifikovaný argumentom command.
        /// \param command príkaz
        ///
        void command(QString command);

    signals:
        ///
        /// \brief sessionProcessRequest signál je vyvolaný, keď príde požiadavka na spracovanie.
        /// \param packet požiadavka
        ///
        void sessionProcessRequest(Darkmires::PacketPointer packet);

        ///
        /// \brief sessionProcessCommand signál je vyvolaný, keď príde príkaz na spracovanie.
        /// \param command príka
        ///
        void sessionProcessCommand(QString command);

        ///
        /// \brief sessionAddUser signál vyvolaný pri pridávaní používateľa.
        /// \param user ID používateľa
        ///
        void sessionAddUser(int user);

        ///
        /// \brief sessionAddUser signál vyvolaný pri odstránení používateľa.
        /// \param user ID používateľa
        ///
        void sessionRemoveUser(int id);

        ///
        /// \brief replySignal signál vyvolaný, keď vnútorné vlákno relácie odpovedá na požiadavku odpoveďou.
        /// \param event odpoveď
        ///
        void replySignal(DsWebEvent * event);

        ///
        /// \brief sessionEvent je sigál vyvolaný pri vnútornej udalost.
        /// \param event
        ///
        void sessionEvent(Darkmires::Event event);

    private slots:
        void replySlot(DsWebEvent * event);
        void eventSlot(Darkmires::Event event);

    private:
        explicit DsSessionInterface(const QString & id);
        DsSessionInterface(const DsSessionInterface &) = delete;

        Darkmires::AuthenticationType m_authentication = Darkmires::AuthenticationNone;
        Darkmires::SessionMode m_mode = Darkmires::SessionNone;

        QString m_name;

        QUuid m_uuid;

        QDateTime m_expiration;

        QMutex m_mutex;
        QThread m_thread;

        class DarkmiresSessionDeleter
        {
            public:
                DarkmiresSessionDeleter() { }

                void operator()(DsSession * p);
        };

        std::unique_ptr<DsSession, DarkmiresSessionDeleter> m_session;

        SessionOwners m_owners;

        SessionProperties m_properties;

        bool init();
        void deinit();

        friend class DsSession;
        friend class DsQuickShareSession;
        friend class DsScriptedSession;
        friend class DsSubversionSession;

        bool authenticateUser(const Darkmires::PacketPointer & packet);

        QSqlDatabase m_database;
};

#endif // DARKMIRESSESSION_H
