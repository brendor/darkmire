/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsscriptedsession.hpp"

#include "dswebevent.hpp"
#include "dsconstants.hpp"

#include "dsuserstorage.hpp"

#include "dsutils.hpp"

#include <QxtLogger>
#include <QxtJSON>

#include <QCoreApplication>

namespace
{
    enum ScriptedRequestType : unsigned int
    {
        ScriptedNormal = 0,
        ScriptedFetchClientScript = 1,
        ScriptedTriggerEvents = 2,
    };

    QScriptValue CreateValue(const QVariant& value, QScriptEngine& engine)
    {
        if(value.type() == QVariant::Map)
        {
            QScriptValue obj = engine.newObject();

            QVariantMap map = value.toMap();
            QVariantMap::const_iterator it = map.begin();
            QVariantMap::const_iterator end = map.end();
            while(it != end)
            {
                obj.setProperty( it.key(), ::CreateValue(it.value(), engine) );
                ++it;
            }

            return obj;
        }

        if(value.type() == QVariant::List)
        {
            QVariantList list = value.toList();
            QScriptValue array = engine.newArray(list.length());
            for(int i=0; i<list.count(); i++)
                array.setProperty(i, ::CreateValue(list.at(i),engine));

            return array;
        }

        switch(value.type())
        {
        case QVariant::String:
            return QScriptValue(value.toString());
        case QVariant::Int:
            return QScriptValue(value.toInt());
        case QVariant::UInt:
            return QScriptValue(value.toUInt());
        case QVariant::Bool:
            return QScriptValue(value.toBool());
        case QVariant::ByteArray:
            return QScriptValue(QLatin1String(value.toByteArray()));
        case QVariant::Double:
            return QScriptValue((qsreal)value.toDouble());
        default:
            break;
        }

        if(value.isNull())
            return QScriptValue(QScriptValue::NullValue);

        return engine.newVariant(value);
    }
}

DsScriptedSession::DsScriptedSession(DsSessionInterface * const interface) :
    DsSession(interface),
    m_scriptEngine(this),
    m_translator(this)
{
    qScriptRegisterMetaType(&m_scriptEngine, DsScriptedSession::packetToScriptValue, DsScriptedSession::packetFromScriptValue);

    m_mode = Darkmires::SessionScripted;
}


DsScriptedSession::~DsScriptedSession()
{
    qxtLog->debug() << "Deleting scripted session";

    if (m_scriptEngine.isEvaluating())
    {
        m_scriptEngine.abortEvaluation();
    }

    QCoreApplication::instance()->removeTranslator(&m_translator);
}


void DsScriptedSession::load(QDataStream & in)
{
    DsSession::load(in);

    QString json;

    in >> json;

    if (! json.isEmpty())
    {
        qxtLog->debug() << json;

        m_this = deserializeScriptObject(json, m_scriptEngine);

        printProperties(m_this);
    }
}


void DsScriptedSession::save(QDataStream & in)
{
    DsSession::save(in);

    in << serializeScriptObject(m_this, m_scriptEngine);
}


bool DsScriptedSession::init()
{
    QFile file(m_interface->m_properties.value("script-file").toString());

    if (! file.exists())
    {
        qxtLog->error() << "Initializing scripted session with non-existing script file: " << file.fileName();
        return false;
    }

    m_fileName = QFileInfo(file).absoluteFilePath();


    if (loadServerScriptFile(file))
    {
        qxtLog->debug() << "Script " << file.fileName() << " OK!";

        QFileInfo languageFile(m_interface->m_properties.value("script-language-file").toString());

        if (languageFile.exists())
        {

            qxtLog->debug() << "Installing translator for language file: " << languageFile.fileName();

            m_translator.load(languageFile.baseName(), languageFile.absolutePath(), "_", ".qm");

            QCoreApplication::instance()->installTranslator(&m_translator);

            m_scriptEngine.installTranslatorFunctions();
        }

        m_this = m_scriptEngine.newObject();

        QScriptValue thisObject = m_scriptEngine.newQObject(this,
                                  QScriptEngine::QtOwnership,
                                  QScriptEngine::ExcludeSlots |
                                  QScriptEngine::ExcludeSuperClassContents |
                                  QScriptEngine::PreferExistingWrapperObject);


        thisObject.setProperty("user", m_scriptEngine.newFunction(DsScriptedSession::getSessionUserScript), QScriptValue::Undeletable);
        thisObject.setProperty("print", m_scriptEngine.newFunction(DsScriptedSession::logMessageScript), QScriptValue::Undeletable);
        thisObject.setProperty("post", m_scriptEngine.newFunction(DsScriptedSession::postScriptEvent), QScriptValue::Undeletable);


        m_scriptEngine.globalObject().setProperty("session", thisObject, QScriptValue::Undeletable);


        m_scriptEngine.globalObject().property("init", QScriptValue::ResolveLocal).call(m_this);


        if (m_scriptEngine.hasUncaughtException())
        {
            reportScriptException();
        }

        m_requestFunction = m_scriptEngine.globalObject().property("request", QScriptValue::ResolveLocal);

        if (! m_requestFunction.isFunction())
        {
            qxtLog->warning() << "Script does not contain the request function!";
        }

        m_userFunction = m_scriptEngine.globalObject().property("user", QScriptValue::ResolveLocal);

        if (! m_userFunction.isFunction())
        {
            qxtLog->warning() << "Script does not contain the user function!";
        }

        m_eventFunction = m_scriptEngine.globalObject().property("event", QScriptValue::ResolveLocal);

        if (! m_eventFunction.isFunction())
        {
            qxtLog->warning() << "Script does not contain the event function!";
        }

        m_scriptEngine.setProcessEventsInterval(m_interface->m_properties.value("script-events-interval", m_scriptEngine.processEventsInterval()).toInt());
    }

    return m_valid;
}


bool DsScriptedSession::loadServerScriptFile(QFile & file)
{
    m_valid = false;

    if (file.open(QIODevice::ReadOnly))
    {

        QTextStream stream(&file);
        QString source = stream.readAll();

        if (m_scriptEngine.isEvaluating())
        {
            m_scriptEngine.abortEvaluation();
        }

        m_program = QScriptProgram(source, file.fileName());

        qxtLog->debug() << "Evaluating script: " << file.fileName();

        QScriptValue result = m_scriptEngine.evaluate(m_program);

        if (result.isError())
        {
            reportScriptError(result);
        }
        else
        {
            m_valid = true;
        }

        file.close();

    }
    else
    {
        qxtLog->error() << "Unable to open script file: " << file.fileName();
    }

    return m_valid;
}


QScriptValue DsScriptedSession::packetToScriptValue(QScriptEngine * engine, const Darkmires::PacketData & packet)
{
    QScriptValue ret = engine->newObject();

    ret.setProperty("json", engine->newVariant(packet.json), QScriptValue::ReadOnly | QScriptValue::Undeletable);
    ret.setProperty("data", engine->newVariant(packet.data), QScriptValue::Undeletable);
    ret.setProperty("event", engine->newVariant(qVariantFromValue((void *)packet.event)), QScriptValue::ReadOnly | QScriptValue::Undeletable);
    ret.setProperty("type", (int)packet.type, QScriptValue::ReadOnly | QScriptValue::Undeletable);
    ret.setProperty("session", packet.session, QScriptValue::ReadOnly | QScriptValue::Undeletable);
    ret.setProperty("uuid", packet.uuid.toString(), QScriptValue::ReadOnly | QScriptValue::Undeletable);
    ret.setProperty("sid", packet.sessionID.toString(), QScriptValue::ReadOnly | QScriptValue::Undeletable);
    ret.setProperty("id", packet.event->sessionID, QScriptValue::ReadOnly | QScriptValue::Undeletable);

    return ret;
}


void DsScriptedSession::packetFromScriptValue(const QScriptValue & value, Darkmires::PacketData & packet)
{
    packet.json = value.property("json").toVariant();
    packet.data = value.property("data").toVariant().toMap();
    packet.event = static_cast<QxtWebRequestEvent *>(value.property("event").toVariant().value<void *>());
    packet.type = (Darkmires::RequestType)value.property("type").toInt32();
    packet.session = value.property("session").toString();
    packet.uuid = value.property("uuid").toString();
    packet.sessionID = value.property("sid").toString();
}


void DsScriptedSession::printProperties(const QScriptValue & value, int level)
{
    QScriptValueIterator it(value);

    while (it.hasNext())
    {
        it.next();

        qxtLog->debug(QString().leftJustified(level * 4, '.'), " ", it.name(), ": ", it.value().toString());

        if (it.value().isObject())
        {
            printProperties(it.value(), level + 1);
        }
    }
}


QString DsScriptedSession::serializeScriptObject(const QScriptValue & value, QScriptEngine & engine)
{
    if (! value.isValid())
    {
        return QString();
    }

    QScriptValue eval = engine.evaluate("({ toString: function() { return JSON.stringify(this); } })");

    return eval.property("toString").call(value).toString();
}


QScriptValue DsScriptedSession::deserializeScriptObject(const QString & json, QScriptEngine & engine)
{
    return engine.evaluate("(" + json + ")");
}


QScriptValue DsScriptedSession::getSessionUserScript(QScriptContext * ctx, QScriptEngine * eng)
{
    if (! ctx->argument(0).isNumber())
    {
        return ctx->throwError(QScriptContext::TypeError, "user(): argument is not a number");
    }

    DsScriptedSession * session = qobject_cast<DsScriptedSession *>(eng->globalObject().property("session").toQObject());

    if (! session)
    {
        return ctx->throwError("user(): internal error");
    }

    const std::shared_ptr<DsSessionUserData> user = session->getSessionUser(ctx->argument(0).toInt32());

    if (! user.get())
    {
        return ctx->throwError(QScriptContext::ReferenceError, "user(): requested user does not exist");
    }

    return session->wrap(user.get());
}


QScriptValue DsScriptedSession::logMessageScript(QScriptContext * ctx, QScriptEngine * eng)
{
    Q_UNUSED(eng)

    qxtLog->debug("[SCRIPT] ", QDateTime::currentDateTimeUtc().toString("hh:mm:ss:zzz"), ctx->argument(0).toString());

    return QScriptValue(QScriptValue::NullValue);
}


QScriptValue DsScriptedSession::postScriptEvent(QScriptContext * ctx, QScriptEngine * eng)
{
    QScriptValue event = ctx->argument(0);

    DsScriptedSession * session = qobject_cast<DsScriptedSession *>(eng->globalObject().property("session").toQObject());

    if (! session)
    {
        return ctx->throwError("post(): internal error");
    }

    if (! session->m_users.contains(ctx->argument(1).toInt32()))
    {
        return ctx->throwError("post(): no such user");
    }

    DsScriptedSessionUserData * userData = static_cast<DsScriptedSessionUserData *>(session->m_users[ctx->argument(1).toInt32()].get());

    userData->getEvents().append(event);

    return QScriptValue(QScriptValue::NullValue);
}


void DsScriptedSession::processRequest(Darkmires::PacketPointer packet)
{
    QMutexLocker mutex(&m_interface->m_mutex);

    QVariantMap in = packet->json.toMap().value(DsConstants::JSON_DARKMIRE_ELEMENT)
                                 .toMap().value(DsConstants::JSON_DATA_ELEMENT).toMap();

    DsScriptedSessionUserData * userData = static_cast<DsScriptedSessionUserData *>(m_users[packet->event->sessionID].get());

    Darkmires::WebEventType status = Darkmires::WebEventSessionSuccess;


    switch ((ScriptedRequestType)in.value("type").toUInt())
    {
        case ScriptedNormal:
            break;

        case ScriptedFetchClientScript:
        {
            QFile file(m_interface->m_properties.value("client-file").toString());

            if (! file.exists())
            {
                qxtLog->error() << "Unable to find client script file!";
                status = Darkmires::WebEventSessionError;
                break;
            }

            if (file.open(QIODevice::ReadOnly))
            {

                QTextStream stream(&file);
                QString source = stream.readAll();

                packet->data["client-script"] = DsUtils::escapeJson(source);
            }
            else
            {
                qxtLog->error() << "Unable to open client script file!";
                status = Darkmires::WebEventSessionError;
                break;
            }

            break;
        }

        case ScriptedTriggerEvents:
        {
            if (! m_valid)
            {
                return sessionError(packet, "Scripted session invalid");
            }

            {
                QScriptValueList args;

                args << packetToScriptValue(&m_scriptEngine, *packet.get());

                QScriptValue ret = m_requestFunction.call(m_this, args);

                if (! ret.isValid())
                {
                    reportScriptError(ret);
                }

                if (m_scriptEngine.hasUncaughtException())
                {
                    reportScriptException();
                }
            }

            for (QVariant event : in.value("events").toList())
            {
                QScriptValueList args;
                args << CreateValue(event, m_scriptEngine);

                QScriptValue ret = m_eventFunction.call(m_this, args);

                if (! ret.isValid())
                {
                    reportScriptError(ret);
                }
            }

            QVariantList clientEvents;

            for (QScriptValue & event : userData->getEvents())
            {
                clientEvents.push_back(QxtJSON::parse(serializeScriptObject(event, m_scriptEngine)));
            }

            packet->data["client-events"] = clientEvents;

            userData->getEvents().clear();

            break;
        }
    }


    if (m_autoChat)
    {
        processChat(packet, in);
    }

    emit replySignal(DsWebEvent::construct(packet, status, packet->data));
}


void DsScriptedSession::addUser(int user)
{    
    if (! m_valid)
    {
        return;
    }

    std::shared_ptr<DsSessionUserData> data(new DsScriptedSessionUserData(user));

    QScriptValue wrapped = wrap(data.get());

    QScriptValue ret = m_userFunction.call(m_this, QScriptValueList() << wrapped);

    if (! ret.isValid())
    {
        return reportScriptError(ret);
    }

    if (m_scriptEngine.hasUncaughtException())
    {
        return reportScriptException();
    }

    m_users[user] = data;

    populate(data);

    emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_USER_JOINED_SESSION, m_interface->m_uuid.toString()));


    //    printProperties(wrapped);
}


void DsScriptedSession::processCommand(QString command)
{
    DsSession::processCommand(command);

    QString result = "Unknown command";

    QStringList split = command.split(' ', QString::SkipEmptyParts, Qt::CaseInsensitive);

    if (split.size())
    {
        if (split[0] == "eval")
        {
            result = "Evaluating command: " + split[1];
        }
        else if (split[0] == "init")
        {
            result = "Re-initializing...";

            init();
        }
    }

    emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_COMMAND_RESULT,
                                     QVariantList() << m_interface->m_uuid.toString() << result));
}


QScriptValue DsScriptedSession::wrap(QObject * pointer)
{
    return m_scriptEngine.newQObject(pointer, QScriptEngine::QtOwnership, QScriptEngine::ExcludeDeleteLater);
}


void DsScriptedSession::reportScriptError(const QScriptValue & value)
{
    qxtLog->error() << QString::fromLatin1("Script error: %0:%1: %2")
                    .arg(m_fileName)
                    .arg(value.property("lineNumber").toInt32())
                    .arg(value.toString());
}


void DsScriptedSession::reportScriptException()
{
    QScriptValue exception = m_scriptEngine.uncaughtException();

    if (exception.isValid())
    {
        qxtLog->error() << QString::fromLatin1("Script uncaught exception: %0:%1: %2")
                        .arg(m_fileName)
                        .arg(m_scriptEngine.uncaughtExceptionLineNumber())
                        .arg(exception.toString());

        for (const QString & line : m_scriptEngine.uncaughtExceptionBacktrace())
        {
            qxtLog->error() << "-> " << line;
        }
    }
}


DsScriptedSessionUserData::DsScriptedSessionUserData(int user) :
    DsSessionUserData(user)
{

}


DsScriptedSessionUserData::~DsScriptedSessionUserData()
{
    qDebug() << "Deleting scripted session user data";
}



void DsScriptedSession::populate(std::shared_ptr<DsSessionUserData> data)
{

}
