/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "darkmiresimpl.hpp"

#include <QxtLogger>

#include "dswebserver.hpp"
#include "dsrootwebservice.hpp"
#include "dsauthenticationlayer.hpp"
#include "dsloggingengine.hpp"
#include "dsuserstorage.hpp"
#include "dsconstants.hpp"

#include <QCoreApplication>


DarkmiresImpl::DarkmiresImpl(QObject * parent) :
    QObject(parent),
    m_server(nullptr),
    m_logger(new DsLoggingEngine)
{
    QCoreApplication::instance()->setApplicationName(DsConstants::APPLICATION_NAME);
    QCoreApplication::instance()->setApplicationVersion(DsConstants::APPLICATION_VERSION);

    qxtLog->addLoggerEngine(DsConstants::LOGGER_ENGINE_NAME, m_logger);

    qxtLog->disableAllLogLevels();

    qxtLog->enableAllLogLevels(DsConstants::LOGGER_ENGINE_NAME);
    qxtLog->enableLogLevels("DEFAULT", QxtLogger::WarningLevel);

    qDebug() << "Active logger engines:" << qxtLog->allEnabledLoggerEngines();

    DsUserStorage::allocateInstance();
}


DarkmiresImpl::~DarkmiresImpl()
{
    qxtLog->info() << "Destroying Darkmire server...";

    DsUserStorage::freeInstance();

    deinit();
}


bool DarkmiresImpl::init(const QHostAddress & host,
                         quint16 port,
                         QxtHttpSessionManager::Connector connector,
                         Darkmires::Flags flags)
{
    m_flags = flags;

    m_server = std::unique_ptr<DsWebServer>(new DsWebServer());

    if (flags & Darkmires::FlagForceHTTPS)
    {
        m_server->getRoot()->setForceHttps(true);
    }

    if (flags & Darkmires::FlagUseThreads)
    {
        m_server->getRoot()->setBehaviorPost(DsRootWebService::POST_FETCH_DATA_THREAD);
    }

    if (flags & Darkmires::FlagAllowLDAP)
    {
        m_server->getRoot()->getAuthenticationLayer()->setAllowLDAP(true);
    }

    if (flags & Darkmires::FlagDenyExternal)
    {
        m_server->getRoot()->setDenyExternal(true);
    }

    return m_server->init(host, port, connector);
}


void DarkmiresImpl::deinit()
{
    if (m_server)
    {
        m_server.reset();
    }

    Darkmires::saveSettings();

    m_flags = Darkmires::NoFlags;
}


bool DarkmiresImpl::start()
{
    qxtLog->info() << "Starting server...";

    bool started = m_server->start();

    if (! started)
    {
        qxtLog->error() << "Failed to start server";

        deinit();
    }

    return started;
}


void DarkmiresImpl::stop()
{
    qxtLog->info() << "Stopping server...";

    deinit();
}
