/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dssessionuserdata.hpp"

#include "dsuser.hpp"
#include "dsuserstorage.hpp"

#include <QxtLogger>


DsSessionUserData::DsSessionUserData(int user) :
    QObject(nullptr),
    m_user(user)
{

}


DsSessionUserData::~DsSessionUserData()
{
    qxtLog->debug() << "Deleting session user data...";
}
