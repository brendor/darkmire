/*
Copyright 2012-2014 Tomáš Kováč (brendor) brendorrmt@gmail.com

This file is part of Darkmire.

Darkmire is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Darkmire is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Darkmire.
If not, see http://www.gnu.org/licenses/.
*/

#include "dsquicksharesession.hpp"

#include <QxtLogger>

#include "dswebevent.hpp"

#include "dsuserstorage.hpp"

#include "dsconstants.hpp"


DsQuickShareSession::DsQuickShareSession(DsSessionInterface * const interface) :
    DsSession(interface)
{
    m_mode = Darkmires::SessionQuickShare;
}


DsQuickShareSession::~DsQuickShareSession()
{
    qxtLog->debug() << "Deleting managed repository";
}


void DsQuickShareSession::processRequest(Darkmires::PacketPointer packet)
{
    QMutexLocker mutex(&m_interface->m_mutex);

    if (! m_users.contains(packet->event->sessionID))
    {
        return sessionError(packet, "User not in session");
    }

    QVariantMap in = packet->json.toMap().value(DsConstants::JSON_DARKMIRE_ELEMENT).toMap().value(DsConstants::JSON_DATA_ELEMENT).toMap();

    DsQuickShareSessionUser * userData = static_cast<DsQuickShareSessionUser *>(m_users[packet->event->sessionID].get());

    Darkmires::WebEventType status = Darkmires::WebEventNormal;

    switch ((RequestType)in.value("type").toUInt())
    {
        case QuickShareRequestNormal:

            status = Darkmires::WebEventSessionSuccess;

            break;

        case QuickShareRequestEntry:
        {
            if (! in.contains("entry"))
            {
                return sessionError(packet, "Entry request contains no entry");
            }

            qxtLog->debug() << "Adding new entry...";

            QVariantMap entryEl = in.value("entry").toMap();

            if (entryEl.isEmpty())
            {
                status = Darkmires::WebEventSessionError;
            }
            else
            {
                status = Darkmires::WebEventSessionSuccess;

                DsQuickShareSessionUser::Entry entry;

                entry.code = entryEl.value("code").toString();
                entry.mime = entryEl.value("mime").toString();
                entry.date = QDateTime::currentDateTimeUtc();

                userData->addEntry(entry);

                emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_SESSION_NEW_DATA, m_interface->m_uuid.toString()));
            }

            break;
        }

        case QuickShareRequestList:
        {
            if (! m_interface->isOwner(packet->uuid))
            {
                return sessionError(packet, "Only session owner may request entry list");
            }

            QVariantList entries;

            for (const std::shared_ptr<DsSessionUserData> & u: m_users)
            {
                DsQuickShareSessionUser * user = static_cast<DsQuickShareSessionUser *>(u.get());

                LockUserStorage
                const QUuid & uuid = DsUserStorage::getInstance()->get(user->getUser())->getUuid();
                QString name = DsUserStorage::getInstance()->get(user->getUser())->getName();
                UnlockUserStorage

                for (const DsQuickShareSessionUser::Entry & entry : user->getEntries())
                {
                    QVariantMap variantEntry;

                    variantEntry["time"] = entry.date.toString(Qt::ISODate);
                    variantEntry["code"] = entry.code;
                    variantEntry["mime"] = entry.mime;
                    variantEntry["author"] = name;
                    variantEntry["uuid"] = uuid.toString();

                    entries.append(variantEntry);
                }
            }

            packet->data["entries"] = entries;

            status = Darkmires::WebEventSessionSuccess;

            break;
        }

        default:
            return sessionError(packet, "Invalid request type");
    }

    processChat(packet, in);

    emit replySignal(DsWebEvent::construct(packet, status, packet->data));
}


void DsQuickShareSession::addUser(int user)
{
    std::shared_ptr<DsSessionUserData> data(new DsQuickShareSessionUser(user));

    m_users[user] = data;

    populate(data);

    emit sessionEvent(DarkmiresEvent(DarkmiresEvent::EVENT_USER_JOINED_SESSION, m_interface->m_uuid.toString()));
}

bool DsQuickShareSession::init()
{
    return true;
}


DsQuickShareSessionUser::DsQuickShareSessionUser(int user) :
    DsSessionUserData(user)
{
}


DsQuickShareSessionUser::~DsQuickShareSessionUser()
{
}


void DsQuickShareSessionUser::addEntry(const DsQuickShareSessionUser::Entry & entry)
{
    m_entries.insert(m_entries.size(), entry);
}


void DsQuickShareSession::populate(std::shared_ptr<DsSessionUserData> data)
{

}
