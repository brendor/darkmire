/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package darkmirec.utils;

/**
 * Predstavuje logovací modul, pomocou ktorého je možné nezávisle od média
 * logovať správy.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public interface IDarkmireLogger
{
    /**
     * Predstavuje prioritu správy.
     */
    public enum LogPriority
    {
        DEBUG (0),
        INFO (1),
        WARNING (2),
        ERROR (3);
        
        private final int code;

        private LogPriority(int code)
        {
            this.code = code;
        }
        
        public int code()
        {
            return code;
        }
        
        private static final LogPriority[] m_values = LogPriority.values();
        
        public static LogPriority fromCode(int code)
        {
            for (LogPriority c : m_values)
            {
                if (c.code == code) return c;
            }
            
            return null;
        }
    };
    
    /**
     * Zaznamená správu s východzou prioritou. 
     * @param message správa
     */
    void log(String message);
    
    /**
     * Zaznamená správu s konkrétnou prioritou.
     * @param message správa
     * @param priority priorita
     */
    void log(String message, LogPriority priority);
}
