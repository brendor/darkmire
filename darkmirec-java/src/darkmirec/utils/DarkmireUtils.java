/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkmirec.utils;

import darkmirec.core.DarkmireException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Trieda, ktorá obsahuje pomocné metódy.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireUtils
{
    /**
     * Odstráni z reťazca znaky nebezpečné pre prenos v notácii JSON alebo URL.
     * @param string surový reťazec
     * @return upravený reťazec
     */
    public static String quote(String string)
    {
         if (string == null || string.length() == 0)
         {
             return "";
         }

         char c = 0;
         int i;
         int len = string.length();
         
         StringBuilder sb = new StringBuilder(len + 4);
         String t;

//         sb.append('"');
         for (i = 0; i < len; i += 1) {
             c = string.charAt(i);
             switch (c) {
             case '\\':
             case '"':
                 sb.append('\\');
                 sb.append(c);
                 break;
             case '/':
                sb.append('\\');

                 sb.append(c);
                 break;
             case '\b':
                 sb.append("\\b");
                 break;
             case '\t':
                 sb.append("\\t");
                 break;
             case '\n':
                 sb.append("\\n");
                 break;
             case '\f':
                 sb.append("\\f");
                 break;
             case '\r':
                sb.append("\\r");
                break;
             default:
                 if (c < ' ')
                 {
                     t = "000" + Integer.toHexString(c);
                     sb.append("\\u").append(t.substring(t.length() - 4));
                 }
                 else
                 {
                     sb.append(c);
                 }
             }
         }
//         sb.append('"');
         return sb.toString();
     }

    public static byte[] compress(byte[] buffer, int level) throws DarkmireException
    {   
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        DeflaterOutputStream deflater = new DeflaterOutputStream(
                bos,
                new Deflater(level));

        ByteArrayInputStream ios = new ByteArrayInputStream(buffer);

        byte[] b = new byte[1000];
        int len;

        try {
            while ((len = ios.read(b, 0, b.length)) > 0) {
                deflater.write(b, 0, len);
            }
        } catch (IOException ex) {
            throw new DarkmireException(ex.getMessage());
        }
        
        try {
            bos.flush();
            
            deflater.close();
            ios.close();
            
        } catch (IOException ex) {
            throw new DarkmireException(ex.getMessage());
        }

        return bos.toByteArray();
    }


    public static byte[] decompress(byte[] buffer, int off, int length) throws DarkmireException
    {
        InflaterInputStream ios = new InflaterInputStream(
                new ByteArrayInputStream(buffer, off, length),
                new Inflater());

        ByteArrayOutputStream bos = new ByteArrayOutputStream(buffer.length);

        byte[] b = new byte[1000];
        int len;

        try {
            while ((len = ios.read(b, 0, b.length)) > 0) {
                bos.write(b, 0, len);
            }
        } catch (IOException ex) {
            throw new DarkmireException(ex.getMessage());
        }
        
        try {
            bos.flush();
            
            ios.close();
            
        } catch (IOException ex) {
            throw new DarkmireException(ex.getMessage());
        }

        return bos.toByteArray();
    }


    /**
     * Source: http://introcs.cs.princeton.edu/java/51data/CRC16CCITT.java.html
     *
     * @param byte array
     * @return CRC16-CCITT value
     */
    public static int Crc16CCITT(byte[] bytes)
    {
        int crc = 0xFFFF;                // initial value
        final int polynomial = 0x1021;   // 0001 0000 0010 0001  (0, 5, 12) 

        for (byte b : bytes) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) {
                    crc ^= polynomial;
                }
            }
        }

        crc &= 0xffff;

        return crc;
    }
}
