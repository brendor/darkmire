package darkmirec.core;

/**
 * Trieda Darkmire umožnuje vytvárať implementáciu klientského rozhrania,
 * teda triedy DarkmireClient.
 * 
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class Darkmire
{
    /**
     * Vytvorí inštanciu triedy implementujúcu rozhranie klienta.
     * @return 
     */
    static public DarkmireClient createClient()
    {
        return new DarkmireClientImpl();
    }
}
