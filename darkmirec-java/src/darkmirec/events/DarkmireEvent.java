package darkmirec.events;

import darkmirec.core.DarkmireException;
import darkmirec.utils.DarkmireEventHandler;
import net.sf.json.JSONObject;

/**
 * Táto trieda predstavuje rozhranie pre udalosti v systéme klienta Darkmire.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public abstract class DarkmireEvent implements Comparable
{
    /**
     * Enumerácia obsahujúca zoznam možných stavov, ktoré server pošle.
     */
    public enum EventStatus {
        
        // Custom codes
        WEB_EVENT_NORMAL (0),
        WEB_EVENT_ERROR (1),
        WEB_EVENT_AUTHENTICATION_ERROR (2),
        WEB_EVENT_AUTHENTICATION_SUCCESS (3),
        WEB_EVENT_AUTHENTICATION_DISCONNECT (7),
        WEB_EVENT_AUTHENTICATION_ALREADY_AUTHENTICATED (9),
        WEB_EVENT_SESSION_ERROR (4),
        WEB_EVENT_SESSION_SUCCESS (8),
        WEB_EVENT_DATA_ERROR (5),
        WEB_EVENT_DATA_SUCCESS (6),

        // These follow standard HTTP error codes
        WEB_NOT_FOUND (404),
        WEB_INTERNAL_ERROR (500),
        WEB_SERVICE_NOT_ALLOWED (405),
        WEB_BAD_REQUEST (400);
        
        private final int code;

        private EventStatus(int code)
        {
            this.code = code;
        }
        
        private static final EventStatus[] m_values = EventStatus.values();
        
        /**
         * Získa stav udalosti na základe jej číselného kódu.
         * @param code kód udalosti
         * @return enumerácia
         */
        public static EventStatus fromInt(int code)
        {
            for (EventStatus c : m_values) {
                if (c.code == code) return c;
            }
            
            return null;
        }
        
        /**
         * Zistí, či daný kód reprezentuje chybný stav.
         * @return true ak áno, inak false
         */
        public boolean isError()
        {
            switch (this)
            {
                case WEB_EVENT_AUTHENTICATION_ERROR:
                    return true;
                case WEB_EVENT_ERROR:
                    return true;
                case WEB_EVENT_SESSION_ERROR:
                    return true;
                case WEB_EVENT_DATA_ERROR:
                    return true;
                case WEB_INTERNAL_ERROR:
                    return true;
            }
            
            return false;
        }
    }
    
    /**
     * Enumerácia predstavuje typ požiadavky.
     */
    public enum RequestType
    {
        REQUEST_NORMAL ("normal"),
        REQUEST_AUTHENTICATE ("authenticate"),
        REQUEST_MODERATE ("admin"),
        REQUEST_LIST_SESSIONS ("sessions"),
        REQUEST_DISCONNECT ("disconnect");
        
        private final String alias;

        private RequestType(String alias)
        {
            this.alias = alias;
        }
        
        /**
         * Získa reťazec s názvom požiadavky.
         * @return reťazec s názvom požiadavky
         */
        public String alias()
        {
            return alias;
        }
    }
    
    /**
     * Enumerácia obsahujúca kódy jednotlivých privilégií,
     * podľa ktorých je možné skonštruovať bitovú masku privilégií.
     */
    public enum UserPrivilege {
        
        USER_NONE (0x00),
        USER_REGULAR (0x01),
        USER_ADMIN (0x02),
        USER_MODERATOR (0x04);
        
        protected final Integer code;

        private UserPrivilege(Integer code)
        {
            this.code = code;
        }
        
        /**
         * Získa kód privilégia.
         * @return kód privilégia
         */
        public Integer code()
        {
            return this.code;
        }


        /**
         * Zmení privilégium na reťazec, ktorý je možné použiť v JSON notácii.
         * @return reťazec
         */
        @Override
        public String toString()
        {
            switch (this) {
                case USER_REGULAR:
                    return "Regular";
                    
                case USER_ADMIN:
                    return "Admin";
                    
                case USER_MODERATOR:
                    return "Moderator";
                    
                default:
                    return "Unknown";
            }
        }
        
        /**
         * Získa najvyššie privilégium obsiahnuté v bitovej maske privilégií.
         * @param code bitová maska
         * @return najvyššie privilégium
         */
        static public UserPrivilege fromInt(int code)
        {
            if ((code & USER_ADMIN.code()) != 0x0)
            {
                return USER_ADMIN;
            }
            else if ((code & USER_MODERATOR.code()) != 0x0)
            {
                return USER_MODERATOR;
            }
            else if ((code & USER_REGULAR.code()) != 0x0)
            {
                return USER_REGULAR;
            }
            
            return USER_NONE;
        }
    }
    
    private final int m_priority = 0;
    
    protected EventStatus m_status = EventStatus.WEB_EVENT_NORMAL;
    
    protected String m_message = null;

    private DarkmireEventHandler m_object = null;

    
    public DarkmireEvent()
    {
        
    }
    
    /**
     * Nastaví objekt spracovača udalostí pre túto udalosť.
     * @param object objekt spracovača udalostí
     */
    public void setObject(DarkmireEventHandler object)
    {
        m_object = object;
    }
    

    /**
     * Získa objekt, ktorý odoslal a spravuje túto udalosť.
     * @return inštancia objektu správu
     */
    public DarkmireEventHandler getObject()
    {
        return m_object;
    }
    
    /**
     * Získa prioritu udalosti.
     * @return priorita udalosti
     */
    public int getPriority()
    {
        return m_priority;
    }

    
    @Override
    public int compareTo(Object o)
    {
        if (o.getClass() == DarkmireEvent.class)
        {
            DarkmireEvent e = (DarkmireEvent)o;
            
            return this.m_priority < e.getPriority() ? -1 :
                   this.m_priority == e.getPriority() ? 0 :
                   1;
        }
        
        return 0;
    }


    /**
     * Získa objekt notácie JSON, ktorý je možné poslať serveru a obaľuje stav
     * tejto udalosti.
     * @return objekt notácie JSON
     * @throws DarkmireException v prípade chyby
     */
    public abstract JSONObject request() throws DarkmireException;
    
    /**
     * Načíta stav objektu do udalosti z objektu v JSON notácii, ktorý pravdepodobne
     * prišiel predtým ako odpoveď zo servera.
     * @param object objekt notácie JSON
     */
    public abstract void response(JSONObject object);
    
    /**
     * Získa stav udalosti. Relevantné pri odpovedi od servera.
     * @return stav udalosti
     */
    public EventStatus getStatus()
    {
        return m_status;
    }
    
    /**
     * Ak bola udalosť chybová, získa prípadnú chybovú hlášku. (ak server nejakú
     * poslal)
     * @return chybová hláška alebo prázdny reťazec 
     */
    public String getErrorMessage()
    {
        return m_message;
    }
    
}
