package darkmirec.events;

import darkmirec.core.DarkmireException;
import net.sf.json.JSONObject;

/**
 * Špeciálny typ požiadavky, ktorý sa používa na vzdialené ovládanie servera
 * používateľom, ako zmena, pridávanie a mazanie údajov, relácií a pod.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireUserEvent extends DarkmireEvent {


    public DarkmireUserEvent()
    {
    }

    @Override
    public JSONObject request() throws DarkmireException
    {
        JSONObject result = new JSONObject();
        
        result.put("type", RequestType.REQUEST_MODERATE.alias());
        
        return result;
    }


    @Override
    public void response(JSONObject object)
    {
        m_status = EventStatus.fromInt(object.optInt("status", 0));

        switch (m_status)
        {
            case WEB_EVENT_SESSION_ERROR:
            case WEB_EVENT_AUTHENTICATION_ERROR:
            case WEB_EVENT_DATA_ERROR:
            case WEB_EVENT_ERROR:
                m_message = object.optString("data", "Unknown user error");
                break;

            default:
                break;
        }
    }
    
}
