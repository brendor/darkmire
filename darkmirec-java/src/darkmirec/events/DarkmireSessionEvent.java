package darkmirec.events;

import darkmirec.sessions.DarkmireSession;
import net.sf.json.JSONObject;

/**
 * Predstavuje udalosť relácie, od ktorej môžu dediť iné špecifickejšie udalosti.
 * Táto udalosť len načíta stav, ktorý server vracia v prípade relačných požiadavok.
 * @author Tomáš Kováč <brendorrmt@gmail.com>
 */
public abstract class DarkmireSessionEvent extends DarkmireEvent
{
    protected DarkmireSession m_session = null;

    /**
     * Skonštruuje relačnú udalosť na základe relácie, v ktorej kontexte
     * vznikla.
     * @param session relácia
     */
    public DarkmireSessionEvent(DarkmireSession session)
    {
        m_session = session;
    }
    
    /**
     * Získa reláciu, ktorá odoslala relačnú požiadavku a ktorej sa vráti odpoveď.
     * @return relácia
     */
    public DarkmireSession getSession()
    {
        return m_session;
    }


    @Override
    public void response(JSONObject object)
    {
        m_status = EventStatus.fromInt(object.optInt("status", 0));

        if (m_status.isError())
        {
            m_message = object.optString("data", "Unknown error");
        }
    }
}
