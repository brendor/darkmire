/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package darkmirec.sessions;

import darkmirec.scripts.DarkmireScriptEvent;
import darkmirec.utils.IDarkmireLogger;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.EvaluatorException;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.Undefined;

/**
 * Táto trieda predstavuje skriptovanú reláciu.
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireScriptedSession extends DarkmireSession
{
    private Scriptable m_scope = null;
    
    /**
     * Špecifikuje typ požiadavky špecifický pre tento typ relácie.
     */
    public enum RequestType
    {
        REQUEST_NORMAL (0),
        REQUEST_FETCH_CLIENT_SCRIPT (1),
        REQUEST_TRIGGER_EVENTS (2);

        private final int code;

        private RequestType(int code)
        {
            this.code = code;
        }
        
        public Integer code()
        {
            return this.code;
        }
    }
    
    private String m_customName;
    
    class DarkmireScriptEventComparator implements Comparator<DarkmireScriptEvent>
    {
        @Override
        public int compare(DarkmireScriptEvent o1, DarkmireScriptEvent o2)
        {
            if (o1.getPriority() < o2.getPriority()) return -1;
            if (o1.getPriority() == o2.getPriority()) return 0;
            if (o1.getPriority() > o2.getPriority()) return 1;
            
            return 0;
        }
    }
    
    private final PriorityBlockingQueue<DarkmireScriptEvent> m_queue = new PriorityBlockingQueue<>(10, new DarkmireScriptEventComparator());
    private final PriorityBlockingQueue<DarkmireScriptEvent> m_accumulator = new PriorityBlockingQueue<>(10, new DarkmireScriptEventComparator());
    
    private IDarkmireLogger m_logger = null;
    
    private final ScheduledExecutorService m_scheduler = Executors.newSingleThreadScheduledExecutor();

    private RunnableScheduledFuture<?> m_task = null;
    
    protected static final int REFRESH_RATE = 2000;

    protected static final int POLL_RATE = 300;
    
    private final Runnable m_job = new Runnable()
    {
        @Override
        public void run()
        {
            System.out.println(String.format("[DSS] Running in thread: %s", Thread.currentThread().getName()));
            
            Object args[] = { REFRESH_RATE };
            
            call("tick", args);
            
            while (! m_queue.isEmpty())
            {
                DarkmireScriptEvent event;
                
                try
                {
                    event = m_queue.poll(POLL_RATE, TimeUnit.MILLISECONDS);
                    
                    DarkmireScriptEvent eargs[] = { event };
                    
                    call("event", eargs);
                }
                catch (InterruptedException ex) 
                {
                    m_logger.log(ex.getMessage(), IDarkmireLogger.LogPriority.ERROR);
                    
                    break;
                }
            }
        }
    };
    
    public class DarkmireScriptInterface
    {
        private final DarkmireScriptedSession m_session;
        
        public DarkmireScriptInterface(DarkmireScriptedSession session)
        {
            m_session = session;
        }
        
        public void post(DarkmireScriptEvent event)
        {
            m_session.accumulate(event);
        }
    }
    
    private DarkmireScriptInterface m_interface = null;
    
    public DarkmireScriptedSession(String id, String name, AuthenticationType auth, boolean owner)
    {
        super(id, name, auth, owner);
        
        m_interface = new DarkmireScriptInterface(this);
        
        m_logger = new IDarkmireLogger()
        {
            @Override
            public void log(String message, IDarkmireLogger.LogPriority priority)
            {
                System.out.println(message);
            }


            @Override
            public void log(String message)
            {
                System.out.println(message);
            }
        };
    }
    
    public void setCustomName(String name)
    {
        m_customName = name;
    }
    
    public String getCustomName()
    {
        return m_customName;
    }

    public void setLogger(IDarkmireLogger m_logger)
    {
        this.m_logger = m_logger;
    }
    
    private boolean call(String function, Object[] args)
    {
        try
        {
            Context cx = Context.enter();
            
            Object fObj = m_scope.get(function, m_scope);
            
            if (! (fObj instanceof Function))
            {
                m_logger.log(String.format("%s is undefined or not a function", function), IDarkmireLogger.LogPriority.WARNING);
            }
            else
            {
                Function f = (Function) fObj;
                Object result = f.call(cx, m_scope, m_scope, args);
                
                m_logger.log(String.format("%s returned: %s", function, Context.toString(result)), IDarkmireLogger.LogPriority.DEBUG);
                
                return true;
            }
        }
        catch (Exception e)
        {
            m_logger.log(e.getMessage(), IDarkmireLogger.LogPriority.ERROR);
        }
        finally
        {
            Context.exit();
        }
        
        return false;
    }
    
    public void initialize(String script)
    {
        if (m_task != null)
        {
            m_task.cancel(true);
            m_task = null;
        }
        
        try
        {
            Context cx = Context.enter();
            
            m_logger.log(String.format("Initializing JavaScript interface... (version %d -> %s)", cx.getLanguageVersion(), cx.getImplementationVersion()),
                    IDarkmireLogger.LogPriority.INFO);
            
            m_scope = cx.initStandardObjects();

            Object result = cx.evaluateString(m_scope, script, "[DSS]", 1, null);

            if (! (result instanceof Undefined))
            {
                m_logger.log(Context.toString(result), IDarkmireLogger.LogPriority.ERROR);
                
                return;
            }
            
            ScriptableObject.defineClass(m_scope, DarkmireScriptEvent.class);
            
            Object wrappedOut = Context.javaToJS(m_logger, m_scope);
            ScriptableObject.putProperty(m_scope, "logger", wrappedOut);
            
            Object wrappedSession = Context.javaToJS(m_interface, m_scope);
            ScriptableObject.putProperty(m_scope, "session", wrappedSession);

            m_task = (RunnableScheduledFuture<?>) m_scheduler.scheduleAtFixedRate(m_job, REFRESH_RATE, REFRESH_RATE, TimeUnit.MILLISECONDS);
            
            call("init", null);
        }
        catch (IllegalAccessException | EvaluatorException | InstantiationException | InvocationTargetException e)
        {
            m_logger.log(e.getMessage(), IDarkmireLogger.LogPriority.ERROR);
        }
        finally
        {
            Context.exit();
        }
    }
    
    public void close()
    {
        if (m_task != null)
        {
            call("deinit", null);
            
            m_task.cancel(false);
            m_task = null;
        }
    }
    
    public void enqueue(DarkmireScriptEvent e)
    {
        m_queue.add(e);
    }
    
    public void accumulate(DarkmireScriptEvent e)
    {
        m_accumulator.add(e);
    }
    
    public JSONArray getEvents()
    {
        JSONArray a = new JSONArray();
        
        for (DarkmireScriptEvent e : m_accumulator)
        {
            a.element(JSONObject.fromObject(e.getJson()));
        }
        
        return a;
    }
    
    public void wipeEvents()
    {
        m_accumulator.clear();
    }
}
