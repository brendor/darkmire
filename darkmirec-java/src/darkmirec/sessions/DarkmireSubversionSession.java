package darkmirec.sessions;

/**
 *
 * @author Tomáš Kováč (brendor) <brendorrmt@gmail.com>
 */
public class DarkmireSubversionSession extends DarkmireSession
{
    /**
     * Špecifikuje typ požiadavky špecifický pre tento typ relácie.
     */
    public enum RequestType
    {
        REQUEST_NORMAL (0),
        REQUEST_CREATE (1),
        REQUEST_STATUS (2),
        REQUEST_LIST (3);

        private final int code;

        private RequestType(int code)
        {
            this.code = code;
        }
        
        public Integer code()
        {
            return this.code;
        }
    }
    
    
    public DarkmireSubversionSession(String id, String name, AuthenticationType auth, boolean owner)
    {
        super(id, name, auth, owner);
    }
    
}
